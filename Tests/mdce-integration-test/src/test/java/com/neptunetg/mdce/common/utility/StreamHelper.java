/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.utility;

import java.io.*;
import java.util.stream.Collectors;

/**
 * Helper utility for input/output stream manipulation.
 */
public class StreamHelper
{
    /**
     * Create from InputStream and output string. Use to read a resource text file.
     */
    public static String inputStreamToString(InputStream is) throws IOException
    {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is)))
        {
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }

    /**
     * Copy input stream to output stream.
     * @throws IOException
     */
    public static void copyStream(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[4096];
        int len;
        while ((len = in.read(buffer)) > 0)
        {
            out.write(buffer, 0, len);
        }
    }
}
