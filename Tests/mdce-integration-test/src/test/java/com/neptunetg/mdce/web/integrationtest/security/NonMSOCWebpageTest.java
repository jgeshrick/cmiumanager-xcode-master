/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.security;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.SeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Test access to Sales/Distributor webpage
 */
public class NonMSOCWebpageTest extends SeleniumTestBase
{
    WebDriverWait webDriverWait;

    /**
     * Start mdce web using sales distributor url instead of MSOC accessible url
     */
    public NonMSOCWebpageTest()
    {
        super(Constants.MDCE_WEB_URL);
    }


    @Test
    public void testMsocModeNotAccessibleFromPublic()
    {
        this.webDriverWait = new WebDriverWait(driver, 10);

        //we should be able to navigate to home page
        driver.get(Constants.MDCE_WEB_URL);

        //Accessible pages
        assertPagesIsAccessible("pages/config", "Command & Control Interface");
        assertPagesIsAccessible("pages/alert/list", "Alerts");
        assertPagesIsAccessible("pages/miu/miulist", "MIU List");
        assertPagesIsAccessible("pages/user/list", "User List");
        assertPagesIsAccessible("pages/user/details/1/admin", "User Details");
        assertPagesIsAccessible("pages/config/image-update", "Update Image");
        assertPagesIsAccessible("pages/user/resetpassword/1/admin", "Reset Password");

        //the following pages should be denied access


    }

    private void assertAccessDenied(String subUrl)
    {
        driver.get(Constants.MDCE_WEB_URL + subUrl);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Access Denied')]")));
    }

    private void assertPagesIsAccessible(String subUrl, String expectedHeading1)
    {
        driver.get(Constants.MDCE_WEB_URL + subUrl);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), '" +
                expectedHeading1 + "')]")));
    }


}
