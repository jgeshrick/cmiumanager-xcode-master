/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.R900FskReadingArray;
import com.neptunetg.common.packet.model.taggeddata.types.R900OokReadingArray;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.neptunetg.common.util.HexUtils;
import com.neptunetg.mdce.common.framework.*;
import com.neptunetg.mdce.common.r900.*;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.common.utility.S3Utility;
import com.neptunetg.mdce.common.utility.StreamHelper;
import com.neptunetg.mdce.common.utility.TrustModifier;
import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Tests sending a tarball to mdce-integration and reading back.
 * Test Interface:
 * - IF 15 Get Packets
 * - IF08 Water usage data (R900) PUT
 */
public class GatewayPutTest extends BaseSetupRestTest
{
    private static final Duration PERMISSIBLE_CLOCK_SKEW = Duration.of(5L, ChronoUnit.SECONDS); //warn if more than 5s clock skew between client and server

    private static final ZoneId TIME_ZONE_FOR_TABLE_ROTATION = ZoneId.of("America/New_York"); //Needs to match com.neptunetg.mdce.common.data.DynamoPacketRepository#TIME_ZONE_FOR_TABLE_ROTATION in mdce-common-aws-storage

    private static final DateTimeFormatter TABLE_NAME_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM");

    private static final Logger logger = LoggerFactory.getLogger(GatewayPutTest.class);
    private static final String GPV300026_042118_0962 = "59111_GPV300026_042118_0962";

    /*
[READINGS OOK]
ImageName="59111_GPV300026_042118_0962.DAT"
CRC32=0x217da44f

[READINGS FSK]
ImageName="59111_GPV300026_042118_0962_FSK.DAT"
CRC32=0xcdf77296

[Application Log]
ImageName="59111_GPV300026_042118_0962.App"
CRC32=0x17ebf064

[Network Transactions Log]
ImageName="59111_GPV300026_042118_0962.Net"
CRC32=0xcce1290d

[RPMC System Log]
ImageName="59111_GPV300026_042118_0962.RPMC"
CRC32=0xf338cc9b

[System Statistics Log]
ImageName="59111_GPV300026_042118_0962.Stat"
CRC32=0xd40346b9

[CONFIG FSK]
ImageName="59111_GPV300026_042118_0962_CONFIG.DAT"
CRC32=0xa2db84ba

[RATE FSK]
ImageName="59111_GPV300026_042118_0962_RATE.DAT"
CRC32=0x00000000

[UNKNOWN FSK]
ImageName="59111_GPV300026_042118_0962_UNK.DAT"
CRC32=0x00000000

[PACKAGE HEADER]
PackageVersion=01.000
ComponentCount=9

     */

    private static final String GPV300026_042118_0962_TAR = GPV300026_042118_0962 + ".tar";

    private static final String[] EMPTY_STRING_ARRAY = {};
    private static final List<MeterReading> EMPTY_METER_READING_LIST = Arrays.asList();


    private ObjectMapper mapper = new ObjectMapper();
    private final int thisTestSiteId = 59111; //to match tarball
    private static final Duration TIMEOUT_FOR_PACKETS_TO_APPEAR_IN_LIST = Duration.ofMinutes(3L);

    private static final GatewayTarballParser tarballParser = new GatewayTarballParser();

    private static final TagSequenceParser tagSequenceParser = new TagSequenceParser();

    public GatewayPutTest()
    {
        super(400000102);
    }

    @Ignore("No longer supporting storage of R900 packets, and so this test is redundant") @Test
    public void testGatewayPut() throws Exception
    {
        final String token = fetchTokenForSiteId(thisTestSiteId);

        final InputStream tarStream = getClass().getResourceAsStream("/" + GPV300026_042118_0962_TAR);

        final GatewayTarball parsedTarball = tarballParser.parse(GPV300026_042118_0962, tarStream);

        final FileInGatewayTarball readingsOok = parsedTarball.getReadingsOokFile();

        final Set<Integer> expectedMiuIds = new HashSet<>();
        final Map<Integer, List<MeterReading>> expectedOokReadings = new HashMap<>();
        final Map<Integer, List<MeterReading>> expectedFskReadings = new HashMap<>();

        int totalPacketCount = 0;

        for (MeterReading mr : readingsOok.getMeterReadings())
        {
            final Integer miuId = mr.getMiuId();

            expectedMiuIds.add(miuId);
            List<MeterReading> packetsForMiu = expectedOokReadings.get(miuId);
            if (packetsForMiu == null)
            {
                packetsForMiu = new ArrayList<>();
                expectedOokReadings.put(miuId, packetsForMiu);
            }
            packetsForMiu.add(mr);
            totalPacketCount++;
        }

        final FileInGatewayTarball readingsFsk = parsedTarball.getReadingsFskFile();

        for (MeterReading mr : readingsFsk.getMeterReadings())
        {
            final Integer miuId = mr.getMiuId();

            expectedMiuIds.add(miuId);
            List<MeterReading> packetsForMiu = expectedFskReadings.get(miuId);
            if (packetsForMiu == null)
            {
                packetsForMiu = new ArrayList<>();
                expectedFskReadings.put(miuId, packetsForMiu);
            }
            packetsForMiu.add(mr);
            totalPacketCount++;
        }

        System.out.println("Test data tarball contains " + totalPacketCount + " packets across " + expectedMiuIds.size() + " MIUs");

        final List<String> expectGatewayPacketsForFiles = parsedTarball.getFiles()
                .stream()
                .filter(f ->
                {
                    final String ms = f.getManifestSectionHeading();
                    return f.bytes() != null && f.bytes().length > 0 && (ms.contains("OOK") || ms.contains("FSK"));
                })
                .map(FileInGatewayTarball::getImageName).collect(Collectors.toList());

        assignMiusToSiteId(expectedMiuIds, thisTestSiteId, token);

        final Instant putTime = postTarballReturnTime(thisTestSiteId, GPV300026_042118_0962_TAR);

        String minInsertDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(putTime.minus(1L, ChronoUnit.MINUTES).atOffset(ZoneOffset.ofHours(-5)));

        final Instant startPollingApiTime = Instant.now();
        final Instant timeoutTime = startPollingApiTime.plus(TIMEOUT_FOR_PACKETS_TO_APPEAR_IN_LIST);

        boolean adjustedForClockSkew = false;

        AssertionError failure;
        Instant clientRequestTime;
        do
        {
            failure = null;
            Thread.sleep(1000L);

            clientRequestTime = Instant.now();

            String responseString = getPackets(minInsertDate, true, token);

            GetPacketsResponse miuList = mapper.readValue(responseString, GetPacketsResponse.class);

            final Instant serverTime = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(miuList.getServerTime(), Instant::from);

            final Duration clockSkew = Duration.between(serverTime, clientRequestTime);
            if (!adjustedForClockSkew && clockSkew.abs().compareTo(PERMISSIBLE_CLOCK_SKEW) > 0)
            {
                logger.warn("Clock skew detected.  Client clock is at " + clockSkew + " relative to server clock.  Repeating request using server time.");
                minInsertDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
                        serverTime
                                .minus(Duration.between(putTime, clientRequestTime))
                                .minus(1L, ChronoUnit.MINUTES)
                                .atOffset(ZoneOffset.ofHours(-5))
                );
                responseString = getPackets(minInsertDate, true, token);
                miuList = mapper.readValue(responseString, GetPacketsResponse.class);
                adjustedForClockSkew = true;
            }

            try
            {
                checkGetPacketsResponseForPostedTarballPackets(miuList, expectedMiuIds, expectedOokReadings, expectedFskReadings, expectGatewayPacketsForFiles);
                break; //nothing wrong now
            }
            catch (AssertionError e)
            {
                logger.info("After polling for " + Duration.between(startPollingApiTime, clientRequestTime).getSeconds() +
                        "s, check failing with " + e.getMessage() + ".  Retrying in case tarball is still halfway through processing or DynamoDB is still catching up.");
                failure = e;
            }

        } while (clientRequestTime.isBefore(timeoutTime));

        if (failure != null)
        {
            logger.info("Failed after polling for " + Duration.between(startPollingApiTime, clientRequestTime).getSeconds() + "s");
            throw failure;
        }

    }

    /**
     * Check the API response for the expected data.  If expected values are missing, return a string indicating what is missing.
     * If unexpected things are present, throw an AssertionError.  We don't throw an AssertionError on partial data because
     * due to the eventual-consistency of Dynamo, we might get partial data if we poll too soon, and so we want to retry in this case
     *
     * @param response       API response
     * @param expectedMiuIds List of expected MIUs
     * @throws AssertionError if assertions fail, might want to retry, so catch this error
     */
    private void checkGetPacketsResponseForPostedTarballPackets(GetPacketsResponse response, Collection<Integer> expectedMiuIds,
                                                                  Map<Integer, List<MeterReading>> expectedOokReadings,
                                                                  Map<Integer, List<MeterReading>> expectedFskReadings,
                                                                  List<String> expectedGatewayPackets
    ) throws AssertionError
    {
        final Set<Integer> checkExpectedMiuIds = new HashSet<Integer>(expectedMiuIds);
        if (response != null)
        {
            final List<MiuResponse> miuResponseList = response.getMius();

            //Check that the number of mius matches
            for (MiuResponse m : miuResponseList)
            {
                final Integer miuId = m.getMiuId();
                if (checkExpectedMiuIds.remove(miuId))
                { //one of our expected ones
                    final List<PacketResponse> packets = m.getPackets();
                    final List<MeterReading> expectedOokReadingsForMiu = getExpectedEmptyIfNull(expectedOokReadings, miuId);
                    final List<MeterReading> expectedFskReadingsForMiu = getExpectedEmptyIfNull(expectedFskReadings, miuId);

                    if (packets != null)
                    {

                        assertEquals("All readings should have been consolidated for MIU " + miuId, 1, packets.size());

                        PacketResponse p = packets.get(0);

                        final String packetHex = p.getPacket();
                        final byte[] packetData = HexUtils.parseHexToByteArray(packetHex);

                        final TaggedPacket parsedTaggedPacket = PacketParser.parseTaggedPacket(packetData);

                        final SecureBlockArrayData secureDataBlock = parsedTaggedPacket.findTag(SecureBlockArrayData.class);

                        final TagSequence secureTags = tagSequenceParser.parseTagSequence(secureDataBlock.getEncryptedData());

                        final R900OokReadingArray ookReadingArray = secureTags.findTag(TagId.R900ReadingsOok, R900OokReadingArray.class);
                        final R900FskReadingArray fskReadingArray = secureTags.findTag(TagId.R900ReadingsFsk, R900FskReadingArray.class);

                        String[] ookReadingsHex = EMPTY_STRING_ARRAY;

                        if (ookReadingArray != null)
                        {
                            assertEquals("Number of OOK packets for MIU " + miuId, expectedOokReadingsForMiu.size(), ookReadingArray.getReadingCount());

                            ookReadingsHex = ookReadingArray.getReadingsOokHex();
                            Arrays.sort(ookReadingsHex);
                        }

                        final String[] expectedOokReadingsHex = expectedOokReadingsForMiu.stream().map(r -> HexUtils.byteArrayToHex(r.getRawPacket())).toArray(size -> new String[size]);
                        Arrays.sort(expectedOokReadingsHex);

                        assertArrayEquals("Expected OOK readings for MIU " + miuId, expectedOokReadingsHex, ookReadingsHex);

                        String[] fskReadingsHex = EMPTY_STRING_ARRAY;

                        if (fskReadingArray != null)
                        {
                            assertEquals("Number of FSK packets for MIU " + miuId, expectedFskReadingsForMiu.size(), fskReadingArray.getReadingCount());

                            fskReadingsHex = fskReadingArray.getReadingsFskHex();
                            Arrays.sort(fskReadingsHex);
                        }

                        final String[] expectedFskReadingsHex = expectedFskReadingsForMiu.stream().map(r -> HexUtils.byteArrayToHex(r.getRawPacket())).toArray(size -> new String[size]);
                        Arrays.sort(expectedFskReadingsHex);

                        assertArrayEquals("Expected FSK readings for MIU " + miuId, expectedFskReadingsHex, fskReadingsHex);
                    }

                }
            }

            if (!checkExpectedMiuIds.isEmpty())
            {
                assertTrue("No packets for " + checkExpectedMiuIds.size() + " MIUs (" + listMiuIds(checkExpectedMiuIds) + ")", checkExpectedMiuIds.isEmpty());
            }
        }
        else
        {
            fail("No packets from any MIUs yet");
        }

        final List<GatewayResponse> gws = response.getGateways();

        if (gws != null)
        {
            final String expectedGatewayId = "59111_GPV300026";
            boolean foundGw = false;
            for (GatewayResponse gr : gws)
            {
                if (gr.getGatewayId().equals(expectedGatewayId))
                {
                    final Set<String> notFoundPacketFromFile = new HashSet<>(expectedGatewayPackets);
                    foundGw = true;

                    final List<PacketResponse> gwPackets = gr.getPackets();

                    if (gwPackets != null)
                    {
                        for (PacketResponse pr : gwPackets)
                        {
                            final String packetContentsText = new String(HexUtils.parseHexToByteArray(pr.getPacket()), StandardCharsets.ISO_8859_1);
                            int firstCr = packetContentsText.indexOf('\r');
                            final String filename = packetContentsText.substring("File=".length(), firstCr);
                            assertTrue("Unexpected filename " + filename + " in gateway packet", notFoundPacketFromFile.remove(filename));
                        }
                    }

                    assertTrue("Did not find gateway packets for files: " + listStrings(notFoundPacketFromFile), notFoundPacketFromFile.isEmpty());

                    break;
                }
            }
            assertTrue("No gateway packets for gateway " + expectedGatewayId, foundGw);

        }
    }

    private List<MeterReading> getExpectedEmptyIfNull(Map<Integer, List<MeterReading>> expectedReadings, Integer miuId)
    {
        final List<MeterReading> ret = expectedReadings.get(miuId);
        if (ret == null)
        {
            return EMPTY_METER_READING_LIST;
        }
        else
        {
            return ret;
        }
    }

    private static String listMiuIds(Set<Integer> checkExpectedMiuIds)
    {
        final Iterator<Integer> i = checkExpectedMiuIds.iterator();
        if (!i.hasNext())
        {
            return "";
        }
        final StringBuilder ret = new StringBuilder(i.next().toString());
        while (i.hasNext())
        {
            ret.append(',').append(i.next());
        }
        return ret.toString();
    }


    private static String listStrings(Set<String> strings)
    {
        final Iterator<String> i = strings.iterator();
        if (!i.hasNext())
        {
            return "";
        }
        final StringBuilder ret = new StringBuilder(i.next());
        while (i.hasNext())
        {
            ret.append(',').append(i.next());
        }
        return ret.toString();
    }

    private Instant postTarballReturnTime(int siteId, String tarball) throws Exception
    {
        final String putUrl = Constants.MDCE_INTEGRATION_URL + "/4/data?site_id=" + siteId + "&collectorname=GPV300026&filename=" + tarball;

        final Instant putTime = Instant.now();

        logger.info("PUTting " + tarball + " to " + putUrl + " at " + putTime);

        HttpUtility.httpPutFile(putUrl, tarball);

        return putTime;
    }

    /**
     * Test storing externalising of large gateway packets to S3.
     *
     * @throws Exception
     * @note requires connection to amazon s3. AWS credential required.
     */
    @Test
    public void testGatewayStoreLargeDataToS3() throws Exception
    {
        final int miuId = 1;
        final String token = fetchTokenForSiteId(thisTestSiteId);

        //get number of items in S3 first
        final String bucketName = "neptune-mdce-" + Constants.ENV_NAME + "-large-packets";

        final YearMonth currentMonth = YearMonth.now(TIME_ZONE_FOR_TABLE_ROTATION);
        final String currentDateString = currentMonth.format(TABLE_NAME_DATE_FORMATTER);

        final String s3Folder = currentDateString + "/" + Constants.ENV_NAME + "-gateway-packets-received-" + currentDateString;
        final S3Utility s3Util = new S3Utility();

        List<S3ObjectSummary> s3Contents = s3Util.getItemsInFolder(bucketName, s3Folder);

        logS3Contents("Initial contents of bucket " + bucketName + " folder " + s3Folder, s3Contents);

        int oldItemSize = s3Contents.size();

        final List<MeterReading> readingsForTarball = Arrays.asList(new MeterReading(miuId, -4, Calendar.getInstance(), 123456));

        //generate a tarball with a big header
        final GatewayTarballGenerator tarballGenerator = new GatewayTarballGenerator("59111", "inttest", Calendar.getInstance());
        final FileInGatewayTarball ookData = tarballGenerator.addReadingsOok(400 * 1024, readingsForTarball); //gateway header is 400KB or more
        final GatewayTarball tarball = tarballGenerator.build();

        final byte[] expectedPacketContents;
        final int addedDataLength;
        {
            final byte[] addedData = ("File=" + ookData.getImageName() + "\r\n").getBytes();
            addedDataLength = addedData.length;
            final ByteBuffer expectedPacketContentsBuilder = ByteBuffer.allocate(ookData.bytes().length + addedData.length);
            expectedPacketContentsBuilder.put(addedData);
            expectedPacketContentsBuilder.put(ookData.bytes());
            expectedPacketContents = expectedPacketContentsBuilder.array();
        }

        final URL putUrl = new URL(Constants.MDCE_INTEGRATION_URL +
                "/4/data?site_id=59111&collectorname=inttest&filename=" + tarball.getFilename(false));

        logger.info("Posting generated tarball " + tarball.getFilename(false) + " to " + putUrl);

        final Instant putTime = Instant.now();

        HttpUtility.httpPutFile(putUrl.toString(), tarball);

        final Instant timeoutWaitingForPacketToFilterThrough = Instant.now().plus(TIMEOUT_FOR_PACKETS_TO_APPEAR_IN_LIST);

        final ObjectMapper mapper = new ObjectMapper();
        Instant minTime = putTime.minus(2L, ChronoUnit.MINUTES);

        AssertionError failure;
        final Instant startPollingApiTime = Instant.now();
        Instant clientRequestTime;
        do
        {
            failure = null;
            //Wait for a second while the MDCE stores the detailed config packet from the simulator
            Thread.sleep(1L * 1000L);

            clientRequestTime = Instant.now();
            final String minInsertDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
                    minTime.atOffset(ZoneOffset.ofHours(-5))
            );
            final String responseString = getPackets(minInsertDate, true, token);

            final GetPacketsResponse packetsResponse = mapper.readValue(responseString, GetPacketsResponse.class);

            final Instant serverTime = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(packetsResponse.getServerTime(), Instant::from);

            final Duration clockSkew = Duration.between(serverTime, clientRequestTime);
            if (clockSkew.abs().compareTo(PERMISSIBLE_CLOCK_SKEW) > 0)
            {
                logger.warn("Clock skew detected.  Client clock is at " + clockSkew + " relative to server clock.");
            }
            minTime = serverTime
                    .minus(Duration.between(putTime, clientRequestTime))
                    .minus(2L, ChronoUnit.MINUTES); //next time, query relative to the server time of the packet send

            List<GatewayResponse> gateways = packetsResponse.getGateways();

            try
            {
                checkGatewaysResponseAndS3Contents(gateways, expectedPacketContents, s3Util, bucketName, s3Folder, oldItemSize);
                break;
            }
            catch (AssertionError e)
            {
                logger.info("After polling for " + Duration.between(startPollingApiTime, clientRequestTime).getSeconds() +
                        "s, check failing with " + e.getMessage() + ".  Retrying in case tarball is still halfway through processing or DynamoDB is still catching up.");
                failure = e;
            }

        } while (clientRequestTime.isBefore(timeoutWaitingForPacketToFilterThrough));

        if (failure != null)
        {
            throw failure;
        }

        //S3 items should have increment by one more
        s3Contents = s3Util.getItemsInFolder(bucketName, s3Folder);

        logS3Contents("Final contents of bucket " + bucketName + " folder " + s3Folder, s3Contents);

    }

    private void checkGatewaysResponseAndS3Contents(List<GatewayResponse> gateways, byte[] expectedPacketContents,
                                                    S3Utility s3Util, String bucketName, String s3Folder, int oldItemSize)
    {
        boolean foundPacket = false;
        for (GatewayResponse gr : gateways)
        {
            if ("59111_inttest".equals(gr.getGatewayId()))
            {
                final List<PacketResponse> packets = gr.getPackets();

                boolean foundBigOokHeader = false;
                for (PacketResponse pr : packets)
                {
                    if (pr.getPacketType() == 1) //gateway OOK header
                    {
                        foundBigOokHeader = true;
                        final String packetDataHex = pr.getPacket();
                        final byte[] packetDataBin = HexUtils.parseHexToByteArray(packetDataHex);
                        assertTrue(packetDataBin.length > 400 * 1024);
                        assertArrayEquals(Arrays.copyOf(expectedPacketContents, packetDataBin.length), packetDataBin); //check it's the same as the original header data
                        foundPacket = true;
                        break;
                    }
                }
                assertTrue("Did not find OOK header", foundBigOokHeader);

            }
        }
        assertTrue("Did not find packet for gateway 59111_inttest in GET PACKETS response", foundPacket);

        //S3 items should have increment by one more
        List<S3ObjectSummary> s3Contents = s3Util.getItemsInFolder(bucketName, s3Folder);

        logS3Contents("Final contents of bucket " + bucketName + " folder " + s3Folder, s3Contents);

        assertThat("Expected new item in bucket " + bucketName + ", folder " + s3Folder, s3Contents.size(), Matchers.greaterThan(oldItemSize));

    }


    private void logS3Contents(String description, List<S3ObjectSummary> s3Contents)
    {
        logger.info(description);
        for (S3ObjectSummary s3Item : s3Contents)
        {
            logger.info("  " + s3Item.getKey() + " last modified " + s3Item.getLastModified());
        }

    }


}
