/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.framework;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ScreenshotManager {

    private static final File outputFolder = new File(SeleniumValues.OUTPUT_FOLDER_SCREENSHOTS);

    private static final Date runDate = new Date();
    private static final List<ScreenshotRecord> screenshots = new ArrayList<ScreenshotRecord>();

    static {
        try {
            FileUtils.deleteDirectory(outputFolder);
            // start with empty output
            // folder on class load
            outputFolder.mkdirs();

        } catch (Exception e) {
            throw new RuntimeException("Exception initialising screenshot folder");
        }
    }

    /**
     *
     */
    public static void writeIndex() {
        try {
            File indexFile = new File(outputFolder, "index.html");
            PrintStream out = new PrintStream(indexFile);

            out.println("<html><head>");
            out.println("<title>Selenium Test screenshots</title>");
            out.println("<style>");
            out.println("img {max-width: 120px; max-height: 120px; width: auto; height: auto;}");
            out.println("</style>");
            out.println("</head><body>");
            out.println("<h1>Selenium Test screenshots</h1>");
            out.printf("<p>Test was run at %1$te/%1$tm/%1$tY %1$tH:%1$tM:%1$tS%n</p>\n", runDate);

            String currentTest = null;
            for (ScreenshotRecord sr : screenshots) {

                if (!sr.testName.equals(currentTest)) {
                    out.printf("<hr/><h2>%s</h2>\n", sr.testName);
                    currentTest = sr.testName;
                }
                out.println("<div class=\"shot\">");
                out.printf("<h3>%s</h3>", sr.testStep);
                String imgUrl = sr.screenshotFile.getName().replace(" ", "%20");
                out.printf("<p><a href=\"%s\"><img src=\"%s\" /></a></p>", imgUrl, imgUrl);
                if (sr.captionFile != null) {
                    out.printf("<p><a href=\"%s\">Caption</a></p>", sr.captionFile.getName());
                }
                out.println("</div>");

            }
            out.println("</body></html>");

            out.close();

        } catch (Exception e) {
            throw new RuntimeException("Failed to write index file", e);
        }
    }

    /**
     * @param testName
     * @param testStep
     * @param screenshotTmp
     */
    public static void moveScreenshot(String testName, String testStep,
                                      File screenshotTmp, String filenameBase) {
        filenameBase = filenameBase.replace(" ", "_").replace(".", "-");
        File screenshot = new File(outputFolder, filenameBase + ".jpg");
        try {
            FileUtils.moveFile(screenshotTmp, screenshot);
        } catch (Exception e) {
            throw new RuntimeException("Error while moving screenshot file to "
                    + screenshot, e);
        }

        screenshots.add(new ScreenshotRecord(testName, testStep, screenshot,
                null));
    }

    private static class ScreenshotRecord {
        private final String testName;
        private final String testStep;
        private final File screenshotFile;
        private final File captionFile;

        /**
         * @param testName
         * @param testStep
         * @param screenshotFile
         * @param captionFile
         */
        private ScreenshotRecord(String testName, String testStep,
                                 File screenshotFile, File captionFile) {
            super();
            this.testName = testName;
            this.testStep = testStep;
            this.screenshotFile = screenshotFile;
            this.captionFile = captionFile;
        }

    }

    /**
     * @param testName
     * @param testStep
     * @param screenshotTmp
     * @param filenameBase
     * @return
     */
    public static PrintStream moveScreenshotAndCreateCaptionFile(
            String testName, String testStep, File screenshotTmp,
            String filenameBase) {

        filenameBase = filenameBase.replace(" ", "_").replace(".", "-");

        File screenshot = new File(outputFolder, filenameBase + ".jpg");
        try {
            FileUtils.moveFile(screenshotTmp, screenshot);
        } catch (Exception e) {
            throw new RuntimeException("Error while moving screenshot file to "
                    + screenshot, e);
        }

        File captionFile = new File(outputFolder, filenameBase + ".txt");

        screenshots.add(new ScreenshotRecord(testName, testStep, screenshot,
                captionFile));

        try {
            PrintStream captionOut = new PrintStream(captionFile);
            return captionOut;
        } catch (Exception e) {
            throw new RuntimeException("Error while creating caption file "
                    + captionFile, e);
        }
    }

}
