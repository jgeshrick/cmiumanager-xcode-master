/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test Interface: IF30 Get Miu List
 */
public class GetMiuListTest extends BaseSetupRestTest
{

    public GetMiuListTest()
    {
        super(400000102);
    }

    @Test
    public void testGetMiuListFromSingleSite() throws Exception
    {
        final int siteId = 16;

        final String token = fetchTokenForSiteId(siteId);
        ensureTestMiuExists(siteId, token);

        String miuListResponse = getMiuList(token);
        Object filteredJsonString = JsonPath.read(miuListResponse, "$.mius[?(@.miu_id==" + getTestCmiuId() + ")]");

        assertEquals(getTestCmiuId(), (int)JsonPath.read(filteredJsonString, "$.[0].miu_id"));
        assertEquals(siteId, (int)JsonPath.read(filteredJsonString, "$.[0].site_id"));

    }

    @Test
    public void testGetMiuListFromAllSites() throws Exception
    {
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);

        //switch to site id 0 to get all cmius
        final String token0 = fetchTokenForSiteId(0);
        final String allMiuList = getMiuList(token0);

        assertTrue(hasMatchingMiuAndSite(allMiuList, 400000002, 1));
        assertTrue(hasMatchingMiuAndSite(allMiuList, 400000004, 2));
        assertTrue(hasMatchingMiuAndSite(allMiuList, 400000006, 16));

    }

    @Test
    public void test_findMius_WithNoSearchParameters_ReturnsEmptySet() throws Exception
    {
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);

        final String token0 = fetchTokenForSiteId(0);
        final String result = HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu_list/find" +
                "?token=" + token0);

        net.minidev.json.JSONArray miuResults = JsonPath.read(result, "$.mius");
        assertEquals(0, miuResults.size());
    }

    @Test
    public void test_findMius_WithMiuIdSearch_ReturnsExpectedMiu() throws Exception
    {
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);

        final String token0 = fetchTokenForSiteId(0);
        final String result = HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu_list/find" +
                "?token=" + token0 + "&find_id=400000002");

        net.minidev.json.JSONArray miuResults = JsonPath.read(result, "$.mius");
        assertEquals(1, miuResults.size());
        assertTrue(hasMatchingMiuAndSite(result, 400000002, 1));
    }

    private boolean hasMatchingMiuAndSite(final String jsonPath, int miu, int site )
    {
        String filteredJsonString = JsonPath.read(jsonPath, "$.mius[?(@.miu_id==" + miu + ")]").toString();

        final int actualMiu = (int)JsonPath.read(filteredJsonString, "$.[0].miu_id");
        final int actualSite = (int)JsonPath.read(filteredJsonString, "$.[0].site_id");

        return (miu == actualMiu) && (site == actualSite);
    }


}
