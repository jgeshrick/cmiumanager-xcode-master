/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.common.framework;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Mapped from JSON response by Jackson
 */
public class GatewayResponse
{
    @JsonProperty("gw_id")
    private String gatewayId;
    private List<PacketResponse> packets;

    public List<PacketResponse> getPackets()
    {
        return packets;
    }

    public void setPackets(List<PacketResponse> packets)
    {
        this.packets = packets;
    }

    public String getGatewayId()
    {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId)
    {
        this.gatewayId = gatewayId;
    }
}
