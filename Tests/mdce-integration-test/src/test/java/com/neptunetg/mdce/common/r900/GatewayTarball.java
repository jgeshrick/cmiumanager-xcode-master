/* *****************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.r900;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipParameters;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Contains metadata about an N_SIGHT package file, and provides methods
 * to write data to a .tar or .tar.gz stream
 */
public class GatewayTarball
{

	private final String filenameBase;

	private FileInGatewayTarball manifest = null;

	private final List<FileInGatewayTarball> nonManifestEntries = new ArrayList<>();

	/**
	 * Initialize the package file
	 * @param filenameBase The base of the filename
	 * @throws IOException If something goes wrong with the streams
	 */
	public GatewayTarball(String filenameBase) {
		this.filenameBase = filenameBase;
	}


	private void write(TarArchiveOutputStream tarOut, FileInGatewayTarball f) throws IOException
    {
		final TarArchiveEntry file = new TarArchiveEntry(f.getImageName());
		final byte[] data = f.bytes();
		file.setSize(data.length);
		tarOut.putArchiveEntry(file);
		tarOut.write(data);
		tarOut.closeArchiveEntry();
	}

    public String getFilename(boolean gzip)
    {
        if (gzip)
        {
            return filenameBase + ".tar.gz";
        }
        else
        {
            return filenameBase + ".tar";
        }
    }

	public void write(OutputStream bytesOut, boolean gzip) throws IOException
	{
		final TarArchiveOutputStream tarOut;
		final GzipCompressorOutputStream gzipOut;

		if (gzip) {
			GzipParameters params = new GzipParameters();
			params.setFilename(filenameBase + ".tar");
			gzipOut = new GzipCompressorOutputStream(bytesOut, params); //output file is GZIPped
			tarOut = new TarArchiveOutputStream(gzipOut); //input to GZIP is tar file

		} else {
			gzipOut = null;
			tarOut = new TarArchiveOutputStream(bytesOut); //output file is TAR
		}

        write(tarOut, this.manifest);
        for (FileInGatewayTarball f : this.nonManifestEntries)
        {
            write(tarOut, f);
        }
        tarOut.close();
        if (gzipOut != null) {
            gzipOut.close();
        }

	}

    public void setManifest(FileInGatewayTarball manifest)
    {
        this.manifest = manifest;
    }

    public Collection<FileInGatewayTarball> getFiles()
    {
        return this.nonManifestEntries;
    }

    public void addFile(FileInGatewayTarball f)
    {
        this.nonManifestEntries.add(f);
    }


    public FileInGatewayTarball getReadingsOokFile()
    {
        return getFile("[READINGS OOK]");
    }


    public FileInGatewayTarball getReadingsFskFile()
    {
        return getFile("[READINGS FSK]");
    }


    public FileInGatewayTarball getFile(String manifestSectionHeading)
    {
        for (FileInGatewayTarball f : this.nonManifestEntries)
        {
            if (manifestSectionHeading.equals(f.getManifestSectionHeading()))
            {
                return f;
            }
        }
        return null;
    }

}
