/* *****************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.r900;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Holds data and metadata about a sub-file within a package
 *
 */
public class FileInGatewayTarball
{
	
	private ByteArrayOutputStream dataOut;
	
	private Checksum checksum = new CRC32();
	
	private final String manifestSectionHeading; //e.g. READINGS OOK
	
	private final String imageName; //e.g. 36078_GPRS10015_40360_1344.DAT

	private List<MeterReading> meterReadings;

    private List<String> gatewayHeaders;

	/**
	 * Create a file data holder
	 * @param manifestSectionHeading Section heading e.g. "READINGS OOK"
	 * @param imageName File name of the sub-file
	 */
	public FileInGatewayTarball(String manifestSectionHeading, String imageName) {
		this.dataOut = new ByteArrayOutputStream();
		this.manifestSectionHeading = manifestSectionHeading;
		this.imageName = imageName;
	}
	
	/**
	 * Append some data to the file data holder
	 * @param newData Data to append
	 * @throws IOException if something goes wrong
	 */
	public void appendBytes(byte[] newData) throws IOException {

		dataOut.write(newData);
		// update the current checksum with the specified array of bytes
		checksum.update(newData, 0, newData.length);
		dataOut.flush();
	}

	public List<MeterReading> getMeterReadings()
	{
		return meterReadings;
	}

	public void setMeterReadings(List<MeterReading> meterReadings)
	{
		this.meterReadings = meterReadings;
	}

    public List<String> getGatewayHeaders()
    {
        return gatewayHeaders;
    }

    public void setGatewayHeaders(List<String> gatewayHeaders)
    {
        this.gatewayHeaders = gatewayHeaders;
    }

    /**
	 * Get the CRC32 of the data
	 * @return Calculated CRC32
	 */
	public long crc32() {
		return checksum.getValue();
	}
	
	/**
	 * Get the file contents and a byte array
	 * @return Byte array
	 */
	public byte[] bytes() {
		return dataOut.toByteArray();
	}

	/**
	 * Get the text for the manifest section
	 * @return heading e.g. READINGS OOK
	 */
	public String getManifestSectionHeading() {
		return manifestSectionHeading;
	}

	/**
	 * Get the sub-file name
	 * @return Name of sub-file
	 */
	public String getImageName() {
		return imageName;
	}
	
	
}
