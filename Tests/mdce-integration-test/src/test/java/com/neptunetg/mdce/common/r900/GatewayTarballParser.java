/* *****************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.mdce.common.r900;

import com.neptunetg.common.packet.model.builder.R900MiuReadingsPacketBuilder;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

/**
 * Parses a gateway tarball and extracts useful information
 */
public class GatewayTarballParser
{
    private static final Pattern IMAGE_NAME_PATTERN = Pattern.compile("ImageName=\"(.*)\"");

    private final int ASCII_LF = 10;

    public GatewayTarball parse(String filenameWithoutExtension, InputStream tarDataIn) throws IOException
    {
        final GatewayTarball ret = new GatewayTarball(filenameWithoutExtension);
        final Map<String, byte[]> tarEntryData = new HashMap<>();

        try (final TarArchiveInputStream untarredIn = new TarArchiveInputStream(tarDataIn))
        {
            while (true)
            {
                TarArchiveEntry entry = untarredIn.getNextTarEntry();

                if (entry == null)
                {
                    break;
                }
                else
                {
                    readTarEntryAndAddToMap(entry, untarredIn, tarEntryData);
                }
            }
        }

        if (tarEntryData.containsKey("Manifest"))
        {
            final byte[] manifestData = tarEntryData.remove("Manifest");
            FileInGatewayTarball manifest = new FileInGatewayTarball("Manifest", "Manifest");
            ret.setManifest(manifest);

            String manifestSection = null;
            try (final BufferedReader manifestReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(manifestData))))
            {
                while (true)
                {
                    String manifestLine = manifestReader.readLine();
                    if (manifestLine == null)
                    {
                        break;
                    }
                    else if (manifestLine.startsWith("["))
                    {
                        manifestSection = manifestLine;
                    }
                    else
                    {
                        Matcher fileDetails = IMAGE_NAME_PATTERN.matcher(manifestLine);
                        if (fileDetails.find())
                        {
                            //file
                            final String filename = fileDetails.group(1);
                            final byte[] data = tarEntryData.get(filename);
                            final FileInGatewayTarball f = new FileInGatewayTarball(manifestSection, filename);
                            f.appendBytes(data);

                            if ("[READINGS OOK]".equals(manifestSection))
                            {
                                parseReadingsOok(f, data);
                            }
                            else if ("[READINGS FSK]".equals(manifestSection))
                            {
                                parseReadingsFsk(f, data);
                            }

                            ret.addFile(f);
                        }
                    }
                }
            }
        }
        return ret;
    }



    /**
     * Read data of a TAR entry and put it in to a map
     * @param entry Entry to read
     * @param dataStream Data stream of entry
     * @param mapToPopulate Map to be populated (name, data)
     * @throws IOException If something goes wrong reading the stream
     */
    private static void readTarEntryAndAddToMap(TarArchiveEntry entry, InputStream dataStream, Map<String, byte[]> mapToPopulate) throws IOException
    {
        int len = (int) entry.getSize();
        final byte[] data = new byte[len];

        int writePos = 0;
        while (writePos < len)
        {
            int bytesRead = dataStream.read(data, writePos, len - writePos);
            if (bytesRead < 0)
            {
                throw new IOException("Unexpected end of stream");
            }
            writePos += bytesRead;
        }

        mapToPopulate.put(entry.getName(), data);

    }


    private void parseReadingsOok(FileInGatewayTarball fileInGatewayTarballToPopulate, byte[] readingsOokData) throws IOException
    {

        final List<String> gatewayHeaderLines = new ArrayList<>();

        int pos = readHeader(readingsOokData, gatewayHeaderLines);

        fileInGatewayTarballToPopulate.setGatewayHeaders(gatewayHeaderLines);


        final byte[] meterEntry = new byte[25];

        final List<MeterReading> readings = new ArrayList<>();

        for(; pos < readingsOokData.length; pos += 25)
        {
            assertTrue("ran out of buffer: " + (readingsOokData.length - pos) + " left", readingsOokData.length >= pos + 25);

            System.arraycopy(readingsOokData, pos, meterEntry, 0, 25);

            final MeterReading reading = parseOokMeterReading(meterEntry);

            readings.add(reading);

        }
        fileInGatewayTarballToPopulate.setMeterReadings(readings);

    }


    private void parseReadingsFsk(FileInGatewayTarball fileInGatewayTarballToPopulate, byte[] readingsFskData) throws IOException
    {

        final List<String> gatewayHeaderLines = new ArrayList<>();

        int pos = readHeader(readingsFskData, gatewayHeaderLines);

        fileInGatewayTarballToPopulate.setGatewayHeaders(gatewayHeaderLines);

        final byte[] meterEntry = new byte[25];

        final List<MeterReading> readings = new ArrayList<>();

        for(; pos < readingsFskData.length; pos += 25)
        {
            assertTrue("ran out of buffer: " + (readingsFskData.length - pos) + " left", readingsFskData.length >= pos + 25);

            System.arraycopy(readingsFskData, pos, meterEntry, 0, 25);

            final MeterReading reading = parseFskMeterReading(meterEntry);

            readings.add(reading);

        }
        fileInGatewayTarballToPopulate.setMeterReadings(readings);

    }


    private int readHeader(byte[] readingsOokData, List<String> gatewayHeaderLines) throws UnsupportedEncodingException
    {
        final ByteArrayOutputStream currentGatewayConfigInfoLine = new ByteArrayOutputStream();

        int pos;
        for (pos = 0; pos < readingsOokData.length; pos++)
        {

            final byte dataByte = readingsOokData[pos];
            if (dataByte == ASCII_LF)
            {
                final String gatewayHeaderLine = currentGatewayConfigInfoLine.toString("ISO-8859-1");
                if (gatewayHeaderLine.contains("DATA BEGINS HERE"))
                {
                    break; //done with the header, time for the readings
                }
                else
                {
                    currentGatewayConfigInfoLine.reset();
                    gatewayHeaderLines.add(gatewayHeaderLine);
                }
            }
            else if (dataByte < 32)
            {
            }
            else
            {
                currentGatewayConfigInfoLine.write(dataByte);
            }

        }

        pos++; //move off the LF
        return pos;
    }


    public MeterReading parseOokMeterReading(byte[] meterEntry)
    {
        ByteBuffer bigEndian = ByteBuffer.wrap(meterEntry).order(ByteOrder.BIG_ENDIAN);
        ByteBuffer littleEndian = ByteBuffer.wrap(meterEntry).order(ByteOrder.LITTLE_ENDIAN);

        int miuId = R900MiuReadingsPacketBuilder.getMiuIdFromOokPacketInBigEndianByteBuffer(bigEndian);
        long readDateEpochMillis = (long)(littleEndian.getInt(10) & 0xFFFFFFFFL) * 1000L + 315532800000L; //Add time in ms between epoch and 01/01/1980 GMT

        Calendar readCal = new GregorianCalendar();
        readCal.setTime(new Date(readDateEpochMillis));

        int rssi = bigEndian.getShort(19);

        int value = bigEndian.getInt(1) & 0xffffff;

        final MeterReading ret = new MeterReading(miuId, rssi, readCal, value);
        ret.setRawPacket(meterEntry);
        return ret;
    }

    public MeterReading parseFskMeterReading(byte[] meterEntry)
    {
        ByteBuffer bigEndian = ByteBuffer.wrap(meterEntry).order(ByteOrder.BIG_ENDIAN);
        ByteBuffer littleEndian = ByteBuffer.wrap(meterEntry).order(ByteOrder.LITTLE_ENDIAN);

        int miuId = R900MiuReadingsPacketBuilder.getMiuIdFromFskPacketInBigEndianByteBuffer(bigEndian);

        long readDateEpochMillis = (long)(bigEndian.getInt(19) & 0xFFFFFFFFL) * 1000L + 315532800000L; //Add time in ms between epoch and 01/01/1980 GMT

        Calendar readCal = new GregorianCalendar();
        readCal.setTime(new Date(readDateEpochMillis));


        int rssi = (int)(meterEntry[23]) & 0xff;

        int value = bigEndian.getInt(3);

        final MeterReading ret = new MeterReading(miuId, rssi, readCal, value);
        ret.setRawPacket(meterEntry);
        return ret;
    }
}
