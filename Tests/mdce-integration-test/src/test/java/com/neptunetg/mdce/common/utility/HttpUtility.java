/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.utility;

import com.neptunetg.mdce.common.r900.GatewayTarball;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

public class HttpUtility
{
    public static final String PUT = "PUT";
    public static final String POST = "POST";
    private static final int MAX_MESSAGE_SIZE_TO_PRINT = 1024;
    private static Logger logger = LoggerFactory.getLogger(HttpUtility.class);

    public static String httpGet(String urlPath) throws Exception
    {
        final HttpURLConnection httpCon = getHttpURLConnection(urlPath, "GET");

        //get response
        final String responseBody = getResponseBody(httpCon);

        logger.debug("Response: " + truncateString(responseBody));

        assertSuccessResponseCode(httpCon, responseBody);

        return responseBody;
    }

    public static Map<String, List<String>> httpGetResponseHeader(String urlPath) throws Exception
    {
        final HttpURLConnection httpCon = getHttpURLConnection(urlPath, "GET");

        //get response
        final String responseBody = getResponseBody(httpCon);

        Map<String, List<String>> responseHeader = httpCon.getHeaderFields();

        assertSuccessResponseCode(httpCon, responseBody);

        return responseHeader;

    }

    public static String httpPost(String urlPath) throws Exception
    {
        HttpURLConnection httpCon = getHttpURLConnection(urlPath, "POST");

        String responseBody = getResponseBody(httpCon);

        assertSuccessResponseCode(httpCon, responseBody);

        return responseBody;
    }

    public static String httpPost(String urlPath, final String jsonRequest) throws Exception
    {

        HttpURLConnection httpCon = getHttpURLConnection(urlPath, "POST");

        copyStreamOfJsonToUrl(jsonRequest, httpCon);

        //get response
        String responseBody = getResponseBody(httpCon);

        assertSuccessResponseCode(httpCon, responseBody);

        return responseBody;
    }

    public static String httpJsonPut(String urlPath, final String jsonRequest) throws Exception
    {

        HttpURLConnection httpCon = getHttpURLConnection(urlPath, PUT);

        copyStreamOfJsonToUrl(jsonRequest, httpCon);

        String responseBody = getResponseBody(httpCon);

        assertSuccessResponseCode(httpCon, responseBody);

        return responseBody;
    }

    public static String httpJsonPost(String urlPath, final String jsonRequest) throws Exception
    {
        HttpURLConnection httpCon = getHttpURLConnection(urlPath, POST);

        copyStreamOfJsonToUrl(jsonRequest, httpCon);

        String responseBody = getResponseBody(httpCon);

        assertSuccessResponseCode(httpCon, responseBody);

        return responseBody;
    }

    public static String httpPutFile(String putUrl, GatewayTarball tarball) throws Exception
    {

        HttpURLConnection httpCon = getHttpURLConnection(putUrl, PUT);

        try (final OutputStream out = httpCon.getOutputStream())
        {
            tarball.write(out, false);
        }

        String responseBody = getResponseBody(httpCon);

        assertSuccessResponseCode(httpCon, responseBody);

        return responseBody;

    }

    public static String httpPutFile(String putUrl, String resourceFile) throws Exception
    {

        HttpURLConnection httpCon = getHttpURLConnection(putUrl, PUT);

        setContentTypeFromFileExtension(httpCon, resourceFile);

        streamResourceFileToUrl(resourceFile, httpCon);

        String responseBody = getResponseBody(httpCon);

        assertSuccessResponseCode(httpCon, responseBody);

        return responseBody;

    }

    private static void setRequestMethod(final HttpURLConnection c, final String value) {
        try {
            final Object target = c;
            final Field f = HttpURLConnection.class.getDeclaredField("method");
            f.setAccessible(true);
            f.set(target, value);
        } catch (IllegalAccessException | NoSuchFieldException ex) {
            throw new AssertionError(ex);
        }
    }

    private static void copyStreamOfJsonToUrl(String jsonRequest, HttpURLConnection httpCon) throws IOException
    {
        logger.debug("PUT body: " + truncateString(jsonRequest));

        InputStream jsonStream = new ByteArrayInputStream(jsonRequest.getBytes(StandardCharsets.UTF_8));
        httpCon.setRequestProperty("Content-Type", "application/json");
        copyStreamToUrl(jsonStream, httpCon);

    }

    private static HttpURLConnection getHttpURLConnection(String urlPath, String verb) throws Exception
    {
        final URL getUrl = new URL(urlPath);
        logger.debug("HTTP " + verb + " request URL: " + urlPath);

        //write request
        HttpURLConnection httpURLConnection = TrustModifier.openConnectionIgnoreSelfSignCert(getUrl);
        httpURLConnection.setDoOutput(true);

        httpURLConnection.setRequestMethod(verb);

        return httpURLConnection;
    }

    private static void copyStreamToUrl(InputStream jsonStream, HttpURLConnection httpCon) throws IOException
    {
        try (final OutputStream out = httpCon.getOutputStream())
        {
            StreamHelper.copyStream(jsonStream, out);
        }
    }

    private static void streamResourceFileToUrl(String resourceFile, HttpURLConnection httpCon) throws IOException
    {
        try (final InputStream tarballData = HttpUtility.class.getResourceAsStream("/" + resourceFile))
        {
            copyStreamToUrl(tarballData, httpCon);
        }
    }

    private static String getResponseBody(HttpURLConnection httpCon)
    {
        //get response
        String responseBody = "";
        try
        {
            InputStream responseStream = httpCon.getInputStream();
            responseBody = StreamHelper.inputStreamToString(responseStream);

            String respMsg = httpCon.getResponseMessage();
            Map<String, List<String>> fields = httpCon.getHeaderFields();

            logger.debug("Response: " + httpCon.getResponseCode() + "\n " + truncateString(responseBody));
        } catch (IOException e)
        {
            logger.error("Error in getResponseBody. {}", e);
        }


        return responseBody;
    }

    private static void assertSuccessResponseCode(HttpURLConnection httpCon, String responseBody) throws IOException
    {
        final String requestSummary =  httpCon.getRequestMethod() + " " + httpCon.getURL().toString();

        switch (httpCon.getResponseCode())
        {
            case 200:
            {
                // Check that response stream is not empty.
                // N_SIGHT's Sybase HTTP client falls over if it gets a 200 but no response body
                final String errorMessage = "HTTP 200 returned from " + requestSummary + " but response stream empty!  Should return 204";
                assertNotEquals(errorMessage, "", responseBody);
            }
            break;
            case 204:
            {
                // Check that response stream is empty.
                // N_SIGHT's Sybase HTTP client falls over if it gets a 200 but no response body
                final String errorMessage = "HTTP 204 returned from " + requestSummary + " but response stream is not empty!  Should return 200";
                assertEquals(errorMessage, "", responseBody);
            }
            break;
            default:
            {
                InputStream error = httpCon.getErrorStream();
                if (error != null)
                {
                    StreamHelper.copyStream(error, System.err);
                }
                fail("HTTP GET response is not OK for " + requestSummary +
                        ". Response code: " + httpCon.getResponseCode() +
                        ". Response message: " + httpCon.getResponseMessage());
            }
        }
    }

    private static void setContentTypeFromFileExtension(HttpURLConnection httpCon, String resourceFile)
    {
        if (resourceFile.endsWith(".json"))
        {
            httpCon.setRequestProperty("Content-Type", "application/json");
        }
    }

    private static String truncateString(String s)
    {
        if (s.length() > MAX_MESSAGE_SIZE_TO_PRINT)
        {
            return s.substring(0, MAX_MESSAGE_SIZE_TO_PRINT) + "... [TRUNCATED]";
        }

        return s;
    }

}
