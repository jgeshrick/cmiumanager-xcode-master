/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseHttpSupportRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;


public class ReferenceDataControllerTest extends BaseHttpSupportRestTest
{
    private static final String DIZZIES = "{" +
            "\"customer_number\":\"7278500\"," +
            "\"info\":{" +
            "\"account_id\":\"2345\",\"system_id\":\"111\",\"account_name\":\"utility, city of\"," +
            "\"address1\":\"address1\",\"address2\":\"address2\",\"address3\":\"address3\"," +
            "\"city\":\"city\",\"state\":\"state\",\"postal_code\":\"postal_code\",\"country\":\"country\"," +
            "\"main_phone\":\"main_phone\",\"fax\":\"fax\"," +
            "\"sales_territory_owner\":\"sales_territory_owner\",\"account_manager\":\"account_manager\",\"regional_manager\":\"regional_manager\"," +
            "\"type\":\"type\",\"sub_type\":\"sub_type\",\"status\":\"ambiguious\"," +
            "\"primary_contact\":{\"name\":\"Christmas\",\"work_phone\":\"work_phone\",\"title\":\"Father\"}," +
            "\"active_secondary_contacts\":[{\"name\":\"name\",\"work_phone\":\"work_phone\",\"title\":\"title\"}]," +
            "\"customer_numbers\":[\"123\",\"125\"]," +
            "\"parent_customer_number\":\"parent_customer_number\"" +
            "}" +
            "}";
    private static final String UTES = "{\"site_id\":32,\"info\":{" +
            "\"account_id\":\"2345\",\"system_id\":\"111\",\"account_name\":\"utility, city of\"," +
            "\"address1\":\"address1\",\"address2\":\"address2\",\"address3\":\"address3\"," +
            "\"city\":\"city\",\"state\":\"state\",\"postal_code\":\"postal_code\",\"country\":\"country\"," +
            "\"main_phone\":\"main_phone\",\"fax\":\"fax\"," +
            "\"sales_territory_owner\":\"sales_territory_owner\",\"account_manager\":\"account_manager\",\"regional_manager\":\"regional_manager\"," +
            "\"type\":\"type\",\"sub_type\":\"sub_type\",\"status\":\"ambiguious\"," +
            "\"primary_contact\":{\"name\":\"Christmas\",\"work_phone\":\"work_phone\",\"title\":\"Father\"}," +
            "\"active_secondary_contacts\":[{\"name\":\"name\",\"work_phone\":\"work_phone\",\"title\":\"title\"}]," +
            "\"customer_numbers\":[" +
            "\"123\",\"125\"" +
            "]," +
            "\"parent_customer_number\":\"7278500\"}}";
    private static final String REF_DATA_REQUEST_BODY = "{" +
            "\"distributers\":[" +
            DIZZIES +
            "]," +
            "\"utilities\":[" +
            UTES +
            "]}";

    private final String urlRefData = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/refdata?token=";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    public ReferenceDataControllerTest()
    {
        super(400000102);
    }

    @Test
    public void testSetRefData() throws Exception
    {
        final String tokenBpc = fetchTokenForBpc();

        String requestBody = REF_DATA_REQUEST_BODY;

        String responseString = HttpUtility.httpJsonPut(urlRefData + tokenBpc, requestBody);

        assertNotNull(responseString);
        assertEquals("", responseString);

        String responseStringForGet = HttpUtility.httpGet(urlRefData + tokenBpc);

        //expecting no exception thrown and this function runs to completion
        assertNotNull(responseStringForGet);

        String expectedDistStart = "{\"distributers\":[{\"customer_number\":";
        assertTrue("\n\nResponse started:" + responseStringForGet.substring(50) + "... ,\n\n Expected: " + expectedDistStart, responseStringForGet.contains(expectedDistStart));
        assertTrue("\n\nResponse was:" + responseStringForGet + ",\n\n Expected: " + DIZZIES, responseStringForGet.contains(DIZZIES));

        String expectedStart = "\"utilities\":[{\"site_id\":";
        assertTrue("\n\nResponse started:" + responseStringForGet + "... ,\n\n Expected: " + expectedStart, responseStringForGet.contains(expectedStart));
        assertTrue("\n\nResponse was:" + responseStringForGet + ",\n\n Expected: " + UTES, responseStringForGet.contains(UTES));

    }

    /**
     * Verify that refdata does not remove existing records when called with the POST verb.
     * @throws Exception
     */
    @Test
    public void testAppendRefData() throws Exception
    {
        final String tokenBpc = fetchTokenForBpc();

        final String distributors = "{" +
                "\"distributers\":[" +
                DIZZIES +
                "]," +
                "\"utilities\":[]}";

        final String utilities = "{" +
                "\"distributers\":[]," +
                "\"utilities\":[" +
                UTES +
                "]}";

        String responseString1 = HttpUtility.httpJsonPut(urlRefData + tokenBpc, distributors);

        assertNotNull(responseString1);
        assertEquals("", responseString1);
        String responseString2 = HttpUtility.httpJsonPost(urlRefData + tokenBpc, utilities);

        assertNotNull(responseString2);
        assertEquals("", responseString2);

        String responseStringForGet = HttpUtility.httpGet(urlRefData + tokenBpc);

        //expecting no exception thrown and this function runs to completion
        assertNotNull(responseStringForGet);

        String expectedDistStart = "{\"distributers\":[{\"customer_number\":";
        assertTrue("\n\nResponse started:" + responseStringForGet.substring(50) + "... ,\n\n Expected: " + expectedDistStart, responseStringForGet.contains(expectedDistStart));
        assertTrue("\n\nResponse was:" + responseStringForGet + ",\n\n Expected: " + DIZZIES, responseStringForGet.contains(DIZZIES));

        String expectedStart = "\"utilities\":[{\"site_id\":";
        assertTrue("\n\nResponse started:" + responseStringForGet + "... ,\n\n Expected: " + expectedStart, responseStringForGet.contains(expectedStart));
        assertTrue("\n\nResponse was:" + responseStringForGet + ",\n\n Expected: " + UTES, responseStringForGet.contains(UTES));

    }

    @Test
    public void testSetRefDataBadToken() throws Exception
    {

        String requestBody = "{" +
                "\"distributers\":[" +
                DIZZIES +
                "]," +
                "\"utilities\":[" +
                UTES +
                "]}";

        this.expectedException.expectMessage(containsString("Response code: 401"));
        String responseString = HttpUtility.httpJsonPut(urlRefData + "BAD", requestBody);


    }

    /**
     * Only BPC can set Ref Data
     * @throws Exception
     */
    @Test
    public void testSetRefDataNotPermitted() throws Exception
    {
        final String token = fetchTokenForSiteId(1);

        String requestBody = REF_DATA_REQUEST_BODY;

        this.expectedException.expectMessage(containsString("Response code: 401"));
        String responseString = HttpUtility.httpJsonPut(urlRefData + token, requestBody);


    }

}
