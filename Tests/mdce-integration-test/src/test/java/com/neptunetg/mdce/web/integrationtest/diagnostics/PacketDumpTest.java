/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.diagnostics;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;
import com.neptunetg.mdce.common.simulator.L900SimulatorLauncher;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;

/**
 * -Setup a new CMIU in auto-test-test-data.sql
 * -Make sure it's claimed to a site ID
 * -Give it ICCID and IMEI details through the test data
 * -Use the CMIU simulator to populate DynamoDB with a bunch of packets
 * -Go to diagnostics page

 * -Repeat following for CMIU ID, Site ID, IMEI, ICCID
 *   -Select the filter type
 *   -Enter the value "CMIU ID, Site ID, IMEI or ICCID
 *   -Click "Download Packets"
 *   -Check that the returned file includes what we expect
 *
 * -Repeat the above, but with bad values, and check the response
 */
public class PacketDumpTest extends MdceWebSeleniumTestBase
{
    private final String DIAGNOSTIC_TOOLS_PAGE_URL = Constants.MDCE_WEB_URL + "pages/diagnostics/diagnosticTools";
    private final int TEST_CMIU = 400000248;
    private final int TEST_CMIU_SITE_ID = 2370;
    private final String TEST_CMIU_ICCID = "89148000001471859976";
    private final String TEST_CMIU_IMEI = "353238060161792";

    private final int TEST_L900 = 700000222;

    @Test
    public void testPacketDumpToolL900() throws IOException, InterruptedException
    {
        wipeDownloadFolder();

        //Publish 10 packets to auto-test
        L900SimulatorLauncher simulatorLauncher = new L900SimulatorLauncher();
        simulatorLauncher.runL900Simulator(Constants.MDCE_LORA_URL, TEST_L900, 1, 3, 100000, false, true);

        //Start up the web driver; gives MDCE a few seconds to get the packets into DynamoDB
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        //Get the diagnostic tools web page
        driver.get(DIAGNOSTIC_TOOLS_PAGE_URL);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("idTypePacketDump")));

        //Create a map of filter names (from the drop down) and the value to for the filter
        Map<String, String> filterTypeDetails = new HashMap<>();
        filterTypeDetails.put("L900 ID", Integer.toString(TEST_L900));

        //Get all the files already in the download folder
        Set<String> initialDownloadedFiles = getDownloadedFiles();

        for (String filterName : filterTypeDetails.keySet())
        {
            final String filterValue = filterTypeDetails.get(filterName);
            //Choose the filter method, enter the filter string, and download the CSV file
            selectOptionByText(driver.findElement(By.id("idTypePacketDump")), filterName);
            final WebElement valPacketDump = driver.findElement(By.id("idValPacketDump"));
            valPacketDump.clear();
            valPacketDump.sendKeys(filterValue);
            driver.findElement(By.id("get-packet-dump-button")).click();

            //Wait a second for the file to download
            Thread.sleep(1000);

            //Get a list of the files in the download folder, and check if there is a new one
            Set<String> newDownloadedFiles = getDownloadedFiles();
            newDownloadedFiles.removeAll(initialDownloadedFiles);
            assertTrue(newDownloadedFiles.size() == 1);
            String newFile = newDownloadedFiles.iterator().next();

            //Get the file as a string (Okay for a integration test, shouldn't ever exceed a few MBs)
            File file = new File(CHROME_DOWNLOAD_LOCATION + "/" + newFile);
            FileInputStream inputStream = new FileInputStream(file);
            byte[] fileData = new byte[(int) file.length()];
            inputStream.read(fileData);
            inputStream.close();
            String fileText = new String(fileData, StandardCharsets.UTF_8);

            //Check that the new file contains some of the strings we would expect
            assertThat("L900 " + TEST_L900 + " present in packet dump for " + filterName, fileText, containsString(Integer.toString(TEST_L900)));

            //Add the new file, to the initial files
            initialDownloadedFiles.add(newFile);
        }
    }

    @Test
    public void testPacketDumpToolCmiu() throws Exception
    {
        wipeDownloadFolder();

        //Publish 10 packets to auto-test
        CmiuSimulatorLauncher cmiuSimulatorLauncher = new CmiuSimulatorLauncher();
        cmiuSimulatorLauncher.sendPackets(TEST_CMIU, 1, 0.002, Constants.BROKER_SERVER, "intervalDataPacket", 3);

        //Start up the web driver; gives MDCE a few seconds to get the packets into DynamoDB
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        //Get the diagnostic tools web page
        driver.get(DIAGNOSTIC_TOOLS_PAGE_URL);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("idTypePacketDump")));

        //Create a map of filter names (from the drop down) and the value to for the filter
        Map<String, String> filterTypeDetails = new HashMap<>();
        filterTypeDetails.put("CMIU ID", Integer.toString(TEST_CMIU));
        filterTypeDetails.put("CMIU ICCID", TEST_CMIU_ICCID);
        filterTypeDetails.put("CMIU IMEI", TEST_CMIU_IMEI);
        filterTypeDetails.put("All CMIUs for site ID", Integer.toString(TEST_CMIU_SITE_ID));

        //Get all the files already in the download folder
        final Set<String> initialDownloadedFiles = getDownloadedFiles();

        for (String filterName : filterTypeDetails.keySet())
        {
            final String filterValue = filterTypeDetails.get(filterName);
            //Choose the filter method, enter the filter string, and download the CSV file
            selectOptionByText(driver.findElement(By.id("idTypePacketDump")), filterName);
            final WebElement valPacketDump = driver.findElement(By.id("idValPacketDump"));
            valPacketDump.clear();
            valPacketDump.sendKeys(filterValue);
            driver.findElement(By.id("get-packet-dump-button")).click();

            //Wait a second for the file to download
            Thread.sleep(1000);

            //Get a list of the files in the download folder, and check if there is a new one
            final Set<String> newDownloadedFiles = getDownloadedFiles();
            newDownloadedFiles.removeAll(initialDownloadedFiles);
            assertEquals(1, newDownloadedFiles.size());
            final String newFile = newDownloadedFiles.iterator().next();

            //Get the file as a string (Okay for a integration test, shouldn't ever exceed a few MBs)
            File file = new File(CHROME_DOWNLOAD_LOCATION + "/" + newFile);
            FileInputStream inputStream = new FileInputStream(file);
            byte[] fileData = new byte[(int) file.length()];
            inputStream.read(fileData);
            inputStream.close();
            String fileText = new String(fileData, StandardCharsets.UTF_8);

            //Check that the new file contains some of the strings we would expect
            assertThat("CMIU " + TEST_CMIU + " present in packet dump filtered by " + filterName + "=" + filterValue, fileText, containsString(Integer.toString(TEST_CMIU)));

            //Add the new file, to the initial files
            initialDownloadedFiles.add(newFile);
        }
    }

    private void wipeDownloadFolder() throws IOException
    {
            FileUtils.cleanDirectory((new File(CHROME_DOWNLOAD_LOCATION)));
    }


    private Set<String> getDownloadedFiles()
    {
        File folder = new File(CHROME_DOWNLOAD_LOCATION);

        Set<String> filenames = new HashSet<>();

        for(File file : folder.listFiles())
        {
            filenames.add(file.getName());
        }

        return filenames;
    }
}
