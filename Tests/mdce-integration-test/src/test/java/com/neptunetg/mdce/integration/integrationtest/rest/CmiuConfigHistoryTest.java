/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Test Interface:
 * IF 18 Get Configuration History
 * IF 38 Set Miu config
 */
public class CmiuConfigHistoryTest extends BaseSetupRestTest
{
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private ObjectMapper mapper = new ObjectMapper();

    public CmiuConfigHistoryTest() throws Exception
    {
        super(400000102, Duration.ofSeconds(1L));
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);
    }

    @Test
    public void testGetConfigHistoryForAllMiuMatchingToken() throws Exception
    {
        final String token = fetchTokenForSiteId(1);

        ensureTestMiuExists(1, token);

        final String responseString = getConfigHistory(null, token);

        assertNotNull(responseString);
        assertNotEquals(responseString, "");

        HashMap response = mapper.readValue(responseString, HashMap.class);

        assertNotNull(response.get("mius"));

        //try using JsonPath to validate json response field
        //https://github.com/jayway/JsonPath

        String filteredJsonString = JsonPath.read(responseString, "$.mius[?(@.miu_id==" + getTestCmiuId() + ")]").toString();

        System.out.println(filteredJsonString);

        assertEquals(400000102, (int) JsonPath.read(filteredJsonString, "$.[0].miu_id"));
        assertEquals("Site ID", 1, (int)JsonPath.read(filteredJsonString, "$.[0].site_id"));

        assertMiuConfigPresent(responseString, 1, 400000002, null);
    }

    @Test
    public void testGetConfigHistoryForAllMiuNotBillable() throws Exception
    {
        final int siteId = 1;
        final String token = fetchTokenForSiteId(siteId);

        ensureTestMiuExists(siteId, token);

        final Boolean billable = false;

        final String requestBody = "{\"miu_id\":" + getTestCmiuId() + ",\"config\":{" +
                "\"recording\":" +
                "   {\"start_time_mins\":11,\"interval_mins\":22}," +
                "\"reporting\":" +
                "   {\"start_time_mins\":33,\"interval_mins\":60,\"num_retries\":55,\"transmit_window_mins\":66}," +
                "\"quiet_time\":" +
                "{\"quiet_start_mins\":77,\"quiet_end_mins\":88}" +
                "}}";

        HttpUtility.httpJsonPut(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/" + getTestCmiuId() + "/config?token=" + token, requestBody);

        final String responseString = getConfigHistory(billable, token);

        assertNotNull(responseString);
        assertNotEquals(responseString, "");

        System.out.println(responseString);
        List<Object> filteredObjects = JsonPath.read(responseString, "$.mius[?(@.miu_id==" + getTestCmiuId() + ")]");

        // Verify that the active MIU is not included in the results.
        assertTrue(filteredObjects.isEmpty());
    }

    @Test
    public void testGetConfigHistoryForAllMiuIsBillable() throws Exception
    {
        final String token = fetchTokenForSiteId(1);

        ensureTestMiuExists(1, token);

        Boolean billable = true;

        final String responseString = getConfigHistory(billable, token);

        HashMap response = mapper.readValue(responseString, HashMap.class);

        assertNotNull(response.get("mius"));

        //try using JsonPath to validate json response field
        //https://github.com/jayway/JsonPath

        String filteredJsonString = JsonPath.read(responseString, "$.mius[?(@.miu_id==" + getTestCmiuId() + ")]").toString();

        assertEquals(400000102, (int)JsonPath.read(filteredJsonString, "$.[0].miu_id"));
        assertEquals(1, (int)JsonPath.read(filteredJsonString, "$.[0].site_id"));

        List<String> billableList = JsonPath.read(filteredJsonString, "$..billable");
        assertFalse(billableList.isEmpty());
        assertEquals("Y", billableList.get(0));

    }

    @Test
    public void testGetConfigHistoryForSingleMiu() throws Exception
    {
        final String token = fetchTokenForSiteId(1);

        ensureTestMiuExists(1, token);

        fetchTokenForSiteId(1);

        long miuId = 400000102;

        String responseString = getConfigHistoryForMiuId(miuId, token);

        assertNotNull(responseString);
        assertNotEquals(responseString, "");

        HashMap response = mapper.readValue(responseString, HashMap.class);

        assertEquals(400000102, response.get("miu_id"));

        assertEquals(400000102, (int)JsonPath.read(responseString, "$.miu_id"));
        assertEquals("site id", 1, (int)JsonPath.read(responseString, "$.site_id"));
        assertEquals("CMIU", JsonPath.read(responseString, "$.device_type"));
        assertEquals("billing site id", 1, (int) JsonPath.read(responseString, "$.configuration_history[0].config.billing.site_id"));
    }

    @Test
    public void testGetConfigHistoryForAllSites() throws Exception
    {
        ensureMiuExistsForSite(1, 400000002);

        ensureMiuExistsForSite(2, 400000004);

        ensureMiuExistsForSite(16, 400000006);

        final String token0 = fetchTokenForSiteId(0);
        final String responseString = getConfigHistory(null, token0);

        assertNotNull(responseString);
        assertNotEquals(responseString, "");

        HashMap response = mapper.readValue(responseString, HashMap.class);

        assertNotNull(response.get("mius"));

        assertMiuConfigPresent(responseString, 1, 400000002, null);
        assertMiuConfigPresent(responseString, 2, 400000004, null);
        assertMiuConfigPresent(responseString, 16, 400000006, null);
    }

    /**
     * Verify that selecting a range of MIU IDs only returns MIUs within that range.
     * @throws Exception
     */
    @Test
    public void testV2GetConfigHistoryFromStartOfSequence() throws Exception
    {
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);

        final String token0 = fetchTokenForSiteId(0);

        final String responseString1 = getV2ConfigHistory(token0);
        final String responseString2 = getV2ConfigHistory(-1, token0);

        assertTrue(responseString1.equalsIgnoreCase(responseString2));
    }

    @Test
    public void testApiV1CompliantResponse() throws Exception
    {
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);

        final String token0 = fetchTokenForSiteId(0);
        final String responseString = getConfigHistory(null, token0);

        assertApiV1CompliantResponse(responseString);
    }

    @Test
    public void testApiV2CompliantResponse() throws Exception
    {
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);

        final String token0 = fetchTokenForSiteId(0);
        final String responseString = getV2ConfigHistory(null, token0);

        assertApiV2CompliantResponse(responseString);
    }

    private static void assertMiuConfigPresent(final String getConfigHistoryJsonResponse, int siteId, int miuId, String expectedBillable)
    {
        String filteredJsonString = JsonPath.read(getConfigHistoryJsonResponse, "$.mius[?(@.miu_id==" + miuId + ")]").toString();

        assertEquals(miuId, (int)JsonPath.read(filteredJsonString, "$.[0].miu_id"));
        assertEquals(siteId, (int)JsonPath.read(filteredJsonString, "$.[0].site_id"));
        assertEquals("CMIU", JsonPath.read(filteredJsonString, "$.[0].device_type"));

        //expect an array
        List<Object> configs = JsonPath.read(filteredJsonString, "$.[0].configuration_history");
        assertThat("No config history found", configs.size(), Matchers.greaterThan(0));    //we have only 1 config items

        //check last element of config field
        assertEquals(siteId, (int) JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.billing.site_id"));
//        assertEquals(999, (int)JsonPath.read(filteredJsonString, "$.mius[0].configuration_history.config[0].billing.distributer_id"));
        if (StringUtils.hasText(expectedBillable))
        {
            assertEquals(expectedBillable, JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.billing.billable"));
        }

        assertEquals(10, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.recording.start_time_mins"));
        assertEquals(20, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.recording.interval_mins"));

        assertEquals(30, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.reporting.start_time_mins"));
        assertEquals(60, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.reporting.interval_mins"));
        assertEquals(3, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.reporting.num_retries"));
        assertEquals(56, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.reporting.transmit_window_mins"));

        assertEquals(12, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.quiet_time.quiet_start_mins"));
        assertEquals(34, (int)JsonPath.read(filteredJsonString, "$.[0].configuration_history[(@.length-1)].config.quiet_time.quiet_end_mins"));


    }

    /**
     * Asserts that the supplied JSON is compliant with the configuration history v1 API (IF18)
     * @param getConfigHistoryJsonResponse
     */
    private static void assertApiV1CompliantResponse(final String getConfigHistoryJsonResponse)
    {
        int jsonMiuCount = ((List<Object>)JsonPath.read(getConfigHistoryJsonResponse, "$.mius")).size();

        for (int i = 0; i < jsonMiuCount; i++)
        {
            assertTrue(0 < Integer.parseInt(tryReadStringFromJson(getConfigHistoryJsonResponse, "$.mius[" + i + "].miu_id")));
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(getConfigHistoryJsonResponse, "$.mius[" + i + "].site_id")));

            String distributorValue = tryReadStringFromJson(getConfigHistoryJsonResponse, "$.mius[" + i + "].distributer_id");

            if (StringUtils.hasText(distributorValue))
            {
                assertTrue(0 <= Integer.parseInt(distributorValue));
            }

            tryReadStringFromJson(getConfigHistoryJsonResponse, "$.mius[" + i + "].device_type"); // No assertion against this

            String billable = tryReadStringFromJson(getConfigHistoryJsonResponse, "$.mius[" + i + "].billable");
            assertTrue("Y".equalsIgnoreCase(billable) || "N".equalsIgnoreCase(billable));
            String configurationHistoryJson = tryReadStringFromJson(getConfigHistoryJsonResponse, "$.mius[" + i + "].configuration_history");
            assertNotNull(configurationHistoryJson);

            assertApiV1CompliantConfigurationHistory(configurationHistoryJson);
        }
    }

    private static void assertApiV1CompliantConfigurationHistory(final String configurationHistoryJson)
    {
        Pattern datePattern = Pattern.compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$");
        int historyEntryCount = ((List<Object>)JsonPath.read(configurationHistoryJson, "$")).size();

        for (int i = 0; i < historyEntryCount; i++)
        {
            // Header
            String startDateValue = tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].start_date");
            assertNotNull(startDateValue);
            assertTrue(datePattern.matcher(startDateValue).find()); // Must be a date of type yyyy-MM-dd

            String endDateValue = tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].end_date");

            if (StringUtils.hasText(endDateValue)) // end_date can be null
            {
                assertTrue(datePattern.matcher(endDateValue).find()); // Must be a date of type yyyy-MM-dd
            }

            assertNull(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].timestamp")); // V2 property only

            // Config header
            tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.CMIU_type"); // Can be missing if MIU type not present

            // Billing
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.billing.site_id")));
            String billable = tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.billing.billable");
            assertTrue("Y".equalsIgnoreCase(billable) || "N".equalsIgnoreCase(billable));

            String distributorValue = tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.billing.distributer_id");

            if (StringUtils.hasText(distributorValue))
            {
                assertTrue(0 <= Integer.parseInt(distributorValue));
            }

            // Recording
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.recording.start_time_mins")));
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.recording.interval_mins")));

            // Reporting
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.reporting.start_time_mins")));
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.reporting.interval_mins")));
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.reporting.num_retries")));
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.reporting.transmit_window_mins")));

            // Quiet Time
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.quiet_time.quiet_start_mins")));
            assertTrue(0 <= Integer.parseInt(tryReadStringFromJson(configurationHistoryJson, "$[" + i + "].config.quiet_time.quiet_end_mins")));
        }
    }

    /**
     * Asserts that the supplied JSON is compliant with the configuration history v2 API (IF18.2)
     * @param getConfigHistoryJsonResponse
     */
    private static void assertApiV2CompliantResponse(final String getConfigHistoryJsonResponse)
    {
        int jsonConfigCount = ((List<Object>)JsonPath.read(getConfigHistoryJsonResponse, "$")).size();

        int lastSequenceId = -1;
        for (int i = 0; i < jsonConfigCount; i++)
        {
            final int sequenceId = JsonPath.read(getConfigHistoryJsonResponse, "$[" + i + "].sequence_id");
            assertNotNull(tryReadStringFromJson(getConfigHistoryJsonResponse, "$[" + i + "].miu_id"));
            assertNotNull(tryReadStringFromJson(getConfigHistoryJsonResponse, "$[" + i + "].received_from_miu"));
            final Instant timestamp = Instant.parse(JsonPath.read(getConfigHistoryJsonResponse, "$[" + i + "].timestamp"));
            assertNotNull(tryReadStringFromJson(getConfigHistoryJsonResponse, "$[" + i + "].site_id"));
            assertNotNull(tryReadStringFromJson(getConfigHistoryJsonResponse, "$[" + i + "].recording_interval_minutes"));
            assertNotNull(tryReadStringFromJson(getConfigHistoryJsonResponse, "$[" + i + "].reporting_interval_minutes"));

            assertTrue("sequence_ids expected to be returned in ascending order", lastSequenceId < sequenceId);
            assertNotNull(timestamp);

            lastSequenceId = sequenceId;
        }
    }

    private static String tryReadStringFromJson(String json, String path)
    {
        String result = null;

        try
        {
            result = JsonPath.read(json, path).toString();
        }
        catch (Exception exception)
        {
            System.out.println("JSON path \"" + path + "\" was not found.");
        }

        return result;
    }
}

// Expect following json for API v1:
/*
    {
       "mius" : [
          {
             "miu_id" : 400000102,
             "site_id" : 1,
             "distributer_id" : 999,
             "device_type" : "CMIU",
             "billable" : "Y",
             "configuration_history" : {
                "start_date" : "2015-06-18",
                "end_date" : "2015-06-18",
                "config" : [
                   {
                      "billing" : {
                         "site_id" : 1,
                         "billable" : "N"
                      },
                      "data_collection" : {
                         "start_time_mins" : 10,
                         "interval_mins" : 20
                      },
                      "reporting" : {
                         "start_time_mins" : 30,
                         "interval_mins" : 4,
                         "num_retries" : 3,
                         "transmit_window_mins" : 56
                      },
                      "quiet_time" : {
                         "quiet_start_mins" : 12,
                         "quiet_end_mins" : 34
                      }
                   }
                ]
             }
          }
       ]
    }
*/