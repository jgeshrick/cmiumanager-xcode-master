/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;
import com.neptunetg.mdce.common.utility.HttpUtility;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

/**
 * Created by WJD1 on 01/06/2016.
 * Class to test the CMIU Lifecycle state handling
 */
public class CmiuLifecycleTest extends BaseAuthenticatingRestTest
{
    private final int CMIU_ID = 400000244;
    private final int TEST_SITE = 1;

    public final String CELLULAR_REQUEST_BODY = "{" +
            "\"miu_id\": " + CMIU_ID + "," +
            "\"modem_config\": " +
            "{" +
            "\"vendor\": \"Telit\"," +
            "\"mno\": \"ATT\"," +  //TODO: using AT&T because using VZW causes activation since doRegisterWithMno parameter was removed from IF40... finding out why
            "\"imei\": \"758495739192837\"" +
            "}," +
            "\"sim_config\": " +
            "{" +
            "\"iccid\": \"19283748275656382389\"" +
            "}}";

    @Test
    public void testExpectedCMIULifecycleStatePath() throws Exception
    {
        //PRE-ACTIVATED - Set cellular details using the SetCellularConfig REST API
        performCmiuStateEvent(CmiuLifecycleStateEnum.PREACTIVATED);
        assertEquals(CmiuLifecycleStateEnum.PREACTIVATED, getCmiuLifecycleState());

        //Todo ACTIVATED - Requires a callback from the cellular provider, can we simulate this somehow?

        //HEARD - Run the CMIU simulator with a CVS packet instigator tag
        performCmiuStateEvent(CmiuLifecycleStateEnum.HEARD);
        assertEquals(CmiuLifecycleStateEnum.HEARD, getCmiuLifecycleState());

        //PRE-PRE-POT - Use SetCmiuLifecycleState REST API
        performCmiuStateEvent(CmiuLifecycleStateEnum.PREPREPOT);
        assertEquals(CmiuLifecycleStateEnum.PREPREPOT, getCmiuLifecycleState());

        //PRE-POT - Use SetCmiuLifecycleState REST API
        performCmiuStateEvent(CmiuLifecycleStateEnum.PREPOT);
        assertEquals(CmiuLifecycleStateEnum.PREPOT, getCmiuLifecycleState());

        //POST-POT - Use SetCmiuLifecycleState REST API
        performCmiuStateEvent(CmiuLifecycleStateEnum.POSTPOT);
        assertEquals(CmiuLifecycleStateEnum.POSTPOT, getCmiuLifecycleState());

        //CONFIRMED - Run the CMIU simulator with a FFTS packet instigator tag
        performCmiuStateEvent(CmiuLifecycleStateEnum.CONFIRMED);
        assertEquals(CmiuLifecycleStateEnum.CONFIRMED, getCmiuLifecycleState());

        //Todo SCANNED - Requires the BPC interface which is not fully implemented yet

        //Todo SHIPPED - Requires the BPC interface which is not fully implemented yet

        //UNCLAIMED - Run the CMIU simulator
        performCmiuStateEvent(CmiuLifecycleStateEnum.UNCLAIMED);
        //Todo - change to UNCLAIMED when logic updated
        assertEquals(CmiuLifecycleStateEnum.CONFIRMED, getCmiuLifecycleState());

        //CLAIMED - Claim the CMIU using the SetMiuOwnership REST API
        performCmiuStateEvent(CmiuLifecycleStateEnum.CLAIMED);
        assertEquals(CmiuLifecycleStateEnum.CLAIMED, getCmiuLifecycleState());
    }

    /**
     * Function to perform an action that should set the CMIU to the specified state
     * @param stateEnum The lifecycle state the CMIU should transition to
     * @return true if the action was performed
     */
    private boolean performCmiuStateEvent(CmiuLifecycleStateEnum stateEnum) throws Exception
    {
        if(stateEnum.equals(CmiuLifecycleStateEnum.PREACTIVATED))
        {
            return setCellularConfig();
        }
        else if(stateEnum.equals(CmiuLifecycleStateEnum.HEARD))
        {
            CmiuSimulatorLauncher sim = new CmiuSimulatorLauncher();
            sim.sendDetailedConfigPacket(CMIU_ID, Constants.BROKER_SERVER, "-cvs");
            return true;
        }
        else if(stateEnum.equals(CmiuLifecycleStateEnum.PREPREPOT))
        {
            return setCmiuLifecycleState(CmiuLifecycleStateEnum.PREPREPOT);
        }
        else if(stateEnum.equals(CmiuLifecycleStateEnum.PREPOT))
        {
            return setCmiuLifecycleState(CmiuLifecycleStateEnum.PREPOT);
        }
        else if(stateEnum.equals(CmiuLifecycleStateEnum.POSTPOT))
        {
            return setCmiuLifecycleState(CmiuLifecycleStateEnum.POSTPOT);
        }
        else if(stateEnum.equals(CmiuLifecycleStateEnum.CONFIRMED))
        {
            CmiuSimulatorLauncher sim = new CmiuSimulatorLauncher();
            sim.sendDetailedConfigPacket(CMIU_ID, Constants.BROKER_SERVER, "-ffts");
            return true;
        }
        else if(stateEnum.equals(CmiuLifecycleStateEnum.UNCLAIMED))
        {
            //Can only unclaimed the Site ID through the web interface, this step should just move a
            //CMIU from a previous state, not from CLAIMED back to UNCLAIMED
            CmiuSimulatorLauncher sim = new CmiuSimulatorLauncher();
            sim.sendDetailedConfigPacket(CMIU_ID, Constants.BROKER_SERVER, "");
            return true;
        }
        else if(stateEnum.equals(CmiuLifecycleStateEnum.CLAIMED))
        {
            return claimCmiu(TEST_SITE);
        }

        return false;
    }

    private boolean setCellularConfig() throws Exception
    {
        final String tokenFpc = fetchTokenForFpc();

        String response = HttpUtility.httpJsonPost(Constants.FPC_URL + "/api/v1/miu/" + CMIU_ID +
                "/cellular-config?token=" + tokenFpc + "&doRegisterWithMno=false", CELLULAR_REQUEST_BODY);

        if(HttpStatus.OK.getReasonPhrase().equals(response))
        {
            return true;
        }

        return false;
    }

    private boolean setCmiuLifecycleState(CmiuLifecycleStateEnum state) throws Exception
    {
        final String json = "{" +
            "\"miu_id\": " + CMIU_ID + "," +
            "\"state\": " +
            "{" +
            "\"id\": \"" + state.getStringValue() + "\"" +
            "}" +
            "}";

        final String tokenFpc = fetchTokenForFpc();

        String response = HttpUtility.httpJsonPost(Constants.FPC_URL + "/api/v1/miu/" + CMIU_ID +
                "/lifecycle-state?token=" + tokenFpc, json);

        if(HttpStatus.OK.getReasonPhrase().equals(response))
        {
            return true;
        }

        return false;
    }

    private CmiuLifecycleStateEnum getCmiuLifecycleState() throws Exception
    {
        String tokenFpc = fetchTokenForFpc();

        String responseBody = HttpUtility.httpGet(Constants.FPC_URL + "/api/v1/miu/" + CMIU_ID +
                "/lifecycle-state?token=" + tokenFpc);

        Pattern pattern = Pattern.compile("\"id\": ?\"([^\"]+)\"");
        Matcher matches = pattern.matcher(responseBody);

        if(matches.find())
        {
            String stateStr = matches.group(1);
            return CmiuLifecycleStateEnum.fromStringValue(stateStr);
        }

        return null;
    }

    private boolean claimCmiu(int siteId) throws Exception
    {
        final String json = "{" +
                "\"site_id\": " + siteId + "," +
                "\"mius\": [" +
                "{" +
                "\"miu_id\": " + CMIU_ID +
                "}" +
                "]}";

        final String token = fetchTokenForSiteId(1);

        String response = HttpUtility.httpJsonPut(Constants.MDCE_INTEGRATION_URL +
                "/mdce/api/v1/miu/ownership?token=" + token, json);

        //Todo - check the response
        return true;
    }

    public enum CmiuLifecycleStateEnum
    {
        PREACTIVATED("PreActivated"),
        ACTIVATED("Activated"),
        HEARD("Heard"),
        PREPREPOT("PrePrePot"),
        PREPOT("PrePot"),
        POSTPOT("PostPot"),
        CONFIRMED("Confirmed"),
        DEVELOPMENT("Development"),
        SCANNED("Scanned"),
        SHIPPED("Shipped"),
        UNCLAIMED("Unclaimed"),
        CLAIMED("Claimed"),
        RMAED("RmaEd"),
        DEAD("Dead"),
        ROGUE("Rogue"),
        AGING("Aging");

        private final String sqlString;

        private CmiuLifecycleStateEnum(String sqlEnumString)
        {
            this.sqlString = sqlEnumString;
        }

        public static CmiuLifecycleStateEnum fromStringValue(String sqlEnumString)
        {
            for(CmiuLifecycleStateEnum state : CmiuLifecycleStateEnum.values())
            {
                if(sqlEnumString.equals(state.getStringValue()))
                {
                    return state;
                }
            }

            return null;
        }

        public String getStringValue()
        {
            return this.sqlString;
        }
    }

}