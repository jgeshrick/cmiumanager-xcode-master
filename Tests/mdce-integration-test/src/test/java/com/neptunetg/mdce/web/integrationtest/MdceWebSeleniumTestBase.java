/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.SeleniumTestBase;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.neptunetg.mdce.web.integrationtest.UserLogin.loginIfRequired;
import static org.junit.Assert.assertEquals;

/**
 * Based class for MDCE web Selenium test which implements admin login functionality
 */
public class MdceWebSeleniumTestBase extends SeleniumTestBase
{
    private static final String AUDIT_ROWS = "//table[@id='table-audit-list']/tbody/tr";
    private static final String LIMIT_TO_TOP_ROWS="[position() < 10]";
    protected final String cmiuAuditLogUrl = Constants.MDCE_WEB_URL + "/pages/audit";
    protected static final String cmiuIdForFotaCommandInjection = "400000102";
    protected static final String cmiuIdForImageUpdateCommandInjection = "400000220";
    protected static final String[] cmiuIdsForUiSortTesting = {"400000212", "400000214"};
    protected static final String cmiuConfigUrl = Constants.MDCE_WEB_URL + "/pages/config";
    protected static final String cmiuImageUpdate = Constants.MDCE_WEB_URL + "/pages/config/image-update";
    protected static final String cmiuFotaUpdate = Constants.MDCE_WEB_URL + "/pages/config/update-modem-firmware";
    protected static final String addUserUrl = Constants.MDCE_WEB_URL + "pages/user/add";
    protected static final String userListUrl = Constants.MDCE_WEB_URL + "pages/user/list";
    protected static final String userLoginUrl = Constants.MDCE_WEB_URL + "pages/user/login";
    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
    protected ZonedDateTime startTime;    //time when this test is started

    @Before
    public void setUp()
    {
        //allow a few mins tolerance between CI server and mdce server.
        startTime = ZonedDateTime.now().minusMinutes(3);
    }

    public static boolean selectOptionByText(WebElement dropdown, String optionText)
    {
        boolean wasFound = false;

        if (dropdown.getAttribute("class").toLowerCase().contains("ui-combobox"))
        {
            //dropdown has been substituted by jQuery-UI, so find the substituted controls instead
            final WebElement dropdownButton = dropdown.findElement(By.xpath("following-sibling::span/button"));
            dropdownButton.click();
            final List<WebElement> options = dropdownButton.findElements(By.xpath("following-sibling::ul/li"));

            for (WebElement element : options)
            {
                if (element.getText().equalsIgnoreCase(optionText))
                {
                    element.click();
                    wasFound = true;
                    break;
                }
            }
        }
        else
        {
            //interact with raw dropdown
            final Select selectElement = new Select(dropdown);

            for (WebElement option : selectElement.getOptions())
            {
                // Ensure the requested option is present
                if (option.getText().equalsIgnoreCase(optionText))
                {
                    selectElement.selectByVisibleText(optionText);
                    wasFound = true;
                    break;
                }
            }
        }

        return wasFound;
    }

    public boolean selectOptionByValue(WebElement dropdown, String optionValue)
    {
        boolean wasFound = false;
        String optionText = null;
        final Select selectElement = new Select(dropdown);

        for (WebElement option : selectElement.getOptions())
        {
            // Ensure the requested option is present
            if (option.getAttribute("value").equalsIgnoreCase(optionValue))
            {
                boolean wasInvisible = false;

                // A little hack to allow the driver to get the display text from the option.
                if ("none".equalsIgnoreCase(dropdown.getCssValue("display")))
                {
                    wasInvisible = true;
                    driver.executeScript("$('#" + dropdown.getAttribute("id") + "').css('display', 'inline');");
                }

                optionText = option.getText();

                // Restore to hidden
                if (wasInvisible)
                {
                    driver.executeScript("$('#" + dropdown.getAttribute("id") + "').css('display', 'none');");
                }

                wasFound = true;
                break;
            }
        }

        if (wasFound)
        {
            wasFound &= selectOptionByText(dropdown, optionText);
        }

        return wasFound;
    }

    protected void checkIfRequiredLogin()
    {
        loginIfRequired(this);
    }

    protected void sendGenericCmiuCommands(String commandLinkText, String expectedHeader)
    {
        sendGenericCmiuCommands(commandLinkText, expectedHeader, () -> true);
    }

    protected void sendGenericCmiuCommands(String commandLinkText, String expectedHeader, Supplier<Boolean> fn)
    {


        clickSendCommand(commandLinkText, expectedHeader);

        //input CMIU
        driver.findElement(By.id("miuInputList0.miuId")).sendKeys(cmiuIdForFotaCommandInjection);

        boolean canProceed = false;

        if (fn != null)
        {
            canProceed = fn.get();
        }

        if (canProceed)
        {
            takeScreenshot(commandLinkText);

            //submit form
            driver.findElementByXPath("//form//input[@value='Send command']").submit();

            final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Command & Control Interface')]")));
            takeScreenshot(commandLinkText + "Command & Control Interface");
        }
    }

    private void clickSendCommand(String commandLinkText, String expectedHeader)
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 10);

        for (int attempt = 0; attempt < 5; attempt++)
        {
            driver.get(cmiuConfigUrl);

            checkIfRequiredLogin();

            //select command from dropdown menu

            try
            {
                Actions builder = new Actions(driver);
                builder.moveToElement(driver.findElement(By.linkText("Send Commands"))).click();
                builder.build().perform();

                driver.findElement(By.linkText(commandLinkText)).click();

                webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), '" +
                        expectedHeader + "')]")));

                webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("miuInputList0.miuId")));

                break;
            }
            catch (Exception e)
            {
                System.err.println("Failed to issue command " + commandLinkText + " (" + e.getClass() + "); attempt " + (attempt + 1) + " / 5");
                //next attempt
            }
        }
        takeScreenshot("Clicked command " + commandLinkText);
    }

    /**
     * Add a new config set, and change the newly created config set again.
     * @param webDriverWait
     * @return
     */
    protected Boolean addAndEditConfigSet(WebDriverWait webDriverWait)
    {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Define new config set')]")));

        //add new config set
        Random rand = new Random();
        final String newConfigSetName = "config set name " + LocalDateTime.now();
        driver.findElement(By.id("name")).sendKeys(newConfigSetName);
        driver.findElement(By.id("cmiuModeName")).sendKeys(newConfigSetName);
        driver.findElement(By.id("reportingPlanStartMins")).sendKeys(Integer.toString(rand.nextInt(60) + 1));
        driver.findElement(By.id("reportingPlanIntervalHours")).sendKeys(Integer.toString(rand.nextInt(12) + 1));
        driver.findElement(By.id("reportingPlanTransmitWindowMins")).sendKeys(Integer.toString(rand.nextInt(30) + 1));
        driver.findElement(By.id("reportingPlanQuietStartMins")).sendKeys(Integer.toString(rand.nextInt(240) + 1));
        driver.findElement(By.id("reportingPlanQuietEndMins")).sendKeys(Integer.toString(rand.nextInt(240) + 1));
        driver.findElement(By.id("recordingPlanStartTimeMins")).sendKeys(Integer.toString(rand.nextInt(240) + 1));
        driver.findElement(By.id("recordingPlanIntervalMins")).sendKeys(Integer.toString(rand.nextInt(240) + 1));

        driver.findElement(By.id("name")).submit();

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'List CMIU config set')]")));
        List<WebElement> foundConfigSet = driver.findElementsByXPath("//tbody/tr/td[2][contains(text(),'" + newConfigSetName + "')]");

        assertEquals(1, foundConfigSet.size());

        //Go to edit config set
        driver.findElementByXPath("//tbody/tr/td[2][contains(text(),'" + newConfigSetName + "')]/..//a")
                .click();

        String inputValue = driver.findElementById("name").getAttribute("value");
        assertEquals(newConfigSetName, inputValue);

        driver.findElement(By.id("name")).submit();

        return false;
    }

    /**
     * Load audit log page and retrieve recent audit log entries and deserialise into objects.
     */
    protected List<AuditLogEntry> getRecentAuditLogEntries(ZonedDateTime startDate, Integer filterByMiuId)
    {
        return getRecentAuditLogEntries(startDate, filterByMiuId, true);
    }

    protected List<AuditLogEntry> getRecentAuditLogEntries(ZonedDateTime startDate, Integer filterByMiuId, boolean recentRows)
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
        driver.get(cmiuAuditLogUrl);

        //wait till the audit log page has loaded completely, signify by the present of #audit-log-end element at the end of the page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='audit-log-end']")));
        takeScreenshot("Audit log - " + ZonedDateTime.now());

        //perform filtering: filter to the test cmiu and show log for today
        if(filterByMiuId != null)
        {
            driver.findElement(By.id("miuId")).clear();
            driver.findElement(By.id("miuId")).sendKeys(String.valueOf(filterByMiuId));
            driver.findElement(By.id("miuId")).submit();
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='audit-log-end']")));
        }

        final String xpath = recentRows? AUDIT_ROWS + LIMIT_TO_TOP_ROWS:AUDIT_ROWS;


        return driver.findElementsByXPath(xpath)
                .stream()
                .map(AuditLogEntry::fromAuditTableRow)
                .filter(entry -> entry.getDate().isAfter(startDate))
                .collect(Collectors.toList());
    }

    protected void addUser(WebDriverWait webDriverWait, String newUserName, String userLevel)
    {
        final String userPassword = "password";

        driver.get(addUserUrl);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Add User')]")));

        driver.findElementById("userName").sendKeys(newUserName);
        Select userLevelSelection = new Select(driver.findElement(By.id("userLevel")));
        userLevelSelection.selectByVisibleText(userLevel);

        driver.findElementById("password").sendKeys(userPassword);
        driver.findElementById("passwordRepeated").sendKeys(userPassword);
        driver.findElementById("email").sendKeys(newUserName + "@server.com");

        driver.findElement(By.id("add-user")).click();

        //back to User List page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'User List')]")));
    }

    protected void deleteUser(WebDriverWait webDriverWait, String userName)
    {
        //at User List, delete the user
        driver.get(userListUrl);
        String deleteUserXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + userName + "')]" +
                "/..//a[contains(@href, 'delete')]";

        String adminUserXpath = "//tr[contains(@class, 'list-item')]/td[contains(@class, 'user-list-username') and contains(text(), 'admin')]";

        driver.findElementByXPath(deleteUserXpath).click();

        final String xpathConfirmDeleteButton = "//button[span[@class='ui-button-text' and contains(text(), 'Confirm delete')]]";

        //confirm modal box
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathConfirmDeleteButton)));
        driver.findElementByXPath(xpathConfirmDeleteButton).click();

        // Verify that the user has been removed.
        // First check for the "admin" user to verify that the browser is back on the user list;
        // then make sure the Delete button is no longer present for the user who's just been deleted.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(adminUserXpath)));
        webDriverWait.until(ExpectedConditions.not(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(deleteUserXpath))));
    }


    protected boolean tryLoginAs(WebDriverWait webDriverWait, String userName, String password)
    {
        final WebDriverWait webDriverShortWait = new WebDriverWait(driver, 2);

        driver.get(userLoginUrl);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("login")));

        driver.findElement(By.id("username")).sendKeys(userName);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("login")).submit();

        //check that we have navigated away from the login webpage
        try
        {
            webDriverShortWait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("//h1[contains(text(), 'Login')]")));
        }
        catch(TimeoutException ex)
        {
            // Ignore
        }

        // Determine success by having been moved off the login page
        return !driver.getCurrentUrl().contains("pages/user/login");
    }


    /**
     * De-serialise and represent a row of audit log entry in /mdce-web/pages/audit
     */
    protected static class AuditLogEntry
    {
        private ZonedDateTime date;
        private String user;
        private Integer miu;
        private String eventType;



        private String oldValue = "";
        private String newValue = "";

        public ZonedDateTime getDate()
        {
            return date;
        }

        public String getUser()
        {
            return user;
        }

        public int getMiu()
        {
            return miu;
        }

        public String getEventType()
        {
            return eventType;
        }

        public String getOldValue()
        {
            return oldValue;
        }

        public String getNewValue()
        {
            return newValue;
        }

        protected static AuditLogEntry fromAuditTableRow(WebElement tableRowWebElement)
        {
            AuditLogEntry auditLogEntry = new AuditLogEntry();

            String date = tableRowWebElement.findElement(By.xpath("td[1]")).getText();
            auditLogEntry.date = ZonedDateTime.parse(date, DATE_TIME_FORMATTER);

            auditLogEntry.user = tableRowWebElement.findElement(By.xpath("td[2]")).getText().trim();
            String miuString = tableRowWebElement.findElement(By.xpath("td[3]")).getText().trim();

            if (!miuString.isEmpty())
            {
                auditLogEntry.miu = Integer.parseInt(miuString);
            }

            auditLogEntry.eventType = tableRowWebElement.findElement(By.xpath("td[4]")).getText().trim();
            auditLogEntry.oldValue = tableRowWebElement.findElement(By.xpath("td[5]")).getText().trim();
            auditLogEntry.newValue = tableRowWebElement.findElement(By.xpath("td[6]")).getText().trim();

            return auditLogEntry;
        }
    }

}
