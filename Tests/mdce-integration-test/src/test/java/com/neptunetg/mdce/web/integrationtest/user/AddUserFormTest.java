/*
 * Neptune Technology Group
 * Copyright 2017 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.user;

import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

import static com.neptunetg.mdce.web.integrationtest.UserLogin.loginAsAdmin;
import static com.neptunetg.mdce.web.integrationtest.UserLogin.logoutUser;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Testing around adding users.
 */
public class AddUserFormTest extends MdceWebSeleniumTestBase
{

    private WebDriverWait webDriverWait;
    private String temporaryUserName;
    private String userPassword;

    @Before
    public void setup()
    {
        webDriverWait = new WebDriverWait(driver, 30);
        temporaryUserName = "user-" + Instant.now().getEpochSecond();
        userPassword = "password";
        // login as admin
        loginAsAdmin(this);
        // create temp user
        addUser(webDriverWait, temporaryUserName, "Site operator");
    }


    @Test
    public void testUserNameExistError()
    {
        // check that user in list
        assertTrue( "User not found in list before adding user with same username", isUsernameInList(temporaryUserName));
        // go to add user
        driver.get(addUserUrl);
        // try adding user with same username

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Add User')]")));

        driver.findElementById("userName").sendKeys(temporaryUserName);
        Select userLevelSelection = new Select(driver.findElement(By.id("userLevel")));
        userLevelSelection.selectByVisibleText("Site operator");

        driver.findElementById("password").sendKeys(userPassword);
        driver.findElementById("passwordRepeated").sendKeys(userPassword);
        driver.findElementById("email").sendKeys(temporaryUserName + "@server.com");

        driver.findElement(By.id("add-user")).click();

        // look for error message about username already exists
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.className("error")));
        assertThat(driver.findElement(By.className("error")).getText(),containsString("Username"));
    }

    @Test
    public void testEmailExistError()
    {
        // check that user in list
        assertTrue( "User not found in list before adding user with same email", isUserEmailInList(temporaryUserName + "@server.com"));
        // go to add user
        driver.get(addUserUrl);
        // try adding user with same username

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Add User')]")));

        driver.findElementById("userName").sendKeys("testUser");
        Select userLevelSelection = new Select(driver.findElement(By.id("userLevel")));
        userLevelSelection.selectByVisibleText("Site operator");

        driver.findElementById("password").sendKeys(userPassword);
        driver.findElementById("passwordRepeated").sendKeys(userPassword);
        driver.findElementById("email").sendKeys(temporaryUserName + "@server.com");

        driver.findElement(By.id("add-user")).click();

        // look for error message about username already exists
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.className("error")));
        assertThat(driver.findElement(By.className("error")).getText(),containsString("Email"));
    }


    @After
    public void tearDown()
    {
        deleteUser(webDriverWait, temporaryUserName);
        logoutUser(this);
    }

    /**
     * looking for user in user table.
     * @param username Users Name
     * @return boolean
     */
    private boolean isUsernameInList(String username)
    {
        for (WebElement elm : driver.findElementsByClassName("user-list-username"))
        {
            if (elm.getText().equals(username))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * looking for user email in user table.
     * @param email uUser Email
     * @return boolean
     */
    private boolean isUserEmailInList(String email)
    {
        for (WebElement elm : driver.findElementsByClassName("user-list-email"))
        {
            if (elm.getText().equals(email))
            {
                return true;
            }
        }
        return false;
    }


}
