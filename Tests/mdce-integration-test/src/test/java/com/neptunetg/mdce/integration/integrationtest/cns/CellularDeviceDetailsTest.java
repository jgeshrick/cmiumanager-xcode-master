/*
 * Neptune Technology Group
 * Copyright 2015, 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.cns;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Exercise Verizon UWS API.
 * This assumes CMIU: 400000102 is associated with this UWS account
 */
public class CellularDeviceDetailsTest extends BaseSetupRestTest
{
    private final String urlStart = Constants.MDCE_INTEGRATION_URL + "mdce/api/v1/miu/cellular-details";
    private String token0;


    public CellularDeviceDetailsTest()
    {
        super(400000102);
    }

    @Before
    public void setUp() throws Exception
    {
        token0 = fetchTokenForSiteId(0);
    }

    @Test
    public void getCmiuDetailsFromIccid() throws Exception
    {

        String responseString = HttpUtility.httpGet(urlStart +
                "?token=" + token0 +
                "&mno=VZW&identifier=iccid" +
                "&value=" + CmiuDetail.getIccid());

        ObjectMapper mapper = new ObjectMapper();
        DeviceCellularInformation response = mapper.readValue(responseString, DeviceCellularInformation.class);

        assertEquals(CmiuDetail.getIccid(), response.getIccid());
    }

    @Test
    public void getCmiuDetailsFromImei() throws Exception
    {
        String responseString = HttpUtility.httpGet(urlStart +
                "?token=" + token0 +
                "&mno=VZW&identifier=imei" +
                "&value=" + CmiuDetail.getImei());

        ObjectMapper mapper = new ObjectMapper();
        DeviceCellularInformation response = mapper.readValue(responseString, DeviceCellularInformation.class);

        assertEquals(CmiuDetail.getIccid(), response.getIccid());
    }

    @Test
    public void getCmiuDetailsFromMsisdn() throws Exception
    {
        String responseString = HttpUtility.httpGet(urlStart +
                "?token=" + token0 +
                "&mno=VZW&identifier=msisdn" +
                "&value=" + CmiuDetail.getMsisdn());

        ObjectMapper mapper = new ObjectMapper();
        DeviceCellularInformation response = mapper.readValue(responseString, DeviceCellularInformation.class);

        assertEquals(CmiuDetail.getIccid(), response.getIccid());
    }

}
