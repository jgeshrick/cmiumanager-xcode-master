/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.user;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

import static com.neptunetg.mdce.web.integrationtest.UserLogin.loginAsAdmin;
import static com.neptunetg.mdce.web.integrationtest.UserLogin.logoutUser;
import static junit.framework.Assert.assertTrue;

/**
 * Tests around the Change Password user form.
 */
public class ChangePasswordTest extends MdceWebSeleniumTestBase
{
    private final String changePasswordUrl = Constants.MDCE_WEB_URL + "pages/user/changePassword";

    @Test
    public void testUserChangingTheirOwnPassword()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        final String temporaryUserName = "user-" + Instant.now().getEpochSecond();

        // Arrange: create temporary user
        loginAsAdmin(this);
        addUser(webDriverWait, temporaryUserName, "Msoc administrator");

        // Act: Change password
        tryLoginAs(webDriverWait, temporaryUserName, "password");
        assertTrue(changePassword(webDriverWait, temporaryUserName, "password", "password2"));

        // Assert: Check that user can log in with new password
        assertTrue(tryLoginAs(webDriverWait, temporaryUserName, "password2"));

        // Clean up
        loginAsAdmin(this);

        deleteUser(webDriverWait, temporaryUserName);

        logoutUser(this);
    }

    private boolean changePassword(WebDriverWait webDriverWait, String temporaryUserName, String currentPassword, String newPassword)
    {
        final WebDriverWait webDriverShortWait = new WebDriverWait(driver, 2);

        driver.get(changePasswordUrl);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='submit' and @id='change-password']")));

        assertTrue(driver.findElement(By.id("username")).getAttribute("value").equals(temporaryUserName));
        driver.findElement(By.id("current-password")).sendKeys(currentPassword);
        driver.findElement(By.id("new-password")).sendKeys(newPassword);
        driver.findElement(By.id("new-password-repeated")).sendKeys(newPassword);
        driver.findElement(By.id("change-password")).click();

        boolean wasSuccessful;
        try
        {
            webDriverShortWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//input[@type='submit' and @id='change-password']")));
            wasSuccessful = true;
        }
        catch(TimeoutException ex)
        {
            wasSuccessful = false;
        }

        return wasSuccessful;
    }

}
