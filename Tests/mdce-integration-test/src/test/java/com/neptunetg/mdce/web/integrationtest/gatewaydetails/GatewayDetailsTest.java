/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.gatewaydetails;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.r900.GatewayTarball;
import com.neptunetg.mdce.common.r900.GatewayTarballParser;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Gateway details page navigation
 */
public class GatewayDetailsTest extends MdceWebSeleniumTestBase
{
    private final String GATEWAY_LIST_URL = Constants.MDCE_WEB_URL + "pages/gateway/gatewaylist";
    private static final String GPV300026_042118_0962 = "59111_GPV300026_042118_0962";
    GatewayTarballParser tarballParser = new GatewayTarballParser();

    @Test
    public void testGatewayDetailsPage() throws Exception
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
        final String TEST_GATEWAY_ID = "59111_GPV300026";
        final String GPV300026_042118_0962_TAR = GPV300026_042118_0962 + ".tar";

        final InputStream tarStream = getClass().getResourceAsStream("/" + GPV300026_042118_0962_TAR);
        final GatewayTarball gatewayTarball = tarballParser.parse(GPV300026_042118_0962, tarStream);

        final String putUrl = Constants.MDCE_INTEGRATION_URL +
                "4/data?site_id=59111&collectorname=GPV300026&filename=" + GPV300026_042118_0962_TAR;
        HttpUtility.httpPutFile(putUrl, GPV300026_042118_0962_TAR);

        //list miu details page

        Instant waitUntil = Instant.now().plus(3, ChronoUnit.MINUTES);

        while(Instant.now().isBefore(waitUntil))
        {
            //Check for gateway every 5 seconds
            Thread.sleep(Duration.ofSeconds(5).toMillis());

            driver.get(GATEWAY_LIST_URL);
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("selectedManager")));

            //wait for table to be reloaded, check for element in td containing test cmiu id as a link
            List<WebElement> elements = driver.findElementsByXPath("//table[@id='table-gateway-list']/tbody/tr/td" +
                    "/a[text()[contains(.,'" + TEST_GATEWAY_ID + "')]]");

            if(elements.size() != 0)
            {
                break;
            }
        }

        takeScreenshot("Gateway detail list, testing with: " + TEST_GATEWAY_ID);

        String gatewayId = "";
        String siteId = "";
        String numberOfMius = "";
        String ookPacketsHeard = "";
        String fskPacketsHeard = "";
        String configPacketsHeard = "";

        Instant timeoutTime = Instant.now();
        timeoutTime = timeoutTime.plus(5, ChronoUnit.MINUTES);

        String gatewayListUrl = driver.getCurrentUrl();

        do
        {
            Thread.sleep(Duration.ofSeconds(5).toMillis());

            //navigate to gateway details page for the TEST_GATEWAY_ID
            driver.get(gatewayListUrl);

            try
            {
                driver.findElementByXPath("//table[@id='table-gateway-list']/tbody/tr/td/a[text()[contains(.,'" +
                        TEST_GATEWAY_ID +
                        "')]]")
                        .click();

                gatewayId = driver.findElementsByXPath("//dl[contains(.,'Gateway ID')]/dd").get(0).getText();
                siteId = driver.findElementsByXPath("//dl[contains(.,'Site ID')]/dd").get(0).getText();
                numberOfMius = driver.findElementsByXPath("//dl[contains(.,'Number of Mius')]/dd").get(0).getText();
                ookPacketsHeard = driver.findElementsByXPath("//dl[contains(.,'OOK Packets Heard')]/dd").get(0).getText();
                fskPacketsHeard = driver.findElementsByXPath("//dl[contains(.,'FSK Packets Heard')]/dd").get(0).getText();
                configPacketsHeard = driver.findElementsByXPath("//dl[contains(.,'Config Packets Heard')]/dd").get(0).getText();
            }
            catch (Exception e)
            {
                //do nothing, we don't expect to get it straight away, but on further retries
            }

        } while(timeoutTime.isAfter(Instant.now())  &&
            (!gatewayId.equals(TEST_GATEWAY_ID)           ||
                !siteId.equals("JG Test Site (59111)")    ||
                !numberOfMius.equals("5702")              ||
                !ookPacketsHeard.equals("6913")           ||
                !fskPacketsHeard.equals("60")             ||
                !configPacketsHeard.equals("3")));

        assertEquals(gatewayId, TEST_GATEWAY_ID);
        assertEquals(siteId, "JG Test Site (59111)");
        assertEquals(numberOfMius, "5702");
        assertEquals(ookPacketsHeard, "6913");
        assertEquals(fskPacketsHeard, "60");
        assertEquals(configPacketsHeard, "3");
    }

    /**
     * Get currently selected option value. This is preferred to using Selenium selection.getFirstSelectedOption().getText();
     * as it is very slow if the options list is huge.
     * @return display string of the selected option
     */
    private String getSelectedSiteDisplayString()
    {
        return driver.findElement(By.xpath("//select[@id='newSiteId']/option[@selected=''] ")).getText().trim();
    }
}
