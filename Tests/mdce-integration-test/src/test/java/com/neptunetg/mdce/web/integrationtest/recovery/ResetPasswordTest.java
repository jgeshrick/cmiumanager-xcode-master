/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.recovery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.internal.email.model.EmailMessage;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import com.neptunetg.mdce.web.integrationtest.UserLogin;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Verify that a user can reset their own password and receive a confirmation email.
 */
public class ResetPasswordTest extends MdceWebSeleniumTestBase
{
    private static final String PASSWORD_RESET_URL = Constants.MDCE_WEB_URL + "pages/recovery/password";
    private static final String DEBUG_LAST_EMAIL_URL = Constants.MDCE_WEB_URL + "pages/debug/last-email";
    private int index;

    @Before
    public void setUp()
    {
        index = 0;
    }

    @Test
    public void testPasswordResetHappyPath()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 5);

        // Create user
        final String temporaryUserNameA = "user-A-" + Instant.now().getEpochSecond();
        final String temporaryUserEmailA = temporaryUserNameA + "@server.com";
        addUser(webDriverWait, temporaryUserNameA, "Msoc operator");

        // Clear the last-email buffer
        getLastEmailMessage();

        // Reset user A's password
        String link = requestPasswordResetLink(webDriverWait, temporaryUserEmailA);
        assertTrue(StringUtils.hasText(link));

        assertTrue(submitPasswordReset(webDriverWait, link, temporaryUserNameA, "password2", "password2"));
        Pair<String, EmailMessage> lastMessage = getLastEmailMessage();
        EmailMessage confirmationEmail = lastMessage.getValue();
        takeScreenshotWithCaption(getSequentialName("After resetting password"), lastMessage.getKey());

        assertEquals("Neptune MDCE Password Reset Confirmation", confirmationEmail.getSubject());
        assertTrue(confirmationEmail.getContents().contains(temporaryUserEmailA));

        // Log on with new password
        assertTrue(tryLoginAs(webDriverWait, temporaryUserNameA, "password2"));

        // Tidy up
        assertTrue(tryLoginAs(webDriverWait, UserLogin.ADMIN_USER, UserLogin.ADMIN_PWD));
        deleteUser(webDriverWait, temporaryUserNameA);
    }

    @Test
    public void testPasswordResetFailureScenariosAndEdgeCases()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 5);

        // Create two users
        final String temporaryUserNameA = "user-A-" + Instant.now().getEpochSecond();
        final String temporaryUserEmailA = temporaryUserNameA + "@server.com";
        addUser(webDriverWait, temporaryUserNameA, "Msoc operator");

        final String temporaryUserNameB = "user-B-" + Instant.now().getEpochSecond();
        final String temporaryUserEmailB = temporaryUserNameB + "@server.com";
        addUser(webDriverWait, temporaryUserNameB, "Msoc operator");

        // Clear the last-email buffer
        getLastEmailMessage();

        // User B tries to reset user A's password
        String link = requestPasswordResetLink(webDriverWait, temporaryUserEmailB);
        final String confirmationMessageWhenKnownEmailAddressIsEntered = driver.findElement(By.id("request-received-info")).getText();
        assertTrue(StringUtils.hasText(link));

        assertFalse(submitPasswordReset(webDriverWait, link, temporaryUserNameA, "password2", "password2"));
        Pair<String, EmailMessage> lastMessage = getLastEmailMessage();
        EmailMessage confirmationEmail = lastMessage.getValue();
        takeScreenshotWithCaption(getSequentialName("Reset password with wrong username"), lastMessage.getKey());

        assertFalse(StringUtils.hasText(confirmationEmail.getSubject()));
        assertFalse(StringUtils.hasText(confirmationEmail.getContents()));

        // User B enters a different password into the "Repeated" input
        assertFalse(submitPasswordReset(webDriverWait, link, temporaryUserNameB, "password2", "DifferentPassword"));
        lastMessage = getLastEmailMessage();
        confirmationEmail = lastMessage.getValue();
        takeScreenshotWithCaption(getSequentialName("Repeated password does not match"), lastMessage.getKey());

        assertFalse(StringUtils.hasText(confirmationEmail.getSubject()));
        assertFalse(StringUtils.hasText(confirmationEmail.getContents()));

        // Request a new password reset link for user B
        String secondLink = requestPasswordResetLink(webDriverWait, temporaryUserEmailB);
        assertTrue(StringUtils.hasText(secondLink));

        // Try to use the old link
        assertFalse(submitPasswordReset(webDriverWait, link, temporaryUserNameB, "password2", "DifferentPassword"));
        takeScreenshot(getSequentialName("Using expired password reset link"));

        // Request a new link with upper-case email address
        String thirdLink = requestPasswordResetLink(webDriverWait, temporaryUserEmailB.toUpperCase());
        assertTrue(StringUtils.hasText(thirdLink));

        // Reset the password using upper case username
        assertTrue(submitPasswordReset(webDriverWait, thirdLink, temporaryUserNameB.toUpperCase(), "password2", "password2"));
        lastMessage = getLastEmailMessage();
        confirmationEmail = lastMessage.getValue();
        takeScreenshotWithCaption(getSequentialName("Password reset with upper case details"), lastMessage.getKey());

        assertEquals("Neptune MDCE Password Reset Confirmation", confirmationEmail.getSubject());
        assertTrue(confirmationEmail.getContents().contains(temporaryUserEmailB.toUpperCase()));

        // Log on with new password
        assertTrue(tryLoginAs(webDriverWait, temporaryUserNameB, "password2"));

        // Request password reset link for unknown email address
        String unknownLink = requestPasswordResetLink(webDriverWait, "unknown.user@unknown.server.com");
        final String confirmationMessageWhenUnknownEmailAddressIsEntered = driver.findElement(By.id("request-received-info")).getText();
        assertFalse(StringUtils.hasText(unknownLink));

        // The "request received" message must be identical whether or not the email address is known.
        assertTrue(
                "Expect the \"request received\" message must be identical whether or not the email address is known",
                confirmationMessageWhenKnownEmailAddressIsEntered
                        .equals(confirmationMessageWhenUnknownEmailAddressIsEntered));

        // Test that two password reset links can coexist
        String linkForUserA = requestPasswordResetLink(webDriverWait, temporaryUserEmailA);
        assertTrue(StringUtils.hasText(linkForUserA));
        String linkForUserB = requestPasswordResetLink(webDriverWait, temporaryUserEmailB);
        assertTrue(StringUtils.hasText(linkForUserB));

        assertTrue(submitPasswordReset(webDriverWait, linkForUserA, temporaryUserNameA, "passwordUserA", "passwordUserA"));
        assertTrue(submitPasswordReset(webDriverWait, linkForUserB, temporaryUserNameB, "passwordUserB", "passwordUserB"));

        assertTrue(tryLoginAs(webDriverWait, temporaryUserNameA, "passwordUserA"));
        assertTrue(tryLoginAs(webDriverWait, temporaryUserNameB, "passwordUserB"));

        // Verify that a password reset link can only be used once.
        link = requestPasswordResetLink(webDriverWait, temporaryUserEmailA);
        assertTrue(StringUtils.hasText(linkForUserA));

        // 1st time works
        assertTrue(submitPasswordReset(webDriverWait, link, temporaryUserNameA, "passwordUserA-1", "passwordUserA-1"));
        // 2nd time fails
        assertFalse(submitPasswordReset(webDriverWait, link, temporaryUserNameA, "passwordUserA-2", "passwordUserA-2"));
        // Verify that the 2nd reset attempt was ignored
        assertTrue(tryLoginAs(webDriverWait, temporaryUserNameA, "passwordUserA-1"));

        // Tidy up
        assertTrue(tryLoginAs(webDriverWait, UserLogin.ADMIN_USER, UserLogin.ADMIN_PWD));
        deleteUser(webDriverWait, temporaryUserNameA);
        deleteUser(webDriverWait, temporaryUserNameB);
    }

    /**
     * Request a password reset and retrieve the password reset link via the debug pages.
     * @param webDriverWait WebDriverWait
     * @param emailAddress The email address to request.
     * @return The password reset URL, or an empty string if not found.
     */
    private String requestPasswordResetLink(WebDriverWait webDriverWait, String emailAddress)
    {
        driver.get(PASSWORD_RESET_URL);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("begin-password-recovery")));

        driver.findElement(By.id("email-address")).sendKeys(emailAddress);
        driver.findElement(By.id("begin-password-recovery")).click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("request-received-info")));

        Pair<String, EmailMessage> lastMessage = getLastEmailMessage();
        EmailMessage emailMessage = lastMessage.getValue();

        String privateUrl = "";
        if (emailMessage != null && StringUtils.hasText(emailMessage.getContents()))
        {
            String[] words = emailMessage.getContents().split("\\s+");
            for (String word : words)
            {
                if (word.contains("/pages/recovery/password/"))
                {
                    privateUrl = word;
                    break;
                }
            }
        }

        takeScreenshotWithCaption(getSequentialName("Requested password reset"), lastMessage.getKey());

        return privateUrl;
    }

    /**
     * Gets the last email message from the debug pages and returns both the raw JSON and the deserialized EmailMessage.
     * @return The JSON and deserialized object, or null if the operation failed.
     */
    private Pair<String, EmailMessage> getLastEmailMessage()
    {
        try
        {
            StringBuilder jsonBuilder = new StringBuilder();
            URL url = new URL(DEBUG_LAST_EMAIL_URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;

            while ((line = rd.readLine()) != null)
            {
                jsonBuilder.append(line);
            }

            rd.close();

            ObjectMapper objectMapper = new ObjectMapper();
            String json = jsonBuilder.toString();
            EmailMessage emailMessage = objectMapper.readValue(json, EmailMessage.class);

            return new Pair<>(json, emailMessage);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private boolean submitPasswordReset(WebDriverWait webDriverWait, String passwordResetLink, String userName,
                                        String newPassword, String newPasswordRepeated)
    {
        boolean result = true;

        driver.get(passwordResetLink);
        try
        {
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("reset-password")));

            driver.findElement(By.id("user-name")).sendKeys(userName);
            driver.findElement(By.id("new-password")).sendKeys(newPassword);
            driver.findElement(By.id("new-password-repeated")).sendKeys(newPasswordRepeated);
            takeScreenshot(getSequentialName("submitPasswordReset-before-submit"));
            driver.findElement(By.id("reset-password")).click();

            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("password-changed-info")));
        }
        catch (Exception e)
        {
            takeScreenshot(getSequentialName("submitPasswordReset-failed"));
            result = false;
        }

        return result;
    }

    private String getSequentialName(String name)
    {
        return String.format("%s_%03d", name, ++index);
    }
}
