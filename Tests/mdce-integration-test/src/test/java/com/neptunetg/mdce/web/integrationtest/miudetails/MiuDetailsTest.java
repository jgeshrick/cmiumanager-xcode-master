/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.miudetails;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.integration.integrationtest.rest.MiuLifecycleHistoryTest;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import com.neptunetg.mdce.web.integrationtest.UserLogin;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Miu details page navigation and changing of miu ownership.
 * Verify audit log when ownership changed.
 */
public class MiuDetailsTest extends MdceWebSeleniumTestBase
{
    private static final String H1_CONTAINS_TEXT_MIU_DETAILS = "//h1[contains(text(), 'MIU Details')]";
    private static final String MIU_LIST_URL = Constants.MDCE_WEB_URL + "pages/miu/miulist";
    private static final String MIU_DETAIL_URL = Constants.MDCE_WEB_URL + "pages/miu/miudetail";

    @Test
    public void testMiuDetailsPage()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
        final String TEST_CMIU_ID = "400000102";

        //list miu details page
        driver.get(MIU_LIST_URL);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("selectedManager")));
        takeScreenshot("Miu list");

        takeScreenshot("Miu detail list filtered to " + TEST_CMIU_ID);

        //wait for table to be reloaded, check for element in td containing test cmiu id as a link
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//table[@id='table-miu-list']/tbody/tr/td/a[text()[contains(.,'" +
                        TEST_CMIU_ID +
                        "')]]")));

        //navigate to miu details page for the TEST_CMIU_ID
        driver.findElementByXPath("//table[@id='table-miu-list']/tbody/tr/td/a[text()[contains(.,'" +
                TEST_CMIU_ID +
                "')]]")
                .click();

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(H1_CONTAINS_TEXT_MIU_DETAILS)));

        //change ownership to something new
        final WebElement siteIdSelect = driver.findElement(By.id("newSiteId"));
        final Select selection = new Select(siteIdSelect);
        final String currentMiuOwnerSiteId = this.getSelectedSiteId();
        //final String currentMiuOwnerSiteDisplayString = this.getSelectedSiteDisplayString();
        String newSiteIdToSelect = null;

        // Find any different Site ID
        for (WebElement option : selection.getOptions())
        {
            String value = option.getAttribute("value");
            if (StringUtils.hasText(value) && !value.equalsIgnoreCase(currentMiuOwnerSiteId))
            {
                newSiteIdToSelect = value;
                break;
            }
        }

        //change to a different site
        this.changeMiuOwner(newSiteIdToSelect);

        //change back to original site
        this.changeMiuOwner(currentMiuOwnerSiteId);
    }

    @Test
    public void testMiuHistoryWithMiuDetailsPage() throws Exception
    {
        // Use IF47 to set the current lifecycle state and verify in the front end that it is updated.

        // Using an instance of MiuLifecycleHistoryTest as it can make the call to the miu_history API.
        final MiuLifecycleHistoryTest miuLifecycleHistoryTest = new MiuLifecycleHistoryTest();

        final Instant timestamp1 = Instant.now().minus(Duration.ofSeconds(1));
        final Instant timestamp2 = Instant.now();

        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
        final String TEST_CMIU_ID = "400000102";

        // Set state
        miuLifecycleHistoryTest.addHistoricMiuLifecycleState(400000102, "Scanned", timestamp1);

        // miu details page
        driver.get(MIU_DETAIL_URL + "/" + TEST_CMIU_ID);

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(H1_CONTAINS_TEXT_MIU_DETAILS)));

        final String currentState = driver.findElement(By.xpath("//dt[text()='Lifecycle State:']/following-sibling::dd")).getText();

        assertEquals("Scanned", currentState);

        // Set state again
        miuLifecycleHistoryTest.addHistoricMiuLifecycleState(400000102, "Shipped", timestamp2);

        driver.get(MIU_DETAIL_URL + "/" + TEST_CMIU_ID);

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(H1_CONTAINS_TEXT_MIU_DETAILS)));

        final String updatedState = driver.findElement(By.xpath("//dt[text()='Lifecycle State:']/following-sibling::dd")).getText();

        assertEquals("Shipped", updatedState);
    }


    private void selectMiddleOption(WebElement dropdown)
    {
        if (dropdown.getAttribute("class").toLowerCase().contains("ui-combobox"))
        {
            //dropdown has been substituted by jQuery-UI, so find the substituted controls instead
            final WebElement dropdownButton = dropdown.findElement(By.xpath("following-sibling::span/button"));
            dropdownButton.click();
            final List<WebElement> options = dropdownButton.findElements(By.xpath("following-sibling::ul/li"));
            options.get(options.size()/2).click();
        }
        else
        { //interact with raw dropdown
            Select regionalManagerSelection = new Select(dropdown);
            regionalManagerSelection.selectByIndex(regionalManagerSelection.getOptions().size()/2);
        }

    }

    /**
     * Navigate through the Miu details to change miu ownership, validate the changes has been performed and logged.
     * @param newSiteId The new Site ID.
     */
    private void changeMiuOwner(String newSiteId)
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 20);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(H1_CONTAINS_TEXT_MIU_DETAILS)));

        WebElement siteIdSelect = driver.findElement(By.id("newSiteId"));

        //get current selection, this should be the current site owning the miu
        final String oldSiteId = this.getSelectedSiteId();

        //select new site
        selectOptionByValue(siteIdSelect, newSiteId);
        driver.findElement(By.id("newSiteId")).submit();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(H1_CONTAINS_TEXT_MIU_DETAILS)));

        //check new changes has been committed
        final String newSiteIdFromHtml = this.getSelectedSiteId();

        //check first row in the audit log entry.
        assertEquals(UserLogin.ADMIN_USER, driver.findElement(By.xpath("//table[@id='table-audit-list']/tbody/tr[1]/td[2]")).getText());
        assertEquals("Set MIU ownership", driver.findElement(By.xpath("//table[@id='table-audit-list']/tbody/tr[1]/td[3]")).getText());

        String oldSiteIdFromAuditLog = driver.findElement(By.xpath("//table[@id='table-audit-list']/tbody/tr[1]/td[4]")).getText();
        String newSiteIdFromAuditLog = driver.findElement(By.xpath("//table[@id='table-audit-list']/tbody/tr[1]/td[5]")).getText();

        assertEquals(newSiteId, newSiteIdFromHtml);
        assertEquals(newSiteId, newSiteIdFromAuditLog);
        assertTrue(oldSiteId.contains(oldSiteIdFromAuditLog));

    }

    /**
     * Get currently selected option value. This is preferred to using Selenium selection.getFirstSelectedOption().getText();
     * as it is very slow if the options list is huge.
     * @return display string of the selected option
     */
    private String getSelectedSiteDisplayString()
    {
        return driver.findElement(By.xpath("//dt[.='Site:']/following-sibling::dd")).getText().trim();
    }

    private String getSelectedSiteId()
    {
        return driver.findElement(By.xpath("//dt[.='Site:']/following-sibling::dd")).getAttribute("site-id").trim();
    }

}
