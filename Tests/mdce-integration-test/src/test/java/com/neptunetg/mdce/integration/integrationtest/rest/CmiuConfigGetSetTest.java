/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

/**
 * Test Interface:
 * - IF16: Get Miu config,
 * - IF38: Set MIU config
 */
public class CmiuConfigGetSetTest extends BaseSetupRestTest
{
    private static final int testCmiu = 400000102;

    /**
     * "{\"miu_id\":" + testCmiuId + ",\"config\":
     * \"recording\":{\"start_time_mins\":0,\"interval_mins\":15},\"reporting\":
     * {\"start_time_mins\":0,\"interval_mins\":60,\"num_retries\":0,\"transmit_window_mins\":15},
     * \"quiet_time\":{\"quiet_start_mins\":0,\"quiet_end_mins\":0}}}"
     */
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private ObjectMapper mapper;

    public CmiuConfigGetSetTest()
    {
        super(testCmiu);
        mapper = new ObjectMapper();
    }

    @Test
    public void testGetMiuConfigByMiu() throws Exception
    {
        final int siteId = 16;
        final String token = fetchTokenForSiteId(siteId);
        ensureTestMiuExists(siteId, token);

        String responseString = getSingleMuiConfig(token);

        HashMap response = mapper.readValue(responseString, HashMap.class);

        assertEquals(testCmiu, response.get("miu_id"));

        assertEquals( testCmiu, (int)JsonPath.read(responseString, "$.miu_id"));
        assertEquals( 16, (int)JsonPath.read(responseString, "$.config.billing.site_id"));
        assertEquals( "Y", JsonPath.read(responseString, "$.config.billing.billable"));
        assertEquals( 10, (int)JsonPath.read(responseString, "$.config.recording.start_time_mins"));
        assertEquals(20, (int) JsonPath.read(responseString, "$.config.recording.interval_mins"));

        assertEquals(30, (int) JsonPath.read(responseString, "$.config.reporting.start_time_mins"));
        assertEquals(60, (int) JsonPath.read(responseString, "$.config.reporting.interval_mins"));
        assertEquals(3, (int) JsonPath.read(responseString, "$.config.reporting.num_retries"));
        assertEquals(56, (int) JsonPath.read(responseString, "$.config.reporting.transmit_window_mins"));

        assertEquals(12, (int) JsonPath.read(responseString, "$.config.quiet_time.quiet_start_mins"));
        assertEquals(34, (int) JsonPath.read(responseString, "$.config.quiet_time.quiet_end_mins"));
    }

    @Test
    public void testGetAllMiuConfig() throws Exception   //no MIU given, get all mius under the specific siteId
    {
        final int siteId = 16;
        final String token = fetchTokenForSiteId(siteId);
        ensureTestMiuExists(siteId, token);

        String responseString = getAllCmiuConfigs(token);
        ObjectMapper mapper = new ObjectMapper();
        HashMap response = mapper.readValue(responseString, HashMap.class);

        assertNotNull(response.get("mius"));

        //pass response through JSONPath and search for miu id matching the testCmiu id
        List<Object> filteredJson = JsonPath.read(responseString, "$.mius[?(@.miu_id==" + testCmiu + ")]");

        //assert testCmiu is present in the response string
        assertEquals(1, filteredJson.size());

        //extract sub path
        String filteredJsonString = JsonPath.read(responseString, "$.mius[?(@.miu_id==" + testCmiu + ")]").toString();

        assertEquals(testCmiu, (int) JsonPath.read(filteredJsonString, "$.[0]miu_id"));

        //verify the config associate to the testCmiu.
        assertEquals(siteId, (int) JsonPath.read(filteredJsonString, "$.[0].config.billing.site_id"));

        assertEquals(10, (int) JsonPath.read(filteredJsonString, "$.[0].config.recording.start_time_mins"));
        assertEquals(20, (int) JsonPath.read(filteredJsonString, "$.[0].config.recording.interval_mins"));

        assertEquals(30, (int) JsonPath.read(filteredJsonString, "$.[0].config.reporting.start_time_mins"));
        assertEquals(60, (int) JsonPath.read(filteredJsonString, "$.[0].config.reporting.interval_mins"));
        assertEquals(3, (int) JsonPath.read(filteredJsonString, "$.[0].config.reporting.num_retries"));
        assertEquals(56, (int) JsonPath.read(filteredJsonString, "$.[0].config.reporting.transmit_window_mins"));

        assertEquals(12, (int) JsonPath.read(filteredJsonString, "$.[0].config.quiet_time.quiet_start_mins"));
        assertEquals(34, (int) JsonPath.read(filteredJsonString, "$.[0].config.quiet_time.quiet_end_mins"));
    }

    @Test
    public void testGetAllMiuConfigForAllSites() throws Exception
    {
        final int siteId = 16;
        final String token16 = fetchTokenForSiteId(siteId);
        ensureTestMiuExists(siteId, token16);

        //create extra miu and assign to other sites.
        ensureMiuExistsForSite(1, 400000002);
        ensureMiuExistsForSite(2, 400000004);
        ensureMiuExistsForSite(16, 400000006);

        final String token0 = fetchTokenForSiteId(0);
        String miuConfigsForAllSitesResponseString = getAllCmiuConfigs(token0);

        //check we have config set for all sites
        assertMiuConfigIsPresent(miuConfigsForAllSitesResponseString, 400000002);
        assertMiuConfigIsPresent(miuConfigsForAllSitesResponseString, 400000004);
        assertMiuConfigIsPresent(miuConfigsForAllSitesResponseString, 400000006);

    }

    @Test
    public void testSetConfig() throws Exception
    {
        final int siteId = 16;
        final String token = fetchTokenForSiteId(siteId);
        ensureTestMiuExists(siteId, token);

        setTestMiuConfig(16, token);   //set to default config first

        String getConfigResponse = getSingleMuiConfig(token);

        final String requestBody = "{\"miu_id\":" + testCmiu + ",\"config\":{" +
                "\"recording\":" +
                "   {\"start_time_mins\":11,\"interval_mins\":22}," +
                "\"reporting\":" +
                "   {\"start_time_mins\":33,\"interval_mins\":60,\"num_retries\":55,\"transmit_window_mins\":66}," +
                "\"quiet_time\":" +
                    "{\"quiet_start_mins\":77,\"quiet_end_mins\":88}" +
                "}}";

        String setResponse = HttpUtility.httpJsonPut(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/" + testCmiu + "/config?token=" + token, requestBody);

        getConfigResponse = getSingleMuiConfig(token);

        assertEquals(testCmiu, (int)JsonPath.read(getConfigResponse, "$.miu_id"));
        assertEquals(11, (int)JsonPath.read(getConfigResponse, "$.config.recording.start_time_mins"));
        assertEquals(22, (int)JsonPath.read(getConfigResponse, "$.config.recording.interval_mins"));
        assertEquals(33, (int)JsonPath.read(getConfigResponse, "$.config.reporting.start_time_mins"));
        assertEquals(60, (int)JsonPath.read(getConfigResponse, "$.config.reporting.interval_mins"));
        assertEquals(55, (int)JsonPath.read(getConfigResponse, "$.config.reporting.num_retries"));
        assertEquals(66, (int)JsonPath.read(getConfigResponse, "$.config.reporting.transmit_window_mins"));
        assertEquals(77, (int)JsonPath.read(getConfigResponse, "$.config.quiet_time.quiet_start_mins"));
        assertEquals(88, (int)JsonPath.read(getConfigResponse, "$.config.quiet_time.quiet_end_mins"));

        //Give the command a short time to get through the system
        Thread.sleep(2500);

        CmiuSimulatorLauncher cmiuSim = new CmiuSimulatorLauncher();
        String output = cmiuSim.sendPackets(testCmiu, 1, 0, Constants.BROKER_SERVER, "detailedConfigPacket", 1);

        assertTrue(output.contains("Received other commands: command id = 10"));

    }

    @Test
    public void testGetConfigFailedNotPermitted() throws Exception
    {
        final int siteId = 16;
        final String token16 = fetchTokenForSiteId(siteId);
        ensureTestMiuExists(siteId, token16);

        setTestMiuConfig(16, token16);   //set to default config first

        String getConfigResponse = getSingleMuiConfig(token16);

        //assume identify of another site
        final String token2 = fetchTokenForSiteId(2);

        //expect it to fail
        this.expectedException.expectMessage(containsString("Response code: 403"));
        String getConfigResponseNotPermitted = getSingleMuiConfig(token2);
    }

    @Test
    public void testSetConfigFailedNotPermitted() throws Exception
    {
        final int siteId = 16;
        final String token16 = fetchTokenForSiteId(siteId);
        ensureTestMiuExists(siteId, token16);

        setTestMiuConfig(16, token16);   //set to default config first

        String getConfigResponse = getSingleMuiConfig(token16);

        //assume identify of another site
        final String token2 = fetchTokenForSiteId(2);

        //expect it to fail
        this.expectedException.expectMessage(containsString("Response code: 403"));
        setTestMiuConfig(16, token2);   //set to default config first
    }


    private void assertMiuConfigIsPresent(final String responseString, int miuId)
    {
        List<Object> foundMiu = JsonPath.read(responseString, "$..mius[?(@.miu_id==" + miuId + ")]");
        assertTrue(foundMiu.size()>0);
    }

}
