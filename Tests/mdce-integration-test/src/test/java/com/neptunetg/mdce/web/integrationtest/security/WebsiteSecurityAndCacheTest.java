/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.security;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.PartnerKeys;
import com.neptunetg.mdce.common.utility.HttpUtility;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertTrue;

/**
 * To check that certain webpages can be accessed and certain cannot
 */
public class WebsiteSecurityAndCacheTest
{
    private final static String INTEGRATION_INTERNAL_SERVICE_PAGE = Constants.MDCE_INTEGRATION_URL + "/service/internal/service-status";
    private final static String INTEGRATION_EXTERNAL_API_GET_TOKEN_URL = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/token";
    private final static String INTEGRATION_DEBUG_PAGES_URL = Constants.MDCE_INTEGRATION_URL + "/pages/debug";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    /**
     * https://<mdce-integration>/pages/debug should be blocked from HTTPS access.
     * This test should fail when running locally because it is access through http
     * @throws Exception
     */
    @Test
    public void testDebugPagesBlockedFromHTTPS() throws Exception
    {
        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 403"));
        this.expectedException.expectMessage(containsString("Forbidden"));

        final Map<String, List<String>> serviceResponseHeaderFields = HttpUtility.httpGetResponseHeader(INTEGRATION_DEBUG_PAGES_URL);
    }

    /**
     * Internal services should be blocked from external (in this case, junit) access when access through HTTPS.
     * This should fail when running locally because it is access through http
     */
    @Test
    public void testInternalServiceBlockedFromHTTPS() throws Exception
    {
        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 403"));
        this.expectedException.expectMessage(containsString("Forbidden"));

        final Map<String, List<String>> serviceResponseHeaderFields = HttpUtility.httpGetResponseHeader(INTEGRATION_INTERNAL_SERVICE_PAGE);
    }

    /**
     * External service HTTP control cache should be disabled.
     * @throws Exception
     */
    @Test
    public void testExternalServiceDisableControlCache() throws Exception
    {
        final int siteId = 0;
        final Map<String, List<String>> apiResponseHeaderFields =
                HttpUtility.httpGetResponseHeader(INTEGRATION_EXTERNAL_API_GET_TOKEN_URL +
                        "?partner=" + PartnerKeys.getPartnerKey(siteId) + "&site_id=" + siteId);

        assertCacheDisabled(apiResponseHeaderFields);
    }



    /**
     * Check response header to verify that cache control is disabled.
     * @param headerFields
     */
    private void assertCacheDisabled(Map<String, List<String>> headerFields)
    {
        assertTrue(headerFields.get("Cache-Control").stream().anyMatch(v -> v.equals("no-store")));
    }


}
