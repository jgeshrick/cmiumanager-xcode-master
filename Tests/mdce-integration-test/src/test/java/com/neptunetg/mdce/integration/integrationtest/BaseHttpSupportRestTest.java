/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import net.minidev.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

public abstract class BaseHttpSupportRestTest extends BaseAuthenticatingRestTest
{
    protected static final String SEND_COMMAND_PACKET = "0523050400000000FE110120010132040000000000000000000000FF";
    private static final String EMPTY_MIU_ID_LIST = "{\"miu_id\":1}";
    protected static final String urlCmiuConfigFpc = Constants.FPC_URL + "/api/v1/miu/";
    protected static final String urlCmiuConfigIntegration = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/";
    private static final String urlCmiuOwnership = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/ownership";
    private static final String urlV1MiuConfigHistory = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/config_history";
    private static final String urlV2MiuConfigHistory = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v2/miu/config_history";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String urlAllCmiuConfig = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/config?token=";

    private int testCmiuId;

    protected BaseHttpSupportRestTest(int testCmiuId)
    {
        this.testCmiuId = testCmiuId;
    }

    protected String sendCommand(String token) throws Exception
    {
        final String requestBody = "{ \t\"miu_id\": \"" + testCmiuId + "\", \"source_device_type\": \"N_SIGHT\", \"target_device_type\": \"CMIU\", \t\t\"packet_type\": 4, \t\"packet\": \"" + SEND_COMMAND_PACKET + "\" } ";

        return HttpUtility.httpPost(urlCmiuConfigIntegration + testCmiuId + "/command?token=" + token, requestBody);
    }

    protected String getCommandHistory(String token) throws Exception
    {
        //get current command history first
        String getCommandHistoryJsonResponse = HttpUtility.httpGet(urlCmiuConfigIntegration + testCmiuId + "/command-history" +
                        "?token=" + token +
                        "&min_insert_date=2015-01-01" +
                        "&max_insert_date=2016-01-02"
        );

        return getCommandHistoryJsonResponse;
    }

    protected JSONArray getJsonArrayOfPacketsOrNull(String commandHistoryJsonResponse)
    {
        if (commandHistoryJsonResponse.equals(EMPTY_MIU_ID_LIST))
        {
            return new JSONArray();

        }
        return JsonPath.read(commandHistoryJsonResponse, "$.packets");
    }

    protected String getPackets(String minInsertDate, boolean includeGatewayPackets, String token) throws Exception
    {
        return HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/packet?token=" + token
                + "&min_insert_date=" + URLEncoder.encode(minInsertDate, "UTF-8")
                + "&include_gateway_packets=" + includeGatewayPackets);
    }

    protected String getMiuList(String token) throws Exception
    {
        //get current command history first
        return HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu_list" +
                "?token=" + token);

    }

    //use with care - uses inner join query
    protected String getMiu(String token) throws Exception
    {
        //get current command history first
        return HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/config" +
                        "?token=" + token
        );
    }

    protected String getSingleMuiConfig(String token) throws Exception
    {
        return getSingleMuiConfig(testCmiuId, token);
    }

    protected String getSingleMuiConfig(int miuId, String token) throws Exception
    {
        return HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/" + miuId + "/config?token=" + token);
    }

    protected String getAllCmiuConfigs(String token) throws Exception
    {
        return HttpUtility.httpGet(urlAllCmiuConfig + token);
    }

    protected String getConfigHistory(Boolean billable, String token) throws Exception
    {
        final String yesterday = getSimpleFormat(getMinus1Day());
        final String tomorrowDate = getSimpleFormat(getPlus1Day());

        return HttpUtility.httpGet(urlV1MiuConfigHistory +
                "?token=" + token +
                (billable != null ? "&billable=" + (billable ? "Y" : "N") : "") +
                "&start_date=" + yesterday +
                "&end_date=" + tomorrowDate);
    }

    protected String getV2ConfigHistory(String token) throws Exception
    {
        return getV2ConfigHistory(null, token);
    }

    protected String getV2ConfigHistory(Integer lastSequenceId, String token) throws Exception
    {
        return HttpUtility.httpGet(urlV2MiuConfigHistory +
                "?token=" + token +
                (lastSequenceId != null ? "&last_sequence=" + lastSequenceId : ""));
    }

    protected String getConfigHistoryForMiuId(long miuId, String token) throws Exception
    {
        final String oneMonthAgo = getSimpleFormat(getMinus1Month());
        final String aMonthsTime = getSimpleFormat(getPlus1Month());

        return HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/" + miuId + "/config_history" +
                "?token=" + token +
                "&start_date=" + oneMonthAgo +
                "&end_date=" + aMonthsTime);
    }

    private String getSimpleFormat(ZonedDateTime plus1Day)
    {
        return DateTimeFormatter.ISO_LOCAL_DATE.format(plus1Day);
    }

    private ZonedDateTime getMinus1Day()
    {
        return getNow().minus(1, ChronoUnit.DAYS);
    }

    private ZonedDateTime getMinus1Month()
    {
        return getNow().minus(1, ChronoUnit.MONTHS);
    }

    private ZonedDateTime getPlus1Month()
    {
        return getNow().plus(1, ChronoUnit.MONTHS);
    }

    private ZonedDateTime getPlus1Day()
    {
        return getNow().plus(1, ChronoUnit.DAYS);
    }

    private ZonedDateTime getNow()
    {
        return ZonedDateTime.now();
    }

    protected String createTestMiu(String tokenForFpc) throws Exception
    {
        final String jsonRequestBody = "{\"miu_id\":" + testCmiuId + ", "
                + "\"modem_config\": {\"vendor\":\"Telit\", \"firmware_revision\": \"0.0.0.0\"}"
                + "}";

        String urlPutPath = urlCmiuConfigFpc + testCmiuId + "/cellular-config?token=" + tokenForFpc + "&doRegisterWithMno=false";
        return HttpUtility.httpJsonPost(urlPutPath, jsonRequestBody);
    }

    protected String createTestMiu(int miuId, String tokenForFpc) throws Exception
    {
        final String jsonRequestBody = "{\"miu_id\":" + miuId + ", "
                + "\"modem_config\": {\"vendor\":\"Telit\", \"firmware_revision\": \"0.0.0.0\"}"
                + "}";

        String urlPutPath = urlCmiuConfigFpc + miuId + "/cellular-config?token=" + tokenForFpc + "&doRegisterWithMno=false";
        return HttpUtility.httpJsonPost(urlPutPath, jsonRequestBody);
    }

    protected void setTestMiuConfig(int siteId, String token) throws Exception
    {
        final String requestBody = "{\"miu_id\":" + testCmiuId + ",\"config\":{\"recording\":{\"start_time_mins\":10,\"interval_mins\":20},\"reporting\":{\"start_time_mins\":30,\"interval_mins\":60,\"num_retries\":3,\"transmit_window_mins\":56},\"quiet_time\":{\"quiet_start_mins\":12,\"quiet_end_mins\":34}}}";

        HttpUtility.httpJsonPut(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/" + testCmiuId + "/config?token=" + token, requestBody);

        //read back and test
        String getConfigResponse = getSingleMuiConfig(token);

        assertEquals(testCmiuId, (int) JsonPath.read(getConfigResponse, "$.miu_id"));
        assertEquals(10, (int) JsonPath.read(getConfigResponse, "$.config.recording.start_time_mins"));
        assertEquals(20, (int) JsonPath.read(getConfigResponse, "$.config.recording.interval_mins"));
        assertEquals(30, (int) JsonPath.read(getConfigResponse, "$.config.reporting.start_time_mins"));
        assertEquals(60, (int) JsonPath.read(getConfigResponse, "$.config.reporting.interval_mins"));
        assertEquals(3, (int) JsonPath.read(getConfigResponse, "$.config.reporting.num_retries"));
        assertEquals(56, (int) JsonPath.read(getConfigResponse, "$.config.reporting.transmit_window_mins"));
        assertEquals(12, (int) JsonPath.read(getConfigResponse, "$.config.quiet_time.quiet_start_mins"));
        assertEquals(34, (int) JsonPath.read(getConfigResponse, "$.config.quiet_time.quiet_end_mins"));
    }

    protected void setMiuConfig(int siteId, int miuId, String token) throws Exception
    {
        final String requestBody = "{\"miu_id\":" + miuId + ",\"config\":{\"recording\":{\"start_time_mins\":10,\"interval_mins\":20},\"reporting\":{\"start_time_mins\":30,\"interval_mins\":60,\"num_retries\":3,\"transmit_window_mins\":56},\"quiet_time\":{\"quiet_start_mins\":12,\"quiet_end_mins\":34}}}";

        HttpUtility.httpJsonPut(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/" + miuId + "/config?token=" + token, requestBody);

        //read back and test
        String getConfigResponse = getSingleMuiConfig(miuId, token);

        assertEquals(miuId, (int) JsonPath.read(getConfigResponse, "$.miu_id"));
        assertEquals(10, (int) JsonPath.read(getConfigResponse, "$.config.recording.start_time_mins"));
        assertEquals(20, (int) JsonPath.read(getConfigResponse, "$.config.recording.interval_mins"));
        assertEquals(30, (int) JsonPath.read(getConfigResponse, "$.config.reporting.start_time_mins"));
        assertEquals(60, (int) JsonPath.read(getConfigResponse, "$.config.reporting.interval_mins"));
        assertEquals(3, (int) JsonPath.read(getConfigResponse, "$.config.reporting.num_retries"));
        assertEquals(56, (int) JsonPath.read(getConfigResponse, "$.config.reporting.transmit_window_mins"));
        assertEquals(12, (int) JsonPath.read(getConfigResponse, "$.config.quiet_time.quiet_start_mins"));
        assertEquals(34, (int) JsonPath.read(getConfigResponse, "$.config.quiet_time.quiet_end_mins"));
    }


    protected void setTestMiuConfigChange(int siteId, String token) throws Exception
    {
        final String requestBody = "{\"miu_id\":" + testCmiuId + ",\"config\":\"recording\":{\"start_time_mins\":10,\"interval_mins\":20},\"reporting\":{\"start_time_mins\":30,\"interval_mins\":60,\"num_retries\":3,\"transmit_window_mins\":56},\"quiet_time\":{\"quiet_start_mins\":12,\"quiet_end_mins\":34}}}";

        HttpUtility.httpJsonPut(Constants.MDCE_INTEGRATION_URL + "\"internal\"miu-config\"" + testCmiuId + "/config?token=" + token, requestBody);

    }

    protected String getPacketsIncludingGateways(String minInsertDate, String token) throws Exception
    {
        String url = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/packet?token=" + token + "&min_insert_date=" + minInsertDate
                + "&include_gateway_packets=true";

        logger.info("Reading packet data from " + url);

        return HttpUtility.httpGet(url);
    }

    protected String assignMiuToSiteId(int miuId, int newSiteId, String token) throws Exception
    {
        return assignMiusToSiteId(Arrays.asList(Integer.valueOf(miuId)), newSiteId, token);
    }


    protected String assignMiusToSiteId(Collection<Integer> miuIds, int newSiteId, String token) throws Exception
    {
        final StringBuilder jsonRequestBodyBuilder = new StringBuilder()
                .append("{\"site_id\":").append(newSiteId)
                .append(",\"mius\":[");

        boolean first = true;
        for (Integer miuId : miuIds)
        {
            if (!first)
            {
                jsonRequestBodyBuilder.append(',');
            }
            jsonRequestBodyBuilder
                    .append("{\"miu_id\":").append(miuId)
                    .append(", \"active\":\"Y\"}");
            first = false;
        }
        jsonRequestBodyBuilder.append("]}");

        String urlPutPath = urlCmiuOwnership + "?token=" + token;
        return HttpUtility.httpJsonPut(urlPutPath, jsonRequestBodyBuilder.toString());

    }

    protected int getTestCmiuId()
    {
        return testCmiuId;
    }
}
