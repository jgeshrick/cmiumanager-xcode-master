/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.framework;

import org.junit.Assert;

/**
 * Provides partner keys for tests
 */
public class PartnerKeys
{
    public static String getPartnerKey(int siteId)
    {
        switch (siteId)
        {
            case -3: return "1b7611151461dcee4de54a60e8bdb4a8";
            case 0: return "c2a31f64e9430f1c8b85ccbbb776ad75";
            case 1: return "7266688fa739b2b3be92961975b095a7";
            case 2: return "647e4b7ec00f23c5bd1d773c3df74bbb";
            case 16: return "62efb5774a31e461911e0912584babdf";
            case 32: return "899dd496a2504aa6ebf84ecc16857493";
            case 64: return "5c9ad98b1d47989177cc8a6738c8ea7d";
            case 141: return "e96aa29de811a75c22ea2db049ee7f4f";
            case 11245: return "61bd45bc68288bef44902ab9aff223e0";
            case 59111: return "ca26e77580941f8964c96dcd475c6215";
            default:
                Assert.fail("Don't have partner key for site ID " + siteId + " - please obtain and add to PartnerKeys.java");
                return null;
        }


    }

    public static String getPartnerKeyForBpc()
    {
        return "d7a300692860293924ec82f849f25691";
    }

    public static String getPartnerKeyForFpc()
    {
        return "eb750ee73b5cf9d04ec78a0b23616e9b";
    }


}
