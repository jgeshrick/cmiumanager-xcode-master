/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.user;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.internal.user.model.MdceUserRole;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;
import java.util.List;

import static com.neptunetg.mdce.web.integrationtest.UserLogin.loginAsAdmin;
import static com.neptunetg.mdce.web.integrationtest.UserLogin.logoutUser;
import static junit.framework.Assert.assertTrue;

/**
 * Using selenium to test user management - list, login, logout, create user etc.
 * Using default user/password: admin/admin
 */
public class UserManagementTest extends MdceWebSeleniumTestBase
{
    private final String cmiuUserLoginUrl = Constants.MDCE_WEB_URL + "/pages/user/login";
    private final String cmiuUserListUrl = Constants.MDCE_WEB_URL + "/pages/user/list";

    @Test
    public void testUserManagement()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        final String temporarySiteOperatorUserName = "siteoperator-" + Instant.now().getEpochSecond();
        final String temporaryMsocAdminUserName = "msocadmin-" + Instant.now().getEpochSecond();
        final String temporaryMsocOperatorUserName = "msocoperator-" + Instant.now().getEpochSecond();

        loginAsAdmin(this);

//        addSiteOperator(webDriverWait, temporarySiteOperatorUserName);
        listAndAddUser(webDriverWait, temporarySiteOperatorUserName, "Site operator");
        listAndAddUser(webDriverWait, temporaryMsocAdminUserName, "Msoc administrator");
        listAndAddUser(webDriverWait, temporaryMsocOperatorUserName, "Msoc operator");

        deleteUser(webDriverWait, temporarySiteOperatorUserName);
        deleteUser(webDriverWait, temporaryMsocAdminUserName);
        deleteUser(webDriverWait, temporaryMsocOperatorUserName);

        logoutUser(this);
    }

    @Test
    public void testChangeUserDetails()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        final String distributor1UserName = "distributor1-" + Instant.now().getEpochSecond();
        final String distributor2UserName = "distributor2-" + Instant.now().getEpochSecond();
        final String distributor2Email = distributor2UserName + "@server.com";

        loginAsAdmin(this);

        // Add user, change username, change level, change email.
        listAndAddUser(webDriverWait, distributor1UserName, "Distributor");
        changeUserName(webDriverWait, distributor1UserName, distributor2UserName);
        changeUserLevel(webDriverWait, distributor2UserName, MdceUserRole.SiteOperator);
        changeEmail(webDriverWait, distributor2UserName, distributor2Email);

        // Tidy up
        deleteUser(webDriverWait, distributor2UserName);

        logoutUser(this);
    }

    private void listAndAddUser(WebDriverWait webDriverWait, String newUserName, String userLevel)
    {
        final String userPassword = "password";

        driver.get(cmiuUserListUrl);

        takeScreenshot("User list - 01");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'User List')]")));

        //click Add User
        driver.findElement(By.id("user-dynamic-list-add-new")).click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Add User')]")));

        driver.findElementById("userName").sendKeys(newUserName);
        Select userLevelSelection = new Select(driver.findElement(By.id("userLevel")));
        userLevelSelection.selectByVisibleText(userLevel);

        driver.findElementById("password").sendKeys(userPassword);
        driver.findElementById("passwordRepeated").sendKeys(userPassword);
        driver.findElementById("email").sendKeys(newUserName + "@server.com");

        driver.findElement(By.id("add-user")).click();

        //back to User List page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'User List')]")));

        //verify audit log
        List<AuditLogEntry> auditLogEntries = getRecentAuditLogEntries(startTime, null);
        assertTrue(auditLogEntries.stream().anyMatch(log -> log.getEventType().contains("User created") &&
                log.getNewValue().contains(newUserName)));
    }

    private void changeUserLevel(WebDriverWait webDriverWait, String userName, MdceUserRole newLevel)
    {
        //at User List, delete the user
        driver.get(cmiuUserListUrl);
        final String userDetailsXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + userName + "')]" +
                "/..//a[contains(@href, 'details')]";

        final String newUserLevelXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + userName + "')]" +
                "/../td[contains(@class, 'user-list-userlevel') and contains(text(), '" + newLevel.name() + "')]";

        final String saveUserDetailsButtonXpath = "//input[@type='submit' and @name='saveUserDetails']";

        final String userLevelSelectXpath = "//form[@id='changeUserDetailsForm']//select[@name='userLevel']";

        String adminUserXpath = "//tr[contains(@class, 'list-item')]/td[contains(@class, 'user-list-username') and contains(text(), 'admin')]";

        driver.findElementByXPath(userDetailsXpath).click();

        // wait for 'Save user details' button
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(saveUserDetailsButtonXpath)));

        // Change user name
        Select dropdown = new Select(driver.findElementByXPath(userLevelSelectXpath));
        dropdown.selectByValue(newLevel.name());
        driver.findElementByXPath(saveUserDetailsButtonXpath).click();

        // Verify that the admin user is still present and that the user level has been changed.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(adminUserXpath)));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(newUserLevelXpath)));
    }

    private void changeEmail(WebDriverWait webDriverWait, String userName, String newEmail)
    {
        //at User List, delete the user
        driver.get(cmiuUserListUrl);
        final String userDetailsXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + userName + "')]" +
                "/..//a[contains(@href, 'details')]";

        final String newEmailXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + userName + "')]" +
                "/../td[contains(@class, 'user-list-email') and contains(text(), '" + newEmail + "')]";

        final String saveUserDetailsButtonXpath = "//input[@type='submit' and @name='saveUserDetails']";

        final String emailInputXpath = "//form[@id='changeUserDetailsForm']/..//input[@type='text' and @name='email']";

        String adminUserXpath = "//tr[contains(@class, 'list-item')]/td[contains(@class, 'user-list-username') and contains(text(), 'admin')]";

        driver.findElementByXPath(userDetailsXpath).click();

        // wait for 'Save user details' button
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(saveUserDetailsButtonXpath)));

        // Change user name
        driver.findElementByXPath(emailInputXpath).clear();
        driver.findElementByXPath(emailInputXpath).sendKeys(newEmail);
        driver.findElementByXPath(saveUserDetailsButtonXpath).click();

        // Verify that the admin user is still present and that the email address has been changed.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(adminUserXpath)));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(newEmailXpath)));
    }

    private void changeUserName(WebDriverWait webDriverWait, String oldUserName, String newUserName)
    {
        //at User List, delete the user
        driver.get(cmiuUserListUrl);
        final String oldDetailsXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + oldUserName + "')]" +
                "/..//a[contains(@href, 'details')]";

        final String newDetailsXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + newUserName + "')]" +
                "/..//a[contains(@href, 'details')]";

        final String saveUserDetailsButtonXpath = "//input[@type='submit' and @name='saveUserDetails']";

        final String userNameInputXpath = "//form[@id='changeUserDetailsForm']//input[@type='text' and @name='userName']";

        String adminUserXpath = "//tr[contains(@class, 'list-item')]/td[contains(@class, 'user-list-username') and contains(text(), 'admin')]";

        driver.findElementByXPath(oldDetailsXpath).click();

        // wait for 'Save user details' button
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(saveUserDetailsButtonXpath)));

        // Change user name
        driver.findElementByXPath(userNameInputXpath).clear();
        driver.findElementByXPath(userNameInputXpath).sendKeys(newUserName);
        driver.findElementByXPath(saveUserDetailsButtonXpath).click();

        // Verify that the admin user is still present and that the user has been renamed.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(adminUserXpath)));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(newDetailsXpath)));
    }
}
