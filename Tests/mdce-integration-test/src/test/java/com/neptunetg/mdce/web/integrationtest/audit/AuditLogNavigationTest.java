/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.audit;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

/**
 * Audit log page navigation to ensure it can talk to back-end without throwing exception
 */
public class AuditLogNavigationTest extends MdceWebSeleniumTestBase
{
    private static final String ADD_NEW_CONFIG_SET_URL = Constants.MDCE_WEB_URL + "/pages/config-set/add";
    private int testCmiuId;

    @Before
    @Override
    public void setUp()
    {
        super.setUp();

        testCmiuId = Integer.parseInt(cmiuIdForFotaCommandInjection);
    }

    @Test
    public void navigationTest()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        driver.get(cmiuAuditLogUrl);

        assertThat(driver.getTitle(), containsString("Audit Log"));

        //perform filtering
        driver.findElement(By.id("miuId")).sendKeys("400000102");
        driver.findElement(By.id("miuId")).submit();

        //wait until the entire page is loaded, this could take some time if the log entry is large
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='audit-log-end']")));

        //get a list of elements from table column #3 "miu" which are NOT of id 400000102
        List<WebElement> nonCmiuIdsElements = driver.findElementsByXPath("//table[@id='table-audit-list']/tbody/tr/td[3][normalize-space(string()) != '400000102']");

        assertEquals(0, nonCmiuIdsElements.size());

    }

    /**
     * Change a cmiu config through web, verify that the change has been log in to the Audit log page.
     */
    @Test
    public void testCmiuConfigChangeAuditLog()
    {
        this.sendGenericCmiuCommands("Recording/Reporting interval", "Set Recording/Reporting Intervals");

        List<AuditLogEntry> auditLogEntries = getRecentAuditLogEntries(startTime, testCmiuId);

        //verify cmiu filter only show testCmiuId
        assertTrue(auditLogEntries.stream().allMatch(log -> log.getMiu() == testCmiuId));

        assertTrue(auditLogEntries.stream().anyMatch(log -> log.getEventType().contains("Send command")));
    }


    /**
     * Verify config set added is logged.
     */
    @Test
    public void testNewAndChangedConfigSetAuditLog()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        System.out.println("Loading page: " + ADD_NEW_CONFIG_SET_URL);
        driver.get(ADD_NEW_CONFIG_SET_URL);

        takeScreenshot("Add new config set");

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Define new config set')]")));
        addAndEditConfigSet(webDriverWait);

        List<AuditLogEntry> auditLogEntries = getRecentAuditLogEntries(startTime, null);
        assertTrue(auditLogEntries.stream().anyMatch(log -> log.getEventType().contains("Config set change")));
    }




}
