/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.commandcontrol;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * End to end test for sending a miu command to Cmiu Simulator, and waiting for an ACK.
 */
public class CmiuCommandEndToEndTest extends MdceWebSeleniumTestBase
{
    private static final String XPATH_CMIU_COMMAND_IN_PROGRESS = "//div[contains(@class, 'commands-in-progress')]" +
            "/div[contains(@class, 'progress-content-group')]" +
            "//ul[contains(@class, 'group-row')]/li[contains(@class, 'list-content-cmiu group-cell')]//a";

    private static final String XPATH_CMIU_COMMAND_ACCEPTED = "//div[contains(@class, 'commands-received')]" +
            "/div[contains(@class, 'progress-content-group')]" +
            "//ul[contains(@class, 'group-row')]/li[contains(@class, 'list-content-cmiu group-cell')]//a";

    private static final String XPATH_CMIU_COMMAND_REJECTED = "//div[contains(@class, 'commands-rejected')]" +
            "/div[contains(@class, 'progress-content-group')]" +
            "//ul[contains(@class, 'group-row')]/li[contains(@class, 'list-content-cmiu group-cell')]//a";

    private static final String XPATH_COMMAND_IN_PROGRESS_SUBMITTED_DATES = "//div[contains(@class, 'commands-in-progress')]" +
            "/div[contains(@class, 'progress-content-group')]" +
            "//ul[contains(@class, 'group-row')]/li[contains(@class, 'list-content-submitted group-cell')]//span";

    private static final String XPATH_COMMAND_IN_PROGRESS_NETWORK = "//div[contains(@class, 'commands-in-progress')]" +
            "/div[contains(@class, 'progress-content-group')]" +
            "//ul[contains(@class, 'group-row')]/li[contains(@class, 'list-content-sim-mno group-cell')]//span";

    @Test
    public void testSendImageUpdateCommandEndToEnd() throws IOException, InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        //flush command for the CMIU first
        flushCommand(webDriverWait, cmiuIdForImageUpdateCommandInjection);

        //flush any pending messages on the broker
        Process cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForImageUpdateCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 1, 15, false, false);
        cmiuSimulatorProcess.waitFor();

        //xpath for getting the number of rows of CMIU in progress
        final String xpathTargetCmiuInProgress = generateXpathForCmiu("commands-in-progress", cmiuIdForImageUpdateCommandInjection);

        driver.get(cmiuConfigUrl);
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        takeScreenshot("Command and Control - before");

        // 1. Load config page, get the initial number of cmiu commands that are still queueing
        int numberOfOriginalTargetCmiuQueuedCommands = driver.findElementsByXPath(xpathTargetCmiuInProgress).size();

        // 2. Navigate to send commands
        driver.get(cmiuImageUpdate);

        takeScreenshot("Update image");

        //3. Register new image
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Update Image')]")));

        driver.findElement(By.id("register-new-image")).click();
        takeScreenshot("Register new image");
        importNewImage();

        //4. Wait to be directed back to Command and Control Interface page, send command page, select image to update. Try checking all checkboxes to verify they are all present.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Update Image')]")));

        driver.findElement(By.id("updateFirmware1")).click();
        Select selection = new Select(driver.findElement(By.id("firmwareImageVersion")));
        selection.selectByIndex(1);

        String selectedApplicationVersion = selection.getFirstSelectedOption().getText();

        //check and then uncheck to ensure these checkboxes are implemented
        driver.findElement(By.id("updateConfig1")).click();
        driver.findElement(By.id("updateARBConfig1")).click();
        driver.findElement(By.id("updateBleConfig1")).click();
        driver.findElement(By.id("updateEncryption1")).click();

        driver.findElement(By.id("updateConfig1")).click();
        driver.findElement(By.id("updateARBConfig1")).click();
        driver.findElement(By.id("updateBleConfig1")).click();
        driver.findElement(By.id("updateEncryption1")).click();

        //input CMIU
        driver.findElement(By.id("miuInputList0.miuId")).sendKeys(cmiuIdForImageUpdateCommandInjection);

        //submit form
        driver.findElementByName("updateCmiuImage").click();

        //5. Wait to be redirect back to Command and Control Interface page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Command & Control Interface')]")));
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        String url = driver.getCurrentUrl();
        takeScreenshot("Command and Control - queued");

        assertTrue(url.contains("pages/config"));

        //verify we can see the CMIU entry in the "Commands In Progress" section
        List<WebElement> discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_IN_PROGRESS);
        Optional<WebElement> result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForImageUpdateCommandInjection)).findAny();
        assertTrue("Expected result not present: " + XPATH_CMIU_COMMAND_IN_PROGRESS + " containing " + cmiuIdForImageUpdateCommandInjection, result.isPresent());

        //queued list for the cmiu has incremented
        assertEquals(numberOfOriginalTargetCmiuQueuedCommands + 1, driver.findElementsByXPath(xpathTargetCmiuInProgress).size());

        int oldCompletedCommands = getRecentlyCompletedCommands(cmiuIdForImageUpdateCommandInjection);

        //Start the CMIU simulator for the test
        cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForImageUpdateCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 1, 1, false, false);

        Instant timeOut = Instant.now().plus(1, ChronoUnit.MINUTES);

        do
        {
            driver.navigate().refresh();
            awaitAllCommandAndControlSectionsLoaded(webDriverWait);

            //verify we can see the CMIU entry in "Image Update Commands Received" section
            discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_ACCEPTED);
            result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForImageUpdateCommandInjection)).findAny();
        } while(!result.isPresent() && timeOut.isAfter(Instant.now()));

        // Make sure the commands-received section in in view
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        scrollToElement("//div[contains(@class, 'commands-rejected')]");
        scrollToElement("//div[contains(@class, 'commands-received')]");

        assertTrue("Expected result not present: " + XPATH_CMIU_COMMAND_ACCEPTED + " containing " + cmiuIdForImageUpdateCommandInjection, result.isPresent());

        int newCompletedCommands;

        do
        {
            //verify Recently completed commands
            newCompletedCommands = getRecentlyCompletedCommands(cmiuIdForImageUpdateCommandInjection);

        } while(((oldCompletedCommands >= newCompletedCommands) ||
            numberOfOriginalTargetCmiuQueuedCommands != driver.findElementsByXPath(xpathTargetCmiuInProgress).size()) &&
            cmiuSimulatorProcess.isAlive());

        // Make sure the commands-recently-completed section in in view
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        scrollToElement("//div[contains(@class, 'commands-rejected')]");
        scrollToElement("//div[contains(@class, 'commands-recently-completed')]");

        takeScreenshot("Command and Control - processed");

        //recently completed list for the cmiu increment by 1
        assertThat("Expected completed command count to have increased", oldCompletedCommands, Matchers.lessThan(newCompletedCommands));

        //command in process list for the cmiu is back to original
        assertThat("Expected decrease in in-progress command count", numberOfOriginalTargetCmiuQueuedCommands,
                Matchers.greaterThanOrEqualTo(driver.findElementsByXPath(xpathTargetCmiuInProgress).size()));

        cmiuSimulatorProcess.destroyForcibly();
    }

    @Test
    public void testSendModemFotaCommandEndToEnd() throws IOException, InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        //flush command for the CMIU first
        flushCommand(webDriverWait, cmiuIdForFotaCommandInjection);

        //flush any pending messages on the broker
        Process cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForFotaCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 1, 15, false, false);
        cmiuSimulatorProcess.waitFor();

        //Start the CMIU simulator for the test
        cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForFotaCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 15, 15, false, false);

        //xpath for getting the number of rows of CMIU in progress
        final String xpathTargetCmiuInProgress = generateXpathForCmiu("commands-in-progress", cmiuIdForFotaCommandInjection);

        driver.get(cmiuConfigUrl);
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        takeScreenshot("Command and Control - before");

        // 1. Load config page, get the initial number of cmiu commands that are still queueing
        int numberOfOriginalTargetCmiuQueuedCommands = driver.findElementsByXPath(xpathTargetCmiuInProgress).size();

        // 2. Navigate to send commands
        driver.get(cmiuFotaUpdate);

        takeScreenshot("CMIU Modem FOTA");

        //Image selection page loaded.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), " +
                "'Update modem firmware with FOTA')]")));

        Select selection = new Select(driver.findElement(By.id("imageName")));
        selection.selectByIndex(0);

        // input expected version
        driver.findElement(By.id("versionNumber")).sendKeys("v12.34.567");

        //input CMIU
        driver.findElement(By.id("miuInputList0.miuId")).sendKeys(cmiuIdForFotaCommandInjection);

        //submit form
        driver.findElementByName("modemFotaSubmit").click();

        //5. Wait to be redirect back to Command and Control Interface page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Command & Control Interface')]")));
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        String url = driver.getCurrentUrl();
        takeScreenshot("Command and Control - queued");

        assertTrue(url.contains("pages/config"));

        //verify we can see the CMIU entry in "Command In Process" section
        List<WebElement> discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_IN_PROGRESS);
        Optional<WebElement> result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForFotaCommandInjection)).findAny();
        assertTrue("Expected result not present: " + XPATH_CMIU_COMMAND_IN_PROGRESS + " containing " + cmiuIdForFotaCommandInjection, result.isPresent());

        //queued list for the cmiu has incremented
        assertEquals(numberOfOriginalTargetCmiuQueuedCommands + 1, driver.findElementsByXPath(xpathTargetCmiuInProgress).size());

        //launch and  wait for CMIU simulator to receive the command and action
        int oldCompletedCommands = getRecentlyCompletedCommands(cmiuIdForFotaCommandInjection);

        Instant timeOut = Instant.now().plus(1, ChronoUnit.MINUTES);

        do
        {
            driver.navigate().refresh();
            awaitAllCommandAndControlSectionsLoaded(webDriverWait);

            //verify we can see the CMIU entry in "Image Update Commands Received" section
            discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_ACCEPTED);
            result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForFotaCommandInjection)).findAny();
        } while(!result.isPresent() && timeOut.isAfter(Instant.now()));

        // Make sure the commands-received section in in view
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        scrollToElement("//div[contains(@class, 'commands-rejected')]");
        scrollToElement("//div[contains(@class, 'commands-received')]");

        assertTrue("Expected result not present: " + XPATH_CMIU_COMMAND_ACCEPTED + " containing " + cmiuIdForFotaCommandInjection, result.isPresent());

        int newCompletedCommands;

        do
        {
            //verify Recently completed commands
            newCompletedCommands = getRecentlyCompletedCommands(cmiuIdForFotaCommandInjection);

        } while(((oldCompletedCommands >= newCompletedCommands) ||
                numberOfOriginalTargetCmiuQueuedCommands != driver.findElementsByXPath(xpathTargetCmiuInProgress).size()) &&
                cmiuSimulatorProcess.isAlive());

        takeScreenshot("Command and Control - processed");

        //recently completed list for the cmiu increment by 1
        assertThat("Expected increase in completed commands count", oldCompletedCommands, Matchers.lessThan(newCompletedCommands));

        //command in process list for the cmiu is back to original
        assertThat("Expected decrease in in-progress command count", numberOfOriginalTargetCmiuQueuedCommands,
                Matchers.greaterThanOrEqualTo(driver.findElementsByXPath(xpathTargetCmiuInProgress).size()));

        cmiuSimulatorProcess.destroyForcibly();
    }

    @Test
    public void testSendModemFotaCommandEndToEndCommandNack() throws IOException, InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        //flush command for the CMIU first
        flushCommand(webDriverWait, cmiuIdForFotaCommandInjection);

        //flush any pending messages on the broker
        Process cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForFotaCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 1, 15, false, false);
        cmiuSimulatorProcess.waitFor();

        //Start the CMIU simulator for the test
        cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForFotaCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 15, 15, true, false);

        //xpath for getting the number of rows of CMIU in progress
        final String xpathTargetCmiuInProgress = generateXpathForCmiu("commands-in-progress", cmiuIdForFotaCommandInjection);

        driver.get(cmiuConfigUrl);
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        takeScreenshot("Command and Control - before");

        // 1. Load config page, get the initial number of cmiu commands that are still queueing
        int numberOfOriginalTargetCmiuQueuedCommands = driver.findElementsByXPath(xpathTargetCmiuInProgress).size();

        // 2. Navigate to send commands
        driver.get(cmiuFotaUpdate);

        takeScreenshot("CMIU Modem FOTA");

        //Image selection page loaded.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), " +
                "'Update modem firmware with FOTA')]")));

        Select selection = new Select(driver.findElement(By.id("imageName")));
        selection.selectByIndex(0);

        // input expected version
        driver.findElement(By.id("versionNumber")).sendKeys("v12.34.567");

        //input CMIU
        driver.findElement(By.id("miuInputList0.miuId")).sendKeys(cmiuIdForFotaCommandInjection);

        //submit form
        driver.findElementByName("modemFotaSubmit").click();

        //5. Wait to be redirect back to Command and Control Interface page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Command & Control Interface')]")));
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        String url = driver.getCurrentUrl();
        takeScreenshot("Command and Control - queued");

        assertTrue(url.contains("pages/config"));

        //verify we can see the CMIU entry in "Command In Process" section
        List<WebElement> discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_IN_PROGRESS);
        Optional<WebElement> result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForFotaCommandInjection)).findAny();
        assertTrue(result.isPresent());

        //queued list for the cmiu has incremented
        assertEquals(numberOfOriginalTargetCmiuQueuedCommands + 1, driver.findElementsByXPath(xpathTargetCmiuInProgress).size());

        Instant timeOut = Instant.now().plus(1, ChronoUnit.MINUTES);

        do
        {
            driver.navigate().refresh();
            awaitAllCommandAndControlSectionsLoaded(webDriverWait);

            //verify we can see the CMIU entry in "Rejected Commands" section
            discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_REJECTED);
            result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForFotaCommandInjection)).findAny();
        } while(!result.isPresent() && timeOut.isAfter(Instant.now()));

        // Make sure the commands-rejected section in in view
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        JavascriptExecutor js = driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        scrollToElement("//div[contains(@class, 'commands-rejected')]");

        assertTrue(result.isPresent());

        //We don't need it any more
        cmiuSimulatorProcess.destroyForcibly();

        takeScreenshot("Command and Control - processed");
    }

    @Test
    public void testSendModemFotaCommandEndToEndCommandFail() throws IOException, InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        //flush command for the CMIU first
        flushCommand(webDriverWait, cmiuIdForFotaCommandInjection);

        //flush any pending messages on the broker
        Process cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForFotaCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 1, 15, false, false);
        cmiuSimulatorProcess.waitFor();

        //Start the CMIU simulator for the test
        cmiuSimulatorProcess = this.launchCmiuSimulator(cmiuIdForFotaCommandInjection, Constants.BROKER_SERVER,
                "detailedConfigPacket", 15, 15, false, true);

        //xpath for getting the number of rows of CMIU in progress
        final String xpathTargetCmiuInProgress = generateXpathForCmiu("commands-in-progress", cmiuIdForFotaCommandInjection);

        driver.get(cmiuConfigUrl);
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        takeScreenshot("Command and Control - before");


        // 1. Load config page, get the initial number of cmiu commands that are still queueing
        int numberOfOriginalTargetCmiuQueuedCommands = driver.findElementsByXPath(xpathTargetCmiuInProgress).size();

        // 2. Navigate to send commands
        driver.get(cmiuFotaUpdate);

        takeScreenshot("CMIU Modem FOTA");

        //Image selection page loaded.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), " +
                "'Update modem firmware with FOTA')]")));

        Select selection = new Select(driver.findElement(By.id("imageName")));
        selection.selectByIndex(0);

        // input expected version
        driver.findElement(By.id("versionNumber")).sendKeys("v12.34.567");

        //input CMIU
        driver.findElement(By.id("miuInputList0.miuId")).sendKeys(cmiuIdForFotaCommandInjection);

        //submit form
        driver.findElementByName("modemFotaSubmit").click();

        //5. Wait to be redirect back to Command and Control Interface page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Command & Control Interface')]")));
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        String url = driver.getCurrentUrl();
        takeScreenshot("Command and Control - queued");

        assertTrue(url.contains("pages/config"));

        //verify we can see the CMIU entry in "Command In Process" section
        List<WebElement> discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_IN_PROGRESS);
        Optional<WebElement> result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForFotaCommandInjection)).findAny();
        assertTrue(result.isPresent());

        //queued list for the cmiu has incremented
        assertEquals(numberOfOriginalTargetCmiuQueuedCommands + 1, driver.findElementsByXPath(xpathTargetCmiuInProgress).size());

        Instant timeOut = Instant.now().plus(1, ChronoUnit.MINUTES);

        do
        {
            driver.navigate().refresh();
            awaitAllCommandAndControlSectionsLoaded(webDriverWait);

            //verify we can see the CMIU entry in "Image Update Commands Received" section
            discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_ACCEPTED);
            result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForFotaCommandInjection)).findAny();
        } while(!result.isPresent() && timeOut.isAfter(Instant.now()));

        // Make sure the commands-received section in in view
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        scrollToElement("//div[contains(@class, 'commands-rejected')]");
        scrollToElement("//div[contains(@class, 'commands-received')]");

        assertTrue(result.isPresent());

        //verify we can see the CMIU entry is still in the "Image Update Commands Received" section
        discoveredElements = driver.findElementsByXPath(XPATH_CMIU_COMMAND_ACCEPTED);
        result = discoveredElements.stream().filter(e -> e.getText().contains(cmiuIdForFotaCommandInjection)).findAny();
        assertTrue(result.isPresent());

        takeScreenshot("Command and Control - processed");

        int newQueuedCommands = 0;

        Instant timeout = Instant.now();
        timeout = timeout.plus(2, ChronoUnit.MINUTES);

        do
        {
            newQueuedCommands = driver.findElementsByXPath(xpathTargetCmiuInProgress).size();
        } while(numberOfOriginalTargetCmiuQueuedCommands != newQueuedCommands &&
                timeout.isAfter(Instant.now()));

        //command in process list for the cmiu is back to original
        assertEquals("Expected that command in process list for the cmiu is back to original", numberOfOriginalTargetCmiuQueuedCommands,
                driver.findElementsByXPath(xpathTargetCmiuInProgress).size());

        cmiuSimulatorProcess.destroyForcibly();
    }

    /**
     * This is to verify the webpage web flow is as expected for all commands, and it does not encounter any backend error.
     *
     * @todo currently cmiu simulator does not support ACK on these command.. to be added
     */
    @Test
    public void testSendOtherCommand()
    {
        //this.sendGenericCmiuCommands("Reboot CMIU", "Reboot CMIU");
        //this.sendGenericCmiuCommands("Emulate Mag Swipe", "Emulate Mag swipe");
        this.sendGenericCmiuCommands("Recording/Reporting interval", "Set Recording/Reporting Intervals");
    }

    /**
     * Test navigation of config set list, and add a new config set.
     */
    @Test
    public void testListAndSetCmiuConfigSet()
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        this.sendGenericCmiuCommands("Recording/Reporting interval", "Set Recording/Reporting Intervals", () -> {
            driver.findElement(By.linkText("List Config Sets")).click();

            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'List CMIU config set')]")));
            driver.findElement(By.partialLinkText("Add Config Set")).click();

            return addAndEditConfigSet(webDriverWait);
        });
    }

    /**
     * Test column sorting & filtering using the Commands In Progress section.
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void testCanSortAndFilterItemsInUserInterface() throws IOException, InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        // Ensure there are some records to sort
        for(String cmiuId : cmiuIdsForUiSortTesting)
        {
            // 1. Navigate to send commands
            driver.get(cmiuImageUpdate);

            // 2. Register new image
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Update Image')]")));

            driver.findElement(By.id("register-new-image")).click();
            importNewImage();

            // 3. Wait to be directed back to Command and Control Interface page, send command page, select image to update. Try checking all checkboxes to verify they are all present.
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Update Image')]")));

            driver.findElement(By.id("updateFirmware1")).click();
            Select selection = new Select(driver.findElement(By.id("firmwareImageVersion")));
            selection.selectByIndex(1);

            //input CMIU
            driver.findElement(By.id("miuInputList0.miuId")).sendKeys(cmiuId);

            //submit form
            driver.findElementByName("updateCmiuImage").click();

            // 4. Wait to be redirect back to Command and Control Interface page
            awaitAllCommandAndControlSectionsLoaded(webDriverWait);
        }

        // Click sort by MIU ID
        driver.findElement(By.xpath(generateXpathForHeaderSortLink("commands-in-progress", "miuId"))).click();
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        // Click sort by Submitted Date
        driver.findElement(By.xpath(generateXpathForHeaderSortLink("commands-in-progress", "submitted"))).click();
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        // Verify that dates are in ascending order (using title attribute for UTC value)
        Instant timeOut = Instant.now().plus(15, ChronoUnit.SECONDS);
        boolean areElementsInExpectedOrder;

        do
        {
            Thread.sleep(Duration.ofSeconds(1).toMillis());
            areElementsInExpectedOrder = areDatesInExpectedOrder(XPATH_COMMAND_IN_PROGRESS_SUBMITTED_DATES, "ASC");

        } while(!areElementsInExpectedOrder && timeOut.isAfter(Instant.now()));

        takeScreenshot("Commands in Progress - sort by Submitted Date Ascending");

        assertTrue("Dates expected to be in ascending order", areElementsInExpectedOrder);

        // Click sort by Submitted Date again to reverse the sort order
        driver.findElement(By.xpath(generateXpathForHeaderSortLink("commands-in-progress", "submitted"))).click();
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        // Verify that dates are in descending order (using title attribute for UTC value)
        timeOut = Instant.now().plus(15, ChronoUnit.SECONDS);

        do
        {
            Thread.sleep(Duration.ofSeconds(1).toMillis());
            areElementsInExpectedOrder = areDatesInExpectedOrder(XPATH_COMMAND_IN_PROGRESS_SUBMITTED_DATES, "DESC");

        } while(!areElementsInExpectedOrder && timeOut.isAfter(Instant.now()));

        takeScreenshot("Commands in Progress - sort by Submitted Date Descending");

        assertTrue("Dates expected to be in descending order", areElementsInExpectedOrder);

        // Try filtering by SIM Cellular Network
        boolean isNetworkFilterApplied;
        new Select(driver.findElement(By.id("networkProvider"))).selectByValue("VZW");
        driver.findElement(By.id("filterMiuButton")).click();
        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        timeOut = Instant.now().plus(15, ChronoUnit.SECONDS);

        do
        {
            Thread.sleep(Duration.ofSeconds(1).toMillis());
            isNetworkFilterApplied = doAllElementsHaveText(XPATH_COMMAND_IN_PROGRESS_NETWORK, "VZW");

        } while(!isNetworkFilterApplied && timeOut.isAfter(Instant.now()));

        takeScreenshot("Commands in Progress - filtered by Network: VZW");

        assertTrue("Network filter expected to be applied", isNetworkFilterApplied);
    }

    private boolean doAllElementsHaveText(String xpath, String text)
    {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));
        boolean doAllHaveMatchingText = true;

        for (WebElement element : elements)
        {
            String thisValue = element.getText();

            doAllHaveMatchingText &= text.equalsIgnoreCase(thisValue);
        }

        return doAllHaveMatchingText;
    }

    private boolean areDatesInExpectedOrder(String selectionXpath, String order)
    {
        if (order != "ASC" && order != "DESC")
        {
            throw new UnsupportedOperationException("'order' parameter must be ASC or DESC.");
        }

        List<WebElement> submittedDates = driver.findElements(By.xpath(selectionXpath));
        String previousValue = null;
        boolean areElementsInExpectedOrder = true;

        for (WebElement submittedDate : submittedDates)
        {
            String thisValue = submittedDate.getAttribute("title");

            if (previousValue != null)
            {
                if (order == "ASC")
                {
                    areElementsInExpectedOrder &= previousValue.compareTo(thisValue) <= 0;
                }
                else
                {
                    areElementsInExpectedOrder &= previousValue.compareTo(thisValue) >= 0;
                }
            }

            previousValue = thisValue;
        }

        return areElementsInExpectedOrder;
    }

    /**
     * Routine to direct the webpage to import/register new images
     */
    private void importNewImage()
    {
        //Assume we are already in the register new image page (http://localhost:8080/mdce-web/pages/config/image-update/register-new)

        Select imageTypeSelection = new Select(driver.findElement(By.id("imageType")));
        Select imageSourceSelection = new Select(driver.findElement(By.id("selectedImageSource")));

        //cycle through all options to ensure they are present
        imageTypeSelection.selectByValue("FirmwareImage");
        imageTypeSelection.selectByValue("ConfigImage");
        imageTypeSelection.selectByValue("TelitModuleImage");
        imageTypeSelection.selectByValue("BleConfigImage");
        imageTypeSelection.selectByValue("ArbConfigImage");
        imageTypeSelection.selectByValue("EncryptionImage");

        imageTypeSelection.selectByValue("FirmwareImage");

        //select latest image, which should be the second one in the option list, first is a dummy
        imageSourceSelection.selectByIndex(1);

        //submit form
        driver.findElement(By.id("registerNewImageForm")).submit();
    }

    /**
     * Get number of entries for a cmiu id from recently completed commands section
     *
     * @param cmiuId the cmiu id to identify
     * @return number of entries matching the specified cmiu
     */
    private int getRecentlyCompletedCommands(String cmiuId)
    {
        //reload the page
        final String cmiuConfigUrl = Constants.MDCE_WEB_URL + "/pages/config";
        driver.get(cmiuConfigUrl);

        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        try
        {
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(
                    By.xpath("//div[contains(@class, 'commands-recently-completed')]" +
                            "/div[contains(@class, 'progress-content-group')]/ul")));
        }
        catch (TimeoutException e)
        {
            //Ignore the timeout, it might be that there are no recently
            //completed commands, and so this section doesn't appear
        }

        try
        {
            Thread.sleep(100);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        List<WebElement> completedCmiuElements =
                driver.findElementsByXPath(generateXpathForCmiu("commands-recently-completed", cmiuId));

        return completedCmiuElements.size();
    }

    /**
     * Get number of entries for a cmiu id from recently rejected commands section
     *
     * @param cmiuId the cmiu id to identify
     * @return number of entries matching the specified cmiu
     */
    private int getRecentlyRejectedCommands(String cmiuId)
    {
        //reload the page
        final String cmiuConfigUrl = Constants.MDCE_WEB_URL + "/pages/config";
        driver.get(cmiuConfigUrl);

        List<WebElement> completedCmiuElements = driver.findElementsByXPath(generateXpathForCmiu("commands-rejected", cmiuId));

        return completedCmiuElements.size();
    }

    /**
     * Get element dl class="progress-status-row" representing the cmiu we are interested in
     *
     * @param sectionClass Class of group div
     * @param cmiuId
     * @return xpath
     */
    private static String generateXpathForCmiu(String sectionClass, String cmiuId)
    {
        final String xpath = "//div[contains(@class, '" + sectionClass +"')]" +
                "/div[contains(@class, 'progress-content-group')]/ul" +
                "/li[contains(@title, 'MIU Commands') and @miuid='" + cmiuId + "']/ul";

        return xpath;
    }

    /**
     * Get the field-sort header for the given section and field name.
     * @param sectionClass Class of group div
     * @param fieldName Internal header name
     * @return Xpath to locate the A-tag that sorts the field
     */
    private static String generateXpathForHeaderSortLink(String sectionClass, String fieldName)
    {
        final String xpath =
                "//div[contains(@class, '" + sectionClass + "')]" +
                "//a[contains(@class, 'column-sort') and @sortby='" + fieldName + "']";

        return xpath;
    }

    private Process launchCmiuSimulator(String cmiuId, String brokerServer, String packetType, int runEveryNSeconds,
                                        int repetitions, boolean commandNAck, boolean commmandFail) throws IOException, InterruptedException
    {
        CmiuSimulatorLauncher cmiuSim = new CmiuSimulatorLauncher();
        ProcessBuilder pb;

        //this process is configured to run for 2.5 minutes. give it enough time for mdce-mqtt to detect a
        //new command and send to the broker, and for Cmiu Simulator to dequeue the command from the broker.
        if(!commandNAck && !commmandFail)
        {
            pb = new ProcessBuilder("python", "CmiuSimManager.py",
                    brokerServer,
                    "-s " + cmiuId,
                    "-u " + runEveryNSeconds/60.0,
                    "-e 1",
                    "-r " + repetitions);
        }
        else if(commandNAck)
        {
            pb = new ProcessBuilder("python", "CmiuSimManager.py",
                    brokerServer,
                    "-s " + cmiuId,
                    "-u " + runEveryNSeconds/60.0,
                    "-f",
                    "-e 1",
                    "-r " + repetitions);
        }
        else
        {
            pb = new ProcessBuilder("python", "CmiuSimManager.py",
                    brokerServer,
                    "-s " + cmiuId,
                    "-u " + runEveryNSeconds/60.0,
                    "-F",
                    "-e 1",
                    "-r " + repetitions);
        }

        pb.directory(CmiuSimulatorLauncher.getUnzippedPath());
        Process p = pb.start();
        System.out.println("Start Cmiu Simulator running at time " + LocalDateTime.now());

        inputStreamToOutputStream(p.getErrorStream(), System.out);
        inputStreamToOutputStream(p.getInputStream(), System.out);

        System.out.println("Exit Cmiu Simulator running at time " + LocalDateTime.now());

        return p;
    }


    /**
     * Print output stream while the process is running.
     * This is used in preference to the more elegant pb.inheritIO() because the '/' character will be interpreted as
     * escape character.
     *
     * @param inputStream input
     * @param out         where to print, normally System.out
     */
    void inputStreamToOutputStream(final InputStream inputStream, final OutputStream out)
    {
        Thread t = new Thread(() -> {
            try
            {
                int d;
                while ((d = inputStream.read()) != -1)
                {
                    out.write(d);
                }
            }
            catch (IOException ex)
            {
                System.out.println(ex);
            }
        });
        t.setDaemon(true);
        t.start();
    }

    private void flushCommand(WebDriverWait webDriverWait, String cmiuIdToFlush) throws InterruptedException
    {
        //flush commands
        System.out.println("Flushing commands");

        try
        {
            Thread.sleep(5000L);    //wait for any new commands to be queued
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        final String xpathTargetCmiuFlushCommandIcon = "//div[contains(@class, 'commands-in-progress')]" +
                "/div[contains(@class, 'progress-content-group')]/ul[.//a[contains(text(), '" + cmiuIdToFlush + "')]]" +
                "/li/a[contains(@class, 'flush-pending-commands')]";

        driver.get(cmiuConfigUrl);

        awaitAllCommandAndControlSectionsLoaded(webDriverWait);

        List<WebElement> identifiedElements = driver.findElementsByXPath(xpathTargetCmiuFlushCommandIcon);

        System.out.println("... found " + identifiedElements.size() + " pending commands");

        //should expect either none or one element
        if (!identifiedElements.isEmpty())
        {
            identifiedElements.get(0).click();


            //wait for flush to completed so we hopefully have clear the queue for cmiuSimulator
            Thread.sleep(10000L);

            //we should be still at the C2 interface
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Command & Control Interface')]")));

            //check commands for the specific CMIU has been flushed from Command In Process table
            assertTrue(driver.findElementsByXPath(xpathTargetCmiuFlushCommandIcon).isEmpty());
        }

    }

    private void awaitAllCommandAndControlSectionsLoaded(WebDriverWait webDriverWait)
    {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class, 'commands-in-progress-loaded')]")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class, 'commands-completed-loaded')]")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class, 'commands-received-loaded')]")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class, 'commands-rejected-loaded')]")));
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loading")));
    }

    private void scrollToElement(String xpath)
    {
        WebElement element = driver.findElement(By.xpath(xpath));
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }
}
