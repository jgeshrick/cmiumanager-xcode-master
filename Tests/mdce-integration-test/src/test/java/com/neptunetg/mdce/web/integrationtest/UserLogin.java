/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.SeleniumTestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

public class UserLogin
{
    private final static String cmiuUserLoginUrl = Constants.MDCE_WEB_URL + "/pages/user/login";
    public static final String ADMIN_USER = "admin";
    public static final String ADMIN_PWD = "admin";

    public static void loginAsAdmin(SeleniumTestBase seleniumTestBase)
    {
        RemoteWebDriver driver = seleniumTestBase.driver;
        driver.get(cmiuUserLoginUrl);

        doLogin(seleniumTestBase, ADMIN_USER, ADMIN_PWD);
    }

    /**
     * Submit username and password in login page and check for page to be redirected
     */
    private static void doLogin(SeleniumTestBase seleniumTestBase, String userName, String password)
    {
        RemoteWebDriver driver = seleniumTestBase.driver;
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("login")));

        driver.findElement(By.id("username")).sendKeys(userName);

        driver.findElement(By.id("password")).sendKeys(password);

        seleniumTestBase.takeScreenshot("Login as admin. " + Instant.now());

        driver.findElement(By.id("login")).submit();

        //check that we have navigated away from the login webpage
        try
        {
            webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("//h1[contains(text(), 'Login')]")));
            seleniumTestBase.takeScreenshot("Redirect to target after login. " + Instant.now());
        }
        catch(TimeoutException ex)
        {
            //is there a login failure?
            if (driver.findElements(By.xpath("//div[contains(@class, 'error')]")).size() > 0)
            {
                seleniumTestBase.takeScreenshot("Login failure " + Instant.now());
                throw new RuntimeException("Error logging to mdce, check user name/password");
            }

            //timeout waiting for webpage to process login
            throw new RuntimeException("Timeout waiting for webpage to login", ex);
        }
    }

    public static void logoutUser(SeleniumTestBase seleniumTestBase)
    {
        RemoteWebDriver driver = seleniumTestBase.driver;
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        System.out.println("Logging out...");
        driver.get(cmiuUserLoginUrl);

        try
        {
            WebElement logoutSubmitButton = driver.findElement(By.xpath("//input[@value='Log out']"));
            logoutSubmitButton.click();

            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Login')]")));

        }
        catch (Exception ex)
        {
            //user not login, so logout element is not displayed

        }

    }

    public static void loginIfRequired(SeleniumTestBase seleniumTestBase)
    {
        RemoteWebDriver driver = seleniumTestBase.driver;
        String pagetitle = driver.getTitle();

        if (pagetitle.contains("Login"))
        {
            doLogin(seleniumTestBase, ADMIN_USER, ADMIN_PWD);
        }
    }


}
