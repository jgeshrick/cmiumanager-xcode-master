/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.security;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import com.neptunetg.mdce.web.integrationtest.UserLogin;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.neptunetg.mdce.web.integrationtest.UserLogin.loginAsAdmin;
import static com.neptunetg.mdce.web.integrationtest.UserLogin.logoutUser;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

/**
 * Verify the account lockout behaviour.
 */
public class DisableUserAfterIncorrectPasswordsTest extends MdceWebSeleniumTestBase
{
    private final String userListUrl = Constants.MDCE_WEB_URL + "/pages/user/list";
    private final String mdceSettingsModifyUrl = Constants.MDCE_WEB_URL + "/pages/settings/modify";
    private static final String TEMP_PASSWORD = "TemporaryPassword1";

    @Test
    public void testUserManagement() throws InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        final String temporaryUserName = "user-" + Instant.now().getEpochSecond();

        // Create new user
        loginAsAdmin(this);
        listAndAddUser(webDriverWait, temporaryUserName, "Msoc operator");

        // Keep a copy of the lockout settings so they can be restored later
        Map<String, String> oldSettings = getCurrentLockoutSettings(webDriverWait);

        // Set low disable time & lockout threshold.
        setCurrentLockoutSettings(webDriverWait, "3", "1");

        // Make sure the account is not locked out by fewer than 3 consecutive bad login attempts.
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertTrue(tryLoginAs(webDriverWait, temporaryUserName, TEMP_PASSWORD));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertTrue(tryLoginAs(webDriverWait, temporaryUserName, TEMP_PASSWORD));

        // Make sure the account is locked after 3 consecutive bad login attempts.
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, TEMP_PASSWORD));

        Instant expectedUnlockTime = Instant.now().plus(1, ChronoUnit.MINUTES);

        // Wait for disabled period to pass
        while (expectedUnlockTime.compareTo(Instant.now()) > 0)
        {
            Thread.sleep(1000);
        }

        // Check that the account is no longer disabled
        assertTrue(tryLoginAs(webDriverWait, temporaryUserName, TEMP_PASSWORD));

        // Lock the user out again
        tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD");
        tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD");
        tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD");

        // Log in as admin and remove the disabled period for the user.
        assertTrue(tryLoginAs(webDriverWait, UserLogin.ADMIN_USER, UserLogin.ADMIN_PWD));
        removeUserLockout(webDriverWait, temporaryUserName);

        // Verify that the user can log in again.
        assertTrue(tryLoginAs(webDriverWait, temporaryUserName, TEMP_PASSWORD));

        // Log in as admin and disable lockouts for the user.
        assertTrue(tryLoginAs(webDriverWait, UserLogin.ADMIN_USER, UserLogin.ADMIN_PWD));
        setLockoutsEnabled(webDriverWait, temporaryUserName, false);

        // Verify that the user isn't locked out after 3 bad password attempts.
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertFalse(tryLoginAs(webDriverWait, temporaryUserName, "WRONG PASSWORD"));
        assertTrue(tryLoginAs(webDriverWait, temporaryUserName, TEMP_PASSWORD));

        // Tidy up.
        assertTrue(tryLoginAs(webDriverWait, UserLogin.ADMIN_USER, UserLogin.ADMIN_PWD));
        deleteUser(webDriverWait, temporaryUserName);
        setCurrentLockoutSettings(webDriverWait, oldSettings.get("maxLoginAttempts"), oldSettings.get("lockedAccountTimeoutMinutes"));

        logoutUser(this);
    }

    private void setLockoutsEnabled(WebDriverWait webDriverWait, String userName, boolean lockoutsEnabled)
    {
        driver.get(userListUrl);
        final String detailsXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + userName + "')]" +
                "/..//a[contains(@href, 'details')]";

        final String saveUserDetailsButtonXpath = "//input[@type='submit' and @name='saveUserDetails']";

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(detailsXpath)));
        driver.findElementByXPath(detailsXpath).click();

        // wait for 'Save user details' button
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(saveUserDetailsButtonXpath)));

        // Change accountLockoutEnabled to the desired value.
        if(driver.findElement(By.id("accountLockoutEnabled")).isSelected() != lockoutsEnabled)
        {
            driver.findElement(By.id("accountLockoutEnabled")).click();
        }

        driver.findElementByXPath(saveUserDetailsButtonXpath).click();

        // Wait for the user list page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("user-dynamic-list")));
    }

    private void removeUserLockout(WebDriverWait webDriverWait, String userName)
    {
        driver.get(userListUrl);
        final String detailsXpath = "//tr[contains(@class, 'list-item')]" +
                "/td[contains(@class, 'user-list-username') and contains(text(), '" + userName + "')]" +
                "/..//a[contains(@href, 'details')]";

        final String saveUserDetailsButtonXpath = "//input[@type='submit' and @name='saveUserDetails']";

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(detailsXpath)));
        driver.findElementByXPath(detailsXpath).click();

        // wait for 'Save user details' button
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(saveUserDetailsButtonXpath)));

        // Un-tick "Login disabled".
        if(driver.findElement(By.id("currentlyDisabled")).isSelected())
        {
            driver.findElement(By.id("currentlyDisabled")).click();
        }

        driver.findElementByXPath(saveUserDetailsButtonXpath).click();

        // Wait for the user list page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("user-dynamic-list")));
    }

    private Map<String, String> getCurrentLockoutSettings(WebDriverWait webDriverWait)
    {
        Map<String, String> settings = new HashMap<>();

        driver.get(mdceSettingsModifyUrl);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("maxLoginAttempts")));

        // Get setting values
        settings.put("maxLoginAttempts", driver.findElement(By.id("maxLoginAttempts")).getAttribute("value"));
        settings.put("lockedAccountTimeoutMinutes", driver.findElement(By.id("lockedAccountTimeoutMinutes")).getAttribute("value"));

        return settings;
    }

    private void setCurrentLockoutSettings(WebDriverWait webDriverWait, String maxLoginAttempts, String lockedAccountTimeoutMinutes)
    {
        Map<String, String> settings = new HashMap<>();

        driver.get(mdceSettingsModifyUrl);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("maxLoginAttempts")));

        // Change settings
        driver.findElement(By.id("maxLoginAttempts")).clear();
        driver.findElement(By.id("maxLoginAttempts")).sendKeys(maxLoginAttempts);
        driver.findElement(By.id("lockedAccountTimeoutMinutes")).clear();
        driver.findElement(By.id("lockedAccountTimeoutMinutes")).sendKeys(lockedAccountTimeoutMinutes);

        driver.findElement(By.xpath("//form[@id='settingsForm']//input[@type='submit']")).click();

        // Detect the link back to the edit screen.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(), 'Modify settings')]")));
    }

    private void listAndAddUser(WebDriverWait webDriverWait, String newUserName, String userLevel)
    {
        driver.get(userListUrl);

        takeScreenshot("User list - 01");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'User List')]")));

        //click Add User
        driver.findElement(By.id("user-dynamic-list-add-new")).click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Add User')]")));

        driver.findElementById("userName").sendKeys(newUserName);
        Select userLevelSelection = new Select(driver.findElement(By.id("userLevel")));
        userLevelSelection.selectByVisibleText(userLevel);

        driver.findElementById("password").sendKeys(TEMP_PASSWORD);
        driver.findElementById("passwordRepeated").sendKeys(TEMP_PASSWORD);
        driver.findElementById("email").sendKeys(newUserName + "@server.com");

        driver.findElement(By.id("add-user")).click();

        //back to User List page
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'User List')]")));

        //verify audit log
        List<AuditLogEntry> auditLogEntries = getRecentAuditLogEntries(startTime, null);
        assertTrue(auditLogEntries.stream().anyMatch(log -> log.getEventType().contains("User created") &&
                log.getNewValue().contains(newUserName)));
    }

}
