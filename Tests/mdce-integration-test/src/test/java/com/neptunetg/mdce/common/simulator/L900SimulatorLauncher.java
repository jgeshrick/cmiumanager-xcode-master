/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.common.simulator;

import com.neptunetg.mdce.common.framework.Constants;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * A class to assist in starting the L900 Simulator written in Python
 */
public class L900SimulatorLauncher
{
    public static File getUnzippedPath()
    {
        return unzippedPath;
    }

    private static final File unzippedPath = new File(System.getProperty("java.io.tmpdir"), "l900Sim");

    static
    {
        final String pathToL900SimulatorZip = Constants.L900_SIMULATOR_ZIP;

        System.out.println("Using L900 simulator: " + pathToL900SimulatorZip);

        try (final ZipFile zf = new ZipFile(pathToL900SimulatorZip))
        {
            unzippedPath.mkdirs();
            unzippedPath.deleteOnExit();

            System.out.println("Unzipping " + pathToL900SimulatorZip + " to " + unzippedPath);

            Enumeration entries = zf.entries();

            for (final Enumeration entriesEnumeration = zf.entries(); entriesEnumeration.hasMoreElements(); )
            {
                final ZipEntry entry = (ZipEntry) entriesEnumeration.nextElement();

                if (!entry.isDirectory())
                {
                    String filename = entry.getName();
                    final int slashPos = filename.indexOf('/');

                    if (slashPos >= 0)
                    {
                        filename = filename.substring(slashPos + 1);
                    }

                    final File extractedFile = new File(unzippedPath, filename);
                    File destDir = new File(extractedFile.toString());
                    destDir.mkdirs();

                    Files.copy(zf.getInputStream(entry), extractedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    extractedFile.deleteOnExit();
                }
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException("Can't extract ZIP file " + pathToL900SimulatorZip);
        }
    }

    /**
     * Trigger L900 Simulator to send a packet MQTT
     * @param brokerServer The MQTT server to point at
     * @param l900Id Start L900 id to run the simulator
     * @param l900Count The number of L900s to simulate
     * @param repeatCount The number of L900 connections to simulate per L900
     * @param timeWarp Speed up the simulation by X times
     * @param verboseLogging Run the L900 simulator with verbose logging
     * @return The stdout from the L900 simulator
     * @throws IOException
     * @throws InterruptedException
     */
    public String runL900Simulator(String brokerServer, int l900Id, int l900Count, int repeatCount, int timeWarp,
                                   boolean verboseLogging, boolean useActility) throws IOException, InterruptedException
    {
        ProcessBuilder pb = new ProcessBuilder("python", "L900Sim.py",
                brokerServer,
                "-s " + l900Id,
                "-n " + l900Count,
                "-r " + repeatCount,
                "-t " + timeWarp,
                useActility ? "-A" : "",
                verboseLogging ? "-v" : "");

        pb.directory(unzippedPath);
        Process p = pb.start();

        BufferedReader stdReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));

        String stdout = "";

        while(!stdout.contains("...L900 SIMULATION FINISHED..."))
        {
            if(stdReader.ready())
            {
                String line = stdReader.readLine();
                stdout += line;
                System.out.println(line);
            }

            if(errReader.ready())
            {
                System.err.println(errReader.readLine());
            }

            outWriter.flush();

            Thread.sleep(10);
        }

        p.destroyForcibly().waitFor();

        return stdout;
    }
}
