/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.common.framework;

/**
 * Created by SML1 on 08/06/2015.
 */
public class SeleniumValues
{
    //Selenium output
    public static final String OUTPUT_FOLDER_BASE = "target/selenium-results";
    public static final String OUTPUT_FOLDER_SCREENSHOTS = OUTPUT_FOLDER_BASE + "/screenshots";

}
