/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.PartnerKeys;
import com.neptunetg.mdce.common.utility.HttpUtility;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseAuthenticatingRestTest
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
   
    public static final String AUTH_TOKEN = "\"auth_token\":\"";

    private Integer siteId = null;
    private String token;


    /**
     * We want to state the name of the test before and after, to make it easier to read the TeamCity build log
     * See http://cwd.dhemery.com/2010/12/junit-rules/ for a good guide to how this works
     */
    @Rule
    public TestRule stateTestNameRule = new StateTestNameRule();



    private class StateTestNameRule implements TestRule
    {

        public Statement apply(Statement statement, Description desc)
        {
            return new StateTestNameStatement(statement, desc);
        }

    }

    private class StateTestNameStatement extends Statement
    {

        private final Statement wrappedStatement;
        private final Description testDescription;

        public StateTestNameStatement(Statement wrappedStatement, Description testDescription)
        {
            super();
            this.wrappedStatement = wrappedStatement;
            this.testDescription = testDescription;
        }


        @Override
        public void evaluate() throws Throwable
        {

            System.out.println("**************  STARTING TEST: " + testDescription.getClassName() + "." + testDescription.getMethodName() + " **********************");

            try
            {
                wrappedStatement.evaluate();
            }
            finally
            {
                System.out.println("**************  FINISHED TEST: " + testDescription.getClassName() + "." + testDescription.getMethodName() + " **********************");

            }
        }

    }


    protected final String fetchTokenForSiteId(int siteId) throws Exception {

        if (this.siteId == null || this.siteId != siteId) {
            String partnerKey = getPartnerKey(siteId);

            this.token = fetchTokenInternal(siteId, partnerKey, false);

        }

        return this.token;

    }

    protected final String fetchTokenForBpc() throws Exception
    {
        this.token =  fetchTokenInternal(0, PartnerKeys.getPartnerKeyForBpc(), false);
        return this.token;

    }


    protected final String fetchTokenForFpc() throws Exception
    {
        this.token =  fetchTokenInternal(0, PartnerKeys.getPartnerKeyForFpc(), true);
        return this.token;
    }

    private String fetchTokenInternal(int siteId, String partnerKey, boolean fromFpc) throws Exception {

        final String url;
        if (fromFpc)
        {
            url = Constants.FPC_URL + "api/v1/token?site_id=" + siteId + "&partner=" + partnerKey;
        }
        else
        {
            url = Constants.MDCE_INTEGRATION_URL + "mdce/api/v1/token?site_id=" + siteId + "&partner=" + partnerKey;
        }

        final String responseIncludingToken = HttpUtility.httpGet(url);

        return extractToken(responseIncludingToken);

    }

    private String getPartnerKey(int siteId) throws Exception {

        return PartnerKeys.getPartnerKey(siteId);

    }

    private String extractToken(final String responseIncludingToken) {
        int i = responseIncludingToken.indexOf(AUTH_TOKEN);
        int length = responseIncludingToken.length() -1;
        String token = responseIncludingToken.substring(i, length);
        token = token.substring(AUTH_TOKEN.length(), token.length() - 1);
        int endIndex = token.contains("\"") ? token.indexOf('"') : token.length() ;
        token = token.substring(0, endIndex);
        logger.info(">>>>>>>" + token + "<<<<");
        return token;
    }

}
