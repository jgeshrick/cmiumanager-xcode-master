/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.cns;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.common.utility.StreamHelper;
import com.neptunetg.mdce.common.utility.TrustModifier;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Emulate Jasper Control Centre automation callback. Send callback http POST to mdce-integration server to verify the
 * handlers are implemented.
 * The secret shared key used for the signature is "neptune"
 */
public class JasperAutomationCallbackTest extends BaseSetupRestTest
{
    private final String JASPER_AUTOMATION_CALLBACK_URL = Constants.MDCE_INTEGRATION_URL + "/cns/jasper";


    public JasperAutomationCallbackTest()
    {
        super(400000102);
    }

    @Before
    public void setUp() throws Exception
    {
        addJasperCellularDeviceIfNotPresent();
    }

    @Test
    public void sessionStartTest() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        final String postParam = "eventId=SESSION_START-6663665025501&eventType=SESSION_START&timestamp=2016-02-09T16%3A07%3A08.855Z&signature=muUw7ULCteaOdl%2B%2FMuMahlCWC%2BI%3D&signature2=p50baiEbdkC6VuPBD%2F%2BrkeSQfuI4RmlP3za8QIzhAbI%3D&data=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CSession+xmlns%3D%22http%3A%2F%2Fapi.jasperwireless.com%2Fws%2Fschema%22%3E%3Ciccid%3E89302720396917146404%3C%2Ficcid%3E%3CipAddress%3E10.209.72.224%3C%2FipAddress%3E%3CdateSessionStarted%3E2016-02-09T16%3A07%3A08.523Z%3C%2FdateSessionStarted%3E%3CdateSessionEnded+xsi%3Anil%3D%22true%22+xmlns%3Axsi%3D%22http%3A%2F%2Fwww.w3.org%2F2001%2FXMLSchema-instance%22%2F%3E%3C%2FSession%3E";
        String response = doHttpFormPost("session-monitoring", postParam);
    }

    @Test
    public void sessionEndTest() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        final String postParam = "eventId=SESSION_STOP-6663668300501&eventType=SESSION_STOP&timestamp=2016-02-09T16%3A07%3A24.844Z&signature=Efvdn5XUwpxWM8mRHgslVQMgeBE%3D&signature2=n%2F47Ry1p8hCGjyHx6JuekrGrztUhYDYkaeTD56zdQIU%3D&data=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CSession+xmlns%3D%22http%3A%2F%2Fapi.jasperwireless.com%2Fws%2Fschema%22%3E%3Ciccid%3E89302720396917146404%3C%2Ficcid%3E%3CipAddress%3E10.209.72.224%3C%2FipAddress%3E%3CdateSessionStarted%3E2016-02-09T16%3A07%3A08.404Z%3C%2FdateSessionStarted%3E%3CdateSessionEnded%3E2016-02-09T16%3A07%3A24.404Z%3C%2FdateSessionEnded%3E%3C%2FSession%3E";
        String response = doHttpFormPost("session-monitoring", postParam);
    }

    @Test
    public void past24hDataUsageExceedNotificationTest() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        final String postParam = "eventId=PAST24H_DATA_USAGE_EXCEEDED-3157118234901&eventType=PAST24H_DATA_USAGE_EXCEEDED&timestamp=2016-02-10T10%3A10%3A57.295Z&signature=ano8Jr%2FTEnVESrC0mKxaAeULGDo%3D&signature2=TX%2Bh8GVld27BWYEE7URzMRAPT1q8K7SjSoWWLWR8qus%3D&data=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CPast24HDataUsage+xmlns%3D%22http%3A%2F%2Fapi.jasperwireless.com%2Fws%2Fschema%22%3E%3Ciccid%3E89302720396917146404%3C%2Ficcid%3E%3CdataUsage%3E50667752%3C%2FdataUsage%3E%3C%2FPast24HDataUsage%3E";
        String response = doHttpFormPost("usage-monitoring", postParam);
    }

    @Test
    public void cycleToDateDataUsageExceedNotificationTest() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        final String postParam = "eventId=CTD_USAGE-3150076761801&eventType=CTD_USAGE&timestamp=2016-02-09T16%3A07%3A28.952Z&signature=oC%2Fm0HLMcQ3w05TZne0pnJ%2BCx6M%3D&signature2=c5un4ETbdAixO2cAZYwwEv4a53kmyNa6Yh25Zhf%2FHPI%3D&data=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CCtdUsage+xmlns%3D%22http%3A%2F%2Fapi.jasperwireless.com%2Fws%2Fschema%22%3E%3Ciccid%3E89302720396917146396%3C%2Ficcid%3E%3CdataUsage%3E23654373%3C%2FdataUsage%3E%3C%2FCtdUsage%3E";
        String response = doHttpFormPost("usage-monitoring", postParam);
    }
    @Test
    public void noConnectionNotificationTest() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        final String postParam = "eventId=SESSION_NO_CONNECTION-1455055808790&eventType=SESSION_NO_CONNECTION&timestamp=2016-02-09T22%3A10%3A08.893Z&signature=g7bjtGp5mPt2P%2F4JvBM265LZEao%3D&signature2=CMxXcqXU%2Bme12KID%2FM4W9oz9b3phXNYZ8FjJAPQ6BIk%3D&data=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CNoConnection+xmlns%3D%22http%3A%2F%2Fapi.jasperwireless.com%2Fws%2Fschema%22%3E%3Ciccid%3E89302720396916964625%3C%2Ficcid%3E%3CaccountName%3ENeptune+Technology+Group+-+Jasper%3C%2FaccountName%3E%3CratePlanName%3EJasper+Test+Drive+Standard+Plan%3C%2FratePlanName%3E%3CevaluatingTime%3E2016-02-09T22%3A10%3A08.790Z%3C%2FevaluatingTime%3E%3CnoConnectionThreshold%3E24%3C%2FnoConnectionThreshold%3E%3C%2FNoConnection%3E";
        String response = doHttpFormPost("usage-monitoring", postParam);
    }

    @Test
    public void simStateChangeToActivatedTest() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        final String postParam = "eventId=SIM_STATE_CHANGE-27753570101&eventType=SIM_STATE_CHANGE&timestamp=2016-02-10T09%3A38%3A56.763Z&signature=kCXRcUC%2B26R0bLayQFmHjLdpSCM%3D&signature2=LhaHhfE%2B%2Br7s%2F2BIppFK0Lx2u611U0xX4XlfugD4Lvs%3D&data=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CSimStateChange+xmlns%3D%22http%3A%2F%2Fapi.jasperwireless.com%2Fws%2Fschema%22%3E%3Ciccid%3E89302720396917146396%3C%2Ficcid%3E%3CpreviousState%3EDeactivated%3C%2FpreviousState%3E%3CcurrentState%3EActivated%3C%2FcurrentState%3E%3CdateChanged%3E2016-02-10T09%3A38%3A44.893Z%3C%2FdateChanged%3E%3C%2FSimStateChange%3E";
        String response = doHttpFormPost("sim-state-change", postParam);
    }

    @Test
    public void simStateChangeToDeactivatedTest() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        final String postParam = "eventId=SIM_STATE_CHANGE-27753569401&eventType=SIM_STATE_CHANGE&timestamp=2016-02-10T09%3A38%3A40.061Z&signature=2U%2Bqyfu24oc8pdM2jrHTIzj%2BQq8%3D&signature2=iWdITrQdFOsmVRXvMqbuuYJ4miWnO07srsgdSe3Huus%3D&data=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CSimStateChange+xmlns%3D%22http%3A%2F%2Fapi.jasperwireless.com%2Fws%2Fschema%22%3E%3Ciccid%3E89302720396917146396%3C%2Ficcid%3E%3CpreviousState%3EActivated%3C%2FpreviousState%3E%3CcurrentState%3EDeactivated%3C%2FcurrentState%3E%3CdateChanged%3E2016-02-10T09%3A38%3A39.989Z%3C%2FdateChanged%3E%3C%2FSimStateChange%3E";
        String response = doHttpFormPost("sim-state-change", postParam);
    }


    /**
     * For a Form post to emulate what Jasper CC is sending.
     * @param subUrl
     * @param requestData
     * @return
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws KeyManagementException
     */
    private String doHttpFormPost(
            final String subUrl,
            final String requestData) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
    {
        final URL postUrl = new URL(JASPER_AUTOMATION_CALLBACK_URL + "/" + subUrl);

        //write request
        HttpURLConnection httpCon = TrustModifier.openConnectionIgnoreSelfSignCert(postUrl) ;

        httpCon.setDoOutput(true);
        httpCon.setRequestMethod("POST");
        httpCon.setUseCaches(false);
        httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        //String requestData = StreamHelper.inputStreamToString(getClass().getResourceAsStream(resourcePath));

        OutputStream reqStream = httpCon.getOutputStream();
        reqStream.write(requestData.getBytes());

        //get response
        String responseBody = "";
        try
        {
            InputStream responseStream = httpCon.getInputStream();
            responseBody = StreamHelper.inputStreamToString(responseStream);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }

        final int responseCode = httpCon.getResponseCode();
        if (responseCode < 200 || responseCode >= 300)
        {
            InputStream error = httpCon.getErrorStream();
            StreamHelper.copyStream(error, System.err);
        }

        assertThat(responseCode, Matchers.is(Matchers.both(Matchers.greaterThanOrEqualTo(200)).and(Matchers.lessThan(300))));

        System.out.println("Response: \n" + responseBody);

        return responseBody;
    }

    /**
     * Create a cellular device entry in the database matching the iccid of the device present in Jasper CC
     */
    private void addJasperCellularDeviceIfNotPresent() throws Exception
    {
        final String CMIUID = "400000400";

        final String ORIGINAL_REQUEST_BODY = "{" +
                "\"miu_id\": " + CMIUID + "," +
                "\"modem_config\": " +
                "{" +
                "\"vendor\": \"Telit\"," +
                "\"mno\": \"ATT\"," +
                "\"imei\": \"456938035643809\"" +           //fake, no IMEI entry in Jasper demo device
                "}," +
                "\"sim_config\": " +
                "{" +
                "\"iccid\": \"89302720396917146396\"" +
                "}}";

        final String token0 = fetchTokenForFpc();

        //OK response is asserted in the post
        HttpUtility.httpJsonPost(Constants.FPC_URL + "api/v1/miu/" +
                CMIUID + "/cellular-config?token=" + token0 + "&doRegisterWithMno=false", ORIGINAL_REQUEST_BODY);

        String getResponse = HttpUtility.httpGet(Constants.FPC_URL + "api/v1/miu/" +
                CMIUID + "/cellular-config?token=" + token0);

        assertEquals("89302720396917146396", JsonPath.read(getResponse, "$.sim_config.iccid"));

    }
}
