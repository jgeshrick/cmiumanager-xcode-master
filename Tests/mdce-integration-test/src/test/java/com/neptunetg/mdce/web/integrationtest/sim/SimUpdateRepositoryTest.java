/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.sim;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.PartnerKeys;
import com.neptunetg.mdce.common.internal.sim.service.SimService;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseAuthenticatingRestTest;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * TODO: This test class uses REST APIs that have been temporarily created in lieu of being able to fake a Verizon
 *       or AT&T cellular device service.
 */
public class SimUpdateRepositoryTest extends MdceWebSeleniumTestBase
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String SAMPLE_ICCID = "91250423440800000196";
    private static final String SAMPLE_MSISDN = "2025550196";
    private static final String SAMPLE_MNO = "VZW";

    private static final String SIM_LIST_URL = Constants.MDCE_WEB_URL + "pages/sim/simlist";

    private Integer siteId = null;
    private String token;


    @Test
    public void testNewSimRegistered() throws Exception
    {
        // Create SIM using temporary REST API
        final String token = fetchTokenForSiteId(1);

        postSimUpdate(token, SAMPLE_ICCID, SAMPLE_MNO, SAMPLE_MSISDN);

        // Verify through front end that the SIM has been created.
        driver.get(SIM_LIST_URL);

        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        boolean found = false;
        while (!found)
        {
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("selectedLifecycleState")));

            try
            {

                final WebElement simLinkElement = driver.findElement(
                        By.xpath("//table[@id='table-sim-list']/tbody/tr/td/a[text()[contains(.,'" +
                                SAMPLE_ICCID +
                                "')]]"));

                assertNotNull(simLinkElement);

                found = true;

            }
            catch (NoSuchElementException e)
            {

                try
                {
                    final WebElement nextPageElement = driver.findElement(By.linkText("Next"));

                    nextPageElement.click();
                }
                catch (NoSuchElementException e2)
                {
                    fail("Could not find SIM " + SAMPLE_ICCID + " on SIM list after looking through all the pages.");
                }
            }
        }
    }

    protected String postSimUpdate(String token, String iccid, String mno, String msisdn) throws Exception
    {
        Map<String, String> options = new HashMap<>();
        options.put(SimService.MSISDN, msisdn);

        return HttpUtility.httpJsonPost(Constants.MDCE_INTEGRATION_URL + "mdce/api/v1/sim/details" +
                "?token=" + token + "&iccid=" + iccid + "&mno=" + mno + "&msisdn=" + msisdn, "");
    }

    /**
     * Duplicated from BaseAuthenticatingRestTest
     */
    protected final String fetchTokenForSiteId(int siteId) throws Exception {

        if (this.siteId == null || this.siteId != siteId) {
            String partnerKey = getPartnerKey(siteId);

            this.token = fetchTokenInternal(siteId, partnerKey, false);

        }

        return this.token;

    }

    /**
     * Duplicated from BaseAuthenticatingRestTest
     */
    private String getPartnerKey(int siteId) throws Exception {

        return PartnerKeys.getPartnerKey(siteId);

    }

    /**
     * Duplicated from BaseAuthenticatingRestTest
     */
    private String fetchTokenInternal(int siteId, String partnerKey, boolean fromFpc) throws Exception {

        final String url;
        if (fromFpc)
        {
            url = Constants.FPC_URL + "api/v1/token?site_id=" + siteId + "&partner=" + partnerKey;
        }
        else
        {
            url = Constants.MDCE_INTEGRATION_URL + "mdce/api/v1/token?site_id=" + siteId + "&partner=" + partnerKey;
        }

        final String responseIncludingToken = HttpUtility.httpGet(url);

        return extractToken(responseIncludingToken);

    }

    /**
     * Duplicated from BaseAuthenticatingRestTest
     */
    private String extractToken(final String responseIncludingToken) {
        int i = responseIncludingToken.indexOf(BaseAuthenticatingRestTest.AUTH_TOKEN);
        int length = responseIncludingToken.length() -1;
        String token = responseIncludingToken.substring(i, length);
        token = token.substring(BaseAuthenticatingRestTest.AUTH_TOKEN.length(), token.length() - 1);
        int endIndex = token.contains("\"") ? token.indexOf('"') : token.length() ;
        token = token.substring(0, endIndex);
        logger.info(">>>>>>>" + token + "<<<<");
        return token;
    }

}