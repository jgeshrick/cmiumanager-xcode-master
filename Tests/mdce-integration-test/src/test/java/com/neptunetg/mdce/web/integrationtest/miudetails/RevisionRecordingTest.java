/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */

package com.neptunetg.mdce.web.integrationtest.miudetails;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static junit.framework.Assert.assertEquals;

/**
 * Test for revision recording
 */
public class RevisionRecordingTest extends MdceWebSeleniumTestBase
{
    private final int MIU_ID = 400000236;
    private final String MIU_DETAILS = Constants.MDCE_WEB_URL + "pages/miu/miudetail/" + MIU_ID;
    private final String CMIU_FIRMWARE_START = "41.42.43.";
    private final String CMIU_CONFIG_START = "43.44.45.";
    private final String CMIU_ARB_START = "47.48.49.";
    private final String CMIU_BLE_START = "51.53.54.";

    /**
     * Launch the simulator to send detailed config packet (which creates the CMIU if not present),
     * then get it to send further detailed config packets showing increased config revision numbers
     * and check the GUI updates.
     */
    @Test
    public void testRecordingCmiuRevisionInformation() throws Exception
    {
        CmiuSimulatorLauncher cmiuSimulatorLauncher = new CmiuSimulatorLauncher();

        final WebDriverWait webDriverWait = new WebDriverWait(driver, 60);

        cmiuSimulatorLauncher.sendPacketWithRevision(MIU_ID, Constants.BROKER_SERVER, "detailedConfigPacket", 1);
        checkRevisionInformation(1, 1, 1, 1);

        cmiuSimulatorLauncher.sendPacketWithRevision(MIU_ID, Constants.BROKER_SERVER, "detailedConfigPacket", 2);
        checkRevisionInformation(2, 2, 2, 2);

        cmiuSimulatorLauncher.sendPacketWithRevision(MIU_ID, Constants.BROKER_SERVER, "revisionTest_missingRevs", 3);
        checkRevisionInformation(3, 2, 3, 3);

        cmiuSimulatorLauncher.sendPacketWithRevision(MIU_ID, Constants.BROKER_SERVER, "detailedConfigPacket", 3);
        checkRevisionInformation(3, 3, 3, 3);
    }

    private void checkRevisionInformation(int expectedFirmware, int expectedConfig, int expectedArb, int expectedBle) throws InterruptedException
    {
        final RevisionDetails expectedRevisionDetails = new RevisionDetails();
        expectedRevisionDetails.setFirmwareRev(CMIU_FIRMWARE_START + expectedFirmware);
        expectedRevisionDetails.setConfigRev(CMIU_CONFIG_START + expectedConfig);
        expectedRevisionDetails.setArbRev(CMIU_ARB_START + expectedArb);
        expectedRevisionDetails.setBleRev(CMIU_BLE_START + expectedBle);

        RevisionDetails revisionDetailsFromPage;
        final Instant timeout = Instant.now().plus(30, ChronoUnit.SECONDS);

        do
        {
            Thread.sleep(200L);
            revisionDetailsFromPage = getRevisionsFromMiuDetailsPage();
        } while(timeout.isAfter(Instant.now()) && !expectedRevisionDetails.nonNullRevisionsMatch(revisionDetailsFromPage));

        assertEquals("Firmware image version", expectedRevisionDetails.getFirmwareRev(), revisionDetailsFromPage.getFirmwareRev());
        assertEquals("Config image version", expectedRevisionDetails.getConfigRev(), revisionDetailsFromPage.getConfigRev());
        assertEquals("ARB image version", expectedRevisionDetails.getArbRev(), revisionDetailsFromPage.getArbRev());
        assertEquals("BLE image version", expectedRevisionDetails.getBleRev(), revisionDetailsFromPage.getBleRev());
    }

    private RevisionDetails getRevisionsFromMiuDetailsPage() throws InterruptedException
    {
        driver.get(MIU_DETAILS);

        RevisionDetails revisionDetails = new RevisionDetails();

        try
        {
            String firmwareRev = driver.findElementsByXPath("//dl[contains(.,'CMIU firmware revision')]/dd").get(0).getText();
            String configRev = driver.findElementsByXPath("//dl[contains(.,'CMIU config revision')]/dd").get(0).getText();
            String encryptionRev = driver.findElementsByXPath("//dl[contains(.,'CMIU encryption revision')]/dd").get(0).getText();
            String bleRev = driver.findElementsByXPath("//dl[contains(.,'CMIU BLE revision')]/dd").get(0).getText();
            String arbRev = driver.findElementsByXPath("//dl[contains(.,'CMIU ARB revision')]/dd").get(0).getText();
            String telitRev = driver.findElementsByXPath("//dl[contains(.,'Telit firmware revision')]/dd").get(0).getText();

            revisionDetails.setFirmwareRev(firmwareRev);
            revisionDetails.setConfigRev(configRev);
            revisionDetails.setEncryptionRev(encryptionRev);
            revisionDetails.setBleRev(bleRev);
            revisionDetails.setArbRev(arbRev);
            revisionDetails.setTelitRev(telitRev);
        }
        catch (Exception e)
        {
            //Do nothing, when the CMIU calls in for the first time, we won't be able to
            //load the miu details page for a few seconds, as the details for the CMIU will
            //need to be created in the database. This means that the XPaths will return an
            //exception.
        }

        return revisionDetails;
    }

    private class RevisionDetails
    {
        private String firmwareRev;
        private String configRev;
        private String encryptionRev;
        private String bleRev;
        private String arbRev;
        private String telitRev;

        public String getFirmwareRev()
        {
            return firmwareRev;
        }

        public void setFirmwareRev(String firmwareRev)
        {
            this.firmwareRev = firmwareRev;
        }

        public String getConfigRev()
        {
            return configRev;
        }

        public void setConfigRev(String configRev)
        {
            this.configRev = configRev;
        }

        public String getEncryptionRev()
        {
            return encryptionRev;
        }

        public void setEncryptionRev(String encryptionRev)
        {
            this.encryptionRev = encryptionRev;
        }

        public String getBleRev()
        {
            return bleRev;
        }

        public void setBleRev(String bleRev)
        {
            this.bleRev = bleRev;
        }

        public String getArbRev()
        {
            return arbRev;
        }

        public void setArbRev(String arbRev)
        {
            this.arbRev = arbRev;
        }

        public String getTelitRev()
        {
            return telitRev;
        }

        public void setTelitRev(String telitRev)
        {
            this.telitRev = telitRev;
        }

        public boolean nonNullRevisionsMatch(RevisionDetails other)
        {
            if (firmwareRev != null && !firmwareRev.equals(other.firmwareRev))
            {
                return false;
            }
            if (configRev != null && !configRev.equals(other.configRev))
            {
                return false;
            }
            if (encryptionRev != null && !encryptionRev.equals(other.encryptionRev))
            {
                return false;
            }
            if (bleRev != null && !bleRev.equals(other.bleRev))
            {
                return false;
            }
            if (arbRev != null && !arbRev.equals(other.arbRev))
            {
                return false;
            }
            if (telitRev != null && !telitRev.equals(other.telitRev))
            {
                return false;
            }
            return true;
        }
    }
}
