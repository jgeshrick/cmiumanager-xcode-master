/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.cns;

/**
 * CMIU for integration test. This assumes that the cellular device is present in the Vzw
 */
public final class CmiuDetail
{
    private static final int cmiu = 400000102;
    private static final String iccid = "89148000001471859842";
    private static final String imei = "353238060024824";
    private static final String msisdn = "13344159614";
    private static final boolean active = true;

    public static int getCmiu()
    {
        return cmiu;
    }

    public static String getIccid()
    {
        return iccid;
    }

    public static String getImei()
    {
        return imei;
    }

    public static String getMsisdn()
    {
        return msisdn;
    }

    public static boolean isActive()
    {
        return active;
    }
}
