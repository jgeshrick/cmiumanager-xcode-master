/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.cns;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.cns.model.UsageHistory;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.net.URLEncoder;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Testing of Rest service for managing CMIU cellular device
 */
public class CmiuCellularManagementTest extends BaseSetupRestTest
{
    private final String URL_CMIU_START = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/" + CmiuDetail.getCmiu() + "/";
    private String token0;

    public CmiuCellularManagementTest()
    {
        super(400000102);
    }

    @Before
    public void setUp() throws Exception
    {
        this.token0 = fetchTokenForSiteId(0);
    }

    @Test
    public void testGetCmiuDetails() throws Exception
    {
        String responseString = HttpUtility.httpGet(URL_CMIU_START + "cellular-details" +
                "?token=" + this.token0);
        ObjectMapper mapper = new ObjectMapper();
        DeviceCellularInformation response = mapper.readValue(responseString, DeviceCellularInformation.class);

        assertEquals(CmiuDetail.getIccid(), response.getIccid());
        assertEquals("active", response.getDeviceState());
    }

    /**
     * Test getting usage data from VZW. Assumes that we would have some usage data from the selected cmiu for the past 60 days.
     * @throws Exception
     */
    @Test
    public void testGetUsageData() throws Exception
    {
        ZonedDateTime currentDateTime = ZonedDateTime.now();

        final String startDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(currentDateTime.minus(60, ChronoUnit.DAYS));
        final String endDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(currentDateTime);

        String responseString = HttpUtility.httpGet(URL_CMIU_START +
                "usage?start-date=" + URLEncoder.encode(startDate, "UTF-8") +
                "&end-date=" + URLEncoder.encode(endDate, "UTF-8") +
                "&token=" + this.token0);

        ObjectMapper mapper = new ObjectMapper();
        List<UsageHistory> usageHistoryList =
                mapper.readValue(responseString, new TypeReference<List<UsageHistory>>() { });

        assertFalse("Empty usage history list", usageHistoryList.isEmpty());
    }

    @Test
    @Ignore ("Suspend/restore has to follow Verizon provisioning rules")
    public void testSuspendCellularDevice() throws Exception
    {
        String responseString = HttpUtility.httpPost(URL_CMIU_START +
                "change-state?new-state=suspend" +
                "&token=" + this.token0);

        assertTrue(responseString.length() > 0);
    }

    @Test
    @Ignore ("Suspend/restore has to follow Verizon provisioning rules")
    public void testRestoreCellularDevice() throws Exception
    {
        String responseString = HttpUtility.httpPost(URL_CMIU_START +
                "change-state?new-state=restore" +
                "&token=" + this.token0);

        assertTrue(responseString.length() > 0);
    }

}
