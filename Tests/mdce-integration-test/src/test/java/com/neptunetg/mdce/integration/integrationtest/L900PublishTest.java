/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.GetPacketsResponse;
import com.neptunetg.mdce.common.framework.MiuResponse;
import com.neptunetg.mdce.common.framework.PacketResponse;
import com.neptunetg.mdce.common.simulator.L900SimulatorLauncher;
import com.neptunetg.mdce.common.utility.HttpUtility;
import org.junit.Test;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 01/06/2016.
 * Class to test L900 from L900 to Get Packets API IF 15
 */
public class L900PublishTest extends BaseAuthenticatingRestTest
{
    final int l900IdHttpSenet = 700000202;
    final int l900IdHttpActility = 700000204;

    L900SimulatorLauncher l900Sim = new L900SimulatorLauncher();

    @Test
    public void testL900PublishEndToEndHttpSenet() throws Exception
    {
        l900PublishEndToEnd(l900IdHttpSenet, false);
    }

    @Test
    public void testL900PublishEndToEndHttpActility() throws Exception
    {
        l900PublishEndToEnd(l900IdHttpActility, true);
    }

    private void l900PublishEndToEnd(int l900Id, boolean useActility) throws Exception
    {
        Instant minTime = Instant.now().minus(30, ChronoUnit.SECONDS);

        System.out.println("Running L900 Simulator from folder: " + Constants.L900_SIMULATOR_ZIP);

        l900Sim.runL900Simulator(Constants.MDCE_LORA_URL,
                l900Id, 1, 2, 10000, true, useActility);

        final String token = fetchTokenForSiteId(1);

        final String dateStr = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
                minTime.atOffset(ZoneOffset.ofHours(-5)))
                .replaceAll("\\+", "%2B");

        boolean foundL900 = false;
        boolean foundRecentL900Packet = false;

        for(int i=0; i<60; i++)
        {
            final String responseString = HttpUtility.httpGet(
                    Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/packet?token=" + token + "&min_insert_date=" + dateStr);

            final ObjectMapper mapper = new ObjectMapper();

            GetPacketsResponse packets = mapper.readValue(responseString, GetPacketsResponse.class);

            for (MiuResponse l900Response : packets.getMius())
            {
                if (l900Response.getMiuId() == l900Id)
                {
                    foundL900 = true;

                    for (PacketResponse packet : l900Response.getPackets())
                    {
                        Instant insertInstant = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(packet.getInsertDate(),
                                Instant::from);

                        if (insertInstant.isAfter(minTime))
                        {
                            foundRecentL900Packet = true;
                        }
                    }
                }
            }

            if (foundL900 && foundRecentL900Packet)
            {
                break;
            }

            Thread.sleep(1000);
        }

        assertTrue(foundL900);
        assertTrue(foundRecentL900Packet);
    }
}
