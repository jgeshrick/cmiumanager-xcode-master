/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.commandcontrol;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Selenium tests to verify behavior of the config set manager.
 */
public class CmiuConfigSetTest extends MdceWebSeleniumTestBase
{
    private static final String CONFIG_SET_URL = Constants.MDCE_WEB_URL + "pages/config-set";
    private static final String CONFIG_SET_IDS_XPATH = "//table[@id='cmiu-config-set-table']/tbody/tr/td[position()=2][string-length(text()) > 0]/../td[1]";
    private static final String REPORTING_INTERVAL_HOURS_XPATH = "//table[@id='cmiu-config-set-table']/tbody/tr/td[position()=1][text()='##ID##']/../td[5]";
    private static final String RECORDING_INTERVAL_MINUTES_XPATH = "//table[@id='cmiu-config-set-table']/tbody/tr/td[position()=1][text()='##ID##']/../td[11]";
    private static final String CMIU_MODE_NAME_XPATH = "//table[@id='cmiu-config-set-table']/tbody/tr/td[position()=1][text()='##ID##']/../td[3]";
    private static final String EDIT_LINK_XPATH = "//table[@id='cmiu-config-set-table']/tbody/tr/td[position()=1][text()='##ID##']/..//a[text()='Edit']";
    private static final String CMIU_MODE_NAME_INPUT_XPATH = "//input[@name='cmiuModeName']";
    private static final String SUBMIT_BUTTON_XPATH = "//input[@type='submit']";

    @Test
    public void testSetCmiuModeNames() throws IOException, InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

        driver.get(CONFIG_SET_URL);

        // Get a list of all config set IDs.
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'List CMIU config set')]")));

        List<WebElement> elements = driver.findElements(By.xpath(CONFIG_SET_IDS_XPATH));
        List<String> configSetIds = elements.stream().map(WebElement::getText).collect(Collectors.toList());

        // Set the CMIU Mode on each config set
        for (String configSetId : configSetIds)
        {
            Integer reportingIntervalHours = Integer.parseInt(
                    driver.findElement(By.xpath(
                            REPORTING_INTERVAL_HOURS_XPATH.replace("##ID##", configSetId))).getText());

            Integer recordingIntervalMinutes = Integer.parseInt(
                    driver.findElement(By.xpath(
                            RECORDING_INTERVAL_MINUTES_XPATH.replace("##ID##", configSetId))).getText());

            driver.findElement(By.xpath(EDIT_LINK_XPATH.replace("##ID##", configSetId))).click();

            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'Edit config set')]")));

            String expectedCmiuModeName = getCmiuModeName(recordingIntervalMinutes, reportingIntervalHours);

            // Enter the mode name
            driver.findElement(By.xpath(CMIU_MODE_NAME_INPUT_XPATH)).clear();
            driver.findElement(By.xpath(CMIU_MODE_NAME_INPUT_XPATH)).sendKeys(expectedCmiuModeName);
            driver.findElement(By.xpath(SUBMIT_BUTTON_XPATH)).click();

            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(), 'List CMIU config set')]")));

            // Verify it was set as expected
            String actualCmiuModeName = driver.findElement(By.xpath(
                    CMIU_MODE_NAME_XPATH.replace("##ID##", configSetId))).getText();

            assertEquals(expectedCmiuModeName, actualCmiuModeName);
        }

        takeScreenshot("CMIU Config Sets with CMIU Modes");
    }

    private static String getCmiuModeName(Integer recordingIntervalMinutes, Integer reportingIntervalHours)
    {
        if (recordingIntervalMinutes == 60 && reportingIntervalHours == 24)
        {
            return "Basic";
        }
        else if (recordingIntervalMinutes == 60 && reportingIntervalHours == 4)
        {
            return "Advanced";
        }
        else if (recordingIntervalMinutes == 15 && reportingIntervalHours == 1)
        {
            return "Pro";
        }
        else
        {
            return "Unknown";
        }
    }
}
