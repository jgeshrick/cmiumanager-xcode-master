/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.CmiuPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.GetPacketsResponse;
import com.neptunetg.mdce.common.framework.MiuResponse;
import com.neptunetg.mdce.common.framework.PacketResponse;
import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;
import com.neptunetg.mdce.common.utility.HttpUtility;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test that sends a packet to Mosquitto and then check that it can be read back from the API
 */
public class CmiuPublishTest extends BaseSetupRestTest
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final Duration PERMISSIBLE_CLOCK_SKEW = Duration.of(5L, ChronoUnit.SECONDS); //warn if more than 5s clock skew between client and server

    public CmiuPublishTest() {
        super(400000110);
    }

    /**
     * Send a packet to Mosquitto and then check that it can be read back from the API
     */
    @Test
    public void testCmiuPublish() throws Exception
    {

        final String token = fetchTokenForSiteId(64);
        ensureTestMiuExists(64, token);

        int timeoutForPacketsToAppearInListSeconds = 30;
        final int cmiuId = (int)getTestCmiuId();

        final CmiuSimulatorLauncher cmiuSim = new CmiuSimulatorLauncher();

        logger.info(String.format("Sending CMIU id: %d detailConfigPackets to MQTT broker at %s",
                cmiuId, Constants.BROKER_SERVER));
        int errorCode = cmiuSim.sendDetailedConfigPacket(cmiuId, Constants.BROKER_SERVER, ""); //packet type 0

        final Instant packetSentTimeClientTime = Instant.now();

        assertEquals("Cmiu Simulator terminated with error", 0, errorCode);


        long timeoutWaitingForPacketToFilterThroughUnixMilli = System.currentTimeMillis() + timeoutForPacketsToAppearInListSeconds * 1000L;
        MiuResponse miuResponse = null;
        final ObjectMapper mapper = new ObjectMapper();
        Instant minTime = packetSentTimeClientTime.minus(10, ChronoUnit.SECONDS);
        Duration clockSkew;
        do
        {

            //Wait for a second while the MDCE stores the detailed config packet from the simulator
            Thread.sleep(1 * 1000);

            //Get time maximumTimeErrorMargin seconds ago for get packet HTTP GET request
            final String dateStr = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
                    minTime.atOffset(ZoneOffset.ofHours(-5)))
                    .replaceAll("\\+", "%2B");

            final Instant clientTime = Instant.now();

            final String responseString = HttpUtility.httpGet(
                    Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/packet?token=" + token + "&min_insert_date=" + dateStr);

            final GetPacketsResponse response = mapper.readValue(responseString, GetPacketsResponse.class);

            final Instant serverTime = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(response.getServerTime(), Instant::from);

            clockSkew = Duration.between(serverTime, clientTime);
            if (clockSkew.abs().compareTo(PERMISSIBLE_CLOCK_SKEW) > 0)
            {
                logger.warn("Clock skew detected.  Client clock is at " + clockSkew + " relative to server clock");
            }
            minTime = serverTime.minus(Duration.between(packetSentTimeClientTime, clientTime)).minus(10, ChronoUnit.SECONDS); //next time, query relative to the server time of the packet send

            int miuIndex = -1;
            for (int i = 0; i < response.getMius().size(); i++)
            {
                if (response.getMius().get(i).getMiuId() == cmiuId)
                {
                    miuIndex = i;
                }
            }
            if (miuIndex >= 0)
            {
                miuResponse = response.getMius().get(miuIndex);
                assertEquals(cmiuId, miuResponse.getMiuId());

                if (miuResponse.getPackets().size() > 0)
                {
                    break;
                }
            }
        } while (System.currentTimeMillis() < timeoutWaitingForPacketToFilterThroughUnixMilli);

        assertEquals("Didn't find a unique packet in GET PACKETS response after retrying for " + timeoutForPacketsToAppearInListSeconds + "s", 1, miuResponse == null ? 0 : miuResponse.getPackets().size());

        PacketResponse packetResponse = miuResponse.getPackets().get(0);
        assertEquals("wrong packet type", 0, packetResponse.getPacketType());
        assertEquals("CMIU", packetResponse.getSourceDeviceType());

        String packetDataHex = packetResponse.getPacket();
        byte[] packetData = DatatypeConverter.parseHexBinary(packetDataHex);

        TaggedPacket taggedPacket = PacketParser.parseTaggedPacket(packetData);
        assertTrue(taggedPacket.tagCount() > 2);

        Iterator<TaggedData> taggedData = taggedPacket.iterator();
        assertEquals(true, taggedData.hasNext());

        TaggedData tag = taggedData.next();
        assertTrue(tag instanceof CmiuPacketHeaderData);

        CmiuPacketHeaderData packetHeader = (CmiuPacketHeaderData) tag;
        assertEquals(cmiuId, packetHeader.getCmiuId());

        final UnixTimestamp packetHeaderTimestamp = packetHeader.getCurrentTimeAndDate();
        final String packetHeaderTimestampString = readableUnixDate(packetHeaderTimestamp);
        logger.info("Packet header timestamp is: " + packetHeaderTimestampString);

        final Instant packetSentTimeServerTime = packetSentTimeClientTime.minus(clockSkew);

        assertTrue("Packet header timestamp is before sent time " + packetSentTimeClientTime + " (client time) " + packetSentTimeServerTime + " (server time)",
                packetHeaderTimestamp.asInstant().isAfter(packetSentTimeServerTime.minus(10L, ChronoUnit.SECONDS)));
        assertTrue("Packet header timestamp is after sent time " + packetSentTimeClientTime + " (client time) " + packetSentTimeServerTime + " (server time)",
                packetHeaderTimestamp.asInstant().isBefore(packetSentTimeServerTime.plus(10L, ChronoUnit.SECONDS)));
    }

    private static String readableUnixDate(UnixTimestamp ts)
    {
        return ts.toEpochMilli() + " (" + ts.asDate() + ")";
    }
}
