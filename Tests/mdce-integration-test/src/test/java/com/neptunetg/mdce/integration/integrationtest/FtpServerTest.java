/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.junit.Test;

import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 21/01/2016.
 * An integration test to check that the FTP server is working correctly
 */
public class FtpServerTest
{
    String ftpServerAddress = "54.191.137.243"; //Auto-test broker
    String ftpUsername = "cmiuftp";
    String ftpPassword = "s@g3nt!@Neptune$";
    String testFileName = "testfile.txt";
    String testFileContents = "Test file.\r\n\r\nThis file is used to test downloading files from vsFTPd.\r\n\r\nBy " +
            "downloading this file we can have greater confidence\r\nthat vsFTPd is configured correctly.\r\n\r\n";

    @Test
    public void testGetTestFileFromServer() throws Exception
    {
        FTPClient ftp = new FTPClient();

        ftp.connect(ftpServerAddress);
        ftp.enterLocalPassiveMode();

        assertTrue("Could not login",ftp.login(ftpUsername, ftpPassword));

        FTPFile[] ftpFiles = ftp.listFiles();

        boolean newFilePresent = false;

        for(FTPFile ftpFile: ftpFiles)
        {
            if(ftpFile.getName().toString().equals(testFileName))
            {
                newFilePresent = true;
            }
        }

        assertTrue("Test file not present", newFilePresent);

        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        ftp.retrieveFile(testFileName, byteOutputStream);
        byteOutputStream.flush();

        byte[] fileData = byteOutputStream.toByteArray();
        byte[] expectedFileData = testFileContents.getBytes();

        assertArrayEquals(expectedFileData, fileData);
    }
}

