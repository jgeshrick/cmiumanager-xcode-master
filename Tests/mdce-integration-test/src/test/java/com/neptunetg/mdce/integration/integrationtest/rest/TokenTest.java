/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.framework.PartnerKeys;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseAuthenticatingRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

/**
 * Test Interface: IF 03.
 */
public class TokenTest extends BaseAuthenticatingRestTest
{
    private final String urlStart = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/token";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Wrong partner key and no site id
     * @throws Exception
     */
    @Test
    public void testGetTokenWithInvalidPartnerKey() throws Exception
    {
        final String invalidPartnerKey = "123456789";

        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 400"));

        getToken(invalidPartnerKey, 0);
    }


    @Test
    public void testGetTokenForAllSitesWithInvalidPartnerKey() throws Exception
    {
        //correct key length
        final String invalidPartnerKey = "12345678901234567890123456789012";

        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 401"));

        getToken(invalidPartnerKey, 0);
    }

    @Test
    public void testGetTokenForAllSitesWithNonHexadecimalPartnerKey() throws Exception
    {
        // Not hex
        final String invalidPartnerKey = "IZEASG78901234567890123456789012";

        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 400"));

        getToken(invalidPartnerKey, 0);
    }

    @Test
    public void testGetTokenForSingleSite() throws Exception
    {
        String token = fetchTokenForSiteId(1);
        assertEquals(16, token.length());
        validateToken(token, 1);

        token = fetchTokenForSiteId(2);
        assertEquals(16, token.length());
        validateToken(token, 2);

        token = fetchTokenForSiteId(16);
        assertEquals(16, token.length());
        validateToken(token, 16);

        token = fetchTokenForSiteId(32);
        assertEquals(16, token.length());
        validateToken(token, 32);

        token = fetchTokenForSiteId(64);
        assertEquals(16, token.length());
        validateToken(token, 64);

    }

    private void validateToken(String token, int siteId) throws Exception {
        String getMiuListResponse = HttpUtility.httpGet(Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu_list?token=" + token);

        assertNotNull(getMiuListResponse);
        assertTrue(getMiuListResponse.startsWith("{\"mius\":["));

        List<Integer> siteList= JsonPath.read(getMiuListResponse, "$.mius..site_id");

        if (siteId != 0)
        {
            //returned miu list should only belong to one site
            assertTrue(siteList.stream().allMatch(i -> i == siteId));
        }
        else
        {
            //get a map with key as siteId, and value as counts of miu with the site id
            Map<Integer, Long> siteHistogram = siteList.stream()
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            assertTrue(siteHistogram.size() > 1);   //we should have more than 1 sites
        }
    }

    @Test
    public void testGetTokenFailsOnExtraneousLetters() throws Exception
    {
        String token = fetchTokenForSiteId(0);

        assertEquals(16, token.length());

        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 401"));

        validateToken(token + "duff", 0);
    }

    @Test
    public void testInvalidToken() throws Exception
    {
        String token = fetchTokenForSiteId(0);

        assertEquals(16, token.length());

        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 401"));

        validateToken("00112233445566778899", 0);
    }

    @Test
    public void testGetTokenForAllSites() throws Exception
    {
        String token = fetchTokenForSiteId(0);

        //check the length of the token. At the current moment, we cannot validate the value of the token
        assertEquals(16, token.length());

        validateToken(token, 0);
    }

    @Test
    public void testGetTokenFailsWithNegativeSiteId() throws Exception
    {

        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 401"));

        fetchTokenForSiteId(-3);
    }

    @Test
    public void testGetTokenFailsWithInvalidSiteId() throws Exception
    {

        final int invalidSiteId = 5;
        final String partnerKey = PartnerKeys.getPartnerKey(0);
        String url =  Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/token?site_id=" + invalidSiteId + "&partner=" + partnerKey;

        this.expectedException.expect(AssertionError.class);
        this.expectedException.expectMessage(containsString("Response code: 401"));

        String responseIncludingToken = HttpUtility.httpGet(url);
    }

    private String getToken(String partnerKey, int siteId) throws Exception
    {
        return HttpUtility.httpGet(urlStart +
                "?partner=" + partnerKey +
                "&site_id=" + siteId);
    }


}
