/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test interface: IF29 Set Miu ownership
 */
public class CmiuOwnershipTest extends BaseSetupRestTest
{
    private final String urlMiuOwnership = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/ownership";
    private final String urlMiuOwnerHistory = Constants.MDCE_INTEGRATION_URL + "/mdce/api/v1/miu/owner_history";

    public CmiuOwnershipTest()
    {
        super(400000102);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test(timeout = 120000)
    public void testSetMiuOwnership() throws Exception
    {

        createMiuAsSiteIdZero();

        final String token = fetchTokenForSiteId(2);

        final String originalRequestBody = "{\n" +
                "\t\"site_id\": 2,\n" +
                "\t\"mius\": [\n" +
                "   \t{\n" +
                "   \t\t\"miu_id\" : 400000102,\n" +
                "   \t\t\"active\" : \"Y\"\n" +
                "   \t}" +
//                ",\n" +
//                "   \t{\n" +
//                "   \t\t\"miu_id\" : 400000104,\n" +
//                "   \t\t\"active\" : \"N\"\n" +
//                "   \t}\n" +
                "   ]\n" +
                "}\n";

        String responseString = HttpUtility.httpJsonPut(urlMiuOwnership + "?token=" + token, originalRequestBody);

        assertEquals("{\"site_id\":2,\"mius\":[{\"miu_id\":400000102,\"active\":\"Y\",\"owned\":\"true\",\"set_ownership_result\":\"successfully_claimed\"}]}", responseString);
//        assertEquals("{\"site_id\":2,\"mius\":[{\"miu_id\":400000102,\"active\":\"Y\",\"owned\":\"true\",\"set_ownership_result\":\"successfully_claimed\"},{\"miu_id\":400000104,\"active\":\"N\",\"owned\":\"false\",\"set_ownership_result\":\"conflict\"}]}", responseString);

    }

    @Test(timeout = 120000)
    public void testSetMiuOwnershipInvalidSiteIdReturnForbiddenResultCode() throws Exception
    {
        createMiuAsSiteIdZero();

        final String token = fetchTokenForSiteId(2);

        final String originalRequestBody = "{\n" +
                "\t\"site_id\": 1,\n" +
                "\t\"mius\": [\n" +
                "   \t{\n" +
                "   \t\t\"miu_id\" : 400000102,\n" +
                "   \t\t\"active\" : \"Y\"\n" +
                "   \t},\n" +
                "   \t{\n" +
                "   \t\t\"miu_id\" : 400000104,\n" +
                "   \t\t\"active\" : \"N\"\n" +
                "   \t}\n" +
                "   ]\n" +
                "}\n";

        this.expectedException.expect(AssertionError.class);

        String responseString = HttpUtility.httpJsonPut(urlMiuOwnership + "?token=" + token, originalRequestBody);

        assertEquals(403, (int)JsonPath.read(403, "$.ResultCode"));
    }

    @Test(timeout = 120000)
    public void testSetMiuOwnershipFindsAlreadyClaimed() throws Exception
    {

        createMiuAsSiteIdZero();

        final String token = fetchTokenForSiteId(2);

        //claim ownership
        String response = assignMiuToSiteId(getTestCmiuId(), 2, token);
        assertNotNull(response);
        assertEquals(2, (int)(JsonPath.read(response, "$.site_id")));
        assertEquals("successfully_claimed", JsonPath.read(response, "$.mius[0].set_ownership_result"));
        assertEquals(400000102, (int)JsonPath.read(response, "$.mius[0].miu_id"));
        assertEquals("true", JsonPath.read(response, "$.mius[0].owned"));

        //try to claim again
        response = assignMiuToSiteId(getTestCmiuId(), 2, token);
        assertNotNull(response);

        assertEquals("{\"site_id\":2,\"mius\":[{\"miu_id\":400000102,\"active\":\"Y\",\"owned\":\"true\",\"set_ownership_result\":\"already_owned\"}]}", response);
        assertEquals(2, (int)(JsonPath.read(response, "$.site_id")));
        assertEquals("already_owned", JsonPath.read(response, "$.mius[0].set_ownership_result"));
        assertEquals(400000102, (int)JsonPath.read(response, "$.mius[0].miu_id"));
        assertEquals("true", JsonPath.read(response, "$.mius[0].owned"));
    }

    @Test(timeout = 120000)
    public void testSetMiuOwnershipFailsWithConflict() throws Exception
    {

        createMiuAsSiteIdZero();

        final String token2 = fetchTokenForSiteId(2);

        assignMiuToSiteId(getTestCmiuId(), 2, token2);

        final String token16 = fetchTokenForSiteId(16);

        final String response = assignMiuToSiteId(getTestCmiuId(), 16, token16);

        assertNotNull(response);
        assertEquals("{\"site_id\":16,\"mius\":[{\"miu_id\":400000102,\"active\":\"Y\",\"owned\":\"false\",\"set_ownership_result\":\"conflict\"}]}", response);
    }


    @Test(timeout = 120000)
    public void testMiuOwnerHistory() throws Exception
    {
        // Set site_id to 0 and verify that it appears as the last entry in the history
        createMiuAsSiteIdZero();

        final String universalToken = fetchTokenForSiteId(0);
        int lastSequence = -1;

        int lastMiuId = -1;
        int lastSiteId = -1;

        String responseString = HttpUtility.httpGet(urlMiuOwnerHistory +"?token=" + universalToken + "&last_sequence=" + lastSequence);
        int jsonHistoryCount = ((List<Object>)JsonPath.read(responseString, "$.owner_history")).size();

        if (jsonHistoryCount != 0)
        {
            lastMiuId = JsonPath.read(responseString, "$.owner_history[" + (jsonHistoryCount - 1) + "].miu_id");
            lastSiteId = JsonPath.read(responseString, "$.owner_history[" + (jsonHistoryCount - 1) + "].site_id");
        }

        assertEquals(400000102, lastMiuId);
        assertEquals(0, lastSiteId);

        // Now set site_id to 2 and check the last history entry again.
        final String token = fetchTokenForSiteId(2);

        final String originalRequestBody = "{\n" +
                "\t\"site_id\": 2,\n" +
                "\t\"mius\": [\n" +
                "   \t{\n" +
                "   \t\t\"miu_id\" : 400000102,\n" +
                "   \t\t\"active\" : \"Y\"\n" +
                "   \t}" +
                "   ]\n" +
                "}\n";

        HttpUtility.httpJsonPut(urlMiuOwnership + "?token=" + token, originalRequestBody);

        lastMiuId = -1;
        lastSiteId = -1;

        responseString = HttpUtility.httpGet(urlMiuOwnerHistory +"?token=" + universalToken + "&last_sequence=" + lastSequence);
        jsonHistoryCount = ((List<Object>)JsonPath.read(responseString, "$.owner_history")).size();

        if (jsonHistoryCount != 0)
        {
            lastMiuId = JsonPath.read(responseString, "$.owner_history[" + (jsonHistoryCount - 1) + "].miu_id");
            lastSiteId = JsonPath.read(responseString, "$.owner_history[" + (jsonHistoryCount - 1) + "].site_id");
        }

        assertEquals(400000102, lastMiuId);
        assertEquals(2, lastSiteId);
    }


    private int getLastSequenceId(String responseString)
    {
        int jsonLifecycleCount = ((List<Object>)JsonPath.read(responseString, "$.owner_history")).size();
        int lastSequenceId = -1;

        if (jsonLifecycleCount != 0)
        {
            lastSequenceId = JsonPath.read(responseString, "$[" + (jsonLifecycleCount - 1) + "].sequence_id");
        }

        return lastSequenceId;
    }

}
