/*
 * Neptune Technology Group
 * Copyright 2016 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.integration.integrationtest.rest;

import com.jayway.jsonpath.JsonPath;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.HttpUtility;
import com.neptunetg.mdce.integration.integrationtest.BaseSetupRestTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test Interface:
 * IF 46 Get lifecycle history.
 */
public class MiuLifecycleHistoryTest extends BaseSetupRestTest
{

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public MiuLifecycleHistoryTest() throws Exception
    {
        super(400000102, Duration.ofSeconds(1L));
    }

    /**
     * Test getting the first page of lifecycle history.
     */
    @Test
    public void testGetLifecycleHistory() throws Exception
    {
        final String token = fetchTokenForSiteId(0);

        ensureTestMiuExists(1, token);

        final String responseString = getMiuLifecycleHistory(null, token);

        assertTrue(StringUtils.hasText(responseString));

        assertApiCompliantResponse(responseString);
    }

    /**
     * Test that no results are returned if the client already has the last sequence ID.
     */
    @Test
    public void testGetLifecycleHistory_ReturnsSingleResult_WhenPenultimateSequenceIdIsSupplied() throws Exception
    {
        final String token = fetchTokenForSiteId(0);
        ensureTestMiuExists(1, token);
        String responseString;
        Integer lastSequenceId = null;

        // Run through all results to get the last sequence ID
        while(true)
        {
            responseString = getMiuLifecycleHistory(lastSequenceId, token);
            int lastSequenceIdInResult = getLastSequenceId(responseString);

            if (lastSequenceIdInResult == -1)
            {
                break;
            }
            else
            {
                lastSequenceId = lastSequenceIdInResult;
            }
        }

        // Check that only one result is returned when the penultimate sequence ID is requested
        assertNotNull(lastSequenceId);
        final int penultimateSequenceId = lastSequenceId - 1;
        responseString = getMiuLifecycleHistory(penultimateSequenceId, token);
        int resultCount = ((List<Object>)JsonPath.read(responseString, "$")).size();

        assertEquals(1, resultCount);
    }

    @Test
    public void testAddLifecycleHistory_ReturnsSuccess_WhenSuppliedLifecycleStateIsValid() throws Exception
    {
        final String token = fetchTokenForSiteId(0);
        ensureTestMiuExists(1, token);
        String responseString;
        Integer lastSequenceId = null;

        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        final String timestamp = ZonedDateTime.ofInstant(Instant.now().minus(Duration.ofDays(60)), ZoneId.of("UTC")).format(dtf);

        final String jsonRequest = "{\"lifecycle_states\":[{"+
                "\"ref_id\":1,"+
                "\"miu_id\":400000102,"+
                "\"lifecycle_state\":\"Scanned\","+
                "\"timestamp\":\"" + timestamp + "\""+
        "}]}";

        // Act
        final String jsonResponse = addMiuLifecycleHistory(jsonRequest, token);

        // First assertion: the response shows successful insert
        final int successCount = JsonPath.read(jsonResponse, "$.succeeded");
        assertEquals(1, successCount);

        // Run through all results to get the last sequence ID
        int lastMiuId = -1;
        String lastLifecycleState = null;
        String lastTimestamp = null;

        while(true)
        {
            responseString = getMiuLifecycleHistory(lastSequenceId, token);
            int lastSequenceIdInResult = getLastSequenceId(responseString);

            if (lastSequenceIdInResult == -1)
            {
                break;
            }
            else
            {
                lastSequenceId = lastSequenceIdInResult;
                lastMiuId = getLastMiuId(responseString);
                lastLifecycleState = getLastMiuLifecycleState(responseString);
                lastTimestamp = getLastTimestamp(responseString);
            }
        }

        // Assert that the new state was appended to the end of the lifecycle history
        assertEquals(400000102, lastMiuId);
        assertEquals("SCANNED", lastLifecycleState);
        assertEquals(timestamp, lastTimestamp);
    }

    @Test
    public void testAddLifecycleHistory_ReturnsError_WhenSuppliedMiuIdIsNotKnown() throws Exception
    {
        final String token = fetchTokenForSiteId(0);
        ensureTestMiuExists(1, token);
        String responseString;
        Integer lastSequenceId = null;

        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        final String timestamp = ZonedDateTime.ofInstant(Instant.now().minus(Duration.ofDays(60)), ZoneId.of("UTC")).format(dtf);

        final String jsonRequest = "{\"lifecycle_states\":[{"+
                "\"ref_id\":95,"+
                "\"miu_id\":909090909,"+
                "\"lifecycle_state\":\"Scanned\","+
                "\"timestamp\":\"" + timestamp + "\""+
                "}]}";

        // Act
        final String jsonResponse = addMiuLifecycleHistory(jsonRequest, token);

        // First assertion: the response shows failed insert
        final int successCount = JsonPath.read(jsonResponse, "$.succeeded");
        final int errorCount = JsonPath.read(jsonResponse, "$.failed");
        final int errorRef = JsonPath.read(jsonResponse, "$.error_details[0].ref_id");
        assertEquals(0, successCount);
        assertEquals(1, errorCount);
        assertEquals(95, errorRef);

        // Run through all results to get the last sequence ID
        String lastTimestamp = null;

        while(true)
        {
            responseString = getMiuLifecycleHistory(lastSequenceId, token);
            int lastSequenceIdInResult = getLastSequenceId(responseString);

            if (lastSequenceIdInResult == -1)
            {
                break;
            }
            else
            {
                lastSequenceId = lastSequenceIdInResult;
                lastTimestamp = getLastTimestamp(responseString);
            }
        }

        // Assert that the new state was NOT appended to the lifecycle history
        assertNotEquals(timestamp, lastTimestamp);
    }

    public String addHistoricMiuLifecycleState(Integer miuId, String lifecycleState, Instant timestamp) throws Exception
    {
        final String token = fetchTokenForSiteId(0);
        ensureTestMiuExists(1, token);
        String responseString;
        Integer lastSequenceId = null;

        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        final String formattedTimestamp = ZonedDateTime.ofInstant(timestamp, ZoneId.of("UTC")).format(dtf);

        final String jsonRequest = "{\"lifecycle_states\":[{"+
                "\"ref_id\":1,"+
                "\"miu_id\":" + miuId.toString() + ","+
                "\"lifecycle_state\":\"" + lifecycleState + "\","+
                "\"timestamp\":\"" + formattedTimestamp + "\""+
                "}]}";

        // Act
        return addMiuLifecycleHistory(jsonRequest, token);
    }

    protected String getMiuLifecycleHistory(Integer lastSequenceId, String token) throws Exception
    {
        String url = Constants.MDCE_INTEGRATION_URL +
                "mdce/api/v1/miu/lifecycle_history" + "?token=" + token +
                (lastSequenceId != null ? "&last_sequence=" + lastSequenceId : "");

        return HttpUtility.httpGet(url);
    }

    protected String addMiuLifecycleHistory(String jsonRequest, String token) throws Exception
    {
        String url = Constants.MDCE_INTEGRATION_URL +
                "mdce/api/v1/miu/lifecycle_history" + "?token=" + token;

        return HttpUtility.httpJsonPost(url, jsonRequest);
    }

    private void assertApiCompliantResponse(String responseString)
    {
        int jsonLifecycleCount = ((List<Object>)JsonPath.read(responseString, "$")).size();
        int lastSequenceId = -1;

        for (int i = 0; i < jsonLifecycleCount; i++)
        {
            final int sequenceId = JsonPath.read(responseString, "$[" + i + "].sequence_id");
            assertNotNull(JsonPath.read(responseString, "$[" + i + "].miu_id"));
            assertNotNull(JsonPath.read(responseString, "$[" + i + "].state"));
            final Instant timestamp = Instant.parse(JsonPath.read(responseString, "$[" + i + "].timestamp"));

            assertTrue("sequence_ids expected to be returned in ascending order", lastSequenceId < sequenceId);
            assertNotNull(timestamp);

            lastSequenceId = sequenceId;
        }
    }

    private int getLastSequenceId(String responseString)
    {
        int jsonLifecycleCount = ((List<Object>)JsonPath.read(responseString, "$")).size();
        int lastSequenceId = -1;

        if (jsonLifecycleCount != 0)
        {
            lastSequenceId = JsonPath.read(responseString, "$[" + (jsonLifecycleCount - 1) + "].sequence_id");
        }

        return lastSequenceId;
    }

    private int getLastMiuId(String responseString)
    {
        int jsonLifecycleCount = ((List<Object>)JsonPath.read(responseString, "$")).size();
        int lastMiuId = -1;

        if (jsonLifecycleCount != 0)
        {
            lastMiuId = JsonPath.read(responseString, "$[" + (jsonLifecycleCount - 1) + "].miu_id");
        }

        return lastMiuId;
    }

    private String getLastMiuLifecycleState(String responseString)
    {
        int jsonLifecycleCount = ((List<Object>)JsonPath.read(responseString, "$")).size();
        String lastMiuLifecycleState = null;

        if (jsonLifecycleCount != 0)
        {
            lastMiuLifecycleState = JsonPath.read(responseString, "$[" + (jsonLifecycleCount - 1) + "].state");
        }

        return lastMiuLifecycleState;
    }

    private String getLastTimestamp(String responseString)
    {
        int jsonLifecycleCount = ((List<Object>)JsonPath.read(responseString, "$")).size();
        String lastTimestamp = null;

        if (jsonLifecycleCount != 0)
        {
            lastTimestamp = JsonPath.read(responseString, "$[" + (jsonLifecycleCount - 1) + "].timestamp");
        }

        return lastTimestamp;
    }
}