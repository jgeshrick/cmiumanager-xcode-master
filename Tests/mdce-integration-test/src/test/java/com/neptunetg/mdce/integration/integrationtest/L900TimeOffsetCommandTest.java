/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.integration.integrationtest;

import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.simulator.L900SimulatorLauncher;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 01/06/2016.
 * Class to test automatic Time And Date Offset packet
 */
public class L900TimeOffsetCommandTest extends BaseAuthenticatingRestTest
{
    final int l900IdSenet = 700000210;
    final int l900IdActility = 700000212;

    L900SimulatorLauncher l900Sim = new L900SimulatorLauncher();

    @Test
    public void testL900SendTimeThenReceiveTimeOffsetHttpSenet() throws Exception
    {
         SendTimeThenReceiveTimeOffsetHttp(l900IdSenet, false);
    }

    @Test
    public void testL900SendTimeThenReceiveTimeOffsetHttpActility() throws Exception
    {
        SendTimeThenReceiveTimeOffsetHttp(l900IdActility, true);
    }

    public void SendTimeThenReceiveTimeOffsetHttp(int l900Id, boolean useActility) throws Exception
    {
        String simOutput = l900Sim.runL900Simulator(Constants.MDCE_LORA_URL, l900Id, 1, 12, 10000, true, useActility);

        Pattern receivedHexPattern = Pattern.compile("L900 time error is now: ([0-9]+)");
        Matcher m = receivedHexPattern.matcher(simOutput);

        assertTrue(m.find());

        Integer timeError = Integer.parseInt(m.group(1));

        //Assert that the reported time error is now less than 5 seconds
        assertTrue(Math.abs(timeError) < 5);
    }
}
