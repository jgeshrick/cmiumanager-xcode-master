/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.mdce.web.integrationtest.alert;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.neptunetg.common.aws.MdceIpcPublishService;
import com.neptunetg.common.aws.SqsMdceIpcPublishService;
import com.neptunetg.mdce.common.framework.Constants;
import com.neptunetg.mdce.common.utility.AwsCredentialFactory;
import com.neptunetg.mdce.web.integrationtest.MdceWebSeleniumTestBase;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test alerts originating from SQS to mdce endpoint.
 */
public class AlertingSQSToEndTest extends MdceWebSeleniumTestBase
{
    private static final int SQS_MESSAGE_PROPAGATION_DELAY_TIME_MILLIS = 20 * 1000;
    private final String alertListUrl = Constants.MDCE_WEB_URL + "/pages/alert/list";

    private MdceIpcPublishService awsQueueService;

    @Before
    public void setUp()
    {
        AwsCredentialFactory awsCredentialProvider = new AwsCredentialFactory(true);
        AWSCredentialsProvider credentialsProvider = awsCredentialProvider.getCredentialProvider();
        final Properties junitEnvProperties = new Properties();
        junitEnvProperties.setProperty("env.name", Constants.ENV_NAME);
        junitEnvProperties.setProperty("mdce.scheduler.localMode", "false");

        try
        {
            awsQueueService = new SqsMdceIpcPublishService(credentialsProvider, junitEnvProperties);
        }
        catch (ParseException e)
        {
            throw new RuntimeException("Error initialising test", e);
        }

        super.setUp();
    }


    @Test
    public void testNonCloudwatchAlerts() throws InterruptedException
    {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, 20);

        String alertString = "NonCloudWatchAlertIntegrationTest/" + ZonedDateTime.now();

        System.out.println("testNonCloudwatchAlerts alert string: " + alertString);

        awsQueueService.sendMessage(MdceIpcPublishService.ALERTS, generateSqsMessage(alertString, "Warning"));
        //Wait for SQS message to work through the system
        Thread.sleep(SQS_MESSAGE_PROPAGATION_DELAY_TIME_MILLIS);
        driver.get(alertListUrl);


        checkIfRequiredLogin();

        takeScreenshot("Alert list - new");

        //Check that the alert has been added
        List<WebElement> testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='newAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(1, testAlertElements.size());

        awsQueueService.sendMessage(MdceIpcPublishService.ALERTS, generateSqsMessage(alertString, "Okay"));

        //Wait for SQS message to work through the system
        Thread.sleep(SQS_MESSAGE_PROPAGATION_DELAY_TIME_MILLIS);

        driver.get(alertListUrl);
        takeScreenshot("Alert list - stale");

        //Check that the alert has been moved from new to stale
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='staleAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(1, testAlertElements.size());
        testAlertElements = driver.findElementsByXPath("//table[@data-form-id='newAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(0, testAlertElements.size());

        //Click through to the view path of the alert, and then press the clear alert button
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='staleAlertForm']/tbody/tr[contains(. , '" + alertString + "')]/td[@class='list-content-note ui-selectee']/a");
        assertEquals(1, testAlertElements.size());
        testAlertElements.get(0).click();
        testAlertElements = getElementsByXPath(webDriverWait, "/html/body/section/a[contains(. , 'Clear Alert')]");
        assertEquals(1, testAlertElements.size());
        testAlertElements.get(0).click();

        takeScreenshot("Alert list - cleared");

        //Check that the alert has moved from stale to cleared
        testAlertElements = driver.findElementsByXPath("//table[@data-form-id='staleAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(0, testAlertElements.size());
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='clearedAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(1, testAlertElements.size());

        awsQueueService.sendMessage(MdceIpcPublishService.ALERTS, generateSqsMessage(alertString, "Error"));

        //Wait for SQS message to work through the system
        Thread.sleep(SQS_MESSAGE_PROPAGATION_DELAY_TIME_MILLIS);
        driver.get(alertListUrl);
        takeScreenshot("Alert list - re-new");

        //Check that a new alert has been added under new
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='newAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(1, testAlertElements.size());
        testAlertElements = driver.findElementsByXPath("//table[@data-form-id='clearedAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(1, testAlertElements.size());

        //Click through to the view path of the alert, and then add a ticket
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='newAlertForm']/tbody/tr[contains(. , '" + alertString + "')]/td[@class='list-content-note ui-selectee']/a");
        assertEquals(1, testAlertElements.size());
        testAlertElements.get(0).click();
        testAlertElements = getElementsByXPath(webDriverWait, "//*[@id='alertTicketId']");
        assertEquals(1, testAlertElements.size());
        testAlertElements.get(0).sendKeys("IntegrationTestTicket123");
        testAlertElements = getElementsByXPath(webDriverWait, "//*[@id='add-ticket']");
        assertEquals(1, testAlertElements.size());
        testAlertElements.get(0).click();
        testAlertElements = getElementsByXPath(webDriverWait, "/html/body/section/a[contains(. , 'Back to Alerts List')]");
        assertEquals(1, testAlertElements.size());
        testAlertElements.get(0).click();

        takeScreenshot("Alert list - handled");

        //Check that the alert has been moved from new to handled
        testAlertElements = driver.findElementsByXPath("//table[@data-form-id='newAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(0, testAlertElements.size());
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='handledAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(1, testAlertElements.size());

        //Select checkbox for alert, and then click the Move Selected Alerts to Cleared button
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='handledAlertForm']/tbody/tr[contains(. , '" + alertString + "')]/td[@class='ui-selectee']/input[@class='alert-selected-checkbox ui-selectee']");
        assertEquals(1, testAlertElements.size());
        testAlertElements.get(0).click();
        testAlertElements = getElementsByXPath(webDriverWait, "//section/div[../form[@id='handledAlertForm']]//a[@class='button right clear-alert-button']");
        assertFalse(testAlertElements.isEmpty());
        testAlertElements.get(0).click();
        testAlertElements = getElementsByXPath(webDriverWait, "//button[contains(. , 'Clear alerts')]");
        assertFalse(testAlertElements.isEmpty());
        testAlertElements.get(0).click();


        //Check the alert has been moved from handled to cleared
        Thread.sleep(3 * 1000); //Wait for previous click to have effect
        takeScreenshot("Alert list - clear handled");

        testAlertElements = driver.findElementsByXPath("//table[@data-form-id='handledAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(0, testAlertElements.size());
        testAlertElements = getElementsByXPath(webDriverWait, "//table[@data-form-id='clearedAlertForm']/tbody/tr[contains(. , '" + alertString + "')]");
        assertEquals(2, testAlertElements.size());

    }

    private static String generateSqsMessage(String alertString, String level)
    {
        return "{\"Source\":\"" + alertString + "\"," +
                "\"Level\":\"" + level + "\"," +
                "\"Message\":\"This is a test alert\"}";
    }

    private List<WebElement> getElementsByXPath(WebDriverWait webDriverWait, String xPath)
    {
        waitForPageElementWithRetries(webDriverWait, xPath);
        return driver.findElementsByXPath(xPath);
    }

    private void waitForPageElementWithRetries(final WebDriverWait webDriverWait, final String xPath)
    {
        final int MAX_ALLOWABLE_ATTEMPTS = 6;
        int attempts = 0;

        do
        {
            try
            {
                Thread.sleep(1000L);
                webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));
                return; //found
            }
            catch (TimeoutException | InterruptedException ex)
            {
                driver.navigate().refresh();
            }
        }
        while (++attempts < MAX_ALLOWABLE_ATTEMPTS);

        //failed to find the element
        throw new TimeoutException("Timeout waiting for elements by xpath: " + xPath);
    }
}

