/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.mdce.common.framework;

import com.neptunetg.mdce.common.internal.InternalApiException;
import com.neptunetg.mdce.common.internal.client.SingleServerClient;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;

@Service
public class BackendManager {

    private URL backendUrl;

    BackendManager() throws MalformedURLException
    {
        backendUrl = new URL(Constants.MDCE_SERVICE_URL);
    }

    public <T> T createClient(Class<T> interfaceClass) throws InternalApiException {

        // Create new client of required type
        return (T) new SingleServerClient("mdce-integration", backendUrl);
    }
}