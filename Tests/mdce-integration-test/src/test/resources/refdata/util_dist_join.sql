SELECT dist.customer_number, info2.account_name, info2.account_id, info.account_name, info.account_id, util.site_id
FROM
	  mdce.ref_data_info as info,
    mdce.ref_data_info as info2,
	  mdce.ref_data_utilities as util,
    mdce.ref_data_distributer as dist
WHERE util.info_id = info.ref_data_info_id
AND info.parent_customer_number = dist.customer_number
AND dist.info_id = info2.ref_data_info_id