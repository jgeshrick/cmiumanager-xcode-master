/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.l900integrationtest;

import com.neptunetg.common.l900c.PacketHandlerLora11ByteConfigStruct;
import com.neptunetg.common.l900integrationtest.framework.PacketHandlerTestBase;
import com.neptunetg.common.lora.pdu.BasicConfigPacket;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 09/08/2016.
 * A test for parsing / building the 11 Byte Config packet for L900
 */
public class BasicConfigPacketTest extends PacketHandlerTestBase
{
    @Test
    public void testBasicConfigPacketJavaToC()
    {
        byte networkTimeLoss = 1;
        byte antennaType = 1;
        byte l900Errors = 2;
        byte batteryStatus = 2;
        byte magSwipe = 1;
        short firmwareVersion = 12345;
        byte deviceType = 14;
        long deviceId = 4321543;
        byte reserved_1 = 1;
        byte reverseFlow_35Day = 1;
        byte reverseFlow_24Hour = 2;
        byte leakState = 3;
        byte batteryLife = 1;
        byte emptyPipe = 2;
        byte excessiveFlow = 3;
        byte tamper = 1;
        byte reserved_2 = 1;
        byte attachedDeviceAlarmMask = 7;

        BasicConfigPacket packet = new BasicConfigPacket(networkTimeLoss, l900Errors, antennaType, batteryStatus, magSwipe,
                firmwareVersion, deviceType, deviceId, reserved_1, reverseFlow_35Day, reverseFlow_24Hour, leakState,
                batteryLife, emptyPipe, excessiveFlow, tamper, reserved_2, attachedDeviceAlarmMask);

        packetHandler.PacketHandler_DLL_Init();

        byte packetBytes[] = packet.getBytes();
        Pointer pointerToPacketBytes = new Memory(packetBytes.length);
        pointerToPacketBytes.write(0, packetBytes, 0, packetBytes.length);

        PacketHandlerLora11ByteConfigStruct packetHandlerLora11ByteConfigStruct = new PacketHandlerLora11ByteConfigStruct();
        packetHandlerLora11ByteConfigStruct.packetBuffer = new byte[11];
        packetHandler.PacketHandler_DLL_Parse_LoRa_Config_11Byte(pointerToPacketBytes, packetHandlerLora11ByteConfigStruct);

        assertEquals("packet type", 0, packetHandlerLora11ByteConfigStruct.packetType);
        assertEquals("network time loss", networkTimeLoss, packetHandlerLora11ByteConfigStruct.networkTimeLoss);
        assertEquals(antennaType, packetHandlerLora11ByteConfigStruct.antennaType);
        assertEquals(l900Errors, packetHandlerLora11ByteConfigStruct.l900errors);
        assertEquals(batteryStatus, packetHandlerLora11ByteConfigStruct.batteryStatus);
        assertEquals(magSwipe, packetHandlerLora11ByteConfigStruct.magSwipe);
        assertEquals(firmwareVersion, packetHandlerLora11ByteConfigStruct.firmwareVersion);
        assertEquals(deviceType, packetHandlerLora11ByteConfigStruct.attachedDeviceType);
        assertEquals(deviceId, packetHandlerLora11ByteConfigStruct.attachedDeviceId);
        assertEquals(reserved_1, packetHandlerLora11ByteConfigStruct.reserved_1);
        assertEquals(reverseFlow_35Day, packetHandlerLora11ByteConfigStruct.reverseFlow_35Day);
        assertEquals(reverseFlow_24Hour, packetHandlerLora11ByteConfigStruct.reverseFlow_24Hour);
        assertEquals(leakState, packetHandlerLora11ByteConfigStruct.leakState);
        assertEquals(batteryLife, packetHandlerLora11ByteConfigStruct.batteryLife);
        assertEquals(emptyPipe, packetHandlerLora11ByteConfigStruct.emptyPipe);
        assertEquals(excessiveFlow, packetHandlerLora11ByteConfigStruct.excessiveFlow);
        assertEquals(tamper, packetHandlerLora11ByteConfigStruct.tamper);
        assertEquals(reserved_2, packetHandlerLora11ByteConfigStruct.reserved_2);
        assertEquals(attachedDeviceAlarmMask, packetHandlerLora11ByteConfigStruct.attachedDeviceAlarmMask);
    }

    @Test
    public void testBasicConfigPacketCToJava()
    {
        byte networkTimeLoss = 1;
        byte antennaType = 1;
        byte l900Errors = 2;
        byte batteryStatus = 2;
        byte magSwipe = 1;
        short firmwareVersion = 12345;
        byte deviceType = 14;
        long deviceId = 4321543;
        byte reserved_1 = 1;
        byte reverseFlow_35Day = 1;
        byte reverseFlow_24Hour = 2;
        byte leakState = 3;
        byte batteryLife = 1;
        byte emptyPipe = 2;
        byte excessiveFlow = 3;
        byte tamper = 1;
        byte reserved_2 = 1;
        byte attachedDeviceAlarmMask = 7;

        packetHandler.PacketHandler_DLL_Init();
        PacketHandlerLora11ByteConfigStruct packetHandlerLora11ByteConfigStruct = new PacketHandlerLora11ByteConfigStruct();
        packetHandlerLora11ByteConfigStruct.packetType = 0;
        packetHandlerLora11ByteConfigStruct.networkTimeLoss = networkTimeLoss;
        packetHandlerLora11ByteConfigStruct.antennaType = antennaType;
        packetHandlerLora11ByteConfigStruct.l900errors = l900Errors;
        packetHandlerLora11ByteConfigStruct.batteryStatus = batteryStatus;
        packetHandlerLora11ByteConfigStruct.magSwipe = magSwipe;
        packetHandlerLora11ByteConfigStruct.firmwareVersion = firmwareVersion;
        packetHandlerLora11ByteConfigStruct.attachedDeviceType = deviceType;
        packetHandlerLora11ByteConfigStruct.attachedDeviceId = deviceId;
        packetHandlerLora11ByteConfigStruct.reserved_1 = reserved_1;
        packetHandlerLora11ByteConfigStruct.reverseFlow_35Day = reverseFlow_35Day;
        packetHandlerLora11ByteConfigStruct.reverseFlow_24Hour = reverseFlow_24Hour;
        packetHandlerLora11ByteConfigStruct.leakState = leakState;
        packetHandlerLora11ByteConfigStruct.batteryLife = batteryLife;
        packetHandlerLora11ByteConfigStruct.emptyPipe = emptyPipe;
        packetHandlerLora11ByteConfigStruct.excessiveFlow = excessiveFlow;
        packetHandlerLora11ByteConfigStruct.tamper = tamper;
        packetHandlerLora11ByteConfigStruct.reserved_2 = reserved_2;
        packetHandlerLora11ByteConfigStruct.attachedDeviceAlarmMask = attachedDeviceAlarmMask;
        packetHandlerLora11ByteConfigStruct.packetBuffer = new byte[11];

        packetHandler.PacketHandler_DLL_Build_LoRa_Config_11Byte(packetHandlerLora11ByteConfigStruct);

        L900Packet packet = new L900Packet(packetHandlerLora11ByteConfigStruct.packetBuffer);

        boolean parsed = false;

        if(packet.getPacketId().equals(PacketId.BASIC_CONFIG_PACKET))
        {
            BasicConfigPacket basicConfigPacket = new BasicConfigPacket(packet.getBytes());

            assertEquals("packet ID", PacketId.BASIC_CONFIG_PACKET.getId(), basicConfigPacket.getPacketId().getId());
            assertEquals("network time loss", networkTimeLoss, basicConfigPacket.getNetworkTimeLoss());
            assertEquals(antennaType, basicConfigPacket.getAntennaType());
            assertEquals(l900Errors, basicConfigPacket.getL900Errors());
            assertEquals(batteryStatus, basicConfigPacket.getBatteryStatus());
            assertEquals(magSwipe, basicConfigPacket.getMagSwipe());
            assertEquals(firmwareVersion, basicConfigPacket.getFirmwareVersion());
            assertEquals(deviceType, basicConfigPacket.getDeviceType());
            assertEquals(deviceId, basicConfigPacket.getDeviceId());
            assertEquals(reserved_1, basicConfigPacket.getReserved_1());
            assertEquals(reverseFlow_35Day, basicConfigPacket.getReverseFlow_35Day());
            assertEquals(reverseFlow_24Hour, basicConfigPacket.getReverseFlow_24Hour());
            assertEquals(leakState, basicConfigPacket.getLeakState());
            assertEquals(batteryLife, basicConfigPacket.getBatteryLife());
            assertEquals(emptyPipe, basicConfigPacket.getEmptyPipe());
            assertEquals(excessiveFlow, basicConfigPacket.getExcessiveFlow());
            assertEquals(tamper, basicConfigPacket.getTamper());
            assertEquals(reserved_2, basicConfigPacket.getReserved_2());
            assertEquals(attachedDeviceAlarmMask, basicConfigPacket.getAttachedDeviceAlarmMask());

            parsed = true;
        }

        assertTrue(parsed);
    }
}
