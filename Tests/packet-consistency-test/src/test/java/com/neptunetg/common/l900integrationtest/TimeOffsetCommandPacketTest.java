/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.l900integrationtest;

import com.neptunetg.common.l900c.PacketHandlerLoraTimeOffsetStruct;
import com.neptunetg.common.l900integrationtest.framework.PacketHandlerTestBase;
import com.neptunetg.common.lora.pdu.*;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.time.Duration;

import static org.junit.Assert.*;

/**
 * Created by WJD1 on 07/07/2016.
 * A test for parsing / building the Time Offset Command Packet for L900
 */
public class TimeOffsetCommandPacketTest extends PacketHandlerTestBase
{
    final long[] timeOffsetArray = {-1234567891011L, -123L, -1L, 0L, 1L, 123L, 1234567891011L};
    final int packetSequenceNumber = 5;

    @Test
    public void testTimeOffsetCommandPacketJavaToC()
    {
        for(long timeOffset : timeOffsetArray)
        {
            TimeOffsetCommandPacket packet = new TimeOffsetCommandPacket(Duration.ofSeconds(timeOffset), packetSequenceNumber);

            packetHandler.PacketHandler_DLL_Init();

            byte packetBytes[] = packet.getBytes();
            Pointer pointerToPacketBytes = new Memory(packetBytes.length);
            pointerToPacketBytes.write(0, packetBytes, 0, packetBytes.length);

            PacketHandlerLoraTimeOffsetStruct packetHandlerLoraTimeOffsetStruct = new PacketHandlerLoraTimeOffsetStruct();
            packetHandlerLoraTimeOffsetStruct.packetBuffer = new byte[11];
            packetHandler.PacketHandler_DLL_Parse_LoRa_TimeOffset(pointerToPacketBytes, packetHandlerLoraTimeOffsetStruct);

            assertEquals(20, packetHandlerLoraTimeOffsetStruct.packetType);
            assertEquals(1, packetHandlerLoraTimeOffsetStruct.command);
            assertEquals(timeOffset, packetHandlerLoraTimeOffsetStruct.timeOffset);
            assertEquals(packetSequenceNumber, packetHandlerLoraTimeOffsetStruct.timeUpdate);
        }
    }

    @Test
    public void testTimeOffsetCommandPacketCToJava()
    {
        for(long timeOffset : timeOffsetArray)
        {
            packetHandler.PacketHandler_DLL_Init();
            PacketHandlerLoraTimeOffsetStruct packetHandlerLoraTimeOffsetStruct = new PacketHandlerLoraTimeOffsetStruct();
            packetHandlerLoraTimeOffsetStruct.packetType = 20;
            packetHandlerLoraTimeOffsetStruct.command = 1;
            packetHandlerLoraTimeOffsetStruct.timeOffset = timeOffset;
            packetHandlerLoraTimeOffsetStruct.timeUpdate = packetSequenceNumber;
            packetHandlerLoraTimeOffsetStruct.packetBuffer = new byte[11];

            packetHandler.PacketHandler_DLL_Build_LoRa_TimeOffset(packetHandlerLoraTimeOffsetStruct);

            L900Packet packet = new L900Packet(packetHandlerLoraTimeOffsetStruct.packetBuffer);

            assertEquals(packet.getPacketId(), PacketId.APP_COMMAND_PACKET);

            CommandPacket commandPacket = new CommandPacket(packet.getBytes());

            if (commandPacket.getCommandId().equals(CommandPacketTypeEnum.TimeOffset))
            {
                TimeOffsetCommandPacket timeOffsetCommandPacket =
                        new TimeOffsetCommandPacket(commandPacket.getBytes());

                assertEquals(20, timeOffsetCommandPacket.getPacketId().getId());
                assertEquals(1, timeOffsetCommandPacket.getCommandId().numericValue());
                assertEquals(timeOffset, timeOffsetCommandPacket.getTimeOffset().getSeconds());
                assertEquals(packetSequenceNumber, timeOffsetCommandPacket.getUpdateIdentifier());
            }
        }
    }
}
