/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing connection timing log payload to packer

 typedef struct S_RECORDING_REPORTING_INTERVAL
 {
 uint16_t    reportingStartMins;
 uint8_t     reportingIntervalHours;
 uint8_t     reportingRetries;
 uint8_t     reportingTransmitWindowsMins;
 uint16_t    reportingQuietStartMins;
 uint16_t    reportingQuietEndMins;
 uint16_t    recordingStartMins;
 uint8_t     recordingIntervalMins;
 } S_RECORDING_REPORTING_INTERVAL;
 */
public class RecordingReportingIntervalStruct extends Structure
{

    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
            "reportingStartMins",
            "reportingIntervalHours",
            "reportingRetries",
            "reportingTransmitWindowsMins",
            "reportingQuietStartMins",
            "reportingQuietEndMins",
            "recordingStartMins",
            "recordingIntervalMins"
    });

    public short reportingStartMins;
    public byte reportingIntervalHours;
    public byte reportingRetries;
    public byte reportingTransmitWindowsMins;
    public short reportingQuietStartMins;
    public short reportingQuietEndMins;
    public short recordingStartMins;
    public byte recordingIntervalMins;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
