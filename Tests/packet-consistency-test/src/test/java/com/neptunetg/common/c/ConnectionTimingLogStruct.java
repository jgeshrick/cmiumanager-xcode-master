/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing connection timing log payload to packer

 {@code
    typedef struct S_CONNECTION_TIMING_LOG
        {
        uint64_t dateOfConnection;
        uint16_t timeFromPowerOnToNetworkRegistration;
        uint16_t timeFromNetworkRegistrationToContextActivation;
        uint16_t timeFromContextActivationToServerConnection;
        uint16_t timeToTransferMessageToServer;
        uint16_t timeToDisconnectAndShutdown;
        } S_CONNECTION_TIMING_LOG;
 }
 */
public class ConnectionTimingLogStruct extends Structure
{

    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "dateOfConnection",
        "timeFromPowerOnToNetworkRegistration",
        "timeFromNetworkRegistrationToContextActivation",
        "timeFromContextActivationToServerConnection",
        "timeToTransferMessageToServer",
        "timeToDisconnectAndShutdown",
    });

    public long dateOfConnection;
    public short timeFromPowerOnToNetworkRegistration;
    public short timeFromNetworkRegistrationToContextActivation;
    public short timeFromContextActivationToServerConnection;
    public short timeToTransferMessageToServer;
    public short timeToDisconnectAndShutdown;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
