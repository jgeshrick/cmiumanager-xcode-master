/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2015 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.integrationtest;

import com.neptunetg.common.c.*;
import com.neptunetg.common.integrationtest.framework.PacketConsistencyTestBase;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by WJD1 on 11/06/2015.
 */
public class IntervalDataPacketConsistencyTest extends PacketConsistencyTestBase
{
    /**
     * Create a Interval data packet in C and parse it in Java
     *
     */
    @Test
    public void testIntervalDataPacketConsistencyCToJava() throws Exception
    {
        //Build packet in C

        final Memory packetBuffer = new Memory(1024L);

        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.IntervalData.getId()); //packet type

        final CmiuPacketHeaderStruct cBuiltPacketHeader = new CmiuPacketHeaderStruct();
        cBuiltPacketHeader.cmiuId = 123456;
        cBuiltPacketHeader.cellularRssiAndBer = 123;
        cBuiltPacketHeader.encryptionMethod = 1;
        cBuiltPacketHeader.keyInfo = 2;
        cBuiltPacketHeader.networkFlags = 3;
        cBuiltPacketHeader.sequenceNumber = 4;
        cBuiltPacketHeader.timeAndDate = 5;
        cBuiltPacketHeader.tokenAesCrc = 6;

        assertTrue(tpbp.Tpbp_PackerAdd_CmiuPacketHeader(packetPacker, cBuiltPacketHeader));

        final Memory secureDataBuffer = new Memory(1024L);

        final TpbpPackerStruct secureDataPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(secureDataPacker, secureDataBuffer, 1024);

        final CmiuFlagsStruct cBuiltCmiuFlags = new CmiuFlagsStruct();
        cBuiltCmiuFlags.cmiuType = 3;
        cBuiltCmiuFlags.numberOfDevices = 1;
        cBuiltCmiuFlags.cmiuFlags = 0x0F0D;

        assertTrue(tpbp.Tpbp_PackerAdd_CmiuStatus(secureDataPacker, cBuiltCmiuFlags));

        final ReportedDeviceConfigurationStruct cBuiltReportedDeviceConfiguration = new ReportedDeviceConfigurationStruct();
        cBuiltReportedDeviceConfiguration.deviceNumber = 123;
        cBuiltReportedDeviceConfiguration.attachedDeviceId = 234;
        cBuiltReportedDeviceConfiguration.deviceType = 7;
        cBuiltReportedDeviceConfiguration.deviceFlags = 0x67FE;

        assertTrue(tpbp.Tpbp_PackerAdd_ReportedDeviceConfig(secureDataPacker, cBuiltReportedDeviceConfiguration));

        final IntervalRecordingConfigStruct cBuiltIntervalRecordingConfig = new IntervalRecordingConfigStruct();
        cBuiltIntervalRecordingConfig.deviceNumber = 92;
        cBuiltIntervalRecordingConfig.intervalFormat = 42;
        cBuiltIntervalRecordingConfig.deviceRecordingInterval = 5;
        cBuiltIntervalRecordingConfig.deviceReportingInterval = 65;
        cBuiltIntervalRecordingConfig.intervalStartTime = 47;
        cBuiltIntervalRecordingConfig.intervalLastReadDateTime = 45;

        assertTrue(tpbp.Tpbp_PackerAdd_IntervalRecordingConfig(secureDataPacker, cBuiltIntervalRecordingConfig));

        int[] testR900Data = new int[] {1,2,3,4}; //4 readings worth
        addExtendedUint32Array(secureDataPacker, TagId.R900IntervalData, testR900Data);

        assertTrue(tpbp.Tpbp_PackerPadZeroToBlockBoundary(secureDataPacker, 16));

        final int secureDataLengthInBytes = tpbp.Tpbp_PackerGetCount(secureDataPacker);
        final Memory encryptedSecureData = new Memory(secureDataLengthInBytes);

        encryptedSecureData.write(0L, secureDataBuffer.getByteArray(0L, secureDataLengthInBytes), 0, secureDataLengthInBytes);

        assertTrue(tpbp.Tpbp_PackerAdd_SecureBlockArray(packetPacker, encryptedSecureData, secureDataLengthInBytes));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //Parse with Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.IntervalData.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final CmiuPacketHeaderData parsedPacketData = (CmiuPacketHeaderData) ti.next();
        assertEquals(cBuiltPacketHeader.cmiuId, parsedPacketData.getCmiuId());
        assertEquals(cBuiltPacketHeader.cellularRssiAndBer, parsedPacketData.getBer() * 256 + parsedPacketData.getRssi());
        assertEquals(cBuiltPacketHeader.encryptionMethod, parsedPacketData.getEncryptionMethod());
        assertEquals(cBuiltPacketHeader.keyInfo, parsedPacketData.getKeyInfo());
        assertEquals(cBuiltPacketHeader.networkFlags, parsedPacketData.getNetworkFlags());
        assertEquals(cBuiltPacketHeader.sequenceNumber, parsedPacketData.getSequenceNumber());
        assertEquals(cBuiltPacketHeader.timeAndDate, parsedPacketData.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cBuiltPacketHeader.tokenAesCrc, parsedPacketData.getToken());

        final SecureBlockArrayData javaParsedSbad = (SecureBlockArrayData) ti.next();

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());

        //read the tags in the secure data

        final ByteBuffer encryptedData = javaParsedSbad.getEncryptedData();

        final ByteBuffer decryptedData = encryptedData.duplicate().order(ByteOrder.LITTLE_ENDIAN); //no decryption

        final TagSequenceParser secureDataParser = new TagSequenceParser();

        final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

        ti = secureTags.iterator();

        final CmiuStatusData javaParsedCmiuStatus = (CmiuStatusData) ti.next();
        assertEquals(cBuiltCmiuFlags.cmiuType, javaParsedCmiuStatus.getCmiuType());
        assertEquals(cBuiltCmiuFlags.numberOfDevices, javaParsedCmiuStatus.getNumberOfDevices());
        assertEquals(cBuiltCmiuFlags.cmiuFlags, javaParsedCmiuStatus.getCmiuFlags());

        final ReportedDeviceConfigurationData javaParsedReportedDeviceConfiguration = (ReportedDeviceConfigurationData) ti.next();
        assertEquals(cBuiltReportedDeviceConfiguration.attachedDeviceId, javaParsedReportedDeviceConfiguration.getAttachedDeviceId());
        assertEquals(cBuiltReportedDeviceConfiguration.deviceFlags, javaParsedReportedDeviceConfiguration.getDeviceFlags());
        assertEquals(cBuiltReportedDeviceConfiguration.deviceNumber, javaParsedReportedDeviceConfiguration.getDeviceNumber());
        assertEquals(cBuiltReportedDeviceConfiguration.deviceType, javaParsedReportedDeviceConfiguration.getDeviceType());

        final IntervalRecordingConfigurationData javaParsedIntervalRecordingConfigurationData = (IntervalRecordingConfigurationData) ti.next();
        assertEquals(cBuiltIntervalRecordingConfig.deviceReportingInterval, javaParsedIntervalRecordingConfigurationData.getReportingIntervalHours());
        assertEquals(cBuiltIntervalRecordingConfig.deviceNumber, javaParsedIntervalRecordingConfigurationData.getDeviceNumber());
        assertEquals(cBuiltIntervalRecordingConfig.deviceRecordingInterval, javaParsedIntervalRecordingConfigurationData.getDeviceRecordingInterval());
        assertEquals(cBuiltIntervalRecordingConfig.intervalFormat, javaParsedIntervalRecordingConfigurationData.getIntervalFormat());
        assertEquals(cBuiltIntervalRecordingConfig.intervalLastReadDateTime, javaParsedIntervalRecordingConfigurationData.getTimeOfMostRecentReading().getEpochSecond());
        assertEquals(cBuiltIntervalRecordingConfig.intervalStartTime, javaParsedIntervalRecordingConfigurationData.getStartTimeOfRecordingInterval());

        checkExtendedUint32Array(ti, TagId.R900IntervalData, testR900Data);

        while(ti.hasNext())
        {
            assertEquals(ti.next().getTagId(), TagId.NullTag);
        }
    }

    @Test
    public void testIntervalDataPacketConsistencyJavaToC() throws Exception
    {
        final CmiuPacketHeaderData javaBuiltHeader = new CmiuPacketHeaderData(
            2 /*cmiuId*/,
            3 /*int sequenceNumber*/,
            4 /*keyInfo*/,
            5 /*encryptionMethod*/,
            6 /*token*/,
            7 /*networkFlags*/,
            8 /*rssi*/,
            9 /*ber*/,
            new UnixTimestamp(10L));

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.IntervalData);
        packetBuilder.appendTagData(javaBuiltHeader);


        final List<TaggedData> tagsToEncrypt = new ArrayList<>();

        final CmiuStatusData javaBuiltCmiuStatus = new CmiuStatusData(1,2,3);
        final ReportedDeviceConfigurationData javaBuiltReportedDeviceConfiguration = new ReportedDeviceConfigurationData(4,5L,6,7,8);
        final IntervalRecordingConfigurationData javaBuiltIntervalRecordingConfiguration =
                new IntervalRecordingConfigurationData(9, 10, 11, 12, 13, new UnixTimestamp(675));
        int[] javaBuiltR900IntervalDataArray = new int[] {14,15,16,17};
        ByteBuffer byteBuffer = ByteBuffer.allocate(javaBuiltR900IntervalDataArray.length * 4).order(ByteOrder.LITTLE_ENDIAN);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(javaBuiltR900IntervalDataArray);
        final IntegerArrayData R900IntervalData = new IntegerArrayData(TagId.R900IntervalData, TagDataType.ExtendedUInt32Array, byteBuffer.array(), 4);

        tagsToEncrypt.add(javaBuiltCmiuStatus);
        tagsToEncrypt.add(javaBuiltReportedDeviceConfiguration);
        tagsToEncrypt.add(javaBuiltIntervalRecordingConfiguration);
        tagsToEncrypt.add(R900IntervalData);

        final TagSequence tagSequenceToEncrypt = new TagSequence(tagsToEncrypt);

        System.out.println("Java built secure data as JSON: ");
        System.out.println(tagSequenceToEncrypt.toJsonPretty());

        byte[] dataToEncrypt = tagSequenceToEncrypt.toByteSequencePadToBlockBoundary(16); //pad to 16 bytes

        byte[] encryptedData = dataToEncrypt; //no encryption

        SecureBlockArrayData secureData = new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, encryptedData);
        packetBuilder.appendTagData(secureData);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        System.out.println("Java built packet as JSON: ");
        System.out.println(p.toJsonPretty());

        final byte[] packetData = p.toByteSequence();

        printByteArray("Java built packet data as hex:", packetData);
        Files.write(Paths.get("javapacket.dat"), packetData);

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();
        final CmiuPacketHeaderStruct cParsedHeader = new CmiuPacketHeaderStruct();


        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.CmiuPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(CmiuPacketHeaderData.SIZE_IN_BYTES, tagStruct.dataSize);

        tpbp.Tpbp_ParserRead_CmiuPacketHeader(packetParserStruct, cParsedHeader);


        assertEquals(javaBuiltHeader.getCmiuId(), cParsedHeader.cmiuId);

        assertEquals(javaBuiltHeader.getBer() * 256 + javaBuiltHeader.getRssi(), cParsedHeader.cellularRssiAndBer);

        assertEquals(cParsedHeader.encryptionMethod, javaBuiltHeader.getEncryptionMethod());
        assertEquals(cParsedHeader.keyInfo, javaBuiltHeader.getKeyInfo());
        assertEquals(cParsedHeader.networkFlags, javaBuiltHeader.getNetworkFlags());
        assertEquals(cParsedHeader.sequenceNumber, javaBuiltHeader.getSequenceNumber());
        assertEquals(cParsedHeader.timeAndDate, javaBuiltHeader.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cParsedHeader.tokenAesCrc, javaBuiltHeader.getToken());

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.SecureDataBlockArray.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), tagStruct.tagType);

        final int secureDataSize = tagStruct.dataSize;
        final Memory encryptedDataFromC = new Memory(secureDataSize);
        tpbp.Tpbp_ParserGetBytes(packetParserStruct, encryptedDataFromC, secureDataSize);

        final Memory decryptedDataFromC = encryptedDataFromC; //no encryption

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);


        final TpbpParserStruct secureDataParserStruct = new TpbpParserStruct();

        tpbp.Tpbp_ParserInit(secureDataParserStruct, decryptedDataFromC, secureDataSize);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuStatus.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(CmiuStatusData.SIZE_IN_BYTES, tagStruct.dataSize);

        final CmiuFlagsStruct cParsedCmiuStatus = new CmiuFlagsStruct();

        tpbp.Tpbp_ParserRead_CmiuStatus(secureDataParserStruct, cParsedCmiuStatus);

        assertEquals(javaBuiltCmiuStatus.getCmiuFlags(), cParsedCmiuStatus.cmiuFlags);
        assertEquals(javaBuiltCmiuStatus.getCmiuType(), cParsedCmiuStatus.cmiuType);
        assertEquals(javaBuiltCmiuStatus.getNumberOfDevices(), cParsedCmiuStatus.numberOfDevices);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ReportedDeviceConfiguration.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(ReportedDeviceConfigurationData.SIZE_IN_BYTES, tagStruct.dataSize);

        final ReportedDeviceConfigurationStruct cParsedReportedDeviceConfiguration = new ReportedDeviceConfigurationStruct();

        tpbp.Tpbp_ParserRead_ReportedDeviceConfig(secureDataParserStruct, cParsedReportedDeviceConfiguration);

        assertEquals(javaBuiltReportedDeviceConfiguration.getAttachedDeviceId(), cParsedReportedDeviceConfiguration.attachedDeviceId);
        assertEquals(javaBuiltReportedDeviceConfiguration.getDeviceFlags(), cParsedReportedDeviceConfiguration.deviceFlags);
        assertEquals(javaBuiltReportedDeviceConfiguration.getDeviceNumber(), cParsedReportedDeviceConfiguration.deviceNumber);
        assertEquals(javaBuiltReportedDeviceConfiguration.getDeviceType(), cParsedReportedDeviceConfiguration.deviceType);
        assertEquals(javaBuiltReportedDeviceConfiguration.getCurrentDeviceData().intValue(), cParsedReportedDeviceConfiguration.currentDeviceData);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.IntervalRecording.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(IntervalRecordingConfigurationData.SIZE_IN_BYTES, tagStruct.dataSize);

        final IntervalRecordingConfigStruct cParsedIntervalRecordingConfig = new IntervalRecordingConfigStruct();

        tpbp.Tpbp_ParserRead_IntervalRecordingConfig(secureDataParserStruct, cParsedIntervalRecordingConfig);

        assertEquals(javaBuiltIntervalRecordingConfiguration.getDeviceNumber(), cParsedIntervalRecordingConfig.deviceNumber);
        assertEquals(javaBuiltIntervalRecordingConfiguration.getDeviceRecordingInterval(), cParsedIntervalRecordingConfig.deviceRecordingInterval);
        assertEquals(javaBuiltIntervalRecordingConfiguration.getIntervalFormat(), cParsedIntervalRecordingConfig.intervalFormat);
        assertEquals(javaBuiltIntervalRecordingConfiguration.getReportingIntervalHours(), cParsedIntervalRecordingConfig.deviceReportingInterval);
        assertEquals(javaBuiltIntervalRecordingConfiguration.getStartTimeOfRecordingInterval(), cParsedIntervalRecordingConfig.intervalStartTime);
        assertEquals(javaBuiltIntervalRecordingConfiguration.getTimeOfMostRecentReading().getEpochSecond(), cParsedIntervalRecordingConfig.intervalLastReadDateTime);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);

        assertEquals(TagId.R900IntervalData.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedUInt32Array.getId(), tagStruct.tagType);
        assertEquals(javaBuiltR900IntervalDataArray.length*4, tagStruct.dataSize);

        for(int i=0; i<(tagStruct.dataSize/4); i++)
        {
            assertEquals(javaBuiltR900IntervalDataArray[i], tpbp.Tpbp_Unpack(secureDataParserStruct, 4));
        }

    }
}
