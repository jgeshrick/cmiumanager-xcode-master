/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.integrationtest;

import com.neptunetg.common.c.*;
import com.neptunetg.common.integrationtest.framework.PacketConsistencyTestBase;
import com.neptunetg.common.packet.model.CmdId;
import com.neptunetg.common.packet.model.ImageId;
import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.*;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Consistency test for packet type 5
 */
public class CommandPacketConsistencyTest extends PacketConsistencyTestBase
{
    /**
     * Create a packet in C and parse it in Java
     */
    @Test
    public void testFirmwareImageUpdateCommandPacketConsistencyCToJava() throws Exception
    {
        //build the packet in C
        final Memory packetBuffer = new Memory(1024L);

        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.CommandConfiguration.getId()); //packet type

        final MqttPacketHeaderStruct cBuiltMqttPacketHeader = new MqttPacketHeaderStruct();
        cBuiltMqttPacketHeader.sequenceNumber = 1;
        cBuiltMqttPacketHeader.keyInfo = 2;
        cBuiltMqttPacketHeader.encryptionMethod = 3;
        cBuiltMqttPacketHeader.token = 4;
        assertTrue(tpbp.Tpbp_PackerAdd_UartPacketHeader(packetPacker, cBuiltMqttPacketHeader));

        final Memory secureDataBuffer = new Memory(1024L);

        final TpbpPackerStruct secureDataPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(secureDataPacker, secureDataBuffer, 1024);

        assertTrue(tpbp.Tpbp_PackerAdd_UInt(secureDataPacker, TagId.Command.getId(), CmdId.UpdateImage.getId(), 1));

        assertTrue(tpbp.Tpbp_PackerAdd_UInt64(secureDataPacker, TagId.TimeAndDate.getId(), 12345678L));

        addExtendedCharArray(secureDataPacker, TagId.Image, "my firmware image update URL");

        final ImageMetadataStruct cBuiltImageMetaData = new ImageMetadataStruct();
        cBuiltImageMetaData.imageType = (byte) ImageId.FirmwareImage.getId();
        cBuiltImageMetaData.startAddress = 5;
        cBuiltImageMetaData.imageSize = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageMetadata(secureDataPacker, cBuiltImageMetaData));

        final RevisionStruct cBuiltFirmwareRevision = new RevisionStruct();
        cBuiltFirmwareRevision.versionMinor = 0x28;
        cBuiltFirmwareRevision.versionMajor = 0x29;
        cBuiltFirmwareRevision.versionYearMonthDay = 0x150916;
        cBuiltFirmwareRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoFirmware(secureDataPacker, cBuiltFirmwareRevision));

        addExtendedCharArray(secureDataPacker, TagId.Image, "my bootloader image update URL");

        final ImageMetadataStruct cBuiltBootloaderImageMetaData = new ImageMetadataStruct();
        cBuiltBootloaderImageMetaData.imageType = (byte)ImageId.BootloaderImage.getId();
        cBuiltBootloaderImageMetaData.startAddress = 5;
        cBuiltBootloaderImageMetaData.imageSize = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageMetadata(secureDataPacker, cBuiltBootloaderImageMetaData));

        final RevisionStruct cBuiltBootloaderRevision = new RevisionStruct();
        cBuiltBootloaderRevision.versionMinor = 0x28;
        cBuiltBootloaderRevision.versionMajor = 0x29;
        cBuiltBootloaderRevision.versionYearMonthDay = 0x150916;
        cBuiltBootloaderRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoBootloader(secureDataPacker, cBuiltBootloaderRevision));

        addExtendedCharArray(secureDataPacker, TagId.Image, "my config image update URL");

        final ImageMetadataStruct cBuiltConfigImageMetaData = new ImageMetadataStruct();
        cBuiltConfigImageMetaData.imageType = (byte)ImageId.ConfigImage.getId();
        cBuiltConfigImageMetaData.startAddress = 5;
        cBuiltConfigImageMetaData.imageSize = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageMetadata(secureDataPacker, cBuiltConfigImageMetaData));

        final RevisionStruct cBuiltConfigRevision = new RevisionStruct();
        cBuiltConfigRevision.versionMinor = 0x28;
        cBuiltConfigRevision.versionMajor = 0x29;
        cBuiltConfigRevision.versionYearMonthDay = 0x150916;
        cBuiltConfigRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoConfig(secureDataPacker, cBuiltConfigRevision));

        addExtendedCharArray(secureDataPacker, TagId.Image, "my ARB config update URL");

        final ImageMetadataStruct cBuiltArbConfigData = new ImageMetadataStruct();
        cBuiltArbConfigData.imageType = (byte)ImageId.ArbConfigImage.getId();
        cBuiltArbConfigData.startAddress = 5;
        cBuiltArbConfigData.imageSize = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageMetadata(secureDataPacker, cBuiltArbConfigData));

        final RevisionStruct cBuiltArbConfigRevision = new RevisionStruct();
        cBuiltArbConfigRevision.versionMinor = 0x28;
        cBuiltArbConfigRevision.versionMajor = 0x29;
        cBuiltArbConfigRevision.versionYearMonthDay = 0x150916;
        cBuiltArbConfigRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoArb(secureDataPacker, cBuiltArbConfigRevision));

        addExtendedCharArray(secureDataPacker, TagId.Image, "my BLE Revision update URL");

        final ImageMetadataStruct cBuiltBleRevisionData = new ImageMetadataStruct();
        cBuiltBleRevisionData.imageType = (byte)ImageId.BleConfigImage.getId();
        cBuiltBleRevisionData.startAddress = 5;
        cBuiltBleRevisionData.imageSize = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageMetadata(secureDataPacker, cBuiltBleRevisionData));

        final RevisionStruct cBuiltBleRevision = new RevisionStruct();
        cBuiltBleRevision.versionMinor = 0x28;
        cBuiltBleRevision.versionMajor = 0x29;
        cBuiltBleRevision.versionYearMonthDay = 0x150916;
        cBuiltBleRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoBle(secureDataPacker, cBuiltBleRevision));

        assertTrue(tpbp.Tpbp_PackerPadZeroToBlockBoundary(secureDataPacker, 16));

        final int secureDataLengthInBytes = tpbp.Tpbp_PackerGetCount(secureDataPacker);
        final Memory encryptedSecureData = new Memory(secureDataLengthInBytes);

        encryptedSecureData.write(0L, secureDataBuffer.getByteArray(0L, secureDataLengthInBytes), 0, secureDataLengthInBytes);

        assertTrue(tpbp.Tpbp_PackerAdd_SecureBlockArray(packetPacker, encryptedSecureData, secureDataLengthInBytes));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //parse the packet data in Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.CommandConfiguration.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final MqttPacketHeaderData mqttPacketHeader = (MqttPacketHeaderData)ti.next();
        assertEquals(cBuiltMqttPacketHeader.sequenceNumber, mqttPacketHeader.getSequenceNumber());
        assertEquals(cBuiltMqttPacketHeader.keyInfo, mqttPacketHeader.getKeyInfo());
        assertEquals(cBuiltMqttPacketHeader.encryptionMethod, mqttPacketHeader.getEncryptionMethod());
        assertEquals(cBuiltMqttPacketHeader.token, mqttPacketHeader.getToken());

        final SecureBlockArrayData javaParsedSbad = (SecureBlockArrayData) ti.next();

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());

        //read the tags in the secure data

        final ByteBuffer encryptedData = javaParsedSbad.getEncryptedData();

        final ByteBuffer decryptedData = encryptedData.duplicate().order(ByteOrder.LITTLE_ENDIAN); //no decryption

        final TagSequenceParser secureDataParser = new TagSequenceParser();

        final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

        ti = secureTags.iterator();

        final IntegerData command = (IntegerData)ti.next();
        assertEquals(TagId.Command, command.getTagId());
        assertEquals(TagDataType.Byte, command.getDataType());
        assertEquals(CmdId.UpdateImage.getId(), command.getValue());

        final IntegerData timeAndDate = (IntegerData)ti.next();
        assertEquals(TagId.TimeAndDate.getId(), timeAndDate.getTagId().getId());
        assertEquals(TagDataType.UInt64, timeAndDate.getDataType());
        assertEquals(12345678L, timeAndDate.getValue());

        checkCharArray(ti, TagId.Image, "my firmware image update URL");

        final ImageMetaData imageMetaData = (ImageMetaData)ti.next();
        assertEquals(cBuiltImageMetaData.imageType, imageMetaData.getImageType());
        assertEquals(cBuiltImageMetaData.startAddress, imageMetaData.getStartAddress());
        assertEquals(cBuiltImageMetaData.imageSize, imageMetaData.getSize());

        checkRevision(ti, TagId.FirmwareRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkCharArray(ti, TagId.Image, "my bootloader image update URL");

        final ImageMetaData imageBootloaderMetaData = (ImageMetaData)ti.next();
        assertEquals(cBuiltBootloaderImageMetaData.imageType, imageBootloaderMetaData.getImageType());
        assertEquals(cBuiltBootloaderImageMetaData.startAddress, imageBootloaderMetaData.getStartAddress());
        assertEquals(cBuiltBootloaderImageMetaData.imageSize, imageBootloaderMetaData.getSize());

        checkRevision(ti, TagId.BootloaderVersion, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkCharArray(ti, TagId.Image, "my config image update URL");

        final ImageMetaData imageConfigMetaData = (ImageMetaData)ti.next();
        assertEquals(cBuiltConfigImageMetaData.imageType, imageConfigMetaData.getImageType());
        assertEquals(cBuiltConfigImageMetaData.startAddress, imageConfigMetaData.getStartAddress());
        assertEquals(cBuiltConfigImageMetaData.imageSize, imageConfigMetaData.getSize());

        checkRevision(ti, TagId.ConfigRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkCharArray(ti, TagId.Image, "my ARB config update URL");

        final ImageMetaData imageArbMetaData = (ImageMetaData)ti.next();
        assertEquals(cBuiltArbConfigData.imageType, imageArbMetaData.getImageType());
        assertEquals(cBuiltArbConfigData.startAddress, imageArbMetaData.getStartAddress());
        assertEquals(cBuiltArbConfigData.imageSize, imageArbMetaData.getSize());

        checkRevision(ti, TagId.ArbConfigRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkCharArray(ti, TagId.Image, "my BLE Revision update URL");

        final ImageMetaData imageBleMetaData = (ImageMetaData)ti.next();
        assertEquals(cBuiltBleRevisionData.imageType, imageBleMetaData.getImageType());
        assertEquals(cBuiltBleRevisionData.startAddress, imageBleMetaData.getStartAddress());
        assertEquals(cBuiltBleRevisionData.imageSize, imageBleMetaData.getSize());

        checkRevision(ti, TagId.BleConfigRevision, new byte[]{0x29, 0x28, 0x0, 0x15, 0x9, 0x16, 0x12, 0x34, 0x56, 0x78});
    }


    /**
     * Create a packet in Java and parse it in C
     */
    @Test
    public void testFirmwareImageCommandPacketConsistencyJavaToC() throws Exception
    {

        final MqttPacketHeaderData javaBuiltHeader = new MqttPacketHeaderData(1, 2, 3, 4);

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.CommandConfiguration);
        packetBuilder.appendTagData(javaBuiltHeader);

        final List<TaggedData> tagsToEncrypt = new ArrayList<>();

        final CommandData commandJBuilt = new CommandData(CmdId.UpdateImage);
        tagsToEncrypt.add(commandJBuilt);

        final IntegerData timeAndDateJBuilt = new IntegerData(TagId.TimeAndDate, TagDataType.UInt64, 12345678L);
        tagsToEncrypt.add(timeAndDateJBuilt);

        final CharArrayData imageJBuilt = new CharArrayData(TagId.Image, TagDataType.ExtendedCharArray, "my image update URL");
        tagsToEncrypt.add(imageJBuilt);

        final ImageMetaData imageMetadataJBuilt = new ImageMetaData(ImageId.FirmwareImage.getId(), 5, 6);
        tagsToEncrypt.add(imageMetadataJBuilt);

        final ImageVersionInfoData firmwareRevisionJBuilt = new ImageVersionInfoData(TagId.FirmwareRevision, new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        tagsToEncrypt.add(firmwareRevisionJBuilt);

        final CharArrayData imageBootloadedJBuilt = new CharArrayData(TagId.Image, TagDataType.ExtendedCharArray, "my bootloader image update URL");
        tagsToEncrypt.add(imageBootloadedJBuilt);

        final ImageMetaData imageBootloaderMetadataJBuilt = new ImageMetaData(ImageId.BootloaderImage.getId(), 5, 6);
        tagsToEncrypt.add(imageBootloaderMetadataJBuilt);

        final ImageVersionInfoData bootloaderRevisionJBuilt = new ImageVersionInfoData(TagId.BootloaderVersion, new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        tagsToEncrypt.add(bootloaderRevisionJBuilt);

        final CharArrayData imageConfigJBuilt = new CharArrayData(TagId.Image, TagDataType.ExtendedCharArray, "my config image update URL");
        tagsToEncrypt.add(imageConfigJBuilt);

        final ImageMetaData imageConfigMetadataJBuilt = new ImageMetaData(ImageId.ConfigImage.getId(), 5, 6);
        tagsToEncrypt.add(imageConfigMetadataJBuilt);

        final ImageVersionInfoData configRevisionJBuilt = new ImageVersionInfoData(TagId.ConfigRevision, new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        tagsToEncrypt.add(configRevisionJBuilt);

        final CharArrayData imageArbJBuilt = new CharArrayData(TagId.Image, TagDataType.ExtendedCharArray, "my ARB config image update URL");
        tagsToEncrypt.add(imageArbJBuilt);

        final ImageMetaData imageArbMetadataJBuilt = new ImageMetaData(ImageId.ArbConfigImage.getId(), 5, 6);
        tagsToEncrypt.add(imageArbMetadataJBuilt);

        final ImageVersionInfoData arbRevisionJBuilt = new ImageVersionInfoData(TagId.ArbConfigRevision, new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        tagsToEncrypt.add(arbRevisionJBuilt);

        final CharArrayData imageBleJBuilt = new CharArrayData(TagId.Image, TagDataType.ExtendedCharArray, "my BLE config image update URL");
        tagsToEncrypt.add(imageBleJBuilt);

        final ImageMetaData imageBleMetadataJBuilt = new ImageMetaData(ImageId.BleConfigImage.getId(), 5, 6);
        tagsToEncrypt.add(imageBleMetadataJBuilt);

        final ImageVersionInfoData bleRevisionJBuilt = new ImageVersionInfoData(TagId.BleConfigRevision, new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        tagsToEncrypt.add(bleRevisionJBuilt);

        final TagSequence tagSequenceToEncrypt = new TagSequence(tagsToEncrypt);

        System.out.println("Java built secure data as JSON: ");
        System.out.println(tagSequenceToEncrypt.toJsonPretty());

        byte[] dataToEncrypt = tagSequenceToEncrypt.toByteSequencePadToBlockBoundary(16); //pad to 16 bytes

        byte[] encryptedData = dataToEncrypt; //no encryption

        SecureBlockArrayData secureData = new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, encryptedData, dataToEncrypt.length / 16);
        packetBuilder.appendTagData(secureData);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        final byte[] packetData = p.toByteSequence();

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();

        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.MQTTPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final MqttPacketHeaderStruct mqttPacketHeader = new MqttPacketHeaderStruct();
        tpbp.Tpbp_ParserRead_UartPacketHeader(packetParserStruct, mqttPacketHeader);
        assertEquals(javaBuiltHeader.getSequenceNumber(), mqttPacketHeader.sequenceNumber);
        assertEquals(javaBuiltHeader.getKeyInfo(), mqttPacketHeader.keyInfo);
        assertEquals(javaBuiltHeader.getEncryptionMethod(), mqttPacketHeader.encryptionMethod);
        assertEquals(javaBuiltHeader.getToken(), mqttPacketHeader.token);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.SecureDataBlockArray.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), tagStruct.tagType);

        final int secureDataSize = tagStruct.dataSize;
        final Memory encryptedDataFromC = new Memory(secureDataSize);
        tpbp.Tpbp_ParserGetBytes(packetParserStruct, encryptedDataFromC, secureDataSize);

        final Memory decryptedDataFromC = encryptedDataFromC; //no encryption

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);


        final TpbpParserStruct secureDataParserStruct = new TpbpParserStruct();
        tpbp.Tpbp_ParserInit(secureDataParserStruct, decryptedDataFromC, secureDataSize);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.Command.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.Byte.getId(), tagStruct.tagType);
        assertEquals(CmdId.UpdateImage.getId(), tpbp.Tpbp_Unpack(secureDataParserStruct, 1));

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.TimeAndDate.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.UInt64.getId(), tagStruct.tagType);
        assertEquals(8, tagStruct.dataSize);
        assertEquals(12345678L,tpbp.Tpbp_Unpack64(secureDataParserStruct));

        checkCharArray(secureDataParserStruct, TagId.Image, "my image update URL");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ImageMetaData.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(9, tagStruct.dataSize);
        final ImageMetadataStruct imageMetaData = new ImageMetadataStruct();
        tpbp.Tpbp_ParserRead_ImageMetadata(secureDataParserStruct, imageMetaData);
        assertEquals(imageMetadataJBuilt.getImageType(), imageMetaData.imageType);
        assertEquals(imageMetadataJBuilt.getStartAddress(), imageMetaData.startAddress);
        assertEquals(imageMetadataJBuilt.getSize(), imageMetaData.imageSize);

        checkRevision(secureDataParserStruct, TagId.FirmwareRevision, new byte[]{1,2,3,4,5,6,7,8,9,10});


        checkCharArray(secureDataParserStruct, TagId.Image, "my bootloader image update URL");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ImageMetaData.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(9, tagStruct.dataSize);
        final ImageMetadataStruct imageBootloaderMetaData = new ImageMetadataStruct();
        tpbp.Tpbp_ParserRead_ImageMetadata(secureDataParserStruct, imageBootloaderMetaData);
        assertEquals(imageBootloaderMetadataJBuilt.getImageType(), imageBootloaderMetaData.imageType);
        assertEquals(imageBootloaderMetadataJBuilt.getStartAddress(), imageBootloaderMetaData.startAddress);
        assertEquals(imageBootloaderMetadataJBuilt.getSize(), imageBootloaderMetaData.imageSize);

        checkRevision(secureDataParserStruct, TagId.BootloaderVersion, new byte[]{1,2,3,4,5,6,7,8,9,10});


        checkCharArray(secureDataParserStruct, TagId.Image, "my config image update URL");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ImageMetaData.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(9, tagStruct.dataSize);
        final ImageMetadataStruct imageConfigMetaData = new ImageMetadataStruct();
        tpbp.Tpbp_ParserRead_ImageMetadata(secureDataParserStruct, imageConfigMetaData);
        assertEquals(imageConfigMetadataJBuilt.getImageType(), imageConfigMetaData.imageType);
        assertEquals(imageConfigMetadataJBuilt.getStartAddress(), imageConfigMetaData.startAddress);
        assertEquals(imageConfigMetadataJBuilt.getSize(), imageConfigMetaData.imageSize);

        checkRevision(secureDataParserStruct, TagId.ConfigRevision, new byte[]{1,2,3,4,5,6,7,8,9,10});


        checkCharArray(secureDataParserStruct, TagId.Image, "my ARB config image update URL");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ImageMetaData.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(9, tagStruct.dataSize);
        final ImageMetadataStruct imageArbMetaData = new ImageMetadataStruct();
        tpbp.Tpbp_ParserRead_ImageMetadata(secureDataParserStruct, imageArbMetaData);
        assertEquals(imageArbMetadataJBuilt.getImageType(), imageArbMetaData.imageType);
        assertEquals(imageArbMetadataJBuilt.getStartAddress(), imageArbMetaData.startAddress);
        assertEquals(imageArbMetadataJBuilt.getSize(), imageArbMetaData.imageSize);

        checkRevision(secureDataParserStruct, TagId.ArbConfigRevision, new byte[]{1,2,3,4,5,6,7,8,9,10});


        checkCharArray(secureDataParserStruct, TagId.Image, "my BLE config image update URL");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ImageMetaData.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(9, tagStruct.dataSize);
        final ImageMetadataStruct imageBleMetaData = new ImageMetadataStruct();
        tpbp.Tpbp_ParserRead_ImageMetadata(secureDataParserStruct, imageBleMetaData);
        assertEquals(imageBleMetadataJBuilt.getImageType(), imageBleMetaData.imageType);
        assertEquals(imageBleMetadataJBuilt.getStartAddress(), imageBleMetaData.startAddress);
        assertEquals(imageBleMetadataJBuilt.getSize(), imageBleMetaData.imageSize);

        checkRevision(secureDataParserStruct, TagId.BleConfigRevision, new byte[]{1,2,3,4,5,6,7,8,9,10});
    }


    /**
     * Create a packet in C and parse it in Java
     */
    @Test
    public void testSetRecordingAndReportingIntervalCommandPacketConsistencyCToJava() throws Exception
    {
        //build the packet in C
        final Memory packetBuffer = new Memory(1024L);

        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.CommandConfiguration.getId()); //packet type

        final MqttPacketHeaderStruct cBuiltMqttPacketHeader = new MqttPacketHeaderStruct();
        cBuiltMqttPacketHeader.sequenceNumber = 1;
        cBuiltMqttPacketHeader.keyInfo = 2;
        cBuiltMqttPacketHeader.encryptionMethod = 3;
        cBuiltMqttPacketHeader.token = 4;
        assertTrue(tpbp.Tpbp_PackerAdd_UartPacketHeader(packetPacker, cBuiltMqttPacketHeader));

        final Memory secureDataBuffer = new Memory(1024L);

        final TpbpPackerStruct secureDataPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(secureDataPacker, secureDataBuffer, 1024);

        assertTrue(tpbp.Tpbp_PackerAdd_UInt(secureDataPacker, TagId.Command.getId(), CmdId.SetRecordingReportingInterval.getId(), 1));

        assertTrue(tpbp.Tpbp_PackerAdd_UInt64(secureDataPacker, TagId.TimeAndDate.getId(), 12345678L));

        final RecordingReportingIntervalStruct cBuiltRecordingReporting = new RecordingReportingIntervalStruct();
        cBuiltRecordingReporting.reportingStartMins = 5;
        cBuiltRecordingReporting.reportingIntervalHours = 6;
        cBuiltRecordingReporting.reportingRetries = 7;
        cBuiltRecordingReporting.reportingTransmitWindowsMins = 8;
        cBuiltRecordingReporting.reportingQuietStartMins = 9;
        cBuiltRecordingReporting.reportingQuietEndMins = 10;
        cBuiltRecordingReporting.recordingStartMins = 11;
        cBuiltRecordingReporting.recordingIntervalMins = 12;
        assertTrue(tpbp.Tpbp_PackerAdd_RecordingReportingInterval(secureDataPacker, cBuiltRecordingReporting));

        assertTrue(tpbp.Tpbp_PackerPadZeroToBlockBoundary(secureDataPacker, 16));

        final int secureDataLengthInBytes = tpbp.Tpbp_PackerGetCount(secureDataPacker);
        final Memory encryptedSecureData = new Memory(secureDataLengthInBytes);

        encryptedSecureData.write(0L, secureDataBuffer.getByteArray(0L, secureDataLengthInBytes), 0, secureDataLengthInBytes);

        assertTrue(tpbp.Tpbp_PackerAdd_SecureBlockArray(packetPacker, encryptedSecureData, secureDataLengthInBytes));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //parse the packet data in Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.CommandConfiguration.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final MqttPacketHeaderData mqttPacketHeader = (MqttPacketHeaderData)ti.next();
        assertEquals(cBuiltMqttPacketHeader.sequenceNumber, mqttPacketHeader.getSequenceNumber());
        assertEquals(cBuiltMqttPacketHeader.keyInfo, mqttPacketHeader.getKeyInfo());
        assertEquals(cBuiltMqttPacketHeader.encryptionMethod, mqttPacketHeader.getEncryptionMethod());
        assertEquals(cBuiltMqttPacketHeader.token, mqttPacketHeader.getToken());

        final SecureBlockArrayData javaParsedSbad = (SecureBlockArrayData) ti.next();

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());

        //read the tags in the secure data

        final ByteBuffer encryptedData = javaParsedSbad.getEncryptedData();

        final ByteBuffer decryptedData = encryptedData.duplicate().order(ByteOrder.LITTLE_ENDIAN); //no decryption

        final TagSequenceParser secureDataParser = new TagSequenceParser();

        final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

        ti = secureTags.iterator();

        final IntegerData command = (IntegerData)ti.next();
        assertEquals(TagId.Command, command.getTagId());
        assertEquals(TagDataType.Byte, command.getDataType());
        assertEquals(CmdId.SetRecordingReportingInterval.getId(), command.getValue());

        final IntegerData timeAndDate = (IntegerData)ti.next();
        assertEquals(TagId.TimeAndDate.getId(), timeAndDate.getTagId().getId());
        assertEquals(TagDataType.UInt64, timeAndDate.getDataType());
        assertEquals(12345678L, timeAndDate.getValue());

        final RecordingAndReportingIntervalData recordingAndReporting = (RecordingAndReportingIntervalData)ti.next();
        assertEquals(cBuiltRecordingReporting.reportingStartMins, recordingAndReporting.getReportingStartMins());
        assertEquals(cBuiltRecordingReporting.reportingIntervalHours, recordingAndReporting.getReportingIntervalHours());
        assertEquals(cBuiltRecordingReporting.reportingRetries, recordingAndReporting.getReportingNumberOfRetries());
        assertEquals(cBuiltRecordingReporting.reportingTransmitWindowsMins, recordingAndReporting.getReportingTransmitWindowsMins());
        assertEquals(cBuiltRecordingReporting.reportingQuietStartMins, recordingAndReporting.getReportingQuietStartMins());
        assertEquals(cBuiltRecordingReporting.reportingQuietEndMins, recordingAndReporting.getReportingQuietEndMins());
        assertEquals(cBuiltRecordingReporting.recordingStartMins, recordingAndReporting.getRecordingStartTimeMins());
        assertEquals(cBuiltRecordingReporting.recordingIntervalMins, recordingAndReporting.getRecordingIntervalMins());
    }


    /**
     * Create a packet in Java and parse it in C
     */
    @Test
    public void testSetRecordingAndReportingIntervalCommandPacketConsistencyJavaToC() throws Exception
    {

        final MqttPacketHeaderData javaBuiltHeader = new MqttPacketHeaderData(1, 2, 3, 4);

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.CommandConfiguration);
        packetBuilder.appendTagData(javaBuiltHeader);

        final List<TaggedData> tagsToEncrypt = new ArrayList<>();

        final CommandData commandJBuilt = new CommandData(CmdId.SetRecordingReportingInterval);
        tagsToEncrypt.add(commandJBuilt);

        final IntegerData timeAndDateJBuilt = new IntegerData(TagId.TimeAndDate, TagDataType.UInt64, 12345678L);
        tagsToEncrypt.add(timeAndDateJBuilt);

        final RecordingAndReportingIntervalData recordingAndReportingJBuilt = new RecordingAndReportingIntervalData(1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10);
        tagsToEncrypt.add(recordingAndReportingJBuilt);

        final TagSequence tagSequenceToEncrypt = new TagSequence(tagsToEncrypt);

        System.out.println("Java built secure data as JSON: ");
        System.out.println(tagSequenceToEncrypt.toJsonPretty());

        byte[] dataToEncrypt = tagSequenceToEncrypt.toByteSequencePadToBlockBoundary(16); //pad to 16 bytes

        byte[] encryptedData = dataToEncrypt; //no encryption

        SecureBlockArrayData secureData = new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, encryptedData, dataToEncrypt.length / 16);
        packetBuilder.appendTagData(secureData);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        final byte[] packetData = p.toByteSequence();

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();

        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.MQTTPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final MqttPacketHeaderStruct mqttPacketHeader = new MqttPacketHeaderStruct();
        tpbp.Tpbp_ParserRead_UartPacketHeader(packetParserStruct, mqttPacketHeader);
        assertEquals(javaBuiltHeader.getSequenceNumber(), mqttPacketHeader.sequenceNumber);
        assertEquals(javaBuiltHeader.getKeyInfo(), mqttPacketHeader.keyInfo);
        assertEquals(javaBuiltHeader.getEncryptionMethod(), mqttPacketHeader.encryptionMethod);
        assertEquals(javaBuiltHeader.getToken(), mqttPacketHeader.token);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.SecureDataBlockArray.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), tagStruct.tagType);

        final int secureDataSize = tagStruct.dataSize;
        final Memory encryptedDataFromC = new Memory(secureDataSize);
        tpbp.Tpbp_ParserGetBytes(packetParserStruct, encryptedDataFromC, secureDataSize);

        final Memory decryptedDataFromC = encryptedDataFromC; //no encryption

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);

        final TpbpParserStruct secureDataParserStruct = new TpbpParserStruct();
        tpbp.Tpbp_ParserInit(secureDataParserStruct, decryptedDataFromC, secureDataSize);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.Command.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.Byte.getId(), tagStruct.tagType);
        assertEquals(CmdId.SetRecordingReportingInterval.getId(), tpbp.Tpbp_Unpack(secureDataParserStruct, 1));

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.TimeAndDate.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.UInt64.getId(), tagStruct.tagType);
        assertEquals(8, tagStruct.dataSize);
        assertEquals(12345678L,tpbp.Tpbp_Unpack64(secureDataParserStruct));

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.RecordingandReportingInterval.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(14, tagStruct.dataSize);

        final RecordingReportingIntervalStruct recordingReporting = new RecordingReportingIntervalStruct();
        tpbp.Tpbp_ParserRead_RecordingReportingInterval(secureDataParserStruct, recordingReporting);
        assertEquals(recordingAndReportingJBuilt.getReportingStartMins(), recordingReporting.reportingStartMins);
        assertEquals(recordingAndReportingJBuilt.getReportingIntervalHours(), recordingReporting.reportingIntervalHours);
        assertEquals(recordingAndReportingJBuilt.getReportingNumberOfRetries(), recordingReporting.reportingRetries);
        assertEquals(recordingAndReportingJBuilt.getReportingTransmitWindowsMins(), recordingReporting.reportingTransmitWindowsMins);
        assertEquals(recordingAndReportingJBuilt.getReportingQuietStartMins(), recordingReporting.reportingQuietStartMins);
        assertEquals(recordingAndReportingJBuilt.getReportingQuietEndMins(), recordingReporting.reportingQuietEndMins);
        assertEquals(recordingAndReportingJBuilt.getRecordingStartTimeMins(), recordingReporting.recordingStartMins);
        assertEquals(recordingAndReportingJBuilt.getRecordingIntervalMins(), recordingReporting.recordingIntervalMins);

    }
}
