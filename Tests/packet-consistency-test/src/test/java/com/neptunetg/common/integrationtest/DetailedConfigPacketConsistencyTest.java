/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.integrationtest;

import com.neptunetg.common.c.*;
import com.neptunetg.common.integrationtest.framework.PacketConsistencyTestBase;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.*;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test packet ID 0
 *
 * TODO: complete this test with the post-pre-alpha detailed config packet definition once agreed
 }
 */
public class DetailedConfigPacketConsistencyTest extends PacketConsistencyTestBase
{
    /**
     * Create a packet in C and parse it in Java
     *
     */
    @Test
    public void testDetailedConfigPacketConsistencyCToJava() throws Exception
    {
        //build the packet in C
        final Memory packetBuffer = new Memory(1024L);

        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.DetailedConfigurationStatus.getId()); //packet type

        final CmiuPacketHeaderStruct cBuiltPacketHeader = new CmiuPacketHeaderStruct();
        cBuiltPacketHeader.cmiuId = 123456;
        cBuiltPacketHeader.cellularRssiAndBer = 123;
        cBuiltPacketHeader.encryptionMethod = 1;
        cBuiltPacketHeader.keyInfo = 2;
        cBuiltPacketHeader.networkFlags = 3;
        cBuiltPacketHeader.sequenceNumber = 4;
        cBuiltPacketHeader.timeAndDate = 5;
        cBuiltPacketHeader.tokenAesCrc = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuPacketHeader(packetPacker, cBuiltPacketHeader));

        final Memory secureDataBuffer = new Memory(1024L);

        final TpbpPackerStruct secureDataPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(secureDataPacker, secureDataBuffer, 1024);

        final CmiuFlagsStruct cBuiltCmiuFlags = new CmiuFlagsStruct();
        cBuiltCmiuFlags.cmiuType = 1;
        cBuiltCmiuFlags.numberOfDevices = 2;
        cBuiltCmiuFlags.cmiuFlags = 3;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuStatus(secureDataPacker, cBuiltCmiuFlags));

        final CmiuBasicConfigurationStruct cBuiltCmiuConfiguration = new CmiuBasicConfigurationStruct();
        cBuiltCmiuConfiguration.intervalCallInTime = 101;
        cBuiltCmiuConfiguration.callInInterval = 102;
        cBuiltCmiuConfiguration.callInWindow = 103;
        cBuiltCmiuConfiguration.reportingWindowRetries = 104;
        cBuiltCmiuConfiguration.quietTimeStartMins = 105;
        cBuiltCmiuConfiguration.quietTimeEndMins = 106;
        cBuiltCmiuConfiguration.numAttachedDevices = 107;
        cBuiltCmiuConfiguration.eventMask = 108;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuBasicConfiguration(secureDataPacker, cBuiltCmiuConfiguration));

        final IntervalRecordingConfigStruct cBuiltIntervalRecordingConfig = new IntervalRecordingConfigStruct();
        cBuiltIntervalRecordingConfig.deviceNumber = 4;
        cBuiltIntervalRecordingConfig.intervalFormat = 5;
        cBuiltIntervalRecordingConfig.deviceRecordingInterval = 6;
        cBuiltIntervalRecordingConfig.deviceReportingInterval = 7;
        cBuiltIntervalRecordingConfig.intervalStartTime = 8;
        cBuiltIntervalRecordingConfig.intervalLastReadDateTime = 9;
        assertTrue(tpbp.Tpbp_PackerAdd_IntervalRecordingConfig(secureDataPacker, cBuiltIntervalRecordingConfig));

        final CmiuDiagnosticsStruct cBuiltCmiuDiagnostics = new CmiuDiagnosticsStruct();
        cBuiltCmiuDiagnostics.diagnosticsResult = 10;
        cBuiltCmiuDiagnostics.processorResetCounter = 11;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuDiagnostics(secureDataPacker, cBuiltCmiuDiagnostics));

        final ReportedDeviceConfigurationStruct cBuiltReportedDeviceConfig = new ReportedDeviceConfigurationStruct();
        cBuiltReportedDeviceConfig.deviceNumber = 12;
        cBuiltReportedDeviceConfig.attachedDeviceId = 13;
        cBuiltReportedDeviceConfig.deviceType = 14;
        cBuiltReportedDeviceConfig.deviceFlags = 15;
        assertTrue(tpbp.Tpbp_PackerAdd_ReportedDeviceConfig(secureDataPacker, cBuiltReportedDeviceConfig));

        addCharArray(secureDataPacker, TagId.ErrorLog, "my error log");

        final ConnectionTimingLogStruct cBuiltConnectionTimingLog = new ConnectionTimingLogStruct();
        cBuiltConnectionTimingLog.dateOfConnection = 16;
        cBuiltConnectionTimingLog.timeFromPowerOnToNetworkRegistration = 17;
        cBuiltConnectionTimingLog.timeFromNetworkRegistrationToContextActivation = 18;
        cBuiltConnectionTimingLog.timeFromContextActivationToServerConnection = 19;
        cBuiltConnectionTimingLog.timeToTransferMessageToServer = 20;
        cBuiltConnectionTimingLog.timeToDisconnectAndShutdown = 21;
        assertTrue(tpbp.Tpbp_PackerAdd_ConnectionTimingLog(secureDataPacker, cBuiltConnectionTimingLog));

        final CmiuInformationStruct cBuiltCmiuInformation = new CmiuInformationStruct();
        cBuiltCmiuInformation.manufactureDate = 22;
        cBuiltCmiuInformation.dateOfInstallation = 23;
        cBuiltCmiuInformation.dateOfLastMagSwipe = 24;
        cBuiltCmiuInformation.magSwipeCounter = 25;
        cBuiltCmiuInformation.estimatedBatteryLifeRemaining = 26;
        cBuiltCmiuInformation.timeErrorOnLastNetworkTimeAccess = 27;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuInformation(secureDataPacker, cBuiltCmiuInformation));

        final RevisionStruct cBuiltHardwareRevision = new RevisionStruct();
        cBuiltHardwareRevision.versionMinor = 0x28;
        cBuiltHardwareRevision.versionMajor = 0x29;
        cBuiltHardwareRevision.versionYearMonthDay = 0x150916;
        cBuiltHardwareRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoHardware(secureDataPacker, cBuiltHardwareRevision));

        final RevisionStruct cBuiltFirmwareRevision = new RevisionStruct();
        cBuiltFirmwareRevision.versionMinor = 0x28;
        cBuiltFirmwareRevision.versionMajor = 0x29;
        cBuiltFirmwareRevision.versionYearMonthDay = 0x150916;
        cBuiltFirmwareRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoFirmware(secureDataPacker, cBuiltFirmwareRevision));

        final RevisionStruct cBuiltBootloadeRevision = new RevisionStruct();
        cBuiltBootloadeRevision.versionMinor = 0x28;
        cBuiltBootloadeRevision.versionMajor = 0x29;
        cBuiltBootloadeRevision.versionYearMonthDay = 0x150916;
        cBuiltBootloadeRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoBootloader(secureDataPacker, cBuiltBootloadeRevision));

        final RevisionStruct cBuiltConfigRevision = new RevisionStruct();
        cBuiltConfigRevision.versionMinor = 0x28;
        cBuiltConfigRevision.versionMajor = 0x29;
        cBuiltConfigRevision.versionYearMonthDay = 0x150916;
        cBuiltConfigRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoConfig(secureDataPacker, cBuiltConfigRevision));

        final RevisionStruct cBuiltArbConfigRevision = new RevisionStruct();
        cBuiltArbConfigRevision.versionMinor = 0x28;
        cBuiltArbConfigRevision.versionMajor = 0x29;
        cBuiltArbConfigRevision.versionYearMonthDay = 0x150916;
        cBuiltArbConfigRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoArb(secureDataPacker, cBuiltArbConfigRevision));

        final RevisionStruct cBuiltBleConfigRevision = new RevisionStruct();
        cBuiltBleConfigRevision.versionMinor = 0x28;
        cBuiltBleConfigRevision.versionMajor = 0x29;
        cBuiltBleConfigRevision.versionYearMonthDay = 0x150916;
        cBuiltBleConfigRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoBle(secureDataPacker, cBuiltBleConfigRevision));

        final RevisionStruct cBuiltEncryptionRevision = new RevisionStruct();
        cBuiltEncryptionRevision.versionMinor = 0x28;
        cBuiltEncryptionRevision.versionMajor = 0x29;
        cBuiltEncryptionRevision.versionYearMonthDay = 0x150916;
        cBuiltEncryptionRevision.versionBuildBcd = 0x12345678;
        assertTrue(tpbp.Tpbp_PackerAdd_ImageVersionInfoEncryption(secureDataPacker, cBuiltEncryptionRevision));

        addCharArray(secureDataPacker, TagId.NetworkOperators, "my network operator");

        addCharArray(secureDataPacker, TagId.NetworkPerformance, "my network performance");

        addCharArray(secureDataPacker, TagId.RegistrationStatus, "my registration status");

        addCharArray(secureDataPacker, TagId.MQTTBrokerIPAddress, "1.2.3.4");

        addCharArray(secureDataPacker, TagId.FallbackMQTTBrokerIPAddress, "2.3.4.5");

        addCharArray(secureDataPacker, TagId.AssignedCmiuIPAddress, "3.4.5.6");

        addCharArray(secureDataPacker, TagId.ModemHardwareRevision, "my modem hardware revision");

        addCharArray(secureDataPacker, TagId.ModemModelIdentificationCode, "my modem model ID");

        addCharArray(secureDataPacker, TagId.ModemManufacturerIdentificationCode, "my modems manufacturer ID");

        addCharArray(secureDataPacker, TagId.ModemSoftwareRevisionNumber, "my modems software revision");

        addCharArray(secureDataPacker, TagId.ModemSerialNumber, "my modems serial number");

        addCharArray(secureDataPacker, TagId.MsisdnRequest, "my MSISDN request");

        addCharArray(secureDataPacker, TagId.SimImsi, "my Sim IMSI");

        addCharArray(secureDataPacker, TagId.SimCardId, "my Sim card ID");

        addCharArray(secureDataPacker, TagId.PinPukPukTwoRequestData, "my pin/puk/puk2 request data");

        addCharArray(secureDataPacker, TagId.LastBleUserId, "my BLE ID");

        assertTrue(tpbp.Tpbp_PackerAdd_UInt64(secureDataPacker, TagId.LastBleUserLoginDate.getId(), 12345678L));

        assertTrue(tpbp.Tpbp_PackerAdd_UInt(secureDataPacker, TagId.PacketInstigator.getId(), 1, 1));

        assertTrue(tpbp.Tpbp_PackerPadZeroToBlockBoundary(secureDataPacker, 16));

        final int secureDataLengthInBytes = tpbp.Tpbp_PackerGetCount(secureDataPacker);
        final Memory encryptedSecureData = new Memory(secureDataLengthInBytes);

        encryptedSecureData.write(0L, secureDataBuffer.getByteArray(0L, secureDataLengthInBytes), 0, secureDataLengthInBytes);

        assertTrue(tpbp.Tpbp_PackerAdd_SecureBlockArray(packetPacker, encryptedSecureData, secureDataLengthInBytes));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //parse the packet data in Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.DetailedConfigurationStatus.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final CmiuPacketHeaderData parsedPacketData = (CmiuPacketHeaderData) ti.next();
        assertEquals(cBuiltPacketHeader.cmiuId, parsedPacketData.getCmiuId());
        assertEquals(cBuiltPacketHeader.cellularRssiAndBer, parsedPacketData.getBer() * 256 + parsedPacketData.getRssi());
        assertEquals(cBuiltPacketHeader.encryptionMethod, parsedPacketData.getEncryptionMethod());
        assertEquals(cBuiltPacketHeader.keyInfo, parsedPacketData.getKeyInfo());
        assertEquals(cBuiltPacketHeader.networkFlags, parsedPacketData.getNetworkFlags());
        assertEquals(cBuiltPacketHeader.sequenceNumber, parsedPacketData.getSequenceNumber());
        assertEquals(cBuiltPacketHeader.timeAndDate, parsedPacketData.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cBuiltPacketHeader.tokenAesCrc, parsedPacketData.getToken());


        final SecureBlockArrayData javaParsedSbad = (SecureBlockArrayData) ti.next();

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());

        //read the tags in the secure data

        final ByteBuffer encryptedData = javaParsedSbad.getEncryptedData();

        final ByteBuffer decryptedData = encryptedData.duplicate().order(ByteOrder.LITTLE_ENDIAN); //no decryption

        final TagSequenceParser secureDataParser = new TagSequenceParser();

        final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

        ti = secureTags.iterator();

        CmiuStatusData cmiuStatus = (CmiuStatusData)ti.next();
        assertEquals(cBuiltCmiuFlags.cmiuType, cmiuStatus.getCmiuType());
        assertEquals(cBuiltCmiuFlags.numberOfDevices, cmiuStatus.getNumberOfDevices());
        assertEquals(cBuiltCmiuFlags.cmiuFlags, cmiuStatus.getCmiuFlags());

        final CmiuBasicConfigurationData cmiuConfig = (CmiuBasicConfigurationData)ti.next();
        assertEquals(cBuiltCmiuConfiguration.intervalCallInTime, cmiuConfig.getReportingStartTime());
        assertEquals(cBuiltCmiuConfiguration.callInWindow, cmiuConfig.getReportingWindow());
        assertEquals(cBuiltCmiuConfiguration.reportingWindowRetries, cmiuConfig.getReportingWindowNumRetries());
        assertEquals(cBuiltCmiuConfiguration.quietTimeStartMins, cmiuConfig.getQuietTimeStartMinutes());
        assertEquals(cBuiltCmiuConfiguration.quietTimeEndMins, cmiuConfig.getQuietTimeEndMinutes());

        IntervalRecordingConfigurationData intervalRecordingConfig = (IntervalRecordingConfigurationData)ti.next();
        assertEquals(cBuiltIntervalRecordingConfig.deviceNumber, intervalRecordingConfig.getDeviceNumber());
        assertEquals(cBuiltIntervalRecordingConfig.intervalFormat, intervalRecordingConfig.getIntervalFormat());
        assertEquals(cBuiltIntervalRecordingConfig.deviceRecordingInterval, intervalRecordingConfig.getDeviceRecordingInterval());
        assertEquals(cBuiltIntervalRecordingConfig.deviceReportingInterval, intervalRecordingConfig.getReportingIntervalHours());
        assertEquals(cBuiltIntervalRecordingConfig.intervalStartTime, intervalRecordingConfig.getStartTimeOfRecordingInterval());
        assertEquals(cBuiltIntervalRecordingConfig.intervalLastReadDateTime, intervalRecordingConfig.getTimeOfMostRecentReading().getEpochSecond());

        CmiuDiagnosticsData cmiuDiagnostics = (CmiuDiagnosticsData)ti.next();
        assertEquals(cBuiltCmiuDiagnostics.diagnosticsResult, cmiuDiagnostics.getDiagnosticFlags());
        assertEquals(cBuiltCmiuDiagnostics.processorResetCounter, cmiuDiagnostics.getProcessorResetCount());

        ReportedDeviceConfigurationData reportedDeviceConfig = (ReportedDeviceConfigurationData)ti.next();
        assertEquals(cBuiltReportedDeviceConfig.deviceNumber, reportedDeviceConfig.getDeviceNumber());
        assertEquals(cBuiltReportedDeviceConfig.attachedDeviceId, reportedDeviceConfig.getAttachedDeviceId());
        assertEquals(cBuiltReportedDeviceConfig.deviceType, reportedDeviceConfig.getDeviceType());
        assertEquals(cBuiltReportedDeviceConfig.deviceFlags, reportedDeviceConfig.getDeviceFlags());

        checkCharArray(ti, TagId.ErrorLog, "my error log");

        ConnectionTimingLogData connectionTimingLog = (ConnectionTimingLogData)ti.next();
        assertEquals(cBuiltConnectionTimingLog.dateOfConnection, connectionTimingLog.getDateOfConnection().getEpochSecond());
        assertEquals(cBuiltConnectionTimingLog.timeFromPowerOnToNetworkRegistration, connectionTimingLog.getTimeFromPowerOnToNetworkRegistration());
        assertEquals(cBuiltConnectionTimingLog.timeFromNetworkRegistrationToContextActivation, connectionTimingLog.getTimeFromNetworkRegistrationToContextActivation());
        assertEquals(cBuiltConnectionTimingLog.timeFromContextActivationToServerConnection, connectionTimingLog.getTimeFromContextActivationToServerConnection());
        assertEquals(cBuiltConnectionTimingLog.timeToTransferMessageToServer, connectionTimingLog.getTimeToTransferMessageToServer());
        assertEquals(cBuiltConnectionTimingLog.timeToDisconnectAndShutdown, connectionTimingLog.getTimeToDisconnectAndShutdown());

        CmiuInformationData cmiuInformation = (CmiuInformationData)ti.next();
        assertEquals(cBuiltCmiuInformation.manufactureDate, cmiuInformation.getManufactureDate().getEpochSecond());
        assertEquals(cBuiltCmiuInformation.dateOfInstallation, cmiuInformation.getDateOfInstallation().getEpochSecond());
        assertEquals(cBuiltCmiuInformation.dateOfLastMagSwipe, cmiuInformation.getDateOfLastMagSwipe().getEpochSecond());
        assertEquals(cBuiltCmiuInformation.magSwipeCounter, cmiuInformation.getMagSwipeCounter());
        assertEquals(cBuiltCmiuInformation.estimatedBatteryLifeRemaining, cmiuInformation.getBatteryCapacityMah());
        assertEquals(cBuiltCmiuInformation.timeErrorOnLastNetworkTimeAccess, cmiuInformation.getTimeErrorOnLastNetworkAccess());

        checkRevision(ti, TagId.HardwareRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkRevision(ti, TagId.FirmwareRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkRevision(ti, TagId.BootloaderVersion, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkRevision(ti, TagId.ConfigRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkRevision(ti, TagId.ArbConfigRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkRevision(ti, TagId.BleConfigRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkRevision(ti, TagId.EncryptionConfigRevision, new byte[]{0x29,0x28,0x0,0x15,0x9,0x16,0x12,0x34,0x56,0x78});

        checkCharArray(ti, TagId.NetworkOperators, "my network operator");

        checkCharArray(ti, TagId.NetworkPerformance, "my network performance");

        checkCharArray(ti, TagId.RegistrationStatus, "my registration status");

        checkCharArray(ti, TagId.MQTTBrokerIPAddress, "1.2.3.4");

        checkCharArray(ti, TagId.FallbackMQTTBrokerIPAddress, "2.3.4.5");

        checkCharArray(ti, TagId.AssignedCmiuIPAddress, "3.4.5.6");

        checkCharArray(ti, TagId.ModemHardwareRevision, "my modem hardware revision");

        checkCharArray(ti, TagId.ModemModelIdentificationCode, "my modem model ID");

        checkCharArray(ti, TagId.ModemManufacturerIdentificationCode, "my modems manufacturer ID");

        checkCharArray(ti, TagId.ModemSoftwareRevisionNumber, "my modems software revision");

        ModemSerialNumberData modemSerialNumberData = (ModemSerialNumberData) ti.next();
        assertEquals(modemSerialNumberData.getAsString(), "my modems serial number");

        checkCharArray(ti, TagId.MsisdnRequest, "my MSISDN request");

        SimImsiData simImsiData = (SimImsiData) ti.next();
        assertEquals(simImsiData.getAsString(), "my Sim IMSI");

        SimCardIdData simCardIdData = (SimCardIdData) ti.next();
        assertEquals(simCardIdData.getAsString(), "my Sim card ID");

        checkCharArray(ti, TagId.PinPukPukTwoRequestData, "my pin/puk/puk2 request data");

        checkCharArray(ti, TagId. LastBleUserId, "my BLE ID");

        final IntegerData lastBleUserDate = (IntegerData)ti.next();
        assertEquals(TagId.LastBleUserLoginDate, lastBleUserDate.getTagId());
        assertEquals(12345678L, lastBleUserDate.getValue());

        final IntegerData packetInstigator = (IntegerData)ti.next();
        assertEquals(TagId.PacketInstigator, packetInstigator.getTagId());
        assertEquals(1, packetInstigator.getValue());
    }


    /**
     * Create a packet in Java and parse it in C
     *
     */
    @Test
    public void testDetailedConfigPacketConsistencyJavaToC() throws Exception
    {

        final CmiuPacketHeaderData javaBuiltHeader = new CmiuPacketHeaderData(
            2 /*cmiuId*/,
            3 /*int sequenceNumber*/,
            4 /*keyInfo*/,
            5 /*encryptionMethod*/,
            6 /*token*/,
            7 /*networkFlags*/,
            8 /*rssi*/,
            9 /*ber*/,
            new UnixTimestamp(10L));

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.DetailedConfigurationStatus);
        packetBuilder.appendTagData(javaBuiltHeader);

        final List<TaggedData> tagsToEncrypt = new ArrayList<>();

        final CmiuStatusData cmiuStatusJBuilt = new CmiuStatusData(1,2,3);
        tagsToEncrypt.add(cmiuStatusJBuilt);

        final CmiuBasicConfigurationData cmiuConfigurationJBuilt = new CmiuBasicConfigurationData(4,5,6,7,8,9,10,11);
        tagsToEncrypt.add(cmiuConfigurationJBuilt);

        final IntervalRecordingConfigurationData intervalRecordingConfigJBuilt =
                new IntervalRecordingConfigurationData(12,13,14,15,16,new UnixTimestamp(1234567L));
        tagsToEncrypt.add(intervalRecordingConfigJBuilt);

        final CmiuDiagnosticsData cmiuDiagnosticsJBuilt = new CmiuDiagnosticsData(17, 18);
        tagsToEncrypt.add(cmiuDiagnosticsJBuilt);

        final ReportedDeviceConfigurationData reportedDeviceConfigJBuilt = new ReportedDeviceConfigurationData(19, 20L, 21, 22, 23);
        tagsToEncrypt.add(reportedDeviceConfigJBuilt);

        tagsToEncrypt.add(new CharArrayData(TagId.ErrorLog, TagDataType.CharArray, "my error log"));

        final ConnectionTimingLogData connectionTimingLogJBuilt = new ConnectionTimingLogData(new UnixTimestamp(123456L), 24, 25, 26, 27, 28);
        tagsToEncrypt.add(connectionTimingLogJBuilt);

        final CmiuInformationData cmiuInformationJBuilt = new CmiuInformationData(new UnixTimestamp(12345L),
                                                                                    new UnixTimestamp(23456L),
                                                                                    new UnixTimestamp(34567L),
                                                                                    29, 30, 31);
        tagsToEncrypt.add(cmiuInformationJBuilt);

        final ImageVersionInfoData hardwareRevisionJBuilt = new ImageVersionInfoData(TagId.HardwareRevision, new byte[] {1,2,3,4,5,6,7,8,9,10});
        tagsToEncrypt.add(hardwareRevisionJBuilt);
        final ImageVersionInfoData firmwareRevisionJBuilt = new ImageVersionInfoData(TagId.FirmwareRevision, new byte[] {2,3,4,5,6,7,8,9,10,11});
        tagsToEncrypt.add(firmwareRevisionJBuilt);
        final ImageVersionInfoData bootloaderRevisionJBuilt = new ImageVersionInfoData(TagId.BootloaderVersion, new byte[] {3,4,5,6,7,8,9,10,11,12});
        tagsToEncrypt.add(bootloaderRevisionJBuilt);
        final ImageVersionInfoData configRevisionJBuilt = new ImageVersionInfoData(TagId.ConfigRevision, new byte[] {4,5,6,7,8,9,10,11,12,13});
        tagsToEncrypt.add(configRevisionJBuilt);
        final ImageVersionInfoData arbConfigRevisionJBuilt = new ImageVersionInfoData(TagId.ArbConfigRevision, new byte[] {5,6,7,8,9,10,11,12,13,14});
        tagsToEncrypt.add(arbConfigRevisionJBuilt);
        final ImageVersionInfoData bleConfigRevisionJBuilt = new ImageVersionInfoData(TagId.BleConfigRevision, new byte[] {6,7,8,9,10,11,12,13,14,15});
        tagsToEncrypt.add(bleConfigRevisionJBuilt);
        final ImageVersionInfoData encryptionConfigRevisionJBuilt = new ImageVersionInfoData(TagId.EncryptionConfigRevision, new byte[] {7,8,9,10,11,12,13,14,15,16});
        tagsToEncrypt.add(encryptionConfigRevisionJBuilt);

        tagsToEncrypt.add(new CharArrayData(TagId.NetworkOperators, TagDataType.CharArray, "my network operator"));
        tagsToEncrypt.add(new CharArrayData(TagId.NetworkPerformance, TagDataType.CharArray, "my network performance"));
        tagsToEncrypt.add(new CharArrayData(TagId.RegistrationStatus, TagDataType.CharArray, "my registration status"));
        tagsToEncrypt.add(new CharArrayData(TagId.MQTTBrokerIPAddress, TagDataType.CharArray, "1.2.3.4"));
        tagsToEncrypt.add(new CharArrayData(TagId.FallbackMQTTBrokerIPAddress, TagDataType.CharArray, "2.3.4.5"));
        tagsToEncrypt.add(new CharArrayData(TagId.AssignedCmiuIPAddress, TagDataType.CharArray, "3.4.5.6"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemHardwareRevision, TagDataType.CharArray, "modem hardware revision"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemModelIdentificationCode, TagDataType.CharArray, "modem model ID"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemManufacturerIdentificationCode, TagDataType.CharArray, "modem manufacturer ID code"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemSoftwareRevisionNumber, TagDataType.CharArray, "modem software revision"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemSerialNumber, TagDataType.CharArray, "modem serial number"));
        tagsToEncrypt.add(new CharArrayData(TagId.MsisdnRequest, TagDataType.CharArray, "MSISDN request"));
        tagsToEncrypt.add(new CharArrayData(TagId.SimImsi, TagDataType.CharArray, "sim IMSI"));
        tagsToEncrypt.add(new CharArrayData(TagId.SimCardId, TagDataType.CharArray, "sim card ID"));
        tagsToEncrypt.add(new CharArrayData(TagId.PinPukPukTwoRequestData, TagDataType.CharArray, "pin/puk/puk two request data"));
        tagsToEncrypt.add(new CharArrayData(TagId.LastBleUserId, TagDataType.CharArray, "Last BLE user ID"));

        tagsToEncrypt.add(new IntegerData(TagId.LastBleUserLoginDate, TagDataType.UInt64, 12345678L));

        final TagSequence tagSequenceToEncrypt = new TagSequence(tagsToEncrypt);

        System.out.println("Java built secure data as JSON: ");
        System.out.println(tagSequenceToEncrypt.toJsonPretty());

        byte[] dataToEncrypt = tagSequenceToEncrypt.toByteSequencePadToBlockBoundary(16); //pad to 16 bytes

        byte[] encryptedData = dataToEncrypt; //no encryption

        SecureBlockArrayData secureData = new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, encryptedData, dataToEncrypt.length/16);
        packetBuilder.appendTagData(secureData);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        final byte[] packetData = p.toByteSequence();

        System.out.println("Java built packet as JSON: ");
        System.out.println(p.toJsonPretty());

        printByteArray("Java built packet data as hex:", packetData);
        Files.write(Paths.get("javapacket.dat"), packetData);

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();
        final CmiuPacketHeaderStruct cParsedHeader = new CmiuPacketHeaderStruct();


        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.CmiuPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(CmiuPacketHeaderData.SIZE_IN_BYTES, tagStruct.dataSize);

        tpbp.Tpbp_ParserRead_CmiuPacketHeader(packetParserStruct, cParsedHeader);


        assertEquals(javaBuiltHeader.getCmiuId(), cParsedHeader.cmiuId);

        assertEquals(javaBuiltHeader.getBer() * 256 + javaBuiltHeader.getRssi(), cParsedHeader.cellularRssiAndBer);

        assertEquals(cParsedHeader.encryptionMethod, javaBuiltHeader.getEncryptionMethod());
        assertEquals(cParsedHeader.keyInfo, javaBuiltHeader.getKeyInfo());
        assertEquals(cParsedHeader.networkFlags, javaBuiltHeader.getNetworkFlags());
        assertEquals(cParsedHeader.sequenceNumber, javaBuiltHeader.getSequenceNumber());
        assertEquals(cParsedHeader.timeAndDate, javaBuiltHeader.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cParsedHeader.tokenAesCrc, javaBuiltHeader.getToken());

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.SecureDataBlockArray.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), tagStruct.tagType);

        final int secureDataSize = tagStruct.dataSize;
        final Memory encryptedDataFromC = new Memory(secureDataSize);
        tpbp.Tpbp_ParserGetBytes(packetParserStruct, encryptedDataFromC, secureDataSize);

        final Memory decryptedDataFromC = encryptedDataFromC; //no encryption

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);


        final TpbpParserStruct secureDataParserStruct = new TpbpParserStruct();

        tpbp.Tpbp_ParserInit(secureDataParserStruct, decryptedDataFromC, secureDataSize);


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuStatus.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final CmiuFlagsStruct cParsedCmiuFlags = new CmiuFlagsStruct();
        tpbp.Tpbp_ParserRead_CmiuStatus(secureDataParserStruct, cParsedCmiuFlags);
        assertEquals(cmiuStatusJBuilt.getCmiuType(), cParsedCmiuFlags.cmiuType);
        assertEquals(cmiuStatusJBuilt.getNumberOfDevices(), cParsedCmiuFlags.numberOfDevices);
        assertEquals(cmiuStatusJBuilt.getCmiuFlags(), cParsedCmiuFlags.cmiuFlags);

        //4,5,6,7,8,9,10,11
        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuConfiguration.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final CmiuBasicConfigurationStruct cParsedBasicConfiguration = new CmiuBasicConfigurationStruct();
        tpbp.Tpbp_ParserRead_CmiuBasicConfiguration(secureDataParserStruct, cParsedBasicConfiguration);
        assertEquals(4, cParsedBasicConfiguration.intervalCallInTime);
        assertEquals(5, cParsedBasicConfiguration.callInInterval);
        assertEquals(6, cParsedBasicConfiguration.callInWindow);
        assertEquals(7, cParsedBasicConfiguration.reportingWindowRetries);
        assertEquals(8, cParsedBasicConfiguration.quietTimeStartMins);
        assertEquals(9, cParsedBasicConfiguration.quietTimeEndMins);
        assertEquals(10, cParsedBasicConfiguration.numAttachedDevices);
        assertEquals(11, cParsedBasicConfiguration.eventMask);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.IntervalRecording.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final IntervalRecordingConfigStruct cParsedIntervalRecording = new IntervalRecordingConfigStruct();
        tpbp.Tpbp_ParserRead_IntervalRecordingConfig(secureDataParserStruct, cParsedIntervalRecording);
        assertEquals(intervalRecordingConfigJBuilt.getDeviceNumber(), cParsedIntervalRecording.deviceNumber);
        assertEquals(intervalRecordingConfigJBuilt.getIntervalFormat(), cParsedIntervalRecording.intervalFormat);
        assertEquals(intervalRecordingConfigJBuilt.getDeviceRecordingInterval(), cParsedIntervalRecording.deviceRecordingInterval);
        assertEquals(intervalRecordingConfigJBuilt.getReportingIntervalHours(), cParsedIntervalRecording.deviceReportingInterval);
        assertEquals(intervalRecordingConfigJBuilt.getStartTimeOfRecordingInterval(), cParsedIntervalRecording.intervalStartTime);
        assertEquals(intervalRecordingConfigJBuilt.getTimeOfMostRecentReading().getEpochSecond(), cParsedIntervalRecording.intervalLastReadDateTime);


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuHwDiagnostics.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final CmiuDiagnosticsStruct cParsedCmiuDiagnostics = new CmiuDiagnosticsStruct();
        tpbp.Tpbp_ParserRead_CmiuDiagnostics(secureDataParserStruct, cParsedCmiuDiagnostics);
        assertEquals(cmiuDiagnosticsJBuilt.getDiagnosticFlags(), cParsedCmiuDiagnostics.diagnosticsResult);
        assertEquals(cmiuDiagnosticsJBuilt.getProcessorResetCount(), cParsedCmiuDiagnostics.processorResetCounter);


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ReportedDeviceConfiguration.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final ReportedDeviceConfigurationStruct cParsedReportedDeviceConfig = new ReportedDeviceConfigurationStruct();
        tpbp.Tpbp_ParserRead_ReportedDeviceConfig(secureDataParserStruct, cParsedReportedDeviceConfig);
        assertEquals(reportedDeviceConfigJBuilt.getDeviceNumber(), cParsedReportedDeviceConfig.deviceNumber);
        assertEquals(reportedDeviceConfigJBuilt.getAttachedDeviceId(), cParsedReportedDeviceConfig.attachedDeviceId);
        assertEquals(reportedDeviceConfigJBuilt.getDeviceType(), cParsedReportedDeviceConfig.deviceType);
        assertEquals(reportedDeviceConfigJBuilt.getDeviceFlags(), cParsedReportedDeviceConfig.deviceFlags);


        checkCharArray(secureDataParserStruct, TagId.ErrorLog, "my error log");


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ConnectionTimingLog.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final ConnectionTimingLogStruct cParsedConnectionTimingLog = new ConnectionTimingLogStruct();
        tpbp.Tpbp_ParserRead_ConnectionTimingLog(secureDataParserStruct, cParsedConnectionTimingLog);
        assertEquals(connectionTimingLogJBuilt.getDateOfConnection().getEpochSecond(), cParsedConnectionTimingLog.dateOfConnection);
        assertEquals(connectionTimingLogJBuilt.getTimeFromPowerOnToNetworkRegistration(), cParsedConnectionTimingLog.timeFromPowerOnToNetworkRegistration);
        assertEquals(connectionTimingLogJBuilt.getTimeFromNetworkRegistrationToContextActivation(), cParsedConnectionTimingLog.timeFromNetworkRegistrationToContextActivation);
        assertEquals(connectionTimingLogJBuilt.getTimeFromContextActivationToServerConnection(), cParsedConnectionTimingLog.timeFromContextActivationToServerConnection);
        assertEquals(connectionTimingLogJBuilt.getTimeToTransferMessageToServer(), cParsedConnectionTimingLog.timeToTransferMessageToServer);
        assertEquals(connectionTimingLogJBuilt.getTimeToDisconnectAndShutdown(), cParsedConnectionTimingLog.timeToDisconnectAndShutdown);


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuInformation.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final CmiuInformationStruct cParsedCmiuInformation = new CmiuInformationStruct();
        tpbp.Tpbp_ParserRead_CmiuInformation(secureDataParserStruct, cParsedCmiuInformation);
        assertEquals(cmiuInformationJBuilt.getManufactureDate().getEpochSecond(), cParsedCmiuInformation.manufactureDate);
        assertEquals(cmiuInformationJBuilt.getDateOfInstallation().getEpochSecond(), cParsedCmiuInformation.dateOfInstallation);
        assertEquals(cmiuInformationJBuilt.getDateOfLastMagSwipe().getEpochSecond(), cParsedCmiuInformation.dateOfLastMagSwipe);
        assertEquals(cmiuInformationJBuilt.getMagSwipeCounter(), cParsedCmiuInformation.magSwipeCounter);
        assertEquals(cmiuInformationJBuilt.getBatteryCapacityMah(), cParsedCmiuInformation.estimatedBatteryLifeRemaining);
        assertEquals(cmiuInformationJBuilt.getTimeErrorOnLastNetworkAccess(), cParsedCmiuInformation.timeErrorOnLastNetworkTimeAccess);


        checkRevision(secureDataParserStruct, TagId.HardwareRevision, new byte[]{1,2,3,4,5,6,7,8,9,10});

        checkRevision(secureDataParserStruct, TagId.FirmwareRevision, new byte[]{2,3,4,5,6,7,8,9,10,11});

        checkRevision(secureDataParserStruct, TagId.BootloaderVersion, new byte[]{3,4,5,6,7,8,9,10,11,12});

        checkRevision(secureDataParserStruct, TagId.ConfigRevision, new byte[]{4,5,6,7,8,9,10,11,12,13});

        checkRevision(secureDataParserStruct, TagId.ArbConfigRevision, new byte[]{5,6,7,8,9,10,11,12,13,14});

        checkRevision(secureDataParserStruct, TagId.BleConfigRevision, new byte[]{6,7,8,9,10,11,12,13,14,15});

        checkRevision(secureDataParserStruct, TagId.EncryptionConfigRevision, new byte[]{7,8,9,10,11,12,13,14,15,16});

        checkCharArray(secureDataParserStruct, TagId.NetworkOperators, "my network operator");

        checkCharArray(secureDataParserStruct, TagId.NetworkPerformance, "my network performance");

        checkCharArray(secureDataParserStruct, TagId.RegistrationStatus, "my registration status");

        checkCharArray(secureDataParserStruct, TagId.MQTTBrokerIPAddress, "1.2.3.4");

        checkCharArray(secureDataParserStruct, TagId.FallbackMQTTBrokerIPAddress, "2.3.4.5");

        checkCharArray(secureDataParserStruct, TagId.AssignedCmiuIPAddress, "3.4.5.6");

        checkCharArray(secureDataParserStruct, TagId.ModemHardwareRevision, "modem hardware revision");

        checkCharArray(secureDataParserStruct, TagId.ModemModelIdentificationCode, "modem model ID");

        checkCharArray(secureDataParserStruct, TagId.ModemManufacturerIdentificationCode, "modem manufacturer ID code");

        checkCharArray(secureDataParserStruct, TagId.ModemSoftwareRevisionNumber, "modem software revision");

        checkCharArray(secureDataParserStruct, TagId.ModemSerialNumber, "modem serial number");

        checkCharArray(secureDataParserStruct, TagId.MsisdnRequest, "MSISDN request");

        checkCharArray(secureDataParserStruct, TagId.SimImsi, "sim IMSI");

        checkCharArray(secureDataParserStruct, TagId.SimCardId, "sim card ID");

        checkCharArray(secureDataParserStruct, TagId.PinPukPukTwoRequestData, "pin/puk/puk two request data");

        checkCharArray(secureDataParserStruct, TagId.LastBleUserId, "Last BLE user ID");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.LastBleUserLoginDate.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.UInt64.getId(), tagStruct.tagType);
        assertEquals(8, tagStruct.dataSize);

        Memory buf = new Memory(8L);
        tpbp.Tpbp_ParserGetBytes(secureDataParserStruct, buf, 8);
        assertEquals(12345678L, buf.getLong(0L));
    }
}
