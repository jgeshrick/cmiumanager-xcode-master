/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900integrationtest.framework;

import com.neptunetg.common.l900c.PacketHandlerInterop;
import com.sun.jna.Native;
import org.junit.BeforeClass;

import static org.junit.Assert.assertEquals;

/**
 * Created by WJD1 on 07/07/2016.
 * Test base for consistency tests against the Packet Handler C library
 */
public class PacketHandlerTestBase
{
    protected static PacketHandlerInterop packetHandler;

    @BeforeClass
    public static void setUpInterop()
    {
        assertEquals("Test can only run in 32 bit JVM because it loads a 32 bit DLL", 4, Native.POINTER_SIZE);

        String dllFile = System.getenv("PACKET_HANDLER_DLL");

        if (dllFile == null || dllFile.trim().length() == 0)
        {
            dllFile = "PacketHandlerLib.dll";
        }

        try
        {
            packetHandler = (PacketHandlerInterop) Native.loadLibrary(dllFile, PacketHandlerInterop.class);
        }
        catch (UnsatisfiedLinkError e)
        {
            throw new RuntimeException("Can't load DLL file " + dllFile + ".  Set environment variable PACKET_HANDLER_DLL to the path of this file.", e);
        }

    }
}
