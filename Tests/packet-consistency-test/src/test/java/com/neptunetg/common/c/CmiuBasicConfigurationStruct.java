/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by WJD1 on 03/07/2015.
 */
public class CmiuBasicConfigurationStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{"intervalCallInTime", "callInInterval",
                                                                    "callInWindow", "reportingWindowRetries",
                                                                    "quietTimeStartMins", "quietTimeEndMins",
                                                                    "numAttachedDevices", "eventMask"});


    public short intervalCallInTime;
    public byte callInInterval;
    public byte callInWindow;
    public byte reportingWindowRetries;
    public short quietTimeStartMins;
    public short quietTimeEndMins;
    public byte numAttachedDevices;
    public byte eventMask;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }
}
