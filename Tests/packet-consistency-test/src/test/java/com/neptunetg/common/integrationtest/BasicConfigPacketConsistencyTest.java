/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.integrationtest;

import com.neptunetg.common.c.*;
import com.neptunetg.common.integrationtest.framework.PacketConsistencyTestBase;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test packet ID 2
 *
 * TODO: complete this test with the post-pre-alpha detailed config packet definition once agreed
 }
 */
public class BasicConfigPacketConsistencyTest extends PacketConsistencyTestBase
{
    /**
     * Create a packet in C and parse it in Java
     *
     */
    @Test
    public void testBasicConfigPacketConsistencyCToJava() throws Exception
    {
        //build the packet in C
        final Memory packetBuffer = new Memory(1024L);

        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.BasicConfigurationStatus.getId()); //packet type

        final CmiuPacketHeaderStruct cBuiltPacketHeader = new CmiuPacketHeaderStruct();
        cBuiltPacketHeader.cmiuId = 123456;
        cBuiltPacketHeader.cellularRssiAndBer = 123;
        cBuiltPacketHeader.encryptionMethod = 1;
        cBuiltPacketHeader.keyInfo = 2;
        cBuiltPacketHeader.networkFlags = 3;
        cBuiltPacketHeader.sequenceNumber = 4;
        cBuiltPacketHeader.timeAndDate = 5;
        cBuiltPacketHeader.tokenAesCrc = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuPacketHeader(packetPacker, cBuiltPacketHeader));

        final Memory secureDataBuffer = new Memory(1024L);

        final TpbpPackerStruct secureDataPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(secureDataPacker, secureDataBuffer, 1024);

        final CmiuFlagsStruct cBuiltCmiuFlags = new CmiuFlagsStruct();
        cBuiltCmiuFlags.cmiuType = 1;
        cBuiltCmiuFlags.numberOfDevices = 2;
        cBuiltCmiuFlags.cmiuFlags = 3;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuStatus(secureDataPacker, cBuiltCmiuFlags));

        final CmiuBasicConfigurationStruct cBuiltCmiuConfiguration = new CmiuBasicConfigurationStruct();
        cBuiltCmiuConfiguration.intervalCallInTime = 101;
        cBuiltCmiuConfiguration.callInInterval = 102;
        cBuiltCmiuConfiguration.callInWindow = 103;
        cBuiltCmiuConfiguration.reportingWindowRetries = 104;
        cBuiltCmiuConfiguration.quietTimeStartMins = 105;
        cBuiltCmiuConfiguration.quietTimeEndMins = 106;
        cBuiltCmiuConfiguration.numAttachedDevices = 107;
        cBuiltCmiuConfiguration.eventMask = 108;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuBasicConfiguration(secureDataPacker, cBuiltCmiuConfiguration));

        final IntervalRecordingConfigStruct cBuiltIntervalRecordingConfig = new IntervalRecordingConfigStruct();
        cBuiltIntervalRecordingConfig.deviceNumber = 4;
        cBuiltIntervalRecordingConfig.intervalFormat = 5;
        cBuiltIntervalRecordingConfig.deviceRecordingInterval = 6;
        cBuiltIntervalRecordingConfig.deviceReportingInterval = 7;
        cBuiltIntervalRecordingConfig.intervalStartTime = 8;
        cBuiltIntervalRecordingConfig.intervalLastReadDateTime = 9;
        assertTrue(tpbp.Tpbp_PackerAdd_IntervalRecordingConfig(secureDataPacker, cBuiltIntervalRecordingConfig));

        final CmiuDiagnosticsStruct cBuiltCmiuDiagnostics = new CmiuDiagnosticsStruct();
        cBuiltCmiuDiagnostics.diagnosticsResult = 10;
        cBuiltCmiuDiagnostics.processorResetCounter = 11;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuDiagnostics(secureDataPacker, cBuiltCmiuDiagnostics));

        final ReportedDeviceConfigurationStruct cBuiltReportedDeviceConfig = new ReportedDeviceConfigurationStruct();
        cBuiltReportedDeviceConfig.deviceNumber = 12;
        cBuiltReportedDeviceConfig.attachedDeviceId = 13;
        cBuiltReportedDeviceConfig.deviceType = 14;
        cBuiltReportedDeviceConfig.deviceFlags = 15;
        assertTrue(tpbp.Tpbp_PackerAdd_ReportedDeviceConfig(secureDataPacker, cBuiltReportedDeviceConfig));

        addCharArray(secureDataPacker, TagId.ErrorLog, "my error log");

        final ConnectionTimingLogStruct cBuiltConnectionTimingLog = new ConnectionTimingLogStruct();
        cBuiltConnectionTimingLog.dateOfConnection = 16;
        cBuiltConnectionTimingLog.timeFromPowerOnToNetworkRegistration = 17;
        cBuiltConnectionTimingLog.timeFromNetworkRegistrationToContextActivation = 18;
        cBuiltConnectionTimingLog.timeFromContextActivationToServerConnection = 19;
        cBuiltConnectionTimingLog.timeToTransferMessageToServer = 20;
        cBuiltConnectionTimingLog.timeToDisconnectAndShutdown = 21;
        assertTrue(tpbp.Tpbp_PackerAdd_ConnectionTimingLog(secureDataPacker, cBuiltConnectionTimingLog));

        assertTrue(tpbp.Tpbp_PackerPadZeroToBlockBoundary(secureDataPacker, 16));

        final int secureDataLengthInBytes = tpbp.Tpbp_PackerGetCount(secureDataPacker);
        final Memory encryptedSecureData = new Memory(secureDataLengthInBytes);

        encryptedSecureData.write(0L, secureDataBuffer.getByteArray(0L, secureDataLengthInBytes), 0, secureDataLengthInBytes);

        assertTrue(tpbp.Tpbp_PackerAdd_SecureBlockArray(packetPacker, encryptedSecureData, secureDataLengthInBytes));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //parse the packet data in Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.BasicConfigurationStatus.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final CmiuPacketHeaderData parsedPacketData = (CmiuPacketHeaderData) ti.next();
        assertEquals(cBuiltPacketHeader.cmiuId, parsedPacketData.getCmiuId());
        assertEquals(cBuiltPacketHeader.cellularRssiAndBer, parsedPacketData.getBer() * 256 + parsedPacketData.getRssi());
        assertEquals(cBuiltPacketHeader.encryptionMethod, parsedPacketData.getEncryptionMethod());
        assertEquals(cBuiltPacketHeader.keyInfo, parsedPacketData.getKeyInfo());
        assertEquals(cBuiltPacketHeader.networkFlags, parsedPacketData.getNetworkFlags());
        assertEquals(cBuiltPacketHeader.sequenceNumber, parsedPacketData.getSequenceNumber());
        assertEquals(cBuiltPacketHeader.timeAndDate, parsedPacketData.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cBuiltPacketHeader.tokenAesCrc, parsedPacketData.getToken());


        final SecureBlockArrayData javaParsedSbad = (SecureBlockArrayData) ti.next();

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());

        //read the tags in the secure data

        final ByteBuffer encryptedData = javaParsedSbad.getEncryptedData();

        final ByteBuffer decryptedData = encryptedData.duplicate().order(ByteOrder.LITTLE_ENDIAN); //no decryption

        final TagSequenceParser secureDataParser = new TagSequenceParser();

        final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

        ti = secureTags.iterator();

        CmiuStatusData cmiuStatus = (CmiuStatusData)ti.next();
        assertEquals(cBuiltCmiuFlags.cmiuType, cmiuStatus.getCmiuType());
        assertEquals(cBuiltCmiuFlags.numberOfDevices, cmiuStatus.getNumberOfDevices());
        assertEquals(cBuiltCmiuFlags.cmiuFlags, cmiuStatus.getCmiuFlags());

        final CmiuBasicConfigurationData cmiuConfig = (CmiuBasicConfigurationData)ti.next();
        assertEquals(cBuiltCmiuConfiguration.intervalCallInTime, cmiuConfig.getReportingStartTime());
        assertEquals(cBuiltCmiuConfiguration.callInWindow, cmiuConfig.getReportingWindow());
        assertEquals(cBuiltCmiuConfiguration.reportingWindowRetries, cmiuConfig.getReportingWindowNumRetries());
        assertEquals(cBuiltCmiuConfiguration.quietTimeStartMins, cmiuConfig.getQuietTimeStartMinutes());
        assertEquals(cBuiltCmiuConfiguration.quietTimeEndMins, cmiuConfig.getQuietTimeEndMinutes());

        IntervalRecordingConfigurationData intervalRecordingConfig = (IntervalRecordingConfigurationData)ti.next();
        assertEquals(cBuiltIntervalRecordingConfig.deviceNumber, intervalRecordingConfig.getDeviceNumber());
        assertEquals(cBuiltIntervalRecordingConfig.intervalFormat, intervalRecordingConfig.getIntervalFormat());
        assertEquals(cBuiltIntervalRecordingConfig.deviceRecordingInterval, intervalRecordingConfig.getDeviceRecordingInterval());
        assertEquals(cBuiltIntervalRecordingConfig.deviceReportingInterval, intervalRecordingConfig.getReportingIntervalHours());
        assertEquals(cBuiltIntervalRecordingConfig.intervalStartTime, intervalRecordingConfig.getStartTimeOfRecordingInterval());
        assertEquals(cBuiltIntervalRecordingConfig.intervalLastReadDateTime, intervalRecordingConfig.getTimeOfMostRecentReading().getEpochSecond());

        CmiuDiagnosticsData cmiuDiagnostics = (CmiuDiagnosticsData)ti.next();
        assertEquals(cBuiltCmiuDiagnostics.diagnosticsResult, cmiuDiagnostics.getDiagnosticFlags());
        assertEquals(cBuiltCmiuDiagnostics.processorResetCounter, cmiuDiagnostics.getProcessorResetCount());

        ReportedDeviceConfigurationData reportedDeviceConfig = (ReportedDeviceConfigurationData)ti.next();
        assertEquals(cBuiltReportedDeviceConfig.deviceNumber, reportedDeviceConfig.getDeviceNumber());
        assertEquals(cBuiltReportedDeviceConfig.attachedDeviceId, reportedDeviceConfig.getAttachedDeviceId());
        assertEquals(cBuiltReportedDeviceConfig.deviceType, reportedDeviceConfig.getDeviceType());
        assertEquals(cBuiltReportedDeviceConfig.deviceFlags, reportedDeviceConfig.getDeviceFlags());

        checkCharArray(ti, TagId.ErrorLog, "my error log");

        ConnectionTimingLogData connectionTimingLog = (ConnectionTimingLogData)ti.next();
        assertEquals(cBuiltConnectionTimingLog.dateOfConnection, connectionTimingLog.getDateOfConnection().getEpochSecond());
        assertEquals(cBuiltConnectionTimingLog.timeFromPowerOnToNetworkRegistration, connectionTimingLog.getTimeFromPowerOnToNetworkRegistration());
        assertEquals(cBuiltConnectionTimingLog.timeFromNetworkRegistrationToContextActivation, connectionTimingLog.getTimeFromNetworkRegistrationToContextActivation());
        assertEquals(cBuiltConnectionTimingLog.timeFromContextActivationToServerConnection, connectionTimingLog.getTimeFromContextActivationToServerConnection());
        assertEquals(cBuiltConnectionTimingLog.timeToTransferMessageToServer, connectionTimingLog.getTimeToTransferMessageToServer());
        assertEquals(cBuiltConnectionTimingLog.timeToDisconnectAndShutdown, connectionTimingLog.getTimeToDisconnectAndShutdown());
    }


    /**
     * Create a packet in Java and parse it in C
     *
     */
    @Test
    public void testBasicConfigPacketConsistencyJavaToC() throws Exception
    {

        final CmiuPacketHeaderData javaBuiltHeader = new CmiuPacketHeaderData(
            2 /*cmiuId*/,
            3 /*int sequenceNumber*/,
            4 /*keyInfo*/,
            5 /*encryptionMethod*/,
            6 /*token*/,
            7 /*networkFlags*/,
            8 /*rssi*/,
            9 /*ber*/,
            new UnixTimestamp(10L));

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.BasicConfigurationStatus);
        packetBuilder.appendTagData(javaBuiltHeader);

        final List<TaggedData> tagsToEncrypt = new ArrayList<>();

        final CmiuStatusData cmiuStatusJBuilt = new CmiuStatusData(1,2,3);
        tagsToEncrypt.add(cmiuStatusJBuilt);

        final CmiuBasicConfigurationData cmiuConfigurationJBuilt = new CmiuBasicConfigurationData(4,5,6,7,8,9,10,11);
        tagsToEncrypt.add(cmiuConfigurationJBuilt);

        final IntervalRecordingConfigurationData intervalRecordingConfigJBuilt =
                new IntervalRecordingConfigurationData(12,13,14,15,16,new UnixTimestamp(1234567L));
        tagsToEncrypt.add(intervalRecordingConfigJBuilt);

        final CmiuDiagnosticsData cmiuDiagnosticsJBuilt = new CmiuDiagnosticsData(17, 18);
        tagsToEncrypt.add(cmiuDiagnosticsJBuilt);

        final ReportedDeviceConfigurationData reportedDeviceConfigJBuilt = new ReportedDeviceConfigurationData(19, 20L, 21, 22, 23);
        tagsToEncrypt.add(reportedDeviceConfigJBuilt);

        tagsToEncrypt.add(new CharArrayData(TagId.ErrorLog, TagDataType.CharArray, "my error log"));

        final ConnectionTimingLogData connectionTimingLogJBuilt = new ConnectionTimingLogData(new UnixTimestamp(123456L), 24, 25, 26, 27, 28);
        tagsToEncrypt.add(connectionTimingLogJBuilt);

        final TagSequence tagSequenceToEncrypt = new TagSequence(tagsToEncrypt);

        System.out.println("Java built secure data as JSON: ");
        System.out.println(tagSequenceToEncrypt.toJsonPretty());

        byte[] dataToEncrypt = tagSequenceToEncrypt.toByteSequencePadToBlockBoundary(16); //pad to 16 bytes

        byte[] encryptedData = dataToEncrypt; //no encryption

        SecureBlockArrayData secureData = new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, encryptedData, dataToEncrypt.length/16);
        packetBuilder.appendTagData(secureData);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        final byte[] packetData = p.toByteSequence();

        System.out.println("Java built packet as JSON: ");
        System.out.println(p.toJsonPretty());

        printByteArray("Java built packet data as hex:", packetData);
        Files.write(Paths.get("javapacket.dat"), packetData);

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();
        final CmiuPacketHeaderStruct cParsedHeader = new CmiuPacketHeaderStruct();


        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.CmiuPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(CmiuPacketHeaderData.SIZE_IN_BYTES, tagStruct.dataSize);

        tpbp.Tpbp_ParserRead_CmiuPacketHeader(packetParserStruct, cParsedHeader);


        assertEquals(javaBuiltHeader.getCmiuId(), cParsedHeader.cmiuId);

        assertEquals(javaBuiltHeader.getBer() * 256 + javaBuiltHeader.getRssi(), cParsedHeader.cellularRssiAndBer);

        assertEquals(cParsedHeader.encryptionMethod, javaBuiltHeader.getEncryptionMethod());
        assertEquals(cParsedHeader.keyInfo, javaBuiltHeader.getKeyInfo());
        assertEquals(cParsedHeader.networkFlags, javaBuiltHeader.getNetworkFlags());
        assertEquals(cParsedHeader.sequenceNumber, javaBuiltHeader.getSequenceNumber());
        assertEquals(cParsedHeader.timeAndDate, javaBuiltHeader.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cParsedHeader.tokenAesCrc, javaBuiltHeader.getToken());

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.SecureDataBlockArray.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), tagStruct.tagType);

        final int secureDataSize = tagStruct.dataSize;
        final Memory encryptedDataFromC = new Memory(secureDataSize);
        tpbp.Tpbp_ParserGetBytes(packetParserStruct, encryptedDataFromC, secureDataSize);

        final Memory decryptedDataFromC = encryptedDataFromC; //no encryption

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);


        final TpbpParserStruct secureDataParserStruct = new TpbpParserStruct();

        tpbp.Tpbp_ParserInit(secureDataParserStruct, decryptedDataFromC, secureDataSize);


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuStatus.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final CmiuFlagsStruct cParsedCmiuFlags = new CmiuFlagsStruct();
        tpbp.Tpbp_ParserRead_CmiuStatus(secureDataParserStruct, cParsedCmiuFlags);
        assertEquals(cmiuStatusJBuilt.getCmiuType(), cParsedCmiuFlags.cmiuType);
        assertEquals(cmiuStatusJBuilt.getNumberOfDevices(), cParsedCmiuFlags.numberOfDevices);
        assertEquals(cmiuStatusJBuilt.getCmiuFlags(), cParsedCmiuFlags.cmiuFlags);

        //4,5,6,7,8,9,10,11
        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuConfiguration.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final CmiuBasicConfigurationStruct cParsedBasicConfiguration = new CmiuBasicConfigurationStruct();
        tpbp.Tpbp_ParserRead_CmiuBasicConfiguration(secureDataParserStruct, cParsedBasicConfiguration);
        assertEquals(4, cParsedBasicConfiguration.intervalCallInTime);
        assertEquals(5, cParsedBasicConfiguration.callInInterval);
        assertEquals(6, cParsedBasicConfiguration.callInWindow);
        assertEquals(7, cParsedBasicConfiguration.reportingWindowRetries);
        assertEquals(8, cParsedBasicConfiguration.quietTimeStartMins);
        assertEquals(9, cParsedBasicConfiguration.quietTimeEndMins);
        assertEquals(10, cParsedBasicConfiguration.numAttachedDevices);
        assertEquals(11, cParsedBasicConfiguration.eventMask);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.IntervalRecording.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final IntervalRecordingConfigStruct cParsedIntervalRecording = new IntervalRecordingConfigStruct();
        tpbp.Tpbp_ParserRead_IntervalRecordingConfig(secureDataParserStruct, cParsedIntervalRecording);
        assertEquals(intervalRecordingConfigJBuilt.getDeviceNumber(), cParsedIntervalRecording.deviceNumber);
        assertEquals(intervalRecordingConfigJBuilt.getIntervalFormat(), cParsedIntervalRecording.intervalFormat);
        assertEquals(intervalRecordingConfigJBuilt.getDeviceRecordingInterval(), cParsedIntervalRecording.deviceRecordingInterval);
        assertEquals(intervalRecordingConfigJBuilt.getReportingIntervalHours(), cParsedIntervalRecording.deviceReportingInterval);
        assertEquals(intervalRecordingConfigJBuilt.getStartTimeOfRecordingInterval(), cParsedIntervalRecording.intervalStartTime);
        assertEquals(intervalRecordingConfigJBuilt.getTimeOfMostRecentReading().getEpochSecond(), cParsedIntervalRecording.intervalLastReadDateTime);


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuHwDiagnostics.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final CmiuDiagnosticsStruct cParsedCmiuDiagnostics = new CmiuDiagnosticsStruct();
        tpbp.Tpbp_ParserRead_CmiuDiagnostics(secureDataParserStruct, cParsedCmiuDiagnostics);
        assertEquals(cmiuDiagnosticsJBuilt.getDiagnosticFlags(), cParsedCmiuDiagnostics.diagnosticsResult);
        assertEquals(cmiuDiagnosticsJBuilt.getProcessorResetCount(), cParsedCmiuDiagnostics.processorResetCounter);


        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ReportedDeviceConfiguration.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final ReportedDeviceConfigurationStruct cParsedReportedDeviceConfig = new ReportedDeviceConfigurationStruct();
        tpbp.Tpbp_ParserRead_ReportedDeviceConfig(secureDataParserStruct, cParsedReportedDeviceConfig);
        assertEquals(reportedDeviceConfigJBuilt.getDeviceNumber(), cParsedReportedDeviceConfig.deviceNumber);
        assertEquals(reportedDeviceConfigJBuilt.getAttachedDeviceId(), cParsedReportedDeviceConfig.attachedDeviceId);
        assertEquals(reportedDeviceConfigJBuilt.getDeviceType(), cParsedReportedDeviceConfig.deviceType);
        assertEquals(reportedDeviceConfigJBuilt.getDeviceFlags(), cParsedReportedDeviceConfig.deviceFlags);

        checkCharArray(secureDataParserStruct, TagId.ErrorLog, "my error log");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ConnectionTimingLog.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final ConnectionTimingLogStruct cParsedConnectionTimingLog = new ConnectionTimingLogStruct();
        tpbp.Tpbp_ParserRead_ConnectionTimingLog(secureDataParserStruct, cParsedConnectionTimingLog);
        assertEquals(connectionTimingLogJBuilt.getDateOfConnection().getEpochSecond(), cParsedConnectionTimingLog.dateOfConnection);
        assertEquals(connectionTimingLogJBuilt.getTimeFromPowerOnToNetworkRegistration(), cParsedConnectionTimingLog.timeFromPowerOnToNetworkRegistration);
        assertEquals(connectionTimingLogJBuilt.getTimeFromNetworkRegistrationToContextActivation(), cParsedConnectionTimingLog.timeFromNetworkRegistrationToContextActivation);
        assertEquals(connectionTimingLogJBuilt.getTimeFromContextActivationToServerConnection(), cParsedConnectionTimingLog.timeFromContextActivationToServerConnection);
        assertEquals(connectionTimingLogJBuilt.getTimeToTransferMessageToServer(), cParsedConnectionTimingLog.timeToTransferMessageToServer);
        assertEquals(connectionTimingLogJBuilt.getTimeToDisconnectAndShutdown(), cParsedConnectionTimingLog.timeToDisconnectAndShutdown);
    }
}
