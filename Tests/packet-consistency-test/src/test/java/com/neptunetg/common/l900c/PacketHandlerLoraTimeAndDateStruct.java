/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing a time offset information
 *
     typedef struct sTag_PACKET_LORA_TIME_AND_DATE
     {
     uint8_t     packetType;
     uint64_t    timeAndDate;
     uint8_t     timeUpdate;
     uint16_t    spare; // 11 bits
     uint8_t     packetBuffer[11];
     } S_PACKET_LORA_TIME_AND_DATE;

 */

public class PacketHandlerLoraTimeAndDateStruct extends Structure

{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "packetType",
        "timeAndDate",
        "timeUpdate",
        "spare",
        "packetBuffer"
    });

    public byte packetType;
    public long timeAndDate;
    public byte timeUpdate;
    public short spare;
    public byte[] packetBuffer;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
