/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing a 11 byte read packet
 *
 typedef struct sTag_PACKET_LORA_11BYTE_READING
 {
 uint8_t packetType;
 UU32 readingCurrent;
 UU32 readingOld;
 UU32 readingOlder;
 uint8_t reverseFlowFlag;
 uint8_t continuousLeakFlag;
 uint8_t packetBuffer[11];
 } S_PACKET_LORA_11BYTE_READING;

 */

public class PacketHandlerLora11ByteReadStruct extends Structure

{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
            "packetType",
            "readingCurrent",
            "readingOld",
            "readingOlder",
            "reverseFlowFlag",
            "continuousLeakFlag",
            "packetBuffer"
    });

    public byte packetType;

    public int readingCurrent;
    public int readingOld;
    public int readingOlder;
    public byte reverseFlowFlag;
    public byte continuousLeakFlag;

    public byte[] packetBuffer;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
