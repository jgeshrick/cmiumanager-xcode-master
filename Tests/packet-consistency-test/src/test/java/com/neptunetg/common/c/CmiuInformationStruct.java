/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by WJD1 on 02/07/2015.
 *
 *typedef struct S_CMIU_INFORMATION
 {
 uint64_t    manufactureDate;
 uint64_t    installationDate;
 uint64_t    dateLastMagSwipe;
 uint8_t     magSwipes;
 uint16_t    batteryRemaining;
 int32_t     timeErrorLastNetworkTimeAccess;
 } S_CMIU_INFORMATION;
 */
public class CmiuInformationStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{"manufactureDate", "dateOfInstallation", "dateOfLastMagSwipe",
            "magSwipeCounter", "estimatedBatteryLifeRemaining", "timeErrorOnLastNetworkTimeAccess"});

    public long manufactureDate;
    public long dateOfInstallation;
    public long dateOfLastMagSwipe;
    public byte magSwipeCounter;
    public short estimatedBatteryLifeRemaining;
    public int timeErrorOnLastNetworkTimeAccess;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }
}
