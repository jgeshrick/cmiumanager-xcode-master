
/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * An object to manage tag lead-ins
 *
 * See Common\TaggedPacketBuilderParser\src\main\c\Tpbp.h
 *
 * {@code
 * Definition of the tag data bytes
typedef struct S_TPBP_TAG
        {
        uint32_t        tagNumber;      //!< The meaning of the packet
        uint8_t        tagType;         //!< If packet is data, then use this
        uint32_t        arraySize;      //!< The number of bytes following this tag
        }
        S_TPBP_TAG;

}
 */


public class TpbpTagStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{"tagNumber", "tagType", "dataSize"});

    public int tagNumber;
    public byte tagType;
    public int dataSize;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }
}
