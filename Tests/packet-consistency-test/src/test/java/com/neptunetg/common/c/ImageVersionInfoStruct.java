/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for E_TAG_TYPE_IMAGE_VERSION_INFO (tag type 14)
 *
typedef struct S_IMAGE_VERSION_INFO
        {
         Major version as binary coded decimal - e.g. version fifteen = 0x15
        uint8_t     versionMajorBcd;

         Minor version as binary coded decimal - e.g. version sixteen = 0x16
        uint8_t     versionMinorBcd;

         Date as binary coded decimal - e.g. 0x150619 for June 19, 2015
        uint32_t    versionYearMonthDayBcd;

         Build number as binary coded decimal - e.g. 0x1234 for one thousand two hundred thirty four
        uint32_t    versionBuildBcd;

        } S_IMAGE_VERSION_INFO;
*/
public class ImageVersionInfoStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
            "versionMajorBcd",
            "versionMinorBcd",
            "versionYearMonthDayBcd",
            "versionBuildBcd"
    });

    public byte versionMajorBcd;
    public byte versionMinorBcd;
    public int versionYearMonthDayBcd;
    public int versionBuildBcd;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
