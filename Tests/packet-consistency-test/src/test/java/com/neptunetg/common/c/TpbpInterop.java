
/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Library;
import com.sun.jna.Pointer;

/**
 * See Tpbp.h
 */
public interface TpbpInterop extends Library
{
    /*
     * Packer functions
     */
    /**
    DLL_EXPORT void      Tpbp_PackerInit( S_TPBP_PACKER* pPacker, uint8_t* pBuffer, uint32_t bufferSize);
     */
    public void Tpbp_PackerInit(TpbpPackerStruct packer, Pointer pBuffer, int bufferSize);

    /**
    DLL_EXPORT uint32_t  Tpbp_PackerGetCount(const S_TPBP_PACKER* pPacker);
    */
    public int Tpbp_PackerGetCount(TpbpPackerStruct packer);

    /**
    DLL_EXPORT bool      Tpbp_Packer_BeginPacket(S_TPBP_PACKER* const pPacker, const E_PACKET_TYPE_ID packetTypeId);
    */
    public boolean Tpbp_Packer_BeginPacket(TpbpPackerStruct packer, int packetTypeId);

    /**
    DLL_EXPORT bool      Tpbp_PackerAddBytes(S_TPBP_PACKER* pPacker, const uint8_t bytes[], uint32_t numBytes);
     */
    public boolean Tpbp_PackerAddBytes(TpbpPackerStruct packer, Pointer bytes, int numBytes);

    /**
    DLL_EXPORT bool      Tpbp_PackerPadZeroToBlockBoundary(S_TPBP_PACKER* pPacker, uint32_t blockSize);
     */
    public boolean Tpbp_PackerPadZeroToBlockBoundary(TpbpPackerStruct packer, int blockSize);

    /**
    DLL_EXPORT bool      Tpbp_PackerTagHelper(S_TPBP_PACKER* pPacker, const S_TPBP_TAG* pTag);
     */
    public boolean Tpbp_PackerTagHelper(TpbpPackerStruct packer, TpbpTagStruct tag);

    /**
     * DLL_EXPORT bool		 Tpbp_PackerAdd_CmiuDiagnostics(S_TPBP_PACKER* pPacker, const S_CMIU_DIAGNOSTICS* data);
     */
    public boolean Tpbp_PackerAdd_CmiuDiagnostics(TpbpPackerStruct packer, CmiuDiagnosticsStruct data);

    /**
     * DLL_EXPORT bool		 Tpbp_PackerAdd_ConnectionTimingLog(S_TPBP_PACKER* pPacker, const S_CONNECTION_TIMING_LOG* data);
     */
    public boolean Tpbp_PackerAdd_ConnectionTimingLog(TpbpPackerStruct packer, ConnectionTimingLogStruct data);

    /**
     * DLL_EXPORT bool		 Tpbp_PackerAdd_SecureBlockArray(S_TPBP_PACKER* pPacker, const uint8_t* dataForArray, uint32_t arrayLengthInBytes);
     */
    public boolean Tpbp_PackerAdd_SecureBlockArray(TpbpPackerStruct packer, Pointer dataForArray, int arrayLengthInBytes);

    /**
     * DLL_EXPORT bool		 Tpbp_PackerAdd_ImageVersionInfo(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const S_IMAGE_VERSION_INFO* const dataToAdd);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfo(TpbpPackerStruct packer, int tagNumber, ImageVersionInfoStruct data);

    /**
    DLL_EXPORT bool      Tpbp_Pack(S_TPBP_PACKER* pPacker, uint32_t n, uint32_t numBytes);
    */
    public boolean Tpbp_Pack(TpbpPackerStruct packer, int n, int numBytes);

    /**
     DLL_EXPORT bool      Tpbp_Pack64(S_TPBP_PACKER* pPacker, uint64_t n);
     */
    public boolean Tpbp_Pack64(TpbpPackerStruct packer, long n);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_CmiuInformation(S_TPBP_PACKER* const pPacker, const S_CMIU_INFORMATION* const data);
     */
    public boolean Tpbp_PackerAdd_CmiuInformation(TpbpPackerStruct packer, CmiuInformationStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageMetadata(S_TPBP_PACKER* const pPacker, const S_IMAGE_METADATA* const pImageMetadata);
     */
    public boolean Tpbp_PackerAdd_ImageMetadata(TpbpPackerStruct packer, ImageMetadataStruct data);


    /*Add a tag, for defined payloads */
    /**
    DLL_EXPORT bool      Tpbp_PackerAdd_CmiuPacketHeader(S_TPBP_PACKER* pPacker, const S_CMIU_PACKET_HEADER* data);
     */
    public boolean Tpbp_PackerAdd_CmiuPacketHeader(TpbpPackerStruct packer, CmiuPacketHeaderStruct data);

    /**
    DLL_EXPORT bool      Tpbp_PackerAdd_CmiuConfiguration(S_TPBP_PACKER* pPacker, const S_CMIU_CONFIGURATION* pkt);
    */
    public boolean Tpbp_PackerAdd_CmiuConfiguration(TpbpPackerStruct packer, CmiuConfigurationStruct data);

    /**
     * DLL_EXPORT bool		 Tpbp_Packer_EndPacket(S_TPBP_PACKER* const pPacker);
     */
    public boolean Tpbp_Packer_EndPacket(TpbpPackerStruct packer);

    /**
     * DLL_EXPORT bool		 Tpbp_PackerAdd_UInt64(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const uint64_t data);
     */
    public boolean Tpbp_PackerAdd_UInt64(TpbpPackerStruct packer, int tagNumber, long data);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_UInt(S_TPBP_PACKER* const pPacker, const E_TAG_NUMBER tagNumber, const uint32_t data, const uint32_t numBytes);
     */
    public boolean Tpbp_PackerAdd_UInt(TpbpPackerStruct packer, int tagNumber, int data, int numBytes);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_CmiuStatus(S_TPBP_PACKER* const pPacker, const S_CMIU_STATUS* const data);
     */
    public boolean Tpbp_PackerAdd_CmiuStatus(TpbpPackerStruct parser, CmiuFlagsStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ReportedDeviceConfig(S_TPBP_PACKER* const pPacker, const S_REPORTED_DEVICE_CONFIG* const data);
     */
    public boolean Tpbp_PackerAdd_ReportedDeviceConfig(TpbpPackerStruct parser, ReportedDeviceConfigurationStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_IntervalRecordingConfig(S_TPBP_PACKER* const pPacker, const S_INTERVAL_RECORDING_CONFIG* const data);
     */
    public boolean Tpbp_PackerAdd_IntervalRecordingConfig(TpbpPackerStruct parser, IntervalRecordingConfigStruct data);


    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoHardware(S_TPBP_PACKER* const pPacker, const S_REVISION* const pRevision);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfoHardware(TpbpPackerStruct packer, RevisionStruct data);


    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoFirmware(S_TPBP_PACKER* const pPacker, const S_REVISION* const pRevision);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfoFirmware(TpbpPackerStruct packer, RevisionStruct data);


    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoBootloader(S_TPBP_PACKER* const pPacker, const S_REVISION* const pRevision);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfoBootloader(TpbpPackerStruct packer, RevisionStruct data);


    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoConfig(S_TPBP_PACKER* const pPacker, const S_REVISION* const pRevision);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfoConfig(TpbpPackerStruct packer, RevisionStruct data);


    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoArb(S_TPBP_PACKER* const pPacker, const S_REVISION* const pRevision);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfoArb(TpbpPackerStruct packer, RevisionStruct data);


    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoBle(S_TPBP_PACKER* const pPacker, const S_REVISION* const pRevision);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfoBle(TpbpPackerStruct packer, RevisionStruct data);


    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_ImageVersionInfoEncryption(S_TPBP_PACKER* const pPacker, const S_REVISION* const pRevision);
     */
    public boolean Tpbp_PackerAdd_ImageVersionInfoEncryption(TpbpPackerStruct packer, RevisionStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_CmiuBasicConfiguration(S_TPBP_PACKER* const pPacker, const S_CMIU_BASIC_CONFIGURATION* const data);
     */
    public boolean Tpbp_PackerAdd_CmiuBasicConfiguration(TpbpPackerStruct packer, CmiuBasicConfigurationStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_UartPacketHeader(S_TPBP_PACKER* const pPacker, const S_MQTT_BLE_UART* const data);
     */
    public boolean Tpbp_PackerAdd_UartPacketHeader(TpbpPackerStruct packer, MqttPacketHeaderStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_PackerAdd_RecordingReportingInterval(S_TPBP_PACKER* const pPacker, const S_RECORDING_REPORTING_INTERVAL* const data);
     */
    public boolean Tpbp_PackerAdd_RecordingReportingInterval(TpbpPackerStruct packer, RecordingReportingIntervalStruct data);




/*
 * Parser functions
 */
    /**
    DLL_EXPORT void      Tpbp_ParserInit(S_TPBP_PARSER* pParser, const uint8_t* pBuffer, uint32_t bufferSize);
    */
    public void Tpbp_ParserInit(TpbpParserStruct parser, Pointer pBuffer, int bufferSize);

/*
    DLL_EXPORT uint32_t  Tpbp_ParserGetBytesRemaining(const S_TPBP_PARSER* pParser);
    */

    /**
    DLL_EXPORT bool      Tpbp_ParserGetBytes(S_TPBP_PARSER* pParser, uint8_t pBuffer[], uint32_t numBytes);
    */
    public boolean Tpbp_ParserGetBytes(TpbpParserStruct parser, Pointer pBuffer, int numBytes);

    /**
    DLL_EXPORT bool      Tpbp_ParserReadTag(S_TPBP_PARSER* pParser,  S_TPBP_TAG* pTag);
    */
    public boolean Tpbp_ParserReadTag(TpbpParserStruct parser, TpbpTagStruct tag);


/*
    DLL_EXPORT uint32_t  Tpbp_Unpack(S_TPBP_PARSER* pParser, uint32_t numBytes);
    DLL_EXPORT uint64_t  Tpbp_Unpack64(S_TPBP_PARSER* pParser);
*/
    public int Tpbp_Unpack(TpbpParserStruct parser,  int numBytes);
    public long Tpbp_Unpack64(TpbpParserStruct parser);

    /**
     * DLL_EXPORT bool		 Tpbp_ParserRead_ImageVersionInfo(S_TPBP_PARSER* const pParser, S_IMAGE_VERSION_INFO* const data);
     */
    public boolean Tpbp_ParserRead_ImageVersionInfo(TpbpParserStruct pParser, ImageVersionInfoStruct data);

    /**
    DLL_EXPORT bool      Tpbp_ParserRead_CmiuPacketHeader(S_TPBP_PARSER* pParser, S_CMIU_PACKET_HEADER* data);
     */
    public boolean Tpbp_ParserRead_CmiuPacketHeader(TpbpParserStruct parser, CmiuPacketHeaderStruct data);

    /**
    DLL_EXPORT bool      Tpbp_ParserRead_CmiuConfiguration(S_TPBP_PARSER* pParser, S_CMIU_CONFIGURATION* data);
    */
    public boolean Tpbp_ParserRead_CmiuConfiguration(TpbpParserStruct parser, CmiuConfigurationStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_ConnectionTimingLog(S_TPBP_PARSER* pParser, S_CONNECTION_TIMING_LOG* data);
     */
    public boolean Tpbp_ParserRead_ConnectionTimingLog(TpbpParserStruct parser, ConnectionTimingLogStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_CmiuDiagnostics(S_TPBP_PARSER* pParser, S_CMIU_DIAGNOSTICS* data);
     */
    public boolean Tpbp_ParserRead_CmiuDiagnostics(TpbpParserStruct parser, CmiuDiagnosticsStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_CmiuStatus(S_TPBP_PARSER* const pPacker,   S_CMIU_STATUS* const data);
     */
    public boolean Tpbp_ParserRead_CmiuStatus(TpbpParserStruct parser, CmiuFlagsStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_ReportedDeviceConfig(S_TPBP_PARSER* const pPacker, S_REPORTED_DEVICE_CONFIG* const data);
     */
    public boolean Tpbp_ParserRead_ReportedDeviceConfig(TpbpParserStruct parser, ReportedDeviceConfigurationStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_IntervalRecordingConfig(S_TPBP_PARSER* const pParser,   S_INTERVAL_RECORDING_CONFIG* const data);
     */
    public boolean Tpbp_ParserRead_IntervalRecordingConfig(TpbpParserStruct parser, IntervalRecordingConfigStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_CmiuInformation(S_TPBP_PARSER* const pParser, S_CMIU_INFORMATION* const data);
     */
    public boolean Tpbp_ParserRead_CmiuInformation(TpbpParserStruct parser, CmiuInformationStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_Revision(S_TPBP_PARSER* const pParser, S_REVISION* const pRevision);
     */
    public boolean Tpbp_ParserRead_Revision(TpbpParserStruct parser, RevisionStruct data);

    /**
     *DLL_EXPORT bool      Tpbp_ParserRead_ImageMetadata(S_TPBP_PARSER* const pParser, S_IMAGE_METADATA* const pImageMetadata);
     */
    public boolean Tpbp_ParserRead_ImageMetadata(TpbpParserStruct parser, ImageMetadataStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_CmiuBasicConfiguration(S_TPBP_PARSER* const S_TPBP_PARSER, S_CMIU_BASIC_CONFIGURATION* const data);
     */
    public boolean Tpbp_ParserRead_CmiuBasicConfiguration(TpbpParserStruct parser, CmiuBasicConfigurationStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_UartPacketHeader(S_TPBP_PARSER* const pParser, S_MQTT_BLE_UART* const data);
     */
    public boolean Tpbp_ParserRead_UartPacketHeader(TpbpParserStruct parser, MqttPacketHeaderStruct data);

    /**
     * DLL_EXPORT bool      Tpbp_ParserRead_RecordingReportingInterval(S_TPBP_PARSER* const pParser, S_RECORDING_REPORTING_INTERVAL* const data);
     */
    public boolean Tpbp_ParserRead_RecordingReportingInterval(TpbpParserStruct parser, RecordingReportingIntervalStruct data);
}
