/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for holding a reading with flags
 *
 #define NUM_OF_READINGS 12
 #define PACKET_SIZE_FOR_53BYTE_LORA 53
 typedef struct sTag_PACKET_LORA_53BYTE_READING
 {
 uint8_t     packetType;    //  5 bits
 S_PACKET_READING_W_FLAGS reading[NUM_OF_READINGS]; // 12 readings plus flags, taking up 32 bits each
 uint16_t     timeLastRead; // 16 bits
 uint32_t    spare;         // 19 bits
 uint8_t     packetBuffer[PACKET_SIZE_FOR_53BYTE_LORA];
 } S_PACKET_LORA_53BYTE_READING;
 */

public class PacketHandlerLoraReadingWFlagsStruct extends Structure

{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
            "reading",
            "spare",
            "revFlowFlag",
            "leakFlag"
    });

    public int reading;
    public byte spare;
    public byte revFlowFlag;
    public byte leakFlag;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
