/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing CMIU Diagnostics payload to packer

 {@code
    typedef struct S_CMIU_DIAGNOSTICS
        {
        uint32_t diagnosticsResult;
        uint8_t processorResetCounter;
        } S_CMIU_DIAGNOSTICS;
 }
 */
public class CmiuDiagnosticsStruct extends Structure
{

    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "diagnosticsResult",
        "processorResetCounter"
    });

    public int diagnosticsResult;
    public byte processorResetCounter;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
