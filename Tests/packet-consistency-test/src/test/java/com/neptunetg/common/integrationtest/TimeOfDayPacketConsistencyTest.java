/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.integrationtest;

import com.neptunetg.common.c.MqttPacketHeaderStruct;
import com.neptunetg.common.c.TpbpPackerStruct;
import com.neptunetg.common.c.TpbpParserStruct;
import com.neptunetg.common.c.TpbpTagStruct;
import com.neptunetg.common.integrationtest.framework.PacketConsistencyTestBase;
import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.MqttPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Consistency test for packet type 4
 */
public class TimeOfDayPacketConsistencyTest extends PacketConsistencyTestBase
{
    /**
     * Create a packet of type 4 in C and parse it in Java
     */
    @Test
    public void testTimeOfDayPacketConsistencyCToJava() throws Exception
    {
        //build the packet in C
        final Memory packetBuffer = new Memory(1024L);

        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.TimeOfDay.getId()); //packet type

        final MqttPacketHeaderStruct cBuiltMqttPacketHeader = new MqttPacketHeaderStruct();
        cBuiltMqttPacketHeader.sequenceNumber = 1;
        cBuiltMqttPacketHeader.keyInfo = 2;
        cBuiltMqttPacketHeader.encryptionMethod = 3;
        cBuiltMqttPacketHeader.token = 4;
        assertTrue(tpbp.Tpbp_PackerAdd_UartPacketHeader(packetPacker, cBuiltMqttPacketHeader));

        assertTrue(tpbp.Tpbp_PackerAdd_UInt64(packetPacker, TagId.CurrentTimeAndDate.getId(), 12345678L));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //parse the packet data in Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.TimeOfDay.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final MqttPacketHeaderData mqttPacketHeader = (MqttPacketHeaderData)ti.next();
        assertEquals(cBuiltMqttPacketHeader.sequenceNumber, mqttPacketHeader.getSequenceNumber());
        assertEquals(cBuiltMqttPacketHeader.keyInfo, mqttPacketHeader.getKeyInfo());
        assertEquals(cBuiltMqttPacketHeader.encryptionMethod, mqttPacketHeader.getEncryptionMethod());
        assertEquals(cBuiltMqttPacketHeader.token, mqttPacketHeader.getToken());

        final IntegerData currentTimeAndDate = (IntegerData)ti.next();
        assertEquals(12345678L, currentTimeAndDate.getValue());

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());
    }



    /**
     * Create a packet in Java and parse it in C
     *
     */
    @Test
    public void testTimeOfDayPacketConsistencyJavaToC() throws Exception
    {
        final MqttPacketHeaderData javaBuiltHeader = new MqttPacketHeaderData(1, 2, 3, 4);

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.TimeOfDay);
        packetBuilder.appendTagData(javaBuiltHeader);

        final IntegerData timeAndDateJBuilt = new IntegerData(TagId.CurrentTimeAndDate, TagDataType.UInt64, 12345678L);
        packetBuilder.appendTagData(timeAndDateJBuilt);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        System.out.println("Java built data as JSON: ");
        System.out.println(p.toJsonPretty());

        final byte[] packetData = p.toByteSequence();

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();

        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.MQTTPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        final MqttPacketHeaderStruct mqttPacketHeader = new MqttPacketHeaderStruct();
        tpbp.Tpbp_ParserRead_UartPacketHeader(packetParserStruct, mqttPacketHeader);
        assertEquals(javaBuiltHeader.getSequenceNumber(), mqttPacketHeader.sequenceNumber);
        assertEquals(javaBuiltHeader.getKeyInfo(), mqttPacketHeader.keyInfo);
        assertEquals(javaBuiltHeader.getEncryptionMethod(), mqttPacketHeader.encryptionMethod);
        assertEquals(javaBuiltHeader.getToken(), mqttPacketHeader.token);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.CurrentTimeAndDate.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.UInt64.getId(), tagStruct.tagType);
        assertEquals(12345678L, tpbp.Tpbp_Unpack64(packetParserStruct));

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);
    }
}
