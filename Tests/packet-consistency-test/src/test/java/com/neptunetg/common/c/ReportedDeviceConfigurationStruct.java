/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for pass reported device configuration to packer
 *
typedef struct S_REPORTED_DEVICE_CONFIG
        {
        uint8_t     deviceNumber;
        uint64_t    attachedDeviceId;
        uint16_t    deviceType;
        uint32_t    currentDeviceData;
        uint16_t    deviceFlags;
        } S_REPORTED_DEVICE_CONFIG;

 */
public class ReportedDeviceConfigurationStruct extends Structure
{

    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "deviceNumber",
        "attachedDeviceId",
        "deviceType",
        "currentDeviceData",
        "deviceFlags"
    });

    public byte deviceNumber;
    public long attachedDeviceId;
    public short deviceType;
    public int currentDeviceData;
    public short deviceFlags;


    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
