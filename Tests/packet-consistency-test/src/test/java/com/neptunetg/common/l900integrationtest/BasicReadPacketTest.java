/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.l900integrationtest;

import com.neptunetg.common.l900c.PacketHandlerLora11ByteReadStruct;
import com.neptunetg.common.l900integrationtest.framework.PacketHandlerTestBase;
import com.neptunetg.common.lora.pdu.BasicReadPacket;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 09/08/2016.
 * A test for parsing / building the 11 Byte Read packet for L900
 */
public class BasicReadPacketTest extends PacketHandlerTestBase
{
    @Test
    public void testBasicReadPacketJavaToC()
    {
        int read1 = 123456789;
        int read2 = 123456789;
        int read3 = 123456789;
        byte reverseFlowFlag = 1;
        byte continuousLeakFlag = 1;

        BasicReadPacket packet = new BasicReadPacket(read1, read2, read3, reverseFlowFlag, continuousLeakFlag);

        packetHandler.PacketHandler_DLL_Init();

        byte packetBytes[] = packet.getBytes();
        Pointer pointerToPacketBytes = new Memory(packetBytes.length);
        pointerToPacketBytes.write(0, packetBytes, 0, packetBytes.length);

        PacketHandlerLora11ByteReadStruct packetHandlerLora11ByteReadStruct = new PacketHandlerLora11ByteReadStruct();
        packetHandlerLora11ByteReadStruct.packetBuffer = new byte[11];
        packetHandler.PacketHandler_DLL_Parse_LoRa_Reading_11Byte(pointerToPacketBytes, packetHandlerLora11ByteReadStruct);

        assertEquals(1, packetHandlerLora11ByteReadStruct.packetType);
        assertEquals(123456789, packetHandlerLora11ByteReadStruct.readingCurrent);
        assertEquals(123456789, packetHandlerLora11ByteReadStruct.readingOld);
        assertEquals(123456789, packetHandlerLora11ByteReadStruct.readingOlder);
        assertEquals(reverseFlowFlag, packetHandlerLora11ByteReadStruct.reverseFlowFlag);
        assertEquals(continuousLeakFlag, packetHandlerLora11ByteReadStruct.continuousLeakFlag);
    }

    @Test
    public void testBasicReadPacketCToJava()
    {
        int read1 = 0xAE6ADF;
        int read2 = 0xAE6ADF;
        int read3 = 0xAE6ADF;
        byte reverseFlowFlag = 1;
        byte continuousLeakFlag = 1;

        packetHandler.PacketHandler_DLL_Init();
        PacketHandlerLora11ByteReadStruct packetHandlerLora11ByteReadStruct = new PacketHandlerLora11ByteReadStruct();
        packetHandlerLora11ByteReadStruct.packetType = 1;
        packetHandlerLora11ByteReadStruct.readingCurrent = read1;
        packetHandlerLora11ByteReadStruct.readingOld = read2;
        packetHandlerLora11ByteReadStruct.readingOlder = read3;
        packetHandlerLora11ByteReadStruct.reverseFlowFlag = reverseFlowFlag;
        packetHandlerLora11ByteReadStruct.continuousLeakFlag = continuousLeakFlag;
        packetHandlerLora11ByteReadStruct.packetBuffer = new byte[11];


        packetHandler.PacketHandler_DLL_Build_LoRa_Reading_11Byte(packetHandlerLora11ByteReadStruct);

        L900Packet packet = new L900Packet(packetHandlerLora11ByteReadStruct.packetBuffer);

        boolean parsed = false;

        if(packet.getPacketId().equals(PacketId.BASIC_READ_PACKET))
        {
            BasicReadPacket basicReadPacket = new BasicReadPacket(packet.getBytes());

            assertEquals(PacketId.BASIC_READ_PACKET.getId(), basicReadPacket.getPacketId().getId());
            assertEquals(123456789, basicReadPacket.getRead1());
            assertEquals(123456789, basicReadPacket.getRead2());
            assertEquals(123456789, basicReadPacket.getRead3());
            assertEquals(reverseFlowFlag, basicReadPacket.getReverseFlowFlag());
            assertEquals(continuousLeakFlag, basicReadPacket.getContinuousLeakFlag());

            parsed = true;
        }

        assertTrue(parsed);
    }
}
