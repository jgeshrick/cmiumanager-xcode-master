/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.integrationtest;

import com.neptunetg.common.c.*;
import com.neptunetg.common.integrationtest.framework.PacketConsistencyTestBase;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.*;
import com.neptunetg.common.packet.model.taggeddata.types.CharArrayData;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test packet ID 0
 *
 * {@code
1	CMIU packet header 	Uint8 Array
254	Begin secure data 	Secure Data array
2	CMIU configuration	Uint8 Array
3	Network operator status	Char Array
4	Network performance 	Char Array
5	Registration status	Char Array
6	Host  IP address 	Char Array
7	Fall back Host IP address 	Char Array
8	Assigned CMIU IP address	Char Array
9	Application update URL address	Char Array
10	Application update User ID	Char Array
11	Application update Password	Char Array
12	Module FOTA address	Char Array
13	Module FOTA User ID	Char Array
14	Module FOTA Password	Char Array
15	Modem hardware revision	Char Array
16	The device model identification code	Char Array
17	Device manufacturer identification code	Char Array
18	Device software revision number	Char Array
19	The product serial number, identified as the IMEI of the mobile	Char Array
37	MSISDN request	Char Array
20	Sim IMSI	Char Array
21	Sim Card ID	Char Array
22	PIN/PUK/PUK2 request status of the device 	Char Array
23	CMIU Diagnostics	Uint8 Array
24	BLE last user logged in ID	Char Array
25	BLE last user logged in date	Uint64
31	Connection timing log	Uint8 Array
0	Null Filler	Uint8
255	EOF 	Uint8

 }
 @deprecated To be removed once we stop using the pre-alpha detailed config packet
 */
@Deprecated
public class PreAlphaDetailedConfigPacketConsistencyTest extends PacketConsistencyTestBase
{

    /**
     * Create a packet in C and parse it in Java
     *
     */
    @Test
    public void testDetailedConfigPacketConsistencyCToJava() throws Exception
    {

        //build the packet in C

        final Memory packetBuffer = new Memory(1024L);


        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.DetailedConfigurationStatus.getId()); //packet type

        final CmiuPacketHeaderStruct cBuiltPacketHeader = new CmiuPacketHeaderStruct();
        cBuiltPacketHeader.cmiuId = 123456;
        cBuiltPacketHeader.cellularRssiAndBer = 123;
        cBuiltPacketHeader.encryptionMethod = 1;
        cBuiltPacketHeader.keyInfo = 2;
        cBuiltPacketHeader.networkFlags = 3;
        cBuiltPacketHeader.sequenceNumber = 4;
        cBuiltPacketHeader.timeAndDate = 5;
        cBuiltPacketHeader.tokenAesCrc = 6;

        assertTrue(tpbp.Tpbp_PackerAdd_CmiuPacketHeader(packetPacker, cBuiltPacketHeader));

        final Memory secureDataBuffer = new Memory(1024L);

        final TpbpPackerStruct secureDataPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(secureDataPacker, secureDataBuffer, 1024);

        final CmiuConfigurationStruct cBuiltConfig = new CmiuConfigurationStruct();
        cBuiltConfig.cmiuType = 7;
        cBuiltConfig.hwRevision = 8;
        cBuiltConfig.fwRelease = 0x0201; //1.2
        cBuiltConfig.fwRevDate = 0x00151231;
        cBuiltConfig.fwBuildNum = 0x00123456;
        cBuiltConfig.blRelease = 0x0203; //3.2
        cBuiltConfig.blRevDate = 0x00151231;
        cBuiltConfig.blBuildNum = 0x00123456;
        cBuiltConfig.configRelease= 0x0205; //5.2
        cBuiltConfig.configRevDate = 0x00151231;
        cBuiltConfig.configBuildNum = 0x00123456;
        cBuiltConfig.manufactureDate = 11;
        cBuiltConfig.initialCallInTime = 12;
        cBuiltConfig.callInInterval = 13;
        cBuiltConfig.callInWindow = 14;
        cBuiltConfig.reportingwindowNRetries = 15;
        cBuiltConfig.minsToReportingWindowQuietTime = 16;
        cBuiltConfig.minsToReportingWindowQuietTimeEnd = 17;
        cBuiltConfig.attachedDevices = 18;
        cBuiltConfig.installationDate = 19;
        cBuiltConfig.dateLastMagSwipe = 20;
        cBuiltConfig.magSwipes = 21;
        cBuiltConfig.batteryRemaining = 22;
        cBuiltConfig.timeErrorLastNetworkTimeAccess = 23;

        assertTrue(tpbp.Tpbp_PackerAdd_CmiuConfiguration(secureDataPacker, cBuiltConfig));

        addCharArray(secureDataPacker, TagId.NetworkOperators, "3:24");
        addCharArray(secureDataPacker, TagId.NetworkPerformance, "4:25");
        addCharArray(secureDataPacker, TagId.RegistrationStatus, "5:26");
        addCharArray(secureDataPacker, TagId.MQTTBrokerIPAddress, "6.27.6.27");
        addCharArray(secureDataPacker, TagId.FallbackMQTTBrokerIPAddress, "7.28.7.28");
        addCharArray(secureDataPacker, TagId.AssignedCmiuIPAddress, "8.29.8.29");
        addCharArray(secureDataPacker, TagId.ModuleFotaFtpUrl, "12.33.12.33");
        addCharArray(secureDataPacker, TagId.ModuleFotaFtpPassword, "14:35");
        addCharArray(secureDataPacker, TagId.ModemHardwareRevision, "15:36");
        addCharArray(secureDataPacker, TagId.ModemModelIdentificationCode, "16:37");
        addCharArray(secureDataPacker, TagId.ModemManufacturerIdentificationCode, "17:38");
        addCharArray(secureDataPacker, TagId.ModemSoftwareRevisionNumber, "18:39");
        addCharArray(secureDataPacker, TagId.ModemSerialNumber, "19:40");
        addCharArray(secureDataPacker, TagId.MsisdnRequest, "37:41");
        addCharArray(secureDataPacker, TagId.SimImsi, "20:42");
        addCharArray(secureDataPacker, TagId.SimCardId, "21:43");
        addCharArray(secureDataPacker, TagId.PinPukPukTwoRequestData, "22:44");

        final CmiuDiagnosticsStruct cBuiltDiagnostics = new CmiuDiagnosticsStruct();
        cBuiltDiagnostics.diagnosticsResult = 45;
        cBuiltDiagnostics.processorResetCounter = 46;

        assertTrue(tpbp.Tpbp_PackerAdd_CmiuDiagnostics(secureDataPacker, cBuiltDiagnostics));

        addCharArray(secureDataPacker, TagId.LastBleUserId, "24:45");

        assertTrue(tpbp.Tpbp_PackerAdd_UInt64(secureDataPacker, TagId.LastBleUserLoginDate.getId(), 250046L));

        final ConnectionTimingLogStruct cBuiltConnectionTimingLog = new ConnectionTimingLogStruct();
        cBuiltConnectionTimingLog.dateOfConnection = 47L;
        cBuiltConnectionTimingLog.timeFromPowerOnToNetworkRegistration = 48;
        cBuiltConnectionTimingLog.timeFromNetworkRegistrationToContextActivation = 49;
        cBuiltConnectionTimingLog.timeFromContextActivationToServerConnection = 50;
        cBuiltConnectionTimingLog.timeToTransferMessageToServer = 51;
        cBuiltConnectionTimingLog.timeToDisconnectAndShutdown = 52;

        assertTrue(tpbp.Tpbp_PackerAdd_ConnectionTimingLog(secureDataPacker, cBuiltConnectionTimingLog));

        assertTrue(tpbp.Tpbp_PackerPadZeroToBlockBoundary(secureDataPacker, 16));

        final int secureDataLengthInBytes = tpbp.Tpbp_PackerGetCount(secureDataPacker);
        final Memory encryptedSecureData = new Memory(secureDataLengthInBytes);

        encryptedSecureData.write(0L, secureDataBuffer.getByteArray(0L, secureDataLengthInBytes), 0, secureDataLengthInBytes);

        assertTrue(tpbp.Tpbp_PackerAdd_SecureBlockArray(packetPacker, encryptedSecureData, secureDataLengthInBytes));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //parse the packet data in Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.DetailedConfigurationStatus.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final CmiuPacketHeaderData parsedPacketData = (CmiuPacketHeaderData) ti.next();
        assertEquals(cBuiltPacketHeader.cmiuId, parsedPacketData.getCmiuId());

        assertEquals(cBuiltPacketHeader.cellularRssiAndBer, parsedPacketData.getBer() * 256 + parsedPacketData.getRssi());

        assertEquals(cBuiltPacketHeader.encryptionMethod, parsedPacketData.getEncryptionMethod());
        assertEquals(cBuiltPacketHeader.keyInfo, parsedPacketData.getKeyInfo());
        assertEquals(cBuiltPacketHeader.networkFlags, parsedPacketData.getNetworkFlags());
        assertEquals(cBuiltPacketHeader.sequenceNumber, parsedPacketData.getSequenceNumber());
        assertEquals(cBuiltPacketHeader.timeAndDate, parsedPacketData.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cBuiltPacketHeader.tokenAesCrc, parsedPacketData.getToken());


        final SecureBlockArrayData javaParsedSbad = (SecureBlockArrayData) ti.next();

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());

        //read the tags in the secure data

        final ByteBuffer encryptedData = javaParsedSbad.getEncryptedData();

        final ByteBuffer decryptedData = encryptedData.duplicate().order(ByteOrder.LITTLE_ENDIAN); //no decryption

        final TagSequenceParser secureDataParser = new TagSequenceParser();

        final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

        ti = secureTags.iterator();
        
        final DetailedCmiuConfigurationData javaParsedConfig = (DetailedCmiuConfigurationData) ti.next();
        
        assertEquals(cBuiltConfig.cmiuType, javaParsedConfig.getCmiuType());
        assertEquals(cBuiltConfig.hwRevision, javaParsedConfig.getDeviceHwRevision());

        DetailedCmiuConfigurationData.ImageRevisionInfo javaParsedRevision = javaParsedConfig.getFirmwareRevision();
        assertEquals(1, javaParsedRevision.getMajorRelease());
        assertEquals(2, javaParsedRevision.getMinorRelease());
        assertEquals(151231, javaParsedRevision.getRevDate());
        assertEquals(123456, javaParsedRevision.getBuildRev());

        javaParsedRevision = javaParsedConfig.getBootloaderVersion();
        assertEquals(3, javaParsedRevision.getMajorRelease());
        assertEquals(2, javaParsedRevision.getMinorRelease());
        assertEquals(151231, javaParsedRevision.getRevDate());
        assertEquals(123456, javaParsedRevision.getBuildRev());

        javaParsedRevision = javaParsedConfig.getConfigVersion();
        assertEquals(5, javaParsedRevision.getMajorRelease());
        assertEquals(2, javaParsedRevision.getMinorRelease());
        assertEquals(151231, javaParsedRevision.getRevDate());
        assertEquals(123456, javaParsedRevision.getBuildRev());

        assertEquals(cBuiltConfig.manufactureDate, javaParsedConfig.getManufactureDate().getEpochSecond());
        assertEquals(cBuiltConfig.initialCallInTime, javaParsedConfig.getReportingStartTime());
        assertEquals(cBuiltConfig.callInInterval, javaParsedConfig.getReportingInterval());
        assertEquals(cBuiltConfig.callInWindow, javaParsedConfig.getReportingWindow());
        assertEquals(cBuiltConfig.reportingwindowNRetries, javaParsedConfig.getReportingWindowNumberOfRetries());
        assertEquals(cBuiltConfig.minsToReportingWindowQuietTime, javaParsedConfig.getMinsToReportingWindowQuietTime());
        assertEquals(cBuiltConfig.minsToReportingWindowQuietTimeEnd, javaParsedConfig.getMinsToReportingWindowQuietTimeEnd());
        assertEquals(cBuiltConfig.attachedDevices, javaParsedConfig.getNumberOfAttachedDevices());
        assertEquals(cBuiltConfig.installationDate, javaParsedConfig.getDateOfInstallation().getEpochSecond());
        assertEquals(cBuiltConfig.dateLastMagSwipe, javaParsedConfig.getDateOfLastMagSwipe().getEpochSecond());
        assertEquals(cBuiltConfig.magSwipes, javaParsedConfig.getMagSwipeCounter());
        assertEquals(cBuiltConfig.batteryRemaining, javaParsedConfig.getEstimateBatteryCapacityRemaining());
        assertEquals(cBuiltConfig.timeErrorLastNetworkTimeAccess, javaParsedConfig.getTimeErrorOnLastNetworkTimeAccess());

        checkCharArray(ti, TagId.NetworkOperators, "3:24");
        checkCharArray(ti, TagId.NetworkPerformance, "4:25");
        checkCharArray(ti, TagId.RegistrationStatus, "5:26");
        checkCharArray(ti, TagId.MQTTBrokerIPAddress, "6.27.6.27");
        checkCharArray(ti, TagId.FallbackMQTTBrokerIPAddress, "7.28.7.28");
        checkCharArray(ti, TagId.AssignedCmiuIPAddress, "8.29.8.29");
        checkCharArray(ti, TagId.ModuleFotaFtpUrl, "12.33.12.33");
        checkCharArray(ti, TagId.ModuleFotaFtpPassword, "14:35");
        checkCharArray(ti, TagId.ModemHardwareRevision, "15:36");
        checkCharArray(ti, TagId.ModemModelIdentificationCode, "16:37");
        checkCharArray(ti, TagId.ModemManufacturerIdentificationCode, "17:38");
        checkCharArray(ti, TagId.ModemSoftwareRevisionNumber, "18:39");
        checkCharArray(ti, TagId.ModemSerialNumber, "19:40");
        checkCharArray(ti, TagId.MsisdnRequest, "37:41");
        checkCharArray(ti, TagId.SimImsi, "20:42");
        checkCharArray(ti, TagId.SimCardId, "21:43");
        checkCharArray(ti, TagId.PinPukPukTwoRequestData, "22:44");

        final CmiuDiagnosticsData javaParsedDiagnostics = (CmiuDiagnosticsData) ti.next();

        assertEquals(cBuiltDiagnostics.diagnosticsResult, javaParsedDiagnostics.getDiagnosticFlags());
        assertEquals(cBuiltDiagnostics.processorResetCounter, javaParsedDiagnostics.getProcessorResetCount());

        checkCharArray(ti, TagId.LastBleUserId, "24:45");

        final IntegerData javaParsedLastBleLoginDate = (IntegerData) ti.next();
        assertEquals(TagId.LastBleUserLoginDate, javaParsedLastBleLoginDate.getTagId());
        assertEquals(250046L, javaParsedLastBleLoginDate.getValue());

        final ConnectionTimingLogData javaParsedConnectionTimingLogData = (ConnectionTimingLogData) ti.next();

        assertEquals(cBuiltConnectionTimingLog.dateOfConnection, javaParsedConnectionTimingLogData.getDateOfConnection().getEpochSecond());
        assertEquals(cBuiltConnectionTimingLog.timeFromPowerOnToNetworkRegistration, javaParsedConnectionTimingLogData.getTimeFromPowerOnToNetworkRegistration());
        assertEquals(cBuiltConnectionTimingLog.timeFromNetworkRegistrationToContextActivation, javaParsedConnectionTimingLogData.getTimeFromNetworkRegistrationToContextActivation());
        assertEquals(cBuiltConnectionTimingLog.timeFromContextActivationToServerConnection, javaParsedConnectionTimingLogData.getTimeFromContextActivationToServerConnection());
        assertEquals(cBuiltConnectionTimingLog.timeToTransferMessageToServer, javaParsedConnectionTimingLogData.getTimeToTransferMessageToServer());
        assertEquals(cBuiltConnectionTimingLog.timeToDisconnectAndShutdown, javaParsedConnectionTimingLogData.getTimeToDisconnectAndShutdown());

    }


    /**
     * Create a packet in Java and parse it in C
     *
     */
    @Test
    public void testDetailedConfigPacketConsistencyJavaToC() throws Exception
    {

        final CmiuPacketHeaderData javaBuiltHeader = new CmiuPacketHeaderData(
            2 /*cmiuId*/,
            3 /*int sequenceNumber*/,
            4 /*keyInfo*/,
            5 /*encryptionMethod*/,
            6 /*token*/,
            7 /*networkFlags*/,
            8 /*rssi*/,
            9 /*ber*/,
            new UnixTimestamp(10L));

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.DetailedConfigurationStatus);
        packetBuilder.appendTagData(javaBuiltHeader);


        final List<TaggedData> tagsToEncrypt = new ArrayList<>();

        final DetailedCmiuConfigurationData.ImageRevisionInfo javaBuiltFwRev = new DetailedCmiuConfigurationData.ImageRevisionInfo(1, 2, 151231, 123456);
        final DetailedCmiuConfigurationData.ImageRevisionInfo javaBuiltBlRev = new DetailedCmiuConfigurationData.ImageRevisionInfo(2, 3, 151231, 123456);
        final DetailedCmiuConfigurationData.ImageRevisionInfo javaBuiltCfgRev = new DetailedCmiuConfigurationData.ImageRevisionInfo(4, 5, 151231, 123456);

        final DetailedCmiuConfigurationData javaBuiltConfig = new DetailedCmiuConfigurationData(
            /* int cmiuType, */
            1,
            /* short deviceHwRevision, */
            (short)2,
            /* ImageRevisionInfo firmwareRevision */
            javaBuiltFwRev,
            /* ImageRevisionInfo bootloaderVersion, */
            javaBuiltBlRev,
            /* ImageRevisionInfo configVersion, */
            javaBuiltCfgRev,
            /* UnixTimestamp manufactureDate, */
            new UnixTimestamp(5L),
            /* short initialCallInTime, */
            6,
            /* int callInInterval, int callInWindow, */
            7, 8,
            /* int reportingWindowNumberOfRetries, */
            9,
            /* int minsToReportingWindowQuietTime, int minsToReportingWindowQuietTimeEnd,*/
            10, 11,
            /* int numberOfAttachedDevices, */
            12,
            /* UnixTimestamp dateOfInstallation, */
            new UnixTimestamp(13L),
            /* UnixTimestamp dateOfLastMagSwipe, */
            new UnixTimestamp(14L),
            /* int magSwipeCounter, */
            15,
            /* int estimateBatteryCapacityRemaining, */
            16,
            /* int timeErrorOnLastNetworkTimeAccess*/
            17);

        tagsToEncrypt.add(javaBuiltConfig);

        tagsToEncrypt.add(new CharArrayData(TagId.NetworkOperators, TagDataType.CharArray, "3:18"));
        tagsToEncrypt.add(new CharArrayData(TagId.NetworkPerformance, TagDataType.CharArray, "4:19"));
        tagsToEncrypt.add(new CharArrayData(TagId.RegistrationStatus, TagDataType.CharArray, "5:20"));
        tagsToEncrypt.add(new CharArrayData(TagId.MQTTBrokerIPAddress, TagDataType.CharArray, "6.21.6.21"));
        tagsToEncrypt.add(new CharArrayData(TagId.FallbackMQTTBrokerIPAddress, TagDataType.CharArray, "7.22.7.22"));
        tagsToEncrypt.add(new CharArrayData(TagId.AssignedCmiuIPAddress, TagDataType.CharArray, "8.23.8.23"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModuleFotaFtpUrl, TagDataType.CharArray, "12.27.12.27"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModuleFotaFtpPassword, TagDataType.CharArray, "14:29"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemHardwareRevision, TagDataType.CharArray, "15:30"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemModelIdentificationCode, TagDataType.CharArray, "16:31"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemManufacturerIdentificationCode, TagDataType.CharArray, "17:32"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemSoftwareRevisionNumber, TagDataType.CharArray, "18:33"));
        tagsToEncrypt.add(new CharArrayData(TagId.ModemSerialNumber, TagDataType.CharArray, "19:34"));
        tagsToEncrypt.add(new CharArrayData(TagId.MsisdnRequest, TagDataType.CharArray, "37:35"));
        tagsToEncrypt.add(new CharArrayData(TagId.SimImsi, TagDataType.CharArray, "20:36"));
        tagsToEncrypt.add(new CharArrayData(TagId.SimCardId, TagDataType.CharArray, "21:37"));
        tagsToEncrypt.add(new CharArrayData(TagId.PinPukPukTwoRequestData, TagDataType.CharArray, "22:38"));

        final CmiuDiagnosticsData javaBuiltDiagnostics = new CmiuDiagnosticsData(39, 40);

        tagsToEncrypt.add(javaBuiltDiagnostics);

        tagsToEncrypt.add(new CharArrayData(TagId.LastBleUserId, TagDataType.CharArray, "24:41"));
        tagsToEncrypt.add(new IntegerData(TagId.LastBleUserLoginDate, TagDataType.UInt64, 2500042L));

        final ConnectionTimingLogData javaBuiltConnTimingLogData = new ConnectionTimingLogData(
                new UnixTimestamp(43L), 44,
                45, 46,
                47, 48);
        tagsToEncrypt.add(javaBuiltConnTimingLogData);

        final TagSequence tagSequenceToEncrypt = new TagSequence(tagsToEncrypt);

        System.out.println("Java built secure data as JSON: ");
        System.out.println(tagSequenceToEncrypt.toJsonPretty());

        byte[] dataToEncrypt = tagSequenceToEncrypt.toByteSequencePadToBlockBoundary(16); //pad to 16 bytes

        byte[] encryptedData = dataToEncrypt; //no encryption

        SecureBlockArrayData secureData = new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, encryptedData, dataToEncrypt.length/16);
        packetBuilder.appendTagData(secureData);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        final byte[] packetData = p.toByteSequence();

        System.out.println("Java built packet as JSON: ");
        System.out.println(p.toJsonPretty());

        printByteArray("Java built packet data as hex:", packetData);
        Files.write(Paths.get("javapacket.dat"), packetData);

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();
        final CmiuPacketHeaderStruct cParsedHeader = new CmiuPacketHeaderStruct();


        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.CmiuPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(CmiuPacketHeaderData.SIZE_IN_BYTES, tagStruct.dataSize);

        tpbp.Tpbp_ParserRead_CmiuPacketHeader(packetParserStruct, cParsedHeader);


        assertEquals(javaBuiltHeader.getCmiuId(), cParsedHeader.cmiuId);

        assertEquals(javaBuiltHeader.getBer() * 256 + javaBuiltHeader.getRssi(), cParsedHeader.cellularRssiAndBer);

        assertEquals(cParsedHeader.encryptionMethod, javaBuiltHeader.getEncryptionMethod());
        assertEquals(cParsedHeader.keyInfo, javaBuiltHeader.getKeyInfo());
        assertEquals(cParsedHeader.networkFlags, javaBuiltHeader.getNetworkFlags());
        assertEquals(cParsedHeader.sequenceNumber, javaBuiltHeader.getSequenceNumber());
        assertEquals(cParsedHeader.timeAndDate, javaBuiltHeader.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cParsedHeader.tokenAesCrc, javaBuiltHeader.getToken());

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.SecureDataBlockArray.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), tagStruct.tagType);

        final int secureDataSize = tagStruct.dataSize;
        final Memory encryptedDataFromC = new Memory(secureDataSize);
        tpbp.Tpbp_ParserGetBytes(packetParserStruct, encryptedDataFromC, secureDataSize);

        final Memory decryptedDataFromC = encryptedDataFromC; //no encryption

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);


        final TpbpParserStruct secureDataParserStruct = new TpbpParserStruct();

        tpbp.Tpbp_ParserInit(secureDataParserStruct, decryptedDataFromC, secureDataSize);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.DetailedCmiuConfiguration.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(DetailedCmiuConfigurationData.SIZE_IN_BYTES, tagStruct.dataSize);

        final CmiuConfigurationStruct cParsedConfig = new CmiuConfigurationStruct();
        
        tpbp.Tpbp_ParserRead_CmiuConfiguration(secureDataParserStruct, cParsedConfig);

        assertEquals(javaBuiltConfig.getCmiuType(), cParsedConfig.cmiuType);
        assertEquals(javaBuiltConfig.getDeviceHwRevision(), cParsedConfig.hwRevision);

        assertEquals(0x0201, cParsedConfig.fwRelease);
        assertEquals(0x00151231, cParsedConfig.fwRevDate);
        assertEquals(0x00123456, cParsedConfig.fwBuildNum);

        assertEquals(0x0302, cParsedConfig.blRelease);
        assertEquals(0x00151231, cParsedConfig.blRevDate);
        assertEquals(0x00123456, cParsedConfig.blBuildNum);

        assertEquals(0x0504, cParsedConfig.configRelease);
        assertEquals(0x00151231, cParsedConfig.configRevDate);
        assertEquals(0x00123456, cParsedConfig.configBuildNum);

        assertEquals(javaBuiltConfig.getManufactureDate().getEpochSecond(), cParsedConfig.manufactureDate);
        assertEquals(javaBuiltConfig.getReportingStartTime(), cParsedConfig.initialCallInTime);
        assertEquals(javaBuiltConfig.getReportingInterval(), cParsedConfig.callInInterval);
        assertEquals(javaBuiltConfig.getReportingWindow(), cParsedConfig.callInWindow);
        assertEquals(javaBuiltConfig.getReportingWindowNumberOfRetries(), cParsedConfig.reportingwindowNRetries);
        assertEquals(javaBuiltConfig.getMinsToReportingWindowQuietTime(), cParsedConfig.minsToReportingWindowQuietTime);
        assertEquals(javaBuiltConfig.getMinsToReportingWindowQuietTimeEnd(), cParsedConfig.minsToReportingWindowQuietTimeEnd);
        assertEquals(javaBuiltConfig.getNumberOfAttachedDevices(), cParsedConfig.attachedDevices);
        assertEquals(javaBuiltConfig.getDateOfInstallation().getEpochSecond(), cParsedConfig.installationDate);
        assertEquals(javaBuiltConfig.getDateOfLastMagSwipe().getEpochSecond(), cParsedConfig.dateLastMagSwipe);
        assertEquals(javaBuiltConfig.getMagSwipeCounter(), cParsedConfig.magSwipes);
        assertEquals(javaBuiltConfig.getEstimateBatteryCapacityRemaining(), cParsedConfig.batteryRemaining);
        assertEquals(javaBuiltConfig.getTimeErrorOnLastNetworkTimeAccess(), cParsedConfig.timeErrorLastNetworkTimeAccess);

        checkCharArray(secureDataParserStruct, TagId.NetworkOperators, "3:18");
        checkCharArray(secureDataParserStruct, TagId.NetworkPerformance, "4:19");
        checkCharArray(secureDataParserStruct, TagId.RegistrationStatus, "5:20");
        checkCharArray(secureDataParserStruct, TagId.MQTTBrokerIPAddress, "6.21.6.21");
        checkCharArray(secureDataParserStruct, TagId.FallbackMQTTBrokerIPAddress, "7.22.7.22");
        checkCharArray(secureDataParserStruct, TagId.AssignedCmiuIPAddress, "8.23.8.23");
        checkCharArray(secureDataParserStruct, TagId.ModuleFotaFtpUrl, "12.27.12.27");
        checkCharArray(secureDataParserStruct, TagId.ModuleFotaFtpPassword, "14:29");
        checkCharArray(secureDataParserStruct, TagId.ModemHardwareRevision, "15:30");
        checkCharArray(secureDataParserStruct, TagId.ModemModelIdentificationCode, "16:31");
        checkCharArray(secureDataParserStruct, TagId.ModemManufacturerIdentificationCode, "17:32");
        checkCharArray(secureDataParserStruct, TagId.ModemSoftwareRevisionNumber, "18:33");
        checkCharArray(secureDataParserStruct, TagId.ModemSerialNumber, "19:34");
        checkCharArray(secureDataParserStruct, TagId.MsisdnRequest, "37:35");
        checkCharArray(secureDataParserStruct, TagId.SimImsi, "20:36");
        checkCharArray(secureDataParserStruct, TagId.SimCardId, "21:37");
        checkCharArray(secureDataParserStruct, TagId.PinPukPukTwoRequestData, "22:38");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.CmiuHwDiagnostics.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(CmiuDiagnosticsData.SIZE_IN_BYTES, tagStruct.dataSize);

        final CmiuDiagnosticsStruct cParsedCmiuDiagnostics = new CmiuDiagnosticsStruct();
        tpbp.Tpbp_ParserRead_CmiuDiagnostics(secureDataParserStruct, cParsedCmiuDiagnostics);

        assertEquals(javaBuiltDiagnostics.getDiagnosticFlags(), cParsedCmiuDiagnostics.diagnosticsResult);
        assertEquals(javaBuiltDiagnostics.getProcessorResetCount(), cParsedCmiuDiagnostics.processorResetCounter);

        checkCharArray(secureDataParserStruct, TagId.LastBleUserId, "24:41");

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.LastBleUserLoginDate.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.UInt64.getId(), tagStruct.tagType);
        assertEquals(8, tagStruct.dataSize);

        Memory buf = new Memory(8L);
        tpbp.Tpbp_ParserGetBytes(secureDataParserStruct, buf, 8);
        assertEquals(2500042L, buf.getLong(0L));

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.ConnectionTimingLog.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(ConnectionTimingLogData.SIZE_IN_BYTES, tagStruct.dataSize);

        final ConnectionTimingLogStruct cParsedTimingLog = new ConnectionTimingLogStruct();
        tpbp.Tpbp_ParserRead_ConnectionTimingLog(secureDataParserStruct, cParsedTimingLog);

        assertEquals(javaBuiltConnTimingLogData.getDateOfConnection().getEpochSecond(), cParsedTimingLog.dateOfConnection);
        assertEquals(javaBuiltConnTimingLogData.getTimeFromPowerOnToNetworkRegistration(), cParsedTimingLog.timeFromPowerOnToNetworkRegistration);
        assertEquals(javaBuiltConnTimingLogData.getTimeFromNetworkRegistrationToContextActivation(), cParsedTimingLog.timeFromNetworkRegistrationToContextActivation);
        assertEquals(javaBuiltConnTimingLogData.getTimeFromContextActivationToServerConnection(), cParsedTimingLog.timeFromContextActivationToServerConnection);
        assertEquals(javaBuiltConnTimingLogData.getTimeToTransferMessageToServer(), cParsedTimingLog.timeToTransferMessageToServer);
        assertEquals(javaBuiltConnTimingLogData.getTimeToDisconnectAndShutdown(), cParsedTimingLog.timeToDisconnectAndShutdown);

    }



}
