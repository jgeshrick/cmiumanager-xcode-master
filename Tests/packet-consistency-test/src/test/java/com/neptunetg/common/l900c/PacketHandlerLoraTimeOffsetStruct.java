/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing a time offset information
 *
     typedef struct sTag_PACKET_LORA_TIME_OFFSET
     {
     uint8_t packetType; // 5 bits
     uint8_t command;    // 8 bits
     int64_t timeOffset; // 64 bits, signed
     uint8_t timeUpdate; // 8 bits
     uint8_t spare;      // 3 bits
     uint8_t packetBuffer[11];
     } S_PACKET_LORA_TIME_OFFSET;
 */

public class PacketHandlerLoraTimeOffsetStruct extends Structure

{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "packetType",
        "command",
        "timeOffset",
        "timeUpdate",
        "spare",
        "packetBuffer"
    });

    public byte packetType;
    public byte command;
    public long timeOffset;
    public byte timeUpdate;
    public byte spare;
    public byte[] packetBuffer;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
