/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */
package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
        * Struct for passing CMIU Packet Header payload to packer
        typedef struct S_CMIU_PACKET_HEADER
        {
        uint32_t  cmiuId;
        uint8_t    sequenceNumber;
        uint8_t    keyInfo;
        uint8_t    encryptionMethod;
        uint8_t    tokenAesCrc;
        uint8_t    networkFlags;
        uint16_t  cellularRssiAndBer;

        uint64_t  timeAndDate;
        } S_CMIU_PACKET_HEADER;
 */
public class CmiuPacketHeaderStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{"cmiuId", "sequenceNumber",
        "keyInfo", "encryptionMethod", "tokenAesCrc", "networkFlags", "cellularRssiAndBer", "timeAndDate"});


    public int cmiuId;
    public byte sequenceNumber;
    public byte    keyInfo;
    public byte    encryptionMethod;
    public byte    tokenAesCrc;
    public byte    networkFlags;
    public short  cellularRssiAndBer;

    public long  timeAndDate;


    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }
}
