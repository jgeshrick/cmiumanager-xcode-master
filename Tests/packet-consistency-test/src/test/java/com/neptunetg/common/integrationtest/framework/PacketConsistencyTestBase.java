/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.integrationtest.framework;

import com.neptunetg.common.c.TpbpInterop;
import com.neptunetg.common.c.TpbpPackerStruct;
import com.neptunetg.common.c.TpbpParserStruct;
import com.neptunetg.common.c.TpbpTagStruct;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.*;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import org.junit.BeforeClass;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Basis for consistency tests between C and Java packet code
 */
public class PacketConsistencyTestBase
{

    protected static TpbpInterop tpbp;

    @BeforeClass
    public static void setUpInterop()
    {
        assertEquals("Test can only run in 32 bit JVM because it loads a 32 bit DLL", 4, Native.POINTER_SIZE);

        String dllFile = System.getenv("TPBP_DLL");
        if (dllFile == null || dllFile.trim().length() == 0) {
            dllFile = "TPBP_DLL.dll";
        }
        try
        {
            tpbp = (TpbpInterop) Native.loadLibrary(dllFile, TpbpInterop.class);
        }
        catch (UnsatisfiedLinkError e)
        {
            throw new RuntimeException("Can't load DLL file " + dllFile + ".  Set environment variable TPBP_DLL to the path of this file.", e);
        }

    }

    protected final void checkCharArray(TpbpParserStruct parser, TagId expectedTagId, String expectedStringValue)
    {
        TpbpTagStruct tagStruct = new TpbpTagStruct();
        tpbp.Tpbp_ParserReadTag(parser, tagStruct);

        assertEquals(expectedTagId.getId(), tagStruct.tagNumber);

        Memory stringBytes = new Memory(tagStruct.dataSize);

        tpbp.Tpbp_ParserGetBytes(parser, stringBytes, tagStruct.dataSize);

        String text = new String(stringBytes.getByteArray(0L, tagStruct.dataSize), StandardCharsets.US_ASCII);

        assertEquals(expectedStringValue, text);
    }

    protected final void checkCharArray(Iterator<TaggedData> ti, TagId expectedTagId, String expectedStringValue)
    {
        final TaggedData td = ti.next();
        assertTrue(td instanceof CharArrayData);

        final CharArrayData ca = (CharArrayData) td;
        assertEquals(expectedTagId, ca.getTagId());

        assertEquals(expectedStringValue, ca.getAsString());

    }

    protected final void checkRevision(Iterator<TaggedData> ti, TagId expectedTagId, byte[] expectedVersion)
    {
        final TaggedData td = ti.next();
        assertTrue(td instanceof ImageVersionInfoData);

        final ImageVersionInfoData ca = (ImageVersionInfoData) td;
        assertEquals(expectedTagId, ca.getTagId());

        byte[] tagBytes = ca.serialize();
        assertArrayEquals(expectedVersion, Arrays.copyOfRange(tagBytes, 2, tagBytes.length));
    }

    protected final void checkRevision(TpbpParserStruct parser, TagId expectedTagId, byte[] expectedRevision)
    {
        TpbpTagStruct tagStruct = new TpbpTagStruct();
        tpbp.Tpbp_ParserReadTag(parser, tagStruct);

        assertEquals(expectedTagId.getId(), tagStruct.tagNumber);

        Memory revisionBytes = new Memory(tagStruct.dataSize);

        tpbp.Tpbp_ParserGetBytes(parser, revisionBytes, tagStruct.dataSize);

        assertArrayEquals(revisionBytes.getByteArray(0, tagStruct.dataSize), expectedRevision);

    }

    protected final void addCharArray(TpbpPackerStruct packer, TagId tagId, String charArrayValue)
    {
        TpbpTagStruct tagStruct = new TpbpTagStruct();
        tagStruct.tagNumber = tagId.getId();
        tagStruct.tagType = TagDataType.CharArray.getId();
        tagStruct.dataSize = charArrayValue.length();

        assertTrue(tpbp.Tpbp_PackerTagHelper(packer, tagStruct));

        final Memory stringBytes = new Memory(charArrayValue.length());
        stringBytes.write(0L, charArrayValue.getBytes(StandardCharsets.US_ASCII), 0, charArrayValue.length());

        assertTrue(tpbp.Tpbp_PackerAddBytes(packer, stringBytes, charArrayValue.length()));

    }

    protected final void addExtendedCharArray(TpbpPackerStruct packer, TagId tagId, String charArrayValue)
    {
        TpbpTagStruct tagStruct = new TpbpTagStruct();
        tagStruct.tagNumber = tagId.getId();
        tagStruct.tagType = TagDataType.ExtendedCharArray.getId();
        tagStruct.dataSize = charArrayValue.length();

        assertTrue(tpbp.Tpbp_PackerTagHelper(packer, tagStruct));

        final Memory stringBytes = new Memory(charArrayValue.length());
        stringBytes.write(0L, charArrayValue.getBytes(StandardCharsets.US_ASCII), 0, charArrayValue.length());

        assertTrue(tpbp.Tpbp_PackerAddBytes(packer, stringBytes, charArrayValue.length()));

    }

    protected final void checkExtendedUint32Array(Iterator<TaggedData> ti, TagId expectedTagId, int[] expectedUint32Array)
    {
        final TaggedData td = ti.next();
        assertTrue(td instanceof IntegerArrayData);

        final IntegerArrayData ia = (IntegerArrayData) td;
        assertEquals(expectedTagId, ia.getTagId());

        ByteBuffer byteBuffer = ByteBuffer.allocate(expectedUint32Array.length * 4);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(expectedUint32Array);

        byte [] myBytes = byteBuffer.array();
        byte [] myBytes2 = ia.getDataBuffer().array();

        assertEquals(byteBuffer.array().length, ia.getDataBuffer().array().length);

        for(int i=0; i<byteBuffer.array().length; i++)
        {
            assertEquals(byteBuffer.array()[i], ia.getDataBuffer().array()[i]);
        }
    }

    protected final void addExtendedUint32Array(TpbpPackerStruct packer, TagId tagId, int[] uint32Array)
    {
        TpbpTagStruct tagStruct = new TpbpTagStruct();
        tagStruct.tagNumber = tagId.getId();
        tagStruct.tagType = TagDataType.ExtendedUInt32Array.getId();
        tagStruct.dataSize = uint32Array.length * 4;

        assertTrue(tpbp.Tpbp_PackerTagHelper(packer, tagStruct));

        final Memory uint32ArrayBytes = new Memory(uint32Array.length * 4);
        ByteBuffer byteBuffer = ByteBuffer.allocate(uint32Array.length * 4);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(uint32Array);
        uint32ArrayBytes.write(0L, byteBuffer.array(), 0, uint32Array.length * 4);

        assertTrue(tpbp.Tpbp_PackerAddBytes(packer, uint32ArrayBytes, uint32Array.length * 4));

    }

    public final void printByteArray(String heading, byte[] arr)
    {
        System.out.println(heading);
        for (int row = 0; row < arr.length; row += 16)
        {
            System.out.print("+" + row + "  \t");

            for (int x = row; x < arr.length && x < row + 8; x++)
            {
                System.out.printf("%2x ", arr[x]);
            }

            System.out.print("  \t");

            for (int x = row + 8; x < arr.length && x < row + 16; x++)
            {
                System.out.printf("%2x ", arr[x]);
            }

            System.out.println();
        }
    }

}
