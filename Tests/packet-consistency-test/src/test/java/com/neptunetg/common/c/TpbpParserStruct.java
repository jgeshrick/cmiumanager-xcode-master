
/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * An object to manage packing
 *
 * See Common\TaggedPacketBuilderParser\src\main\c\Tpbp.h
 *
 * {@code
typedef struct S_TPBP_PARSER
{
const uint8_t* pInputBuffer;   //!< The pointer to the start of the memory buffer
uint32_t      readPosition;  //!< The current offset (where next byte will be written)
uint32_t      bufferSize;     //!< The size in bytes of the buffer
}
S_TPBP_PARSER;

}
 */


public class TpbpParserStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{"pInputBuffer", "readPosition", "bufferSize"});

    public Pointer pInputBuffer;
    public int readPosition;
    public int bufferSize;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }
}
