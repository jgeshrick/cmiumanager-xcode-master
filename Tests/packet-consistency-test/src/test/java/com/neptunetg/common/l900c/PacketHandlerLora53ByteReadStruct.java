/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing a 53 byte read packet
 #define NUM_OF_READINGS 12
 #define PACKET_SIZE_FOR_53BYTE_LORA 53
 typedef struct sTag_PACKET_LORA_53BYTE_READING
 {
 uint8_t     packetType;    //  5 bits
 S_PACKET_READING_W_FLAGS reading[NUM_OF_READINGS]; // 12 readings plus flags, taking up 32 bits each
 uint16_t     timeLastRead; // 16 bits
 uint32_t    spare;         // 19 bits
 uint8_t     packetBuffer[PACKET_SIZE_FOR_53BYTE_LORA];
 } S_PACKET_LORA_53BYTE_READING;
 */

public class PacketHandlerLora53ByteReadStruct extends Structure

{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
            "packetType",
            "reading",
            "timeLastRead",
            "spare",
            "packetBuffer"
    });

    public byte packetType;

    public PacketHandlerLoraReadingWFlagsStruct[] reading;
    public short timeLastRead;
    public int spare;

    public byte[] packetBuffer;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
