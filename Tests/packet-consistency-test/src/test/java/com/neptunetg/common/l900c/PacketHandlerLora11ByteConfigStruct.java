/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing a 11 byte config packet
 *
 //////////////////////////
 // LORA 11 BYTE CONFIG  //
 //////////////////////////
 typedef struct sTag_PACKET_LORA_11BYTE_CONFIG
 {
 uint8_t packetType;             //  5 bits

 // Breaking down L900 flags into discrete subfields
 uint8_t networkTimeLoss;        //  2 bit
 uint8_t antennaType;             //  1 bits
 uint8_t L900errors;             //  2 bits
 uint8_t batteryStatus;          //  2 bits
 uint8_t magSwipe;               //  1 bit

 uint16_t firmwareVersion;       // 16 bits
 uint8_t attachedDeviceType;     //  5 bits
 uint64_t attachedDeviceID;      // 34 bits
 //uint16_t attachedDeviceFlags;   // Replaced by the next fields up to, but not including attachedDeviceAlarmMask

 // Per ETI 05-03, section 11.2.7, the DeviceFlags will be split into discrete fields
 uint8_t spare;                  //  1 bit; set to 0
 uint8_t reserved_1;             //  1 bit; set to 0
 uint8_t reverseFlow_35Day;      //  2 bit
 uint8_t reverseFlow_24Hour;     //  2 bit
 uint8_t leakState;              //  2 bit
 uint8_t batteryLife;            //  2 bit
 uint8_t emptyPipe;              //  2 bit
 uint8_t excessiveFlow;          //  2 bit
 uint8_t tamper;                 //  1 bit
 uint8_t reserved_2;             //  1 bit; set to 0

 uint8_t attachedDeviceAlarmMask;//  4 bits; set to 0000 for the time being
 // 88 bits total
 uint8_t packetBuffer[11];
 } S_PACKET_LORA_11BYTE_CONFIG;

 */

public class PacketHandlerLora11ByteConfigStruct extends Structure

{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "packetType",
        "networkTimeLoss",
        "l900errors",
        "antennaType",
        "batteryStatus",
        "magSwipe",

        "firmwareVersion",
        "attachedDeviceType",
        "attachedDeviceId",

        "spare",
        "reserved_1",
        "reverseFlow_35Day",
        "reverseFlow_24Hour",
        "leakState",
        "batteryLife",
        "emptyPipe",
        "excessiveFlow",
        "tamper",
        "reserved_2",

        "attachedDeviceAlarmMask",

        "packetBuffer"
    });

    public byte packetType;
    public byte networkTimeLoss;
    public byte l900errors;
    public byte antennaType;
    public byte batteryStatus;
    public byte magSwipe;

    public short firmwareVersion;
    public byte attachedDeviceType;
    public long attachedDeviceId;

    public byte spare;
    public byte reserved_1;
    public byte reverseFlow_35Day;
    public byte reverseFlow_24Hour;
    public byte leakState;
    public byte batteryLife;
    public byte emptyPipe;
    public byte excessiveFlow;
    public byte tamper;
    public byte reserved_2;

    public byte attachedDeviceAlarmMask;

    public byte[] packetBuffer;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
