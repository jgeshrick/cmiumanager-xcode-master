/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by WJD1 on 03/07/2015.
 * typedef struct S_IMAGE_METADATA
 {
 uint8_t     imageType;
 uint32_t    startAddress;
 uint32_t    imageSize;
 } S_IMAGE_METADATA;
 */
public class ImageMetadataStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
            "imageType",
            "startAddress",
            "imageSize"
    });

    public byte imageType;
    public int startAddress;
    public int imageSize;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }
}
