/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.l900integrationtest;

import com.neptunetg.common.l900c.PacketHandlerLoraTimeAndDateStruct;
import com.neptunetg.common.l900integrationtest.framework.PacketHandlerTestBase;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.common.lora.pdu.TimeAndDatePacket;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 07/07/2016.
 * A test for parsing / building the Time And Date packet for L900
 */
public class TimeAndDatePacketTest extends PacketHandlerTestBase
{
    @Test
    public void testTimeAndDatePacketJavaToC()
    {
        final Instant timeAndDate = Instant.now();
        final int packetSequenceNumber = 9;

        TimeAndDatePacket packet = new TimeAndDatePacket(timeAndDate, packetSequenceNumber);

        packetHandler.PacketHandler_DLL_Init();

        byte packetBytes[] = packet.getBytes();
        Pointer pointerToPacketBytes = new Memory(packetBytes.length);
        pointerToPacketBytes.write(0, packetBytes, 0, packetBytes.length);

        PacketHandlerLoraTimeAndDateStruct packetHandlerLoraTimeAndDateStruct = new PacketHandlerLoraTimeAndDateStruct();
        packetHandlerLoraTimeAndDateStruct.packetBuffer = new byte[11];
        packetHandler.PacketHandler_DLL_Parse_LoRa_TimeAndDate(pointerToPacketBytes, packetHandlerLoraTimeAndDateStruct);

        assertEquals(6, packetHandlerLoraTimeAndDateStruct.packetType);
        assertEquals(timeAndDate.getEpochSecond(), packetHandlerLoraTimeAndDateStruct.timeAndDate);
        assertEquals(packetSequenceNumber, packetHandlerLoraTimeAndDateStruct.timeUpdate);
    }

    @Test
    public void testTimeAndDatePacketCToJava()
    {
        final Instant timeAndDate = Instant.now();
        final int packetSequenceNumber = 9;

        packetHandler.PacketHandler_DLL_Init();
        PacketHandlerLoraTimeAndDateStruct packetHandlerLoraTimeAndDateStruct = new PacketHandlerLoraTimeAndDateStruct();
        packetHandlerLoraTimeAndDateStruct.packetType = 6;
        packetHandlerLoraTimeAndDateStruct.timeAndDate = timeAndDate.getEpochSecond();
        packetHandlerLoraTimeAndDateStruct.timeUpdate = packetSequenceNumber;
        packetHandlerLoraTimeAndDateStruct.packetBuffer = new byte[11];

        packetHandler.PacketHandler_DLL_Build_LoRa_TimeAndDate(packetHandlerLoraTimeAndDateStruct);

        L900Packet packet = new L900Packet(packetHandlerLoraTimeAndDateStruct.packetBuffer);

        assertEquals(packet.getPacketId(), PacketId.TIME_AND_DATE);

        TimeAndDatePacket timeAndDatePacket = new TimeAndDatePacket(packet.getBytes());

        assertEquals(PacketId.TIME_AND_DATE.getId(), timeAndDatePacket.getPacketId().getId());
        assertEquals(timeAndDate.getEpochSecond(), timeAndDatePacket.getTimeAndDate().getEpochSecond());
        assertEquals(packetSequenceNumber, timeAndDatePacket.getUpdateIdentifier());
    }
}
