/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing CMIU flags payload to packer
 *
 * Struct for Status flags (tag number 26)
 * typedef struct S_CMIU_STATUS
 * {
 * uint8_t cmiuType;
 * uint8_t numberOfDevices;
 * uint16_t cmiuFlags;
 * } S_CMIU_STATUS;
 */
public class CmiuFlagsStruct extends Structure
{

    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "cmiuType",
        "numberOfDevices",
        "cmiuFlags"
    });

    public byte cmiuType;
    public byte numberOfDevices;
    public short cmiuFlags;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
