/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Struct for passing CMIU Configuration payload to packer

 {@code
typedef struct S_CMIU_CONFIGURATION
        {
        uint8_t cmiuType;
        uint16_t hwRevision;

        uint16_t fwRelease;
        uint32_t fwRevDate;
        uint32_t fwBuildNum;

        uint16_t blRelease;
        uint32_t blRevDate;
        uint32_t blBuildNum;

        uint16_t configRelease;
        uint32_t configRevDate;
        uint32_t configBuildNum;

        uint64_t manufactureDate;
        uint16_t initialCallInTime;
        uint16_t callInInterval;
        uint16_t callInWindow;
        uint8_t reportingwindowNRetries;

        uint16_t quietTimeStartMins;
        uint16_t quietTimeEndMins;

        uint8_t numAttachedDevices;
        uint64_t installationDate;
        uint64_t dateLastMagSwipe;
        uint8_t magSwipes;
        uint16_t batteryRemaining;
        int8_t timeErrorLastNetworkTimeAccess;
        } S_CMIU_CONFIGURATION;
 }

        */
public class CmiuConfigurationStruct extends Structure
{

    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "cmiuType",
        "hwRevision",

        "fwRelease",
        "fwRevDate",
        "fwBuildNum",

        "blRelease",
        "blRevDate",
        "blBuildNum",

        "configRelease",
        "configRevDate",
        "configBuildNum",

        "manufactureDate",
        "initialCallInTime",
        "callInInterval",
        "callInWindow",
        "reportingwindowNRetries",
        "minsToReportingWindowQuietTime",
        "minsToReportingWindowQuietTimeEnd",
        "attachedDevices",
        "installationDate",
        "dateLastMagSwipe",
        "magSwipes",
        "batteryRemaining",
        "timeErrorLastNetworkTimeAccess"
    });

    public byte cmiuType;
    public short hwRevision;
    public short fwRelease;
    public int fwRevDate;
    public int fwBuildNum;
    public short blRelease;
    public int blRevDate;
    public int blBuildNum;
    public short configRelease;
    public int configRevDate;
    public int configBuildNum;
    public long manufactureDate;
    public short initialCallInTime;
    public short callInInterval;
    public short callInWindow;
    public byte reportingwindowNRetries;
    public short minsToReportingWindowQuietTime;
    public short minsToReportingWindowQuietTimeEnd;
    public byte attachedDevices;
    public long installationDate;
    public long dateLastMagSwipe;
    public byte magSwipes;
    public short batteryRemaining;
    public byte timeErrorLastNetworkTimeAccess;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
