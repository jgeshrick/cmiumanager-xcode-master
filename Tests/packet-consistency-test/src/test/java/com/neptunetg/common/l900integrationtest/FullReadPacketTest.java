/* ***************************************************************************
*
*    Neptune Technology Group
*    Copyright 2016 as unpublished work.
*    All rights reserved
*
*    The information contained herein is confidential
*    property of Neptune Technology Group. The use, copying, transfer
*    or disclosure of such information is prohibited except by express
*    written agreement with Neptune Technology Group.
*
*****************************************************************************/

package com.neptunetg.common.l900integrationtest;

import com.neptunetg.common.l900c.PacketHandlerLora53ByteReadStruct;
import com.neptunetg.common.l900c.PacketHandlerLoraReadingWFlagsStruct;
import com.neptunetg.common.l900integrationtest.framework.PacketHandlerTestBase;
import com.neptunetg.common.lora.pdu.FullReadPacket;
import com.neptunetg.common.lora.pdu.L900Packet;
import com.neptunetg.common.lora.pdu.PacketId;
import com.neptunetg.common.lora.pdu.ReadingWithFlags;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by WJD1 on 09/08/2016.
 * A test for parsing / building the 53 Byte Read packet for L900
 */
public class FullReadPacketTest extends PacketHandlerTestBase
{
    @Test
    public void testFullReadPacketJavaToC()
    {
        ReadingWithFlags[] reads = new ReadingWithFlags[12];

        for(int i=0; i<12; i++)
        {
            reads[i] = new ReadingWithFlags();
            reads[i].setReading(123456789);
            reads[i].setLeakFlag((byte)1);
            reads[i].setRevFlowFlag((byte)2);
        }

        short timeLastRead = 23456;

        FullReadPacket packet = new FullReadPacket(reads, timeLastRead);

        packetHandler.PacketHandler_DLL_Init();

        byte packetBytes[] = packet.getBytes();
        Pointer pointerToPacketBytes = new Memory(packetBytes.length);
        pointerToPacketBytes.write(0, packetBytes, 0, packetBytes.length);

        PacketHandlerLora53ByteReadStruct packetHandlerLora53ByteReadStruct = new PacketHandlerLora53ByteReadStruct();
        packetHandlerLora53ByteReadStruct.reading = new PacketHandlerLoraReadingWFlagsStruct[12];
        packetHandlerLora53ByteReadStruct.packetBuffer = new byte[53];
        packetHandler.PacketHandler_DLL_Parse_LoRa_Reading_53Byte(pointerToPacketBytes, packetHandlerLora53ByteReadStruct);

        assertEquals(4, packetHandlerLora53ByteReadStruct.packetType);

        for(int i=0; i<12; i++)
        {
            assertEquals(123456789, packetHandlerLora53ByteReadStruct.reading[i].reading);
            assertEquals(1, packetHandlerLora53ByteReadStruct.reading[i].leakFlag);
            assertEquals(2, packetHandlerLora53ByteReadStruct.reading[i].revFlowFlag);
        }

        assertEquals(timeLastRead, packetHandlerLora53ByteReadStruct.timeLastRead);
    }

    @Test
    public void testFullReadPacketCToJava()
    {
        PacketHandlerLoraReadingWFlagsStruct[] reads = new PacketHandlerLoraReadingWFlagsStruct[12];

        for(int i=0; i<12; i++)
        {
            reads[i] = new PacketHandlerLoraReadingWFlagsStruct();
            reads[i].reading = 0x15CD5BE9;
        }

        short timeLastRead = 3456;

        packetHandler.PacketHandler_DLL_Init();
        PacketHandlerLora53ByteReadStruct packetHandlerLora53ByteReadStruct = new PacketHandlerLora53ByteReadStruct();
        packetHandlerLora53ByteReadStruct.packetType = 1;
        packetHandlerLora53ByteReadStruct.reading = reads;
        packetHandlerLora53ByteReadStruct.timeLastRead = timeLastRead;
        packetHandlerLora53ByteReadStruct.packetBuffer = new byte[53];

        packetHandler.PacketHandler_DLL_Build_LoRa_Reading_53Byte(packetHandlerLora53ByteReadStruct);

        L900Packet packet = new L900Packet(packetHandlerLora53ByteReadStruct.packetBuffer);

        boolean parsed = false;

        if(packet.getPacketId().equals(PacketId.FULL_READ_PACKET))
        {
            FullReadPacket fullReadPacket = new FullReadPacket(packet.getBytes());

            assertEquals(PacketId.FULL_READ_PACKET.getId(), fullReadPacket.getPacketId().getId());

            for(int i=0; i<12; i++)
            {
                assertEquals(123456789, fullReadPacket.getReading(i).getReading());
                assertEquals(1, fullReadPacket.getReading(i).getLeakFlag());
                assertEquals(2, fullReadPacket.getReading(i).getRevFlowFlag());
            }

            assertEquals(timeLastRead, fullReadPacket.getTimeSinceLastRead());

            parsed = true;
        }

        assertTrue(parsed);
    }
}
