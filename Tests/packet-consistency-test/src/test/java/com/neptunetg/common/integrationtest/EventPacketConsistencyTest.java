/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.integrationtest;

import com.neptunetg.common.c.CmiuPacketHeaderStruct;
import com.neptunetg.common.c.TpbpPackerStruct;
import com.neptunetg.common.c.TpbpParserStruct;
import com.neptunetg.common.c.TpbpTagStruct;
import com.neptunetg.common.integrationtest.framework.PacketConsistencyTestBase;
import com.neptunetg.common.packet.model.UnixTimestamp;
import com.neptunetg.common.packet.model.builder.PacketBuilder;
import com.neptunetg.common.packet.model.taggeddata.packets.TagSequence;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedDataPacketType;
import com.neptunetg.common.packet.model.taggeddata.packets.TaggedPacket;
import com.neptunetg.common.packet.model.taggeddata.tags.CmiuPacketHeaderData;
import com.neptunetg.common.packet.model.taggeddata.tags.SecureBlockArrayData;
import com.neptunetg.common.packet.model.taggeddata.tags.TagId;
import com.neptunetg.common.packet.model.taggeddata.types.IntegerData;
import com.neptunetg.common.packet.model.taggeddata.types.TagDataType;
import com.neptunetg.common.packet.model.taggeddata.types.TaggedData;
import com.neptunetg.common.packet.parser.PacketParser;
import com.neptunetg.common.packet.parser.TagSequenceParser;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by WJD1 on 03/07/2015.
 */
public class EventPacketConsistencyTest extends PacketConsistencyTestBase
{
    /**
     * Create a packet in C and parse it in Java
     *
     */
    @Test
    public void testEventPacketConsistencyCToJava() throws Exception
    {
        //build the packet in C
        final Memory packetBuffer = new Memory(1024L);

        final TpbpPackerStruct packetPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(packetPacker, packetBuffer, 1023);

        tpbp.Tpbp_Packer_BeginPacket(packetPacker, TaggedDataPacketType.Event.getId()); //packet type

        final CmiuPacketHeaderStruct cBuiltPacketHeader = new CmiuPacketHeaderStruct();
        cBuiltPacketHeader.cmiuId = 123456;
        cBuiltPacketHeader.cellularRssiAndBer = 123;
        cBuiltPacketHeader.encryptionMethod = 1;
        cBuiltPacketHeader.keyInfo = 2;
        cBuiltPacketHeader.networkFlags = 3;
        cBuiltPacketHeader.sequenceNumber = 4;
        cBuiltPacketHeader.timeAndDate = 5;
        cBuiltPacketHeader.tokenAesCrc = 6;
        assertTrue(tpbp.Tpbp_PackerAdd_CmiuPacketHeader(packetPacker, cBuiltPacketHeader));

        final Memory secureDataBuffer = new Memory(1024L);

        final TpbpPackerStruct secureDataPacker = new TpbpPackerStruct();

        tpbp.Tpbp_PackerInit(secureDataPacker, secureDataBuffer, 1024);

        assertTrue(tpbp.Tpbp_PackerAdd_UInt(secureDataPacker, TagId.Event.getId(), 2, 1)); //Event enum 2

        assertTrue(tpbp.Tpbp_PackerAdd_UInt64(secureDataPacker, TagId.TimeAndDate.getId(), 12345678L));

        assertTrue(tpbp.Tpbp_PackerPadZeroToBlockBoundary(secureDataPacker, 16));

        final int secureDataLengthInBytes = tpbp.Tpbp_PackerGetCount(secureDataPacker);
        final Memory encryptedSecureData = new Memory(secureDataLengthInBytes);

        encryptedSecureData.write(0L, secureDataBuffer.getByteArray(0L, secureDataLengthInBytes), 0, secureDataLengthInBytes);

        assertTrue(tpbp.Tpbp_PackerAdd_SecureBlockArray(packetPacker, encryptedSecureData, secureDataLengthInBytes));

        assertTrue(tpbp.Tpbp_Packer_EndPacket(packetPacker));

        //parse the packet data in Java

        final PacketParser packetParser = new PacketParser();
        final byte[] packetBytes = packetBuffer.getByteArray(0, packetPacker.writePosition);

        printByteArray("C built packet data", packetBytes);
        Files.write(Paths.get("cpacket.dat"), packetBytes);

        final TaggedPacket parsedPacket = packetParser.parseTaggedPacket(packetBytes);

        assertEquals(TaggedDataPacketType.Event.getId(), parsedPacket.getPacketId());
        Iterator<TaggedData> ti = parsedPacket.iterator();

        final CmiuPacketHeaderData parsedPacketData = (CmiuPacketHeaderData) ti.next();
        assertEquals(cBuiltPacketHeader.cmiuId, parsedPacketData.getCmiuId());
        assertEquals(cBuiltPacketHeader.cellularRssiAndBer, parsedPacketData.getBer() * 256 + parsedPacketData.getRssi());
        assertEquals(cBuiltPacketHeader.encryptionMethod, parsedPacketData.getEncryptionMethod());
        assertEquals(cBuiltPacketHeader.keyInfo, parsedPacketData.getKeyInfo());
        assertEquals(cBuiltPacketHeader.networkFlags, parsedPacketData.getNetworkFlags());
        assertEquals(cBuiltPacketHeader.sequenceNumber, parsedPacketData.getSequenceNumber());
        assertEquals(cBuiltPacketHeader.timeAndDate, parsedPacketData.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cBuiltPacketHeader.tokenAesCrc, parsedPacketData.getToken());

        final SecureBlockArrayData javaParsedSbad = (SecureBlockArrayData) ti.next();

        final TaggedData eofTag = ti.next();
        assertEquals(TagId.Eof, eofTag.getTagId());

        assertFalse(ti.hasNext());

        //read the tags in the secure data

        final ByteBuffer encryptedData = javaParsedSbad.getEncryptedData();

        final ByteBuffer decryptedData = encryptedData.duplicate().order(ByteOrder.LITTLE_ENDIAN); //no decryption

        final TagSequenceParser secureDataParser = new TagSequenceParser();

        final TagSequence secureTags = secureDataParser.parseTagSequence(decryptedData);

        ti = secureTags.iterator();

        final IntegerData event = (IntegerData)ti.next();
        assertEquals(TagId.Event, event.getTagId());
        assertEquals(TagDataType.Byte, event.getDataType());
        assertEquals(2, event.getValue());

        final IntegerData timeAndDate = (IntegerData)ti.next();
        assertEquals(TagId.TimeAndDate.getId(), timeAndDate.getTagId().getId());
        assertEquals(TagDataType.UInt64, timeAndDate.getDataType());
        assertEquals(12345678L, timeAndDate.getValue());
    }



    /**
     * Create a packet in Java and parse it in C
     *
     */
    @Test
    public void testEventPacketConsistencyJavaToC() throws Exception
    {
        final CmiuPacketHeaderData javaBuiltHeader = new CmiuPacketHeaderData(
            2 /*cmiuId*/,
            3 /*int sequenceNumber*/,
            4 /*keyInfo*/,
            5 /*encryptionMethod*/,
            6 /*token*/,
            7 /*networkFlags*/,
            8 /*rssi*/,
            9 /*ber*/,
            new UnixTimestamp(10L));

        final PacketBuilder packetBuilder = new PacketBuilder(TaggedDataPacketType.Event);
        packetBuilder.appendTagData(javaBuiltHeader);

        final List<TaggedData> tagsToEncrypt = new ArrayList<>();

        final IntegerData eventJBuilt = new IntegerData(TagId.Event, TagDataType.Byte, 2);
        tagsToEncrypt.add(eventJBuilt);

        final IntegerData timeAndDateJBuilt = new IntegerData(TagId.TimeAndDate, TagDataType.UInt64, 12345678L);
        tagsToEncrypt.add(timeAndDateJBuilt);

        final TagSequence tagSequenceToEncrypt = new TagSequence(tagsToEncrypt);

        System.out.println("Java built secure data as JSON: ");
        System.out.println(tagSequenceToEncrypt.toJsonPretty());

        byte[] dataToEncrypt = tagSequenceToEncrypt.toByteSequencePadToBlockBoundary(16); //pad to 16 bytes

        byte[] encryptedData = dataToEncrypt; //no encryption

        SecureBlockArrayData secureData = new SecureBlockArrayData(TagDataType.ExtendedSecureBlockArray, encryptedData, dataToEncrypt.length / 16);
        packetBuilder.appendTagData(secureData);

        final TaggedPacket p = packetBuilder.buildTaggedPacket();

        final byte[] packetData = p.toByteSequence();

        //parse the packet data in C

        final Memory packetDataMemory = new Memory(packetData.length);
        packetDataMemory.write(0L, packetData, 0, packetData.length);

        final Pointer startOfTags = packetDataMemory.share(1L);

        final TpbpParserStruct packetParserStruct = new TpbpParserStruct();
        final TpbpTagStruct tagStruct = new TpbpTagStruct();
        final CmiuPacketHeaderStruct cParsedHeader = new CmiuPacketHeaderStruct();

        tpbp.Tpbp_ParserInit(packetParserStruct, startOfTags, packetData.length - 1);

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.CmiuPacketHeader.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.ByteRecord.getId(), tagStruct.tagType);
        assertEquals(CmiuPacketHeaderData.SIZE_IN_BYTES, tagStruct.dataSize);
        tpbp.Tpbp_ParserRead_CmiuPacketHeader(packetParserStruct, cParsedHeader);
        assertEquals(javaBuiltHeader.getBer() * 256 + javaBuiltHeader.getRssi(), cParsedHeader.cellularRssiAndBer);
        assertEquals(cParsedHeader.encryptionMethod, javaBuiltHeader.getEncryptionMethod());
        assertEquals(cParsedHeader.keyInfo, javaBuiltHeader.getKeyInfo());
        assertEquals(cParsedHeader.networkFlags, javaBuiltHeader.getNetworkFlags());
        assertEquals(cParsedHeader.sequenceNumber, javaBuiltHeader.getSequenceNumber());
        assertEquals(cParsedHeader.timeAndDate, javaBuiltHeader.getCurrentTimeAndDate().getEpochSecond());
        assertEquals(cParsedHeader.tokenAesCrc, javaBuiltHeader.getToken());

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.SecureDataBlockArray.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(TagDataType.ExtendedSecureBlockArray.getId(), tagStruct.tagType);

        final int secureDataSize = tagStruct.dataSize;
        final Memory encryptedDataFromC = new Memory(secureDataSize);
        tpbp.Tpbp_ParserGetBytes(packetParserStruct, encryptedDataFromC, secureDataSize);

        final Memory decryptedDataFromC = encryptedDataFromC; //no encryption

        tpbp.Tpbp_ParserReadTag(packetParserStruct, tagStruct);
        assertEquals(TagId.Eof.getId(), tagStruct.tagNumber & 0xff);
        assertEquals(0, tagStruct.tagType);
        assertEquals(0, tagStruct.dataSize);

        final TpbpParserStruct secureDataParserStruct = new TpbpParserStruct();
        tpbp.Tpbp_ParserInit(secureDataParserStruct, decryptedDataFromC, secureDataSize);

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.Event.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.Byte.getId(), tagStruct.tagType);
        assertEquals(1, tagStruct.dataSize);
        assertEquals(2,tpbp.Tpbp_Unpack(secureDataParserStruct, 1));

        tpbp.Tpbp_ParserReadTag(secureDataParserStruct, tagStruct);
        assertEquals(TagId.TimeAndDate.getId(), tagStruct.tagNumber);
        assertEquals(TagDataType.UInt64.getId(), tagStruct.tagType);
        assertEquals(8, tagStruct.dataSize);
        assertEquals(12345678L,tpbp.Tpbp_Unpack64(secureDataParserStruct));
    }
}
