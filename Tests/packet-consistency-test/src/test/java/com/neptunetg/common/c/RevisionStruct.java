/*
 * Neptune Technology Group
 * Copyright 2015 as unpublished work.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Neptune Technology Group. The use, copying, transfer
 * or disclosure of such information is prohibited except by express
 * written agreement with Neptune Technology Group.
 *
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by WJD1 on 02/07/2015.
 * typedef struct S_REVISION
 {
 uint8_t     versionNumberBytes[S_REVISION_PACKED_SIZE]; // 10 bytes
 } S_REVISION;
 */
public class RevisionStruct extends Structure
{
    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{"versionMajor", "versionMinor",
                                                                                "versionYearMonthDay", "versionBuildBcd"});

    public byte versionMajor;
    public byte versionMinor;
    public int versionYearMonthDay;
    public int versionBuildBcd;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }
}
