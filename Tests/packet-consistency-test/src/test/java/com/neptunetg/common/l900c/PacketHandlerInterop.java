/*
 * Neptune Technology Group
 *      Copyright 2016 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.l900c;

import com.sun.jna.Library;
import com.sun.jna.Pointer;

/**
 * Created by WJD1 on 07/07/2016.
 * Interface between C DLL and Java
 */
public interface PacketHandlerInterop extends Library
{
    public void PacketHandler_DLL_Init();

    public int PacketHandler_DLL_GetPacket(Pointer bytes);

    public int PacketHandler_DLL_DetermineDownlinkCommand(Pointer bytes);

    public void PacketHandler_DLL_Build_LoRa_TimeOffset(PacketHandlerLoraTimeOffsetStruct packetHandlerLoraTimeOffsetStruct);
    public void PacketHandler_DLL_Parse_LoRa_TimeOffset(Pointer bytes, PacketHandlerLoraTimeOffsetStruct packetHandlerLoraTimeOffsetStruct);

    public void PacketHandler_DLL_Build_LoRa_TimeAndDate(PacketHandlerLoraTimeAndDateStruct packetHandlerLoraTimeAndDateStruct);
    public void PacketHandler_DLL_Parse_LoRa_TimeAndDate(Pointer bytes, PacketHandlerLoraTimeAndDateStruct packetHandlerLoraTimeAndDateStruct);

    public void PacketHandler_DLL_Build_LoRa_Config_11Byte(PacketHandlerLora11ByteConfigStruct packetHandlerLora11ByteConfigStruct);
    public void PacketHandler_DLL_Parse_LoRa_Config_11Byte(Pointer bytes, PacketHandlerLora11ByteConfigStruct packetHandlerLora11ByteConfigStruct);

    public void PacketHandler_DLL_Build_LoRa_Reading_11Byte(PacketHandlerLora11ByteReadStruct packetHandlerLora11ByteReadStruct);
    public void PacketHandler_DLL_Parse_LoRa_Reading_11Byte(Pointer bytes, PacketHandlerLora11ByteReadStruct packetHandlerLora11ByteReadStruct);

    public void PacketHandler_DLL_Build_LoRa_Reading_53Byte(PacketHandlerLora53ByteReadStruct packetHandlerLora53ByteReadStruct);
    public void PacketHandler_DLL_Parse_LoRa_Reading_53Byte(Pointer bytes, PacketHandlerLora53ByteReadStruct packetHandlerLora53ByteReadStruct);
}
