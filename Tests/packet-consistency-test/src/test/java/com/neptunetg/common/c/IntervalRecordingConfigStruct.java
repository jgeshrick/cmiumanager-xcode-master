/*
 * Neptune Technology Group
 *      Copyright 2015 as unpublished work.
 *      All rights reserved
 *
 *      The information contained herein is confidential
 *      property of Neptune Technology Group. The use, copying, transfer
 *      or disclosure of such information is prohibited except by express
 *      written agreement with Neptune Technology Group.
 */

package com.neptunetg.common.c;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 /**
 * Struct for INTERVAL_RECORDING_CONFIG (tag number 34)
 *
 *      typedef struct S_INTERVAL_RECORDING_CONFIG
 *      {
 *          uint8_t     deviceNumber;
 *          uint8_t     intervalFormat;
 *          uint8_t     deviceReadInterval;
 *          uint8_t     deviceLogInterval;
 *          uint16_t    intervalStartTime;
 *          uint64_t    intervalLastReadDateTime;
 *      } S_INTERVAL_RECORDING_CONFIG;
 */
public class IntervalRecordingConfigStruct extends Structure
{

    private static final List<String> FIELD_ORDER = Arrays.asList(new String[]{
        "deviceNumber",
        "intervalFormat",
        "deviceRecordingInterval",
        "deviceReportingInterval",
        "intervalStartTime",
        "intervalLastReadDateTime"
    });

    public byte deviceNumber;
    public byte intervalFormat;
    public byte deviceRecordingInterval;
    public byte deviceReportingInterval;
    public short intervalStartTime;
    public long intervalLastReadDateTime;

    @Override
    protected List<String> getFieldOrder() {
        return FIELD_ORDER;
    }

}
