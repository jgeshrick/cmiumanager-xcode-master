/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.mdce.integration.integrationtest.cns.jasper;

import com.jasperwireless.api.BillingServiceStub;
import com.jasperwireless.api.EchoService;
import com.jasperwireless.api.EchoServiceStub;
import com.jasperwireless.api.TerminalServiceStub;
import com.jasperwireless.api.ws.schema.EchoRequest;
import com.jasperwireless.api.ws.schema.EchoResponse;
import com.jasperwireless.api.ws.schema.EditTerminalRequest;
import com.jasperwireless.api.ws.schema.EditTerminalRequestParamGroup;
import com.jasperwireless.api.ws.schema.EditTerminalResponse;
import com.jasperwireless.api.ws.schema.GetModifiedTerminalsRequest;
import com.jasperwireless.api.ws.schema.GetModifiedTerminalsResponse;
import com.jasperwireless.api.ws.schema.GetSessionInfoRequest;
import com.jasperwireless.api.ws.schema.GetSessionInfoResponse;
import com.jasperwireless.api.ws.schema.GetTerminalAuditTrailRequest;
import com.jasperwireless.api.ws.schema.GetTerminalAuditTrailResponse;
import com.jasperwireless.api.ws.schema.GetTerminalDetailsRequest;
import com.jasperwireless.api.ws.schema.GetTerminalDetailsResponse;
import com.jasperwireless.api.ws.schema.GetTerminalLatestRegistrationRequest;
import com.jasperwireless.api.ws.schema.GetTerminalLatestRegistrationRequestParamGroup;
import com.jasperwireless.api.ws.schema.GetTerminalLatestRegistrationResponse;
import com.jasperwireless.api.ws.schema.GetTerminalUsageDataDetailsRequest;
import com.jasperwireless.api.ws.schema.GetTerminalUsageDataDetailsResponse;
import com.jasperwireless.api.ws.schema.GetTerminalUsageRequest;
import com.jasperwireless.api.ws.schema.GetTerminalUsageResponse;
import com.jasperwireless.api.ws.schema.Iccids_type3;
import com.jasperwireless.api.ws.schema.SessionInfoType;
import com.jasperwireless.api.ws.schema.TerminalChangeType;
import com.jasperwireless.api.ws.schema.Terminals_type1;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.StringReader;
import java.lang.Exception;import java.lang.String;import java.lang.System;import java.rmi.RemoteException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * To test Jasper API directly for understanding how it works
 */
@Ignore
public class JasperAPIDirectTest
{
    private static final String LICENSE_KEY = "6853d052-db17-4f6b-9a0d-e608f9855729";
    private static final String USERNAME = "MdceApi";
    private static final String PASSWORD = "s@g3nt!@";
    private static final String JASPER_API_VERSION = "5.90";
    private static final String TEST_ICCID_1 = "89302720396916964625";
    private static final String TEST_ICCID_2 = "89302720396916964617";


    @Test
    public void testEcho() throws RemoteException
    {
        EchoService echoService = new EchoServiceStub();

        EchoRequest echoRequest = new EchoRequest();

        echoRequest.setValue("this is a test");
        echoRequest.setMessageId(Instant.now().toString());
        echoRequest.setLicenseKey(LICENSE_KEY);
        echoRequest.setVersion(JASPER_API_VERSION);

        EchoResponse echoResponse = echoService.echo(echoRequest);

        String responseValue = echoResponse.getValue();
    }

    @Ignore("This always seems to return null")
    @Test
    public void testGetTerminalList() throws RemoteException
    {
        TerminalServiceStub terminalService = new TerminalServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        terminalService._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        GetModifiedTerminalsRequest modifiedTerminalsRequest = new GetModifiedTerminalsRequest();
        modifiedTerminalsRequest.setMessageId(Instant.now().toString());
        modifiedTerminalsRequest.setVersion(JASPER_API_VERSION);
        modifiedTerminalsRequest.setLicenseKey(LICENSE_KEY);

//        modifiedTerminalsRequest.setSimState(SimStateType.ACTIVATED);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Date.from(Instant.now().minus(30, ChronoUnit.DAYS)));

//        modifiedTerminalsRequest.setSince(calendar);

        GetModifiedTerminalsResponse response = terminalService.getModifiedTerminals(modifiedTerminalsRequest);

        assertNotNull(response.getIccids().getIccid());
        assertEquals(2, response.getIccids().getIccid().length);


    }

    @Test
    public void testGetTerminalDetails() throws RemoteException
    {
        TerminalServiceStub terminalService = new TerminalServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        terminalService._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        GetTerminalDetailsRequest request = new GetTerminalDetailsRequest();
        request.setMessageId(Instant.now().toString());
        request.setVersion(JASPER_API_VERSION);
        request.setLicenseKey(LICENSE_KEY);

        Iccids_type3 iccids = new Iccids_type3();
        iccids.addIccid(TEST_ICCID_2);
        iccids.addIccid(TEST_ICCID_1);
        request.setIccids(iccids);

        GetTerminalDetailsResponse response = terminalService.getTerminalDetails(request);
        Terminals_type1 terminals = response.getTerminals();

        assertEquals(2, terminals.getTerminal().length);
        assertEquals(TEST_ICCID_2, terminals.getTerminal()[0].getIccid());
    }

    @Test
    public void testGetTerminalUsage() throws RemoteException
    {
        BillingServiceStub service = new BillingServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        service._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        GetTerminalUsageRequest request = new GetTerminalUsageRequest();
        request.setMessageId(Instant.now().toString());
        request.setVersion(JASPER_API_VERSION);
        request.setLicenseKey(LICENSE_KEY);
        request.setIccid(TEST_ICCID_1);
        request.setCycleStartDate(Date.from(Instant.now()));
//        request.setCycleStartDate(Date.from(YearMonth.now().minusMonths(1).atDay(1).atStartOfDay().toInstant(ZoneOffset.UTC)));

        try
        {
            GetTerminalUsageResponse response = service.getTerminalUsage(request);
            assertNotNull(response);
        }
        catch (AxisFault ex)
        {
            if (ex.getMessage().equals("200200"))
            {
                System.out.println("No data usage found for this billing cycle.");
            }
            else
            {
                System.out.println(ex.getFaultDetailElement().toString());
            }

            throw ex;
        }

    }

    @Test
    public void testGetTerminalDataUsage() throws RemoteException
    {
        BillingServiceStub service = new BillingServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        service._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        GetTerminalUsageDataDetailsRequest request = new GetTerminalUsageDataDetailsRequest();
        request.setMessageId(Instant.now().toString());
        request.setVersion(JASPER_API_VERSION);
        request.setLicenseKey(LICENSE_KEY);
        request.setIccid(TEST_ICCID_1);

        request.setCycleStartDate(Date.from(Instant.now().truncatedTo(ChronoUnit.DAYS)));

        Date date = Date.from(LocalDateTime.now().withDayOfMonth(1).toInstant(ZoneOffset.UTC)); //first day of current month
//        Date date = Date.from(LocalDateTime.now().minusMonths(1).withDayOfMonth(1).toInstant(ZoneOffset.UTC)); //first day of last month
        request.setCycleStartDate(date);

//        request.setCycleStartDate(Date.from(YearMonth.now()/*.minusMonths(1)*/.atDay(1).atStartOfDay().toInstant(ZoneOffset.UTC)));

        try
        {

            GetTerminalUsageDataDetailsResponse response = service.getTerminalUsageDataDetails(request);

            assertNotNull(response);
        }
        catch (AxisFault ex)
        {
            printJasperError(ex);
            throw ex;
        }

    }

    /**
     * Get the last time the device starts a session
     * @throws RemoteException
     */
    @Test
    public void testLatestTerminalRegistration() throws RemoteException
    {
        TerminalServiceStub service = new TerminalServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        service._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        GetTerminalLatestRegistrationRequest request = new GetTerminalLatestRegistrationRequest();
        request.setMessageId(Instant.now().toString());
        request.setVersion(JASPER_API_VERSION);
        request.setLicenseKey(LICENSE_KEY);

        GetTerminalLatestRegistrationRequestParamGroup paramGroup = new GetTerminalLatestRegistrationRequestParamGroup();
        paramGroup.setImsi("302720391779882");

        request.setGetTerminalLatestRegistrationRequestParamGroup(paramGroup);

        try
        {

            GetTerminalLatestRegistrationResponse response = service.getTerminalLatestRegistration(request);

            assertNotNull(response);
        }
        catch (AxisFault ex)
        {
            printJasperError(ex);
            throw ex;
        }

    }

    @Test
    public void testEditTerminal() throws RemoteException
    {
        TerminalServiceStub service = new TerminalServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        service._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        EditTerminalRequestParamGroup requestParamGroup = new EditTerminalRequestParamGroup();
        requestParamGroup.setIccid(TEST_ICCID_2);

        TerminalChangeType terminalChangeType = new TerminalChangeType();
        terminalChangeType.setTerminalChangeType(3);    //SIM STATUS

        requestParamGroup.setChangeType(terminalChangeType);
        requestParamGroup.setTargetValue("TEST_READY_NAME");

        EditTerminalRequest request = new EditTerminalRequest();
        request.setMessageId(Instant.now().toString());
        request.setVersion(JASPER_API_VERSION);
        request.setLicenseKey(LICENSE_KEY);
        request.setEditTerminalRequestParamGroup(requestParamGroup);


        try
        {
            EditTerminalResponse response = service.editTerminal(request);
        }
        catch (AxisFault ex)
        {
            printJasperError(ex);
            throw ex;
        }

    }

    @Test
    public void testGetTerminalAuditTrial() throws RemoteException
    {
        TerminalServiceStub service = new TerminalServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        service._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        GetTerminalAuditTrailRequest request = new GetTerminalAuditTrailRequest();
        request.setMessageId(Instant.now().toString());
        request.setVersion(JASPER_API_VERSION);
        request.setLicenseKey(LICENSE_KEY);
        request.setIccid(TEST_ICCID_1);

        try
        {

            GetTerminalAuditTrailResponse response = service.getTerminalAuditTrail(request);

            assertNotNull(response);
        }
        catch (AxisFault ex)
        {
            printJasperError(ex);
            throw ex;
        }

    }

    @Test
    public void testGetCurrentSession() throws RemoteException
    {
        TerminalServiceStub service = new TerminalServiceStub("https://api.jasperwireless.com/ws/service/terminal");
        service._getServiceClient().addHeader(getSecurityToken(USERNAME, PASSWORD));

        GetSessionInfoRequest request = new GetSessionInfoRequest();
        request.setMessageId(Instant.now().toString());
        request.setVersion(JASPER_API_VERSION);
        request.setLicenseKey(LICENSE_KEY);

        request.setIccid(new String[]{TEST_ICCID_1});

        try
        {

            GetSessionInfoResponse response = service.getSessionInfo(request);

            SessionInfoType[] sessionInfoArray = response.getSessionInfo().getSession();

            //sessionInfoArray will be null if the device is not currently connected to a session

            assertNotNull(response);
        }
        catch (AxisFault ex)
        {
            printJasperError(ex);
            throw ex;
        }

    }

    /**
     * Generate login credentials to be inserted into SOAP header for all Jasper Request
     * @param username
     * @param password
     * @return
     */
    private static OMElement getSecurityToken(String username, String password) {

        OMFactory factory = OMAbstractFactory.getOMFactory();

        OMNamespace namespaceWSSE = factory
                .createOMNamespace(
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "wsse");

        OMElement securityElement = factory.createOMElement("Security", namespaceWSSE);

        OMAttribute attribute = factory.createOMAttribute("mustUnderstand", null, "1");

        securityElement.addAttribute(attribute);


        OMElement usernameTokenElement = factory.createOMElement("UsernameToken", namespaceWSSE);

        OMNamespace namespaceWSU = factory
                .createOMNamespace(
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd",
                        "wsu");

        attribute = factory.createOMAttribute("Id", namespaceWSU,
                "UsernameToken-1");

        usernameTokenElement.addAttribute(attribute);

        securityElement.addChild(usernameTokenElement);

        OMElement usernameElement = factory.createOMElement("Username", namespaceWSSE);

        usernameElement.setText(username);

        OMElement passwordElement = factory.createOMElement("Password", namespaceWSSE);

        attribute = factory
                .createOMAttribute(
                        "Type",
                        null,
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");

        passwordElement.setText(password);

        usernameTokenElement.addChild(usernameElement);
        usernameTokenElement.addChild(passwordElement);

        return securityElement;
    }

    private static void printJasperError(final AxisFault ex)
    {
        System.out.println("Error: " + ex.toString());
        System.out.println("Fault code: " + ex.getMessage());
        System.out.println("Detail: \n" + ex.getFaultDetailElement().toString());
    }

    @Test
    public void testParseXmlStringInfoAxis2GeneratedClass() throws Exception
    {
        String xmlString = "<Session xmlns=\"http://api.jasperwireless.com/ws/schema\">\n" +
                "         <iccid>8901650500000002918</iccid>\n" +
                "         <ipAddress>10.98.214.86</ipAddress>\n" +
                "         <dateSessionStarted>2009-12-10T01:16:20.026Z</dateSessionStarted>\n" +
                "         <dateSessionEnded>2009-12-10T01:16:25.026Z</dateSessionEnded>\n" +
                "      </Session>";

        XMLInputFactory factory = XMLInputFactory.newInstance(); // Or newFactory()
        XMLStreamReader xmlReader = factory.createXMLStreamReader(new StringReader(xmlString));

        SessionInfoType sessionInfo = SessionInfoType.Factory.parse(xmlReader);

        assertEquals("8901650500000002918", sessionInfo.getIccid());
        assertEquals("10.98.214.86", sessionInfo.getIpAddress());
        assertEquals("2009-12-10T01:16:20.026Z", sessionInfo.getDateSessionStarted().toInstant().toString());
        assertEquals("2009-12-10T01:16:25.026Z", sessionInfo.getDateSessionEnded().toInstant().toString());
    }

}
