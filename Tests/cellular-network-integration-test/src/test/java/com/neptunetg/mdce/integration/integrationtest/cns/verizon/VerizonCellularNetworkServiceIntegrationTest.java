/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.mdce.integration.integrationtest.cns.verizon;

import com.neptunetg.common.cns.CellularNetworkService;
import com.neptunetg.common.cns.CellularNetworkServiceFactory;
import com.neptunetg.common.cns.CnsAccountDetails;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.cns.model.ConnectionEventDetail;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.cns.model.UsageHistory;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.Exception;import java.lang.String;import java.rmi.RemoteException;
import java.util.List;

import static com.neptunetg.common.cns.CellularNetworkServiceFactory.ServiceProviderName;
import static org.junit.Assert.*;

/**
 * Test for CellularNetworkServiceImpl
 */
public class VerizonCellularNetworkServiceIntegrationTest
{
    public static final String fakeIccid = "89148000001471859500";
    public static final String availableIccid = "89148000001471859883";
    public static final String noHistoryIccid = "89148000002036034244";

    public static final String[] frequentlyConnectingIccids = {
        "89148000001887724366", //CMIUID 400000252
        "89148000002036033949"  //CMIUID 400002084
    };

    public static final String notAvailableIccid = "89148000002036032636";
    public static final String accountName = "0823797736-00001";
    private CellularNetworkService cellularNetworkService;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception
    {
        final CnsAccountDetails accountDetails = new CnsAccountDetails(
                "NEPTUNEUWS",
                "Ze?2j3Pr",
                accountName,
                "M2MSH4G1MBONNET",
                "",
                "http://54.187.239.195:8080/mdce-integration/soap");

        final CellularNetworkServiceFactory factory = new CellularNetworkServiceFactory();

        cellularNetworkService = factory.getCellularNetworkService(ServiceProviderName.VERIZON,
                accountDetails);

    }

    @Test
    public void testGetDeviceInformation() throws Exception
    {
        DeviceCellularInformation result = cellularNetworkService.getDeviceInformation(availableIccid);
        assertNotNull(result);
        assertEquals(accountName, result.getAccountName());
    }


    @Test
    public void testGetDeviceInformationNonExistentDevice() throws Exception
    {
        DeviceCellularInformation result = cellularNetworkService.getDeviceInformation(notAvailableIccid);
        assertNull(result);
    }

    @Test
    public void testGetDeviceList() throws CnsException
    {
        List<DeviceCellularInformation> result = cellularNetworkService.getDeviceList();

        assertNotNull(result);
        assertThat(result.size(), Matchers.greaterThan(0));
    }

    @Test
    public void testGetDeviceUsageHistory() throws Exception
    {
        final List<UsageHistory> result = cellularNetworkService.getDeviceUsageHistory(availableIccid); //Neptune wireless modem?
        assertNotNull(result);
        assertThat(result.size(), Matchers.greaterThan(0));
    }

    @Test
    public void testGetDeviceUsageHistoryWithPreviouslyFailingIccid() throws Exception
    {
        // Tests an ICCID that had failures logged in nonprod due to having no usage history.
        final List<UsageHistory> result = cellularNetworkService.getDeviceUsageHistory(noHistoryIccid); //Neptune wireless modem?
        assertNotNull(result);
    }

    @Test
    public void testGetDeviceUsageHistoryNonExistentDevice() throws Exception
    {
        final List<UsageHistory> result = cellularNetworkService.getDeviceUsageHistory(notAvailableIccid); //Neptune wireless modem?
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void testGetDeviceProvisioningStatus() throws Exception
    {
        String status = cellularNetworkService.getDeviceProvisioningStatus(availableIccid);
        assertEquals("active", status);
    }

    @Test
    public void testGetDeviceProvisioningStatusNonExistentDevice() throws Exception
    {

        String status = cellularNetworkService.getDeviceProvisioningStatus(notAvailableIccid);
        assertNull(status);
    }

    @Test
    public void testSuspendDevice() throws Exception
    {
        String requestId = cellularNetworkService.suspendDevice(fakeIccid);
        assertFalse(requestId.isEmpty());
    }

    @Test
    public void testDeactivateDevice() throws Exception
    {
        String requestId = cellularNetworkService.deactivateDevice(fakeIccid);
        assertFalse(requestId.isEmpty());
    }

    @Test
    public void testRestoreDevice() throws Exception
    {
        String requestId = cellularNetworkService.restoreDevice(fakeIccid);
        assertFalse(requestId.isEmpty());
    }

    @Test
    public void testGetDeviceConnectionEvents() throws Exception
    {
        List<ConnectionEventDetail> result = null;
        for (String frequentlyConnectingIccid : frequentlyConnectingIccids)
        {
            result = cellularNetworkService.getDeviceConnectionEvents(frequentlyConnectingIccid, 10);
            if (result != null && !result.isEmpty())
            {
                break;
            }
        }
        assertNotNull(result);
        assertThat(result.size(), Matchers.greaterThan(0));
    }


    @Test
    public void testGetDeviceConnectionEventsNonExistentDevice() throws Exception
    {
        List<ConnectionEventDetail> result = cellularNetworkService.getDeviceConnectionEvents(notAvailableIccid, 100);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }
}