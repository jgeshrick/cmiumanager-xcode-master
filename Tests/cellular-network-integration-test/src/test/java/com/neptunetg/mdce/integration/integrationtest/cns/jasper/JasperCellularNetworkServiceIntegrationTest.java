/*
 * *************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2016 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * *************************************************************************
 */

package com.neptunetg.mdce.integration.integrationtest.cns.jasper;

import com.neptunetg.common.cns.CnsAccountDetails;
import com.neptunetg.common.cns.CnsException;
import com.neptunetg.common.cns.jasper.JasperCellularNetworkService;import com.neptunetg.common.cns.model.ConnectionEventDetail;
import com.neptunetg.common.cns.model.DeviceCellularInformation;
import com.neptunetg.common.cns.model.DeviceProvisioningEvent;
import com.neptunetg.common.cns.model.UsageHistory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.lang.Exception;import java.lang.String;import java.lang.System;import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Integration Test Jasper CNS... mainly used to manually verify data content in Jasper API response.
 * Need to connect to network with the test ICCID online.
 */
public class JasperCellularNetworkServiceIntegrationTest
{
    private static final String LICENSE_KEY = "6853d052-db17-4f6b-9a0d-e608f9855729";
    private static final String USERNAME = "MdceApi";
    private static final String PASSWORD = "s@g3nt!@";
    private static final long   ACCOUNT_ID = 100494101;
    private static final String SERVICE_PLAN = "DUMMY";
    private static final String DEMO_SIM1_ICCID = "89302720396916964625";
    private static final String DEMO_SIM1_MSISDN = "12269289299";
    private static final String DEMO_SIM1_IMEI = "0133230002908709";
    private static final String NON_EXISTENT_ICCID = "89302720396916999999";
    private static final int LAST_TEN_YEARS = 3653;

    private JasperCellularNetworkService cellularNetworkService;

    @Before
    public void setUp() throws AxisFault
    {
        final CnsAccountDetails accountDetails = new CnsAccountDetails(
                USERNAME, PASSWORD, String.valueOf(ACCOUNT_ID), SERVICE_PLAN, LICENSE_KEY, "");

        final ConfigurationContext configContext = ConfigurationContextFactory.createConfigurationContextFromFileSystem(null, null);

        cellularNetworkService = new JasperCellularNetworkService(configContext, accountDetails);
    }


    @Test
    public void integrationTestGetDeviceUsageHistoryWithinDuration() throws Exception
    {
        final ZonedDateTime currentDateTime = ZonedDateTime.now();
        final ZonedDateTime fromDateTime = currentDateTime.minusMonths(2);
        final ZonedDateTime toDateTime = currentDateTime; //currentDateTime.plusMonths(2);

        List<UsageHistory> usageHistory = cellularNetworkService.getDeviceUsageHistory(DEMO_SIM1_ICCID, fromDateTime, toDateTime);

        assertNotNull(usageHistory);
    }

    @Test
    public void integrationTestGetDeviceUsageHistoryForParticularDay() throws Exception
    {
        final LocalDate date = LocalDate.of(2015, Month.DECEMBER, 25);
        List<UsageHistory> usageHistory = cellularNetworkService.getDeviceUsageHistory(DEMO_SIM1_ICCID, date);

        assertEquals(6, usageHistory.size());   //might not be true
        for(UsageHistory usage : usageHistory)
        {
            assertEquals(usage.getTimeStamp().toInstant().truncatedTo(ChronoUnit.DAYS),
                    date.atStartOfDay().toInstant(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS));
        }

    }


    @Test
    public void integrationTestGetDeviceUsageHistoryNonExistentDevice() throws Exception
    {
        final LocalDate date = LocalDate.of(2015, Month.DECEMBER, 25);
        List<UsageHistory> usageHistory = cellularNetworkService.getDeviceUsageHistory(NON_EXISTENT_ICCID, date);

        assertNotNull(usageHistory);
        assertTrue(usageHistory.isEmpty());
    }


    @Test
    public void integrationTestGetDeviceCurrentMonthUsageHistory() throws Exception
    {
        List<UsageHistory> usageHistory = cellularNetworkService.getDeviceUsageHistory(DEMO_SIM1_ICCID);

        assertNotNull(usageHistory);
        //may or may not be empty
    }

    @Test
    public void integrationTestGetDeviceCurrentMonthUsageHistoryNonExistentDevice() throws Exception
    {
        List<UsageHistory> usageHistory = cellularNetworkService.getDeviceUsageHistory(NON_EXISTENT_ICCID);

        assertNotNull(usageHistory);
        assertTrue(usageHistory.isEmpty());
    }


    @Test
    public void integrationTestGetDeviceCurrentMonthDataUsageBytes() throws Exception
    {
        long usageBytes = cellularNetworkService.getDeviceCurrentMonthDataUsageBytes(DEMO_SIM1_ICCID);

        List<UsageHistory> usageHistory = cellularNetworkService.getDeviceUsageHistory(DEMO_SIM1_ICCID);
//        long usageHistoryBytesUsed = usageHistory.stream()
//                .mapToLong(UsageHistory::getBytesUsed)
//                .sum();

//        assertTrue(usageBytes > 0);

        //FIXME The two values does not match, is current month data usage bytes delayed?
        //assertEquals(usageBytes, usageHistoryBytesUsed);
    }

    @Test
    public void integrationTestGetDeviceInformation() throws Exception
    {
        final DeviceCellularInformation deviceInformation = cellularNetworkService.getDeviceInformation(DEMO_SIM1_ICCID);

        assertNotNull(deviceInformation);
        assertEquals(deviceInformation.getIccid(), DEMO_SIM1_ICCID);
        assertEquals(deviceInformation.getImei(), DEMO_SIM1_IMEI);
        assertEquals(deviceInformation.getMsisdn(), DEMO_SIM1_MSISDN);

        if (deviceInformation.isConnected())
        {
            assertNotNull(deviceInformation.getIpAddress());    //IP address field is populated if it is connected
        }
    }


    @Test
    public void integrationTestGetDeviceInformationNonExistentDevice() throws Exception
    {
        final DeviceCellularInformation deviceInformation = cellularNetworkService.getDeviceInformation(NON_EXISTENT_ICCID);

        assertNull(deviceInformation);
    }

    @Test
    public void integrationTestGetDeviceConnectionEvents() throws Exception
    {
        List<ConnectionEventDetail> events = cellularNetworkService.getDeviceConnectionEvents(DEMO_SIM1_ICCID, 100);

        System.out.println(events.size() + " connection events for ICCID " + DEMO_SIM1_ICCID + " in last 100 days");
    }


    @Test
    public void integrationTestGetDeviceConnectionEventsNonExistentDevice() throws Exception
    {
        List<ConnectionEventDetail> events = cellularNetworkService.getDeviceConnectionEvents(NON_EXISTENT_ICCID, 100);

        assertNotNull(events);
        assertTrue(events.isEmpty());
    }

    @Test
    public void integrationTestGetDeviceList() throws CnsException
    {
        List<DeviceCellularInformation> deviceList = cellularNetworkService.getDeviceList();

        assertNotNull(deviceList);
        assertThat(deviceList.size(), Matchers.greaterThan(0));
    }

    @Test
    public void integrationTestGetDeviceProvisioningEvents() throws Exception
    {
        List<DeviceProvisioningEvent> events = cellularNetworkService.getDeviceProvisioningEvents(DEMO_SIM1_ICCID, LAST_TEN_YEARS);
        assertFalse("no provisioning events", events.isEmpty());
    }

    @Test
    public void integrationTestGetDeviceProvisioningEventsNonExistentDevice() throws Exception
    {
        List<DeviceProvisioningEvent> events = cellularNetworkService.getDeviceProvisioningEvents(NON_EXISTENT_ICCID, LAST_TEN_YEARS);

        assertNotNull(events);
        assertTrue("unexpected provisioning events", events.isEmpty());
    }



}