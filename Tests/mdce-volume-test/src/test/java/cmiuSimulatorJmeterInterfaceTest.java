/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import cmiuSimulator.CmiuSimulatorJmeterInterface;
import org.junit.Test;

/**
 * Created by WJD1 on 24/11/2015.
 */
public class cmiuSimulatorJmeterInterfaceTest
{
    //@Test
    public void runCmiuSimulatorJmeterInterfaceTest()
    {
        CmiuSimulatorJmeterInterface myTest = new CmiuSimulatorJmeterInterface();
        Arguments arguments = new Arguments();

        Argument cmiuIdStart = new Argument();
        cmiuIdStart.setName("CmiuIdStart");
        cmiuIdStart.setValue("0");
        arguments.addArgument(cmiuIdStart);

        Argument numberOfCmius = new Argument();
        numberOfCmius.setName("NumberOfCmius");
        numberOfCmius.setValue("100");
        arguments.addArgument(numberOfCmius);

        Argument simulationPeriodMins = new Argument();
        simulationPeriodMins.setName("SimulationPeriodMins");
        simulationPeriodMins.setValue("1");
        arguments.addArgument(simulationPeriodMins);

        Argument brokerAddress = new Argument();
        brokerAddress.setName("BrokerAddress");
        brokerAddress.setValue("localhost");
        arguments.addArgument(brokerAddress);

        Argument packetType = new Argument();
        packetType.setName("PacketType");
        packetType.setValue("intervalDataPacket");
        arguments.addArgument(packetType);

        myTest.runTest(new JavaSamplerContext(arguments));
    }
}
