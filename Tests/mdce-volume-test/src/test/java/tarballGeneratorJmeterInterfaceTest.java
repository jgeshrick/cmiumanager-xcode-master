/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.junit.Test;
import tarballGenerator.TarballGeneratorJmeterInterface;

/**
 * Created by WJD1 on 24/11/2015.
 */
public class tarballGeneratorJmeterInterfaceTest
{
    //@Test
    public void tarballGeneratorJmeterInterfaceTest()
    {
        TarballGeneratorJmeterInterface tarballGeneratorJmeterInterface = new TarballGeneratorJmeterInterface();

        Arguments arguments = new Arguments();

        Argument startMiuId = new Argument();
        startMiuId.setName("StartMiuId");
        startMiuId.setValue("0");
        arguments.addArgument(startMiuId);

        Argument endMiuId = new Argument();
        endMiuId.setName("EndMiuId");
        endMiuId.setValue("1000");
        arguments.addArgument(endMiuId);

        Argument totalReadings = new Argument();
        totalReadings.setName("TotalReadings");
        totalReadings.setValue("1500");
        arguments.addArgument(totalReadings);

        Argument siteId = new Argument();
        siteId.setName("SiteId");
        siteId.setValue("1");
        arguments.addArgument(siteId);

        Argument collectorName = new Argument();
        collectorName.setName("CollectorName");
        collectorName.setValue("myCollector");
        arguments.addArgument(collectorName);

        Argument putUrl = new Argument();
        putUrl.setName("PutUrl");
        putUrl.setValue("http://localhost:9080/mdce-integration/4/data");
        arguments.addArgument(putUrl);

        tarballGeneratorJmeterInterface.runTest(new JavaSamplerContext(arguments));
    }
}
