/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */
package cmiuSimulator;

import com.neptunetg.mdce.common.simulator.CmiuSimulatorLauncher;

import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import java.io.IOException;

/**
 * Created by WJD1 on 19/11/2015.
 */
public class CmiuSimulatorJmeterInterface extends AbstractJavaSamplerClient
{
    @Override
    public Arguments getDefaultParameters()
    {
        Arguments arguments = new Arguments();

        Argument cmiuIdStart = new Argument();
        cmiuIdStart.setName("CmiuIdStart");
        cmiuIdStart.setValue("0");
        arguments.addArgument(cmiuIdStart);

        Argument numberOfCmius = new Argument();
        numberOfCmius.setName("NumberOfCmiusPerDay");
        numberOfCmius.setValue("10");
        arguments.addArgument(numberOfCmius);

        Argument simulationPeriodMins = new Argument();
        simulationPeriodMins.setName("SimulationPeriodMins");
        simulationPeriodMins.setValue("1");
        arguments.addArgument(simulationPeriodMins);

        Argument brokerAddress = new Argument();
        brokerAddress.setName("BrokerAddress");
        brokerAddress.setValue("localhost");
        arguments.addArgument(brokerAddress);

        Argument packetType = new Argument();
        packetType.setName("PacketType");
        packetType.setValue("intervalDataPacket");
        arguments.addArgument(packetType);

        return arguments;
    }

    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext)
    {
        SampleResult sampleResult = new SampleResult();
        sampleResult.setSuccessful(true);

        CmiuSimulatorLauncher cmiuSimulatorLauncher = new CmiuSimulatorLauncher();

        try
        {
            cmiuSimulatorLauncher.sendPackets(javaSamplerContext.getIntParameter("CmiuIdStart"),
                    javaSamplerContext.getIntParameter("NumberOfCmiusPerDay"),
                    javaSamplerContext.getIntParameter("SimulationPeriodMins"),
                    javaSamplerContext.getParameter("BrokerAddress"),
                    javaSamplerContext.getParameter("PacketType"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
            sampleResult.setSuccessful(false);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            sampleResult.setSuccessful(false);
        }

        return sampleResult;
    }
}
