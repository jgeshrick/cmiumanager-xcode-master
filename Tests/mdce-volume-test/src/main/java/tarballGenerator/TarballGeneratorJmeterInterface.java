/*
 *
 * **************************************************************************
 *
 *     Neptune Technology Group
 *     Copyright 2015 as unpublished work.
 *     All rights reserved
 *
 *     The information contained herein is confidential
 *     property of Neptune Technology Group. The use, copying, transfer
 *     or disclosure of such information is prohibited except by express
 *     written agreement with Neptune Technology Group.
 *
 * **************************************************************************
 *
 *
 */
 package tarballGenerator;

import com.neptunetg.mdce.common.r900.GatewayTarball;
import com.neptunetg.mdce.common.r900.GatewayTarballGenerator;
import com.neptunetg.mdce.common.r900.MeterReading;
import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.util.*;

/**
 * Created by WJD1 on 24/11/2015.
 */
public class TarballGeneratorJmeterInterface extends AbstractJavaSamplerClient
{
    @Override
    public Arguments getDefaultParameters()
    {
        Arguments arguments = new Arguments();

        Argument startMiuId = new Argument();
        startMiuId.setName("StartMiuId");
        startMiuId.setValue("0");
        startMiuId.setDescription("The start of the range of MIUs to add to the tarball");
        arguments.addArgument(startMiuId);

        Argument endMiuId = new Argument();
        endMiuId.setName("EndMiuId");
        endMiuId.setValue("1000");
        endMiuId.setDescription("The end of the range of MIUs to add to the tarball");
        arguments.addArgument(endMiuId);

        Argument startExtraMiuRangeId = new Argument();
        startExtraMiuRangeId.setName("StartExtraMiuRangeId");
        startExtraMiuRangeId.setValue("0");
        startExtraMiuRangeId.setDescription("The start of the range of the MIUs to pick from for overlapping packets");
        arguments.addArgument(startExtraMiuRangeId);

        Argument endExtraMiuRangeId = new Argument();
        endExtraMiuRangeId.setName("EndExtraMiuRangeId");
        endExtraMiuRangeId.setValue("100000");
        endExtraMiuRangeId.setDescription("The end of the range of the MIUs to pick from for overlapping packets");
        arguments.addArgument(endExtraMiuRangeId);

        Argument totalReadings = new Argument();
        totalReadings.setName("TotalReadings");
        totalReadings.setValue("1500");
        totalReadings.setDescription("The total number of readings to include in the tarball");
        arguments.addArgument(totalReadings);

        Argument siteId = new Argument();
        siteId.setName("SiteId");
        siteId.setValue("1");
        siteId.setDescription("The Site ID");
        arguments.addArgument(siteId);

        Argument collectorName = new Argument();
        collectorName.setName("CollectorName");
        collectorName.setValue("myCollector");
        collectorName.setDescription("The Collectors name");
        arguments.addArgument(collectorName);

        Argument putUrl = new Argument();
        putUrl.setName("PutUrl");
        putUrl.setDescription("The URL to PUT to");
        arguments.addArgument(putUrl);

        return arguments;
    }

    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext)
    {
        SampleResult sampleResult = new SampleResult();
        sampleResult.setSuccessful(true);

        Collection<MeterReading> meterReadings = new ArrayList<MeterReading>();
        Random random = new Random();

        for(int i=javaSamplerContext.getIntParameter("StartMiuId"); i<javaSamplerContext.getIntParameter("EndMiuId"); i++)
        {
            MeterReading meterReading = new MeterReading(i, 50, Calendar.getInstance(), random.nextInt());
            meterReadings.add(meterReading);
        }

        for(int i=0; i<(javaSamplerContext.getIntParameter("TotalReadings") - meterReadings.size()); i++)
        {
            int miuId = javaSamplerContext.getIntParameter("StartExtraMiuRangeId") +
                    random.nextInt(javaSamplerContext.getIntParameter("EndExtraMiuRangeId") -
                                    javaSamplerContext.getIntParameter("StartExtraMiuRangeId"));

            MeterReading meterReading = new MeterReading(miuId, 50, Calendar.getInstance(), random.nextInt());
            meterReadings.add(meterReading);
        }

        GatewayTarballGenerator gatewayTarballGenerator = new GatewayTarballGenerator(javaSamplerContext.getParameter("SiteId"),
                javaSamplerContext.getParameter("CollectorName"),
                Calendar.getInstance());

        GatewayTarball gatewayTarball = null;

        try
        {
            gatewayTarballGenerator.addReadingsOok(100, meterReadings);
            gatewayTarball = gatewayTarballGenerator.build();

            URL url = new URL(javaSamplerContext.getParameter("PutUrl") +
                                "?site_id=" + javaSamplerContext.getParameter("SiteId") +
                    "&collectorname=" + javaSamplerContext.getParameter("CollectorName") +
                                "&filename=" + gatewayTarball.getFilename(false));

            sampleResult.sampleStart();

            Instant startConnectionTime = Instant.now();
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            Instant endConnectionTime = Instant.now();
            sampleResult.setLatency(endConnectionTime.toEpochMilli() - startConnectionTime.toEpochMilli());

            httpURLConnection.setRequestProperty("Content-Type", "application/x-gzip");
            httpURLConnection.setRequestMethod("PUT");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);

            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());

            gatewayTarball.write(dataOutputStream, false);

            String responseMessage = httpURLConnection.getResponseMessage();

            dataOutputStream.flush();
            dataOutputStream.close();

            httpURLConnection.disconnect();

            sampleResult.sampleEnd();
            sampleResult.setResponseData(responseMessage);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            sampleResult.setSuccessful(false);
            return sampleResult;
        }

        return sampleResult;
    }
}
